﻿using System.Collections.Generic;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class QuikOperationRepository : BaseRepository
    {
        public QuikOperationRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<ClientFirm> GetClientFirms()
        {
            return GetList<ClientFirm>(new DynamicParameters(), "sp_GetClientFirms");
        }
    }
}
