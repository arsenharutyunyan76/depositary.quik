﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class TradeRepository : BaseRepository
    {
        public TradeRepository(string connectionString) : base(connectionString)
        {
        }

        public Guid SaveTrade(Trade trade, SqlConnection connection, SqlTransaction trans)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("TRADE_NUMBER", trade.TRADE_NUMBER);
            parameters.Add("TRADE_DATE", trade.TRADE_DATE);
            parameters.Add("ISIN", trade.ISIN);
            parameters.Add("AUCTION_TYPE_ID", trade.AUCTION_TYPE_ID);
            parameters.Add("OPERATION_TYPE_ID", trade.OPERATION_TYPE_ID);
            parameters.Add("PRICE", trade.PRICE);
            parameters.Add("VALUE", trade.VALUE);
            parameters.Add("YIELD", trade.YIELD);
            parameters.Add("QUANTITY", trade.QUANTITY);
            parameters.Add("SETTLE_DATE", trade.SETTLE_DATE);
            parameters.Add("BANK_CODE", trade.BANK_CODE);
            parameters.Add("CP_BANK_CODE", trade.CP_BANK_CODE);
            parameters.Add("CLIENT_CODE", trade.CLIENT_CODE);
            parameters.Add("BROKER_REFERENCE", trade.BROKER_REFERENCE);
            parameters.Add("USER_ID", trade.USER_ID);
            parameters.Add("ORDER_NUMBER", trade.ORDER_NUMBER);
            parameters.Add("ID", dbType: DbType.Guid, direction: ParameterDirection.InputOutput);
            connection.Execute("sp_SaveTrade", parameters, commandType: CommandType.StoredProcedure, commandTimeout: 180, transaction: trans);
            return parameters.Get<Guid>("ID");
        }

        public IEnumerable<Trade> GetBankmailMessage575Trades(Guid id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            return GetList<Trade>(parameters, "sp_GetBankmailMessage575Trades");
        }

        public void SetTradeBankmailMessage575(Guid id, Guid id_575, SqlConnection connection, SqlTransaction trans)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("BANKMAIL_MESSAGE_575_ID", id_575);
            connection.Execute("sp_SetTradeBankmailMessage575", parameters, commandType: CommandType.StoredProcedure, commandTimeout: 180, transaction: trans);
        }

        public void SetTradeBankmailMessage515(Guid id, Guid id_515, SqlConnection connection, SqlTransaction trans)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("BANKMAIL_MESSAGE_515_ID", id_515);
            connection.Execute("sp_SetTradeBankmailMessage515", parameters, commandType: CommandType.StoredProcedure, commandTimeout: 180, transaction: trans);
        }

        public void SetTradesBankmailMessage515Acknowledged(Guid id_515)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("BANKMAIL_MESSAGE_515_ID", id_515);
            Execute(parameters, "sp_SetTradesBankmailMessage515Acknowledged");
        }

        public int GetSameAuctionTradesCount(DateTime tradeDate, string isin, byte auctionTypeId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("TRADE_DATE", tradeDate);
            parameters.Add("ISIN", isin);
            parameters.Add("AUCTION_TYPE_ID", auctionTypeId);
            return GetSingle<int>(parameters, "sp_GetSameAuctionTradesCount");
        }
    }
}
