﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class BankRepository : BaseRepository
    {
        public BankRepository(string connectionString) : base(connectionString)
        {
        }

        public Bank GetBank(string code)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("CODE", code);
            return GetSingle<Bank>(parameters, "sp_GetBank");
        }

        public string GetBankByShortName(string shortName)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("SHORT_NAME", shortName);
            return GetSingle<string>(parameters, "sp_GetBankByShortName");
        }

        public async Task<IEnumerable<Bank>> GetBanks()
        {
            return await GetListAsync<Bank>(new DynamicParameters(), "sp_GetBanks");
        }

        public async Task SaveBank(Bank bank)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("CODE", bank.CODE);
            parameters.Add("SHORT_NAME", bank.SHORT_NAME);
            parameters.Add("IS_ACTIVE", bank.IS_ACTIVE);
            parameters.Add("NAME_AM", bank.NAME_AM);
            parameters.Add("NAME_EN", bank.NAME_EN);
            parameters.Add("AS_CODE", bank.AS_CODE);
            parameters.Add("EMAIL", bank.EMAIL);
            AddTableValuedParameter<Account>(parameters, "ACCOUNTS", bank.Accounts);
            await ExecuteAsync(parameters, "sp_SaveBank");
        }
    }
}
