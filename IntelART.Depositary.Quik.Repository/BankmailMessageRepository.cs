﻿using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class BankmailMessageRepository : BaseRepository
    {
        public BankmailMessageRepository(string connectionString) : base(connectionString)
        {
        }

        public Guid SaveBankmailMessage(BankmailMessage message, SqlConnection connection, SqlTransaction trans, ref int number, ref string documentNumber)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MESSAGE_TYPE", message.MESSAGE_TYPE);
            parameters.Add("SENDER_BANK", message.SENDER_BANK);
            parameters.Add("RECEIVER_BANK", message.RECEIVER_BANK);
            parameters.Add("NUMBER", dbType: DbType.Int32, direction: ParameterDirection.InputOutput);
            parameters.Add("DOCUMENT_NUMBER", dbType: DbType.String, direction: ParameterDirection.InputOutput, size: 16);
            parameters.Add("ID", dbType: DbType.Guid, direction: ParameterDirection.InputOutput);
            connection.Execute("sp_SaveBankmailMessage", parameters, commandType: CommandType.StoredProcedure, commandTimeout: 180, transaction: trans);
            number = parameters.Get<int>("NUMBER");
            documentNumber = parameters.Get<string>("DOCUMENT_NUMBER");
            return parameters.Get<Guid>("ID");
        }

        public Guid? GetAcknowledgedBankmailMessage(string reference, string messageType)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MESSAGE_TYPE", messageType);
            parameters.Add("DOCUMENT_NUMBER", reference);
            return GetSingle<Guid>(parameters, "sp_GetAcknowledgedBankmailMessage");
        }

        public void AcknowledgeBankmailMessage(Guid id, bool messageResult)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("MESSAGE_RESULT", messageResult);
            Execute(parameters, "sp_AcknowledgeBankmailMessage");
        }
    }
}
