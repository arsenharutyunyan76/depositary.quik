﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class StockAccountRepository : BaseRepository
    {
        public StockAccountRepository(string connectionString) : base(connectionString)
        {
        }

        public string GetStockAccountByCurrency(string currency)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("CURRENCY", currency);
            return GetSingle<string>(parameters, "sp_GetStockAccountByCurrency");
        }

        public async Task<IEnumerable<StockAccount>> GetStockAccounts()
        {
            return await GetListAsync<StockAccount>(new DynamicParameters(), "sp_GetStockAccounts");
        }

        public async Task SaveStockAccount(StockAccount stockAccount)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("CURRENCY", stockAccount.CURRENCY);
            parameters.Add("ACCOUNT", stockAccount.ACCOUNT);
            await ExecuteAsync(parameters, "sp_SaveStockAccount");
        }
    }
}
