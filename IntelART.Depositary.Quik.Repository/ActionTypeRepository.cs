﻿using Dapper;
using System;

namespace IntelART.Depositary.Quik.Repository
{
    public class ActionTypeRepository : BaseRepository
    {
        public ActionTypeRepository(string connectionString) : base(connectionString)
        {

        }

        // Update service last operation date.
        public void UpdateServiceLastOperationDate(byte typeId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", typeId);
            Execute(parameters, "sp_UpdateActionTypeLastOperationDate");
        }
    }
}
