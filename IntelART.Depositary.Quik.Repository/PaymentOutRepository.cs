﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System;
using System.Collections.Generic;
using System.Data;

namespace IntelART.Depositary.Quik.Repository
{
    public class PaymentOutRepository : BaseRepository
    {
        public PaymentOutRepository(string connectionString) : base(connectionString)
        {
        }

        public void SetProcessedPaymentOutMessage(string ID)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", ID);
            Execute(parameters, "sp_SetProcessedPaymentOutMessage");
        }

        public void SavePaymentOutMessage(PaymentOutMessage message, ref int number, ref string documentNumber, ref string MessageID)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", message.ID, dbType: DbType.String, direction: ParameterDirection.InputOutput, size: 50);
            parameters.Add("MESSAGE_TYPE", message.MESSAGE_TYPE);
            parameters.Add("NUMBER", dbType: DbType.Int32, direction: ParameterDirection.InputOutput);
            parameters.Add("DOCUMENT_NUMBER", dbType: DbType.String, direction: ParameterDirection.InputOutput, size: 16);
            parameters.Add("SECURITY_ID", message.SECURITY_ID);
            parameters.Add("CURRENCY", message.CURRENCY);
            parameters.Add("INITIAL_AMOUNT", message.INITIAL_AMOUNT);
            parameters.Add("AMOUNT", message.AMOUNT);
            parameters.Add("PAYER", message.PAYER);
            parameters.Add("RECEIVER", message.RECEIVER);
            parameters.Add("RECEIVER_NAME", message.RECEIVER_NAME);
            parameters.Add("IS_FINAL_CALCULATION", message.IS_FINAL_CALCULATION);
            parameters.Add("OPCODE", message.OPCODE);
            parameters.Add("CODE", message.CODE);
            Execute(parameters, "sp_SavePaymentOutMessage");
            number = parameters.Get<int>("NUMBER");
            documentNumber = parameters.Get<string>("DOCUMENT_NUMBER");
            MessageID = parameters.Get<string>("ID");
        }

        public IEnumerable<PaymentOutMessage> GetPaymentOutMessage(DateTime dateFrom, DateTime dateTo)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MESSAGE_DATE_FROM", dateFrom);
            parameters.Add("MESSAGE_DATE_TO", dateTo);
            return GetList<PaymentOutMessage>(parameters, "sp_GetPaymentOutMessage");
        }

    }
}
