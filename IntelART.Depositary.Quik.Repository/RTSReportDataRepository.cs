﻿using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class RTSReportDataRepository : BaseRepository
    {
        public RTSReportDataRepository(string connectionString) : base(connectionString)
        {
        }

        public void SaveRTSReportDataQuote(QuoteHist quoteData)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", quoteData.ID);
            parameters.Add("STATUS", quoteData.STATUS);
            parameters.Add("ISSUE_NAME", quoteData.ISSUE_NAME);
            parameters.Add("PRICE", quoteData.PRICE);
            parameters.Add("YIELD", quoteData.YIELD);
            parameters.Add("HISTORY_ID", quoteData.HISTORY_ID);
            parameters.Add("HISTORY_ACTION", quoteData.HISTORY_ACTION);
            parameters.Add("HISTORY_MOMENT", quoteData.HISTORY_MOMENT);
            parameters.Add("LSTG", quoteData.LSTG);
            parameters.Add("INIT_QTY", quoteData.INIT_QTY);
            parameters.Add("MOMENT", quoteData.MOMENT);
            parameters.Add("FIRM_NAME", quoteData.FIRM_NAME);
            parameters.Add("WKS", quoteData.WKS);
            parameters.Add("SETTL_PAIR", quoteData.SETTL_PAIR);
            parameters.Add("ORDER_TYPE", quoteData.ORDER_TYPE);
            parameters.Add("ALL_NON", quoteData.ALL_NON);
            parameters.Add("LEAVE", quoteData.LEAVE);
            parameters.Add("CATALYST", quoteData.CATALYST);
            parameters.Add("TYPE", quoteData.TYPE);
            parameters.Add("HISTORY_EVENT", quoteData.HISTORY_EVENT);
            parameters.Add("DEPO_ACCOUNTS", quoteData.DEPO_ACCOUNTS);
            parameters.Add("QUOTE_QTY", quoteData.QUOTE_QTY);
            Execute(parameters, "sp_SaveRTSReportDataQuote");
        }

        public void SaveRTSReportDataTrade(TradeHist tradeData)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", tradeData.ID);
            parameters.Add("STATUS", tradeData.STATUS);
            parameters.Add("ISSUE_NAME", tradeData.ISSUE_NAME);
            parameters.Add("PRICE", tradeData.PRICE);
            parameters.Add("HISTORY_ID", tradeData.HISTORY_ID);
            parameters.Add("HISTORY_ACTION", tradeData.HISTORY_ACTION);
            parameters.Add("HISTORY_MOMENT", tradeData.HISTORY_MOMENT);
            parameters.Add("LSTG", tradeData.LSTG);
            parameters.Add("QTY", tradeData.QTY);
            parameters.Add("VOLUME", tradeData.VOLUME);
            parameters.Add("TYPE_EXT", tradeData.TYPE_EXT);
            parameters.Add("RATE", tradeData.RATE);
            parameters.Add("DELIVERY_DATE", tradeData.DELIVERY_DATE);
            parameters.Add("NEW_DELIVERY_DATE", tradeData.NEW_DELIVERY_DATE);
            parameters.Add("INIT_NAME", tradeData.INIT_NAME);
            parameters.Add("INIT_WKS", tradeData.INIT_WKS);
            parameters.Add("INIT_ACTION", tradeData.INIT_ACTION);
            parameters.Add("INIT_PAIR", tradeData.INIT_PAIR);
            parameters.Add("CONF_NAME", tradeData.CONF_NAME);
            parameters.Add("CONF_QUOTEID", tradeData.CONF_QUOTEID);
            parameters.Add("TRADE_REF", tradeData.TRADE_REF);
            parameters.Add("REPO_ACTION", tradeData.REPO_ACTION);
            parameters.Add("CONF_ACTION", tradeData.CONF_ACTION);
            parameters.Add("CONF_PAIR", tradeData.CONF_PAIR);
            parameters.Add("TRADE_MOMENT", tradeData.TRADE_MOMENT);
            parameters.Add("PRICE_CURRENCY", tradeData.PRICE_CURRENCY);
            parameters.Add("TYPE", tradeData.TYPE);
            parameters.Add("CONF_WKS", tradeData.CONF_WKS);
            parameters.Add("PRICE_RATE", tradeData.PRICE_RATE);
            parameters.Add("HISTORY_EVENT", tradeData.HISTORY_EVENT);
            parameters.Add("DEPO_ACCOUNTS", tradeData.DEPO_ACCOUNTS);
            Execute(parameters, "sp_SaveRTSReportDataTrade");
        }
    }
}
