﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class CurrencyRepository : BaseRepository
    {
        public CurrencyRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<Currency>> GetCurrencies()
        {
            DynamicParameters parameters = new DynamicParameters();
            return await GetListAsync<Currency>(parameters, "sp_GetCurrencies");
        }

        public async Task SaveCurrency(Currency cur)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("CODE", cur.CODE);
            parameters.Add("CURRENCY_TYPE", cur.CURRENCY_TYPE);
            await ExecuteAsync(parameters, "sp_SaveCurrency");
        }
    }
}
