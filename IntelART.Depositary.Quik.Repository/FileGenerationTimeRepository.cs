﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System;

namespace IntelART.Depositary.Quik.Repository
{
    public class FileGenerationTimeRepository : BaseRepository
    {
        public FileGenerationTimeRepository(string connectionString) : base(connectionString)
        {
        }

        public FileGenerationTime GetFileGenerationTime(int fileType)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("FILE_TYPE", fileType);
            FileGenerationTime result = GetSingle<FileGenerationTime>(parameters, "sp_GetFileGenerationTime");
            if (result == null)
                result = new FileGenerationTime()
                {
                    GENERATE_DATE = null
                };
            return result;
        }

        public void SetFileGenerationTime(int fileType, DateTime dt)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("FILE_TYPE", fileType);
            parameters.Add("GENERATE_DATE", dt);
            Execute(parameters, "sp_SetFileGenerationTime");
        }
    }
}
