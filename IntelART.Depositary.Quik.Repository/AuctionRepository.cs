﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.Repository
{
    public class AuctionRepository : BaseRepository
    {
        public AuctionRepository(string connectionString) : base(connectionString)
        {
        }

        public Auction GetAuctionData(DateTime date, string isin, byte auctionTypeId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("AUCTION_DATE", date);
            parameters.Add("ISIN", isin);
            parameters.Add("AUCTION_TYPE_ID", auctionTypeId);
            return GetSingle<Auction>(parameters, "sp_GetAuctionData");
        }

        public async Task<IEnumerable<Auction>> GetAuctions(DateTime dateFrom, DateTime dateTo, string isin = null)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("AUCTION_DATE_FROM", dateFrom);
            parameters.Add("AUCTION_DATE_TO", dateTo);
            parameters.Add("ISIN", isin);
            return await GetListAsync<Auction>(parameters, "sp_GetAuctions");
        }

        public async Task SetAuctionDate(Auction auction)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", auction.ID);
            parameters.Add("AUCTION_ID", auction.AUCTION_ID);
            parameters.Add("TIME_FROM", auction.TIME_FROM);
            parameters.Add("TIME_TO", auction.TIME_TO);
            parameters.Add("ORDER_TIME_TO", auction.ORDER_TIME_TO);
            parameters.Add("IS_CANCELLED", auction.IS_CANCELLED);
            await ExecuteAsync(parameters, "sp_SetAuctionDate");
        }

        public async Task DeleteAuction(int id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            await ExecuteAsync(parameters, "sp_DeleteAuction");
        }

        public Guid SaveAuctionParam(AuctionParameter auctionParameter, SqlConnection connection, SqlTransaction trans)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("AUCTION_YIELD", auctionParameter.AUCTION_YIELD);
            parameters.Add("AVERAGE_YIELD", auctionParameter.AVERAGE_YIELD);
            parameters.Add("MAXIMUM_YIELD", auctionParameter.MAXIMUM_YIELD);
            parameters.Add("MINIMUM_YIELD", auctionParameter.MINIMUM_YIELD);
            parameters.Add("PARTICIPANT_COUNT", auctionParameter.PARTICIPANT_COUNT);
            parameters.Add("TRADE_QUANTITY", auctionParameter.TRADE_QUANTITY);
            parameters.Add("CLASS_CODE", auctionParameter.CLASS_CODE);
            parameters.Add("SEC_CODE", auctionParameter.SEC_CODE);
            parameters.Add("TRADE_DATE", auctionParameter.TRADE_DATE);

            parameters.Add("ID", dbType: DbType.Guid, direction: ParameterDirection.InputOutput);
            connection.Execute("sp_SaveParam", parameters, commandType: CommandType.StoredProcedure, commandTimeout: 180, transaction: trans);
            return parameters.Get<Guid>("ID");
        }

        public AuctionParameter GetAuctionParameters(DateTime tradeDate, string secCode, string classCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ClassCode", classCode);
            parameters.Add("SecCode", secCode);
            parameters.Add("TradeDate", tradeDate);
            return GetSingle<AuctionParameter>(parameters, "sp_GetAuctionParameters");
        }
    }
}
