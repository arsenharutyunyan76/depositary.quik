﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Collections.Generic;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class ReportRepository : BaseRepository
    {
        public ReportRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<ReportSchedule>> GetReportSchedules()
        {
            return await GetListAsync<ReportSchedule>(new DynamicParameters(), "sp_GetReportSchedules");
        }

        public async Task SetReportTime(ReportSchedule report)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", report.ID);
            parameters.Add("GENERATION_TIME", report.GENERATION_TIME);
            parameters.Add("LAST_GENERATE_DATE", report.LAST_GENERATE_DATE);
            parameters.Add("EMAIL", report.EMAIL);
            parameters.Add("EMAIL_SUBJECT", report.EMAIL_SUBJECT);
            parameters.Add("EMAIL_BODY", report.EMAIL_BODY);
            await ExecuteAsync(parameters, "sp_SetReportTime");
        }

        public async Task<Guid> CreateReportApplication(byte id, DateTime dateFrom, DateTime dateTo)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("REPORT_ID", id);
            parameters.Add("REPORT_FROM_DATE", dateFrom);
            parameters.Add("REPORT_TO_DATE", dateTo);
            parameters.Add("ID", dbType: DbType.Guid, direction: ParameterDirection.InputOutput);
            await ExecuteAsync(parameters, "sp_CreateReportApplication");
            return parameters.Get<Guid>("ID");
        }

        public async Task<AuctionAnnouncement> GetAuctionAnnouncement()
        {
            return await GetSingleAsync<AuctionAnnouncement>(new DynamicParameters(), "sp_GetAuctionAnnouncement");
        }

        public async Task SaveAuctionAnnouncement(AuctionAnnouncement announcement)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("EMAIL", announcement.EMAIL);
            parameters.Add("EMAIL_SUBJECT", announcement.EMAIL_SUBJECT);
            parameters.Add("EMAIL_BODY", announcement.EMAIL_BODY);
            await ExecuteAsync(parameters, "sp_SaveAuctionAnnouncement");
        }

        public async Task<IEnumerable<ReportApplication>> GetReportApplicationsByDate(DateTime dateFrom, DateTime dateTo)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("DATE_FROM", dateFrom);
            parameters.Add("DATE_TO", dateTo);
            return await GetListAsync<ReportApplication>(parameters, "sp_GetReportApplicationsByDate");
        }

        public async Task DeleteReportApplication(Guid id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            await ExecuteAsync(parameters, "sp_DeleteReportApplication");
        }
    }
}
