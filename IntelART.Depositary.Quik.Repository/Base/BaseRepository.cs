﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.Repository
{
    public class BaseRepository
    {
        public string ConnectionString;

        public BaseRepository(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        public IEnumerable<T> GetList<T>(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                return connection.Query<T>(procedureName, parameters, commandType: cmdType, commandTimeout: timeoutInterval);
            }
        }

        public async Task<IEnumerable<T>> GetListAsync<T>(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                return await connection.QueryAsync<T>(procedureName, parameters, commandType: cmdType, commandTimeout: timeoutInterval);
            }
        }

        public T GetSingle<T>(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            return GetList<T>(parameters, procedureName, timeoutInterval, cmdType).FirstOrDefault();
        }

        public async Task<T> GetSingleAsync<T>(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                return await connection.QueryFirstOrDefaultAsync<T>(procedureName, parameters, commandType: cmdType, commandTimeout: timeoutInterval);
            }
        }

        public void Execute(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                connection.Execute(procedureName, parameters, commandType: cmdType, commandTimeout: timeoutInterval);
            }
        }

        public async Task ExecuteAsync(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                await connection.ExecuteAsync(procedureName, parameters, commandType: cmdType, commandTimeout: timeoutInterval);
            }
        }

        public async void ExecuteAsyncNoWait(DynamicParameters parameters, string procedureName, int timeoutInterval = 180, CommandType cmdType = CommandType.StoredProcedure)
        {
            using (IDbConnection connection = new SqlConnection(ConnectionString))
            {
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                await connection.ExecuteAsync(procedureName, parameters, commandType: cmdType, commandTimeout: timeoutInterval);
            }
        }

        public void AddTableValuedParameter<T>(DynamicParameters parameters, string parameterName, IEnumerable<T> rows)
        {
            DataTable dataTable = new DataTable();
            PropertyInfo[] properties = typeof(T).GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                dataTable.Columns.Add(new DataColumn(properties[i].Name, properties[i].PropertyType));
            }
            object[] values = new object[properties.Length];
            foreach (T row in rows)
            {
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(row);
                }
                dataTable.Rows.Add(values);
            }

            parameters.Add(parameterName, dataTable, DbType.Object);
        }

        public void AddSystemTableValuedParameter<T>(DynamicParameters parameters, string parameterName, IEnumerable<T> rows)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(new DataColumn("Value", typeof(T)));
            foreach (T row in rows)
            {
                dataTable.Rows.Add(row);
            }

            parameters.Add(parameterName, dataTable, DbType.Object);
        }
    }
}
