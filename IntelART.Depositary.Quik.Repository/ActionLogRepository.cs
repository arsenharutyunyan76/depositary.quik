﻿using System;
using System.Threading.Tasks;
using Dapper;
using System.Collections.Generic;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class ActionLogRepository : BaseRepository
    {
        public ActionLogRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<ActionLog>> GetActionLog(DateTime dateFrom, DateTime dateTo, bool? isFail)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("DATE_FROM", dateFrom);
            parameters.Add("DATE_TO", dateTo);
            parameters.Add("IS_FAIL", isFail);
            return await GetListAsync<ActionLog>(parameters, "sp_GetActionLog");
        }

        public void SaveActionLog(ActionLog actionLog)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ACTION_TYPE_ID", actionLog.ACTION_TYPE_ID);
            parameters.Add("IS_FAIL", actionLog.IS_FAIL);
            parameters.Add("DETAILS", actionLog.DETAILS);
            Execute(parameters, "sp_SaveActionLog");
        }

        public async Task<IEnumerable<ActionLog>> GetRecentFailedActions(string userID = null)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("USER_ID", userID);
            return await GetListAsync<ActionLog>(parameters, "sp_GetRecentFailedActions");
        }
    }
}
