﻿using System;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class QuikRepository : BaseRepository
    {
        public QuikRepository(string connectionString) : base(connectionString)
        {
        }

        public void ReplicateTrade(Guid id, Guid processedId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("PROCESSED_ID", processedId);
            Execute(parameters, "sp_ReplicateTrade");
        }

        public void ReplicateOrder(Guid id, Guid processedId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("PROCESSED_ID", processedId);
            Execute(parameters, "sp_ReplicateOrder");
        }

        public void ReplicateAuctionParam(Guid id, Guid processedId)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("PROCESSED_ID", processedId);
            Execute(parameters, "sp_ReplicateAuctionParam");
        }
    }
}
