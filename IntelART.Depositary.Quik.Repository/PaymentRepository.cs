﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System;
using System.Collections.Generic;

namespace IntelART.Depositary.Quik.Repository
{
    public class PaymentRepository : BaseRepository
    {
        public PaymentRepository(string connectionString) : base(connectionString)
        {
        }

        public void SavePaymentMessage(PaymentMessage message)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", message.ID);
            parameters.Add("MESSAGE_TYPE", message.MESSAGE_TYPE);
            parameters.Add("DOCUMENT_NUMBER", message.DOCUMENT_NUMBER);
            parameters.Add("SECURITY_ID", message.SECURITY_ID);
            parameters.Add("CURRENCY", message.CURRENCY);
            parameters.Add("AMOUNT", message.AMOUNT);
            parameters.Add("PAYER", message.PAYER);
            parameters.Add("RECEIVER", message.RECEIVER);
            Execute(parameters, "sp_SavePaymentMessage");
        }

        public void SetProcessedPaymentMessage(IEnumerable<PaymentMessage> messages)
        {
            DynamicParameters parameters;
            foreach(PaymentMessage message in messages)
            {
                parameters = new DynamicParameters();
                parameters.Add("ID", message.ID);
                Execute(parameters, "sp_SetProcessedPaymentMessage");
            }

        }

        public IEnumerable<PaymentMessage> GetPaymentMessage(DateTime dateFrom, DateTime dateTo)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MESSAGE_DATE_FROM", dateFrom);
            parameters.Add("MESSAGE_DATE_TO", dateTo);
            return GetList<PaymentMessage>(parameters, "sp_GetPaymentMessage");
        }
    }
}
