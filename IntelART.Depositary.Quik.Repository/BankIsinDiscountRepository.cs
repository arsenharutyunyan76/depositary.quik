﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class BankIsinDiscountRepository : BaseRepository
    {
        public BankIsinDiscountRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<BankIsinDiscount>> GetBankIsinDiscount(string bankCode, string isinCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("BANK_CODE", bankCode);
            parameters.Add("ISIN_CODE", isinCode);
            return await GetListAsync<BankIsinDiscount>(parameters, "sp_GetBankIsinDiscount");
        }

        public async Task SaveBankIsinDiscount(BankIsinDiscount bankIsinDiscount)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("BANK_CODE", bankIsinDiscount.BANK_CODE);
            parameters.Add("ISIN_CODE", bankIsinDiscount.ISIN_CODE);
            parameters.Add("DISCOUNT", bankIsinDiscount.DISCOUNT);
            parameters.Add("IS_ACTIVE", bankIsinDiscount.IS_ACTIVE);
            await ExecuteAsync(parameters, "sp_SaveBankIsinDiscount");
        }
    }
}
