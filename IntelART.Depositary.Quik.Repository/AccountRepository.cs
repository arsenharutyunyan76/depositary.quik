﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class AccountRepository : BaseRepository
    {
        public AccountRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<Account>> GetAccounts(string bankCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("BANK_CODE", bankCode);
            return await GetListAsync<Account>(parameters, "sp_GetAccounts");
        }

        public Account GetAccountBank(string accountCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("CODE", accountCode);
            return GetSingle<Account>(parameters, "sp_GetAccountBank");
        }
        public IEnumerable<AccountPairs> GetAccountPairs()
        {
            return GetList<AccountPairs>(new DynamicParameters(), "sp_GetAccountPairs");
        }

    }
}
