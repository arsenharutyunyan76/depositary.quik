﻿using System;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class OrderRepository : BaseRepository
    {
        public OrderRepository(string connectionString) : base(connectionString)
        {
        }

        public Guid SaveOrder(Order order, SqlConnection connection, SqlTransaction trans)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ORDER_NUMBER", order.ORDER_NUMBER);
            parameters.Add("ORDER_DATE", order.ORDER_DATE);
            parameters.Add("ISIN", order.ISIN);
            parameters.Add("AUCTION_TYPE_ID", order.AUCTION_TYPE_ID);
            parameters.Add("OPERATION_TYPE_ID", order.OPERATION_TYPE_ID);
            parameters.Add("PRICE", order.PRICE);
            parameters.Add("VALUE", order.VALUE);
            parameters.Add("YIELD", order.YIELD);
            parameters.Add("QUANTITY", order.QUANTITY);
            parameters.Add("BALANCE", order.BALANCE);
            parameters.Add("WITHDRAW_DATE", order.WITHDRAW_DATE);
            parameters.Add("BANK_CODE", order.BANK_CODE);
            parameters.Add("CLIENT_CODE", order.CLIENT_CODE);
            parameters.Add("BROKER_REFERENCE", order.BROKER_REFERENCE);
            parameters.Add("USER_ID", order.USER_ID);
            parameters.Add("ORDER_STATUS_ID", order.ORDER_STATUS_ID);
            parameters.Add("ID", dbType: DbType.Guid, direction: ParameterDirection.InputOutput);
            connection.Execute("sp_SaveOrder", parameters, commandType: CommandType.StoredProcedure, commandTimeout: 180, transaction: trans);
            return parameters.Get<Guid>("ID");
        }
    }
}
