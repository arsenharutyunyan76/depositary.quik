﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.Repository
{
    public class DependDataSetRepository : BaseRepository
    {
        public DependDataSetRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task SaveDependListOrder(DependOrder dependOrder)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ACCOUNT_REFERENCE", dependOrder.ACCOUNT_REFERENCE);
            parameters.Add("ATSORDER_ID", dependOrder.ATSORDER_ID);
            parameters.Add("ATSORDER_LINKED_ACCOUNT_OWNER_LIST", dependOrder.ATSORDER_LINKED_ACCOUNT_OWNER_LIST);
            parameters.Add("ATSORDER_LINKED_ACCOUNT_REFERENCE", dependOrder.ATSORDER_LINKED_ACCOUNT_REFERENCE);
            parameters.Add("ATS_NAME", dependOrder.ATS_NAME);
            parameters.Add("BALCHANGE_QTY", dependOrder.BALCHANGE_QTY);
            parameters.Add("DEPOPART_CODE", dependOrder.DEPOPART_CODE);
            parameters.Add("ISIN_CODE", dependOrder.ISIN_CODE);
            parameters.Add("ISIN_SHORT_NAME", dependOrder.ISIN_SHORT_NAME);
            await ExecuteAsync(parameters, "sp_SaveDependListOrder");
        }

        public async Task SaveDependIsin(DependIsin dependIsin)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ISIN_CCY", dependIsin.ISIN_CCY);
            parameters.Add("ISIN_TYPE", dependIsin.ISIN_TYPE);
            parameters.Add("ISIN_CODE", dependIsin.ISIN_CODE);
            await ExecuteAsync(parameters, "sp_SaveDependListIsin");
        }

        public async Task SaveDependHolder(DependHolder dependHolder)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ACCOUNT_ID", dependHolder.ACCOUNT_ID);
            parameters.Add("ACCOUNT_REFERENCE", dependHolder.ACCOUNT_REFERENCE);
            await ExecuteAsync(parameters, "sp_SaveDependListHolder");
        }

        public async Task SaveDependListMandate(DependMandate dependMandate)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MANDATE_DESCRIPTION", dependMandate.MANDATE_DESCRIPTION);
            parameters.Add("MANDATE_TYPE", dependMandate.MANDATE_TYPE);
            parameters.Add("ACCOUNT_REFERENCE", dependMandate.ACCOUNT_REFERENCE);
            await ExecuteAsync(parameters, "sp_SaveDependListMandate");
        }

        public async Task SaveDependListTransaction(DependTransaction dependTransaction)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("TRANS_ID", dependTransaction.TRANS_ID);
            parameters.Add("BALCHANGE_QTY", dependTransaction.BALCHANGE_QTY);
            parameters.Add("ISIN_CODE", dependTransaction.ISIN_CODE);
            parameters.Add("ISIN_SHORT_NAME", dependTransaction.ISIN_SHORT_NAME);
            parameters.Add("TRANS_REMARKS", dependTransaction.TRANS_REMARKS);
            parameters.Add("TRANS_REFERENCE", dependTransaction.TRANS_REFERENCE);
            await ExecuteAsync(parameters, "sp_SaveDependListTransaction");
        }

        public async Task SetDependOrderStatus(int atsOrderId, bool isCanceled)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ATSORDER_ID", atsOrderId);
            parameters.Add("IS_CANCELED", isCanceled);
            await ExecuteAsync(parameters, "sp_SetDependOrderStatus");
        }

        public void SetDependFileGenerationStatus(int atsOrderId, bool isFileGenerated)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ATSORDER_ID", atsOrderId);
            parameters.Add("IS_FILE_GENERATED", isFileGenerated);
            Execute(parameters, "sp_SetDependFileGenerationStatus");
        }

        public IEnumerable<DependFileInfo> GetDependFileInfo()
        {
            return GetList<DependFileInfo>(null, "sp_GetDependFileInfo");
        }

        public void SaveDependUndeposition(DependUndeposition dependUndeposition, bool isFinal)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("DEPO_ACCOUNTS", dependUndeposition.DEPO_ACCOUNTS);
            parameters.Add("DEPOPART_CODE", dependUndeposition.DEPOPART_CODE);
            parameters.Add("ACCOUNT_CODE", dependUndeposition.ACCOUNT_CODE);
            parameters.Add("ISIN_SHORT_NAME", dependUndeposition.ISIN_SHORT_NAME);
            parameters.Add("BALCHANGE_QTY", dependUndeposition.BALCHANGE_QTY);
            parameters.Add("ACCOUNT_REFERENCE", dependUndeposition.ACCOUNT_REFERENCE);
            parameters.Add("LINKED_ACCOUNT_REFERENCE", dependUndeposition.LINKED_ACCOUNT_REFERENCE);
            parameters.Add("PROCESSING_STATUS", null);
            parameters.Add("FINAL_CALCULATION", isFinal);

            Execute(parameters, "sp_SaveDependUndepositionInfo");
        }

        public IEnumerable<DependUndeposition> GetDependUndeposition(bool isForFinalCalculation)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("IS_FINAL", isForFinalCalculation);
            return GetList<DependUndeposition>(parameters, "sp_GetDependUndeposition");
        }

        public void SetDependUndepositionStatus(int id, bool isProcessed)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            parameters.Add("PROCESSING_STATUS", isProcessed);
            Execute(parameters, "sp_SetDependUndepositionStatus");
        }
    }
}