﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.Repository
{
    public class RTSOutDataRepository : BaseRepository
    {
        public RTSOutDataRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<QuoteHist> GetNotProcessedQUOTE_HIST()
        {
            return GetList<QuoteHist>(new DynamicParameters(), "sp_GetNotProcessedQUOTE_HIST");
        }

        public IEnumerable<TradeHist> GetNotProcessedTRADE_HIST()
        {
            return GetList<TradeHist>(new DynamicParameters(), "sp_GetNotProcessedTRADE_HIST");
        }

        public async Task<IEnumerable<Isin>> GetNotProcessedISIN()
        {
            return await GetListAsync<Isin>(new DynamicParameters(), "sp_GetNotProcessedISIN");
        }

        public void ProcessISIN(Isin isin)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("RTS_CODE", isin.RTS_CODE);
            parameters.Add("ISIN_CODE", isin.ISIN_CODE);
            Execute(parameters, "sp_ProcessISIN");
        }

        public void ProcessASSET_IO(string id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            Execute(parameters, "sp_ProcessASSET_IO");
        }

        public void ProcessQUOTE_HIST(int id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            Execute(parameters, "sp_ProcessQUOTE_HIST");
        }

        public void ProcessTRADE_HIST(int id)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", id);
            Execute(parameters, "sp_ProcessTRADE_HIST");
        }
    }
}
