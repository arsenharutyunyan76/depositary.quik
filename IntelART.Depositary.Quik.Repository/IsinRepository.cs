﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.Repository
{
    public class IsinRepository : BaseRepository
    {
        public IsinRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task SaveIsin(Isin isin)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("RTS_CODE", isin.RTS_CODE);
            parameters.Add("ISIN_CODE", isin.ISIN_CODE);
            parameters.Add("CURRENCY", isin.CURRENCY);
            parameters.Add("NOMINAL", isin.NOMINAL);
            parameters.Add("REGISTRATION_NUMBER", isin.REGISTRATION_NUMBER);
            parameters.Add("ISSUE_DATE", isin.ISSUE_DATE);
            parameters.Add("MATURITY_DATE", isin.MATURITY_DATE);
            parameters.Add("FREQUENCY", isin.FREQUENCY);
            parameters.Add("RATE", isin.RATE);
            parameters.Add("FORMULA", isin.FORMULA);
            await ExecuteAsync(parameters, "sp_SaveIsin");
        }

        public string GetRTScodeByIsin(string isinCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ISIN_CODE", isinCode);
            return GetSingle<string>(parameters, "sp_GetRTScodeByIsin");
        }

        public async Task<IEnumerable<Isin>> GetIsins()
        {
            DynamicParameters parameters = new DynamicParameters();
            return await GetListAsync<Isin>(parameters, "sp_GetIsin");
        }

        private async Task<IEnumerable<IsinCompact>> GetIsinFRFNullRows()
        {
            DynamicParameters parameters = new DynamicParameters();
            return await GetListAsync<IsinCompact>(parameters, "sp_GetIsinFRFNullRows");
        }

        private void ParseAndFillFRFNullRows(IEnumerable<IsinCompact> isinFRFNullRows) 
        {
            var spacesPattern = @"\s+";
            var formulaMapping = new Dictionary<string, string>()
            {
                { "bn","AUC/AUC" },
                { "s","30E/360"},
                { "t360","AUC/360"}
            };

            int formulaIndex = 0;
            int rateIndex = 1;
            int frequencyIndex = 2;
            var formula = string.Empty;

            foreach (var row in isinFRFNullRows)
            {
                Regex.Replace(row.ISIN_CODE, spacesPattern, string.Empty);
                Regex.Replace(row.REGISTRATION_NUMBER, spacesPattern, string.Empty);

                if(row.ISIN_CODE.StartsWith("AMGT"))
                {
                    if(formulaMapping.TryGetValue(row.REGISTRATION_NUMBER, out formula))
                    {
                        row.FORMULA = formula;
                    }
                }
                else
                {
                    var splittedRegNumber = row.REGISTRATION_NUMBER.Split(';');
                    decimal rate = default;
                    int frequency = default;

                    if(splittedRegNumber.Length >= 3)
                    {
                        if(formulaMapping.TryGetValue(splittedRegNumber[formulaIndex], out formula))
                        {
                            row.FORMULA = formula;

                            if (decimal.TryParse(splittedRegNumber[rateIndex], out rate))
                            {
                                row.RATE = rate;
                            }

                            if (int.TryParse(splittedRegNumber[frequencyIndex], out frequency))
                            {
                                row.FREQUENCY = frequency;
                            }
                        }
                    }
                }
            }
        }

        private async Task UpdateIsinFRFNullRows(IEnumerable<IsinCompact> isins)
        {
            DynamicParameters parameters = new DynamicParameters();
            AddTableValuedParameter<IsinCompact>(parameters, "Isins", isins);
            await ExecuteAsync(parameters, "sp_BulkUpdateIsinFRFNullRows");
        }

        public async Task ProcessFRFNullRows()
        {
            var rows = await GetIsinFRFNullRows();

            if (!rows.Any())
            {
                await Task.CompletedTask;
                return;
            }

            ParseAndFillFRFNullRows(rows);

            await UpdateIsinFRFNullRows(rows);
        }

    }
}
