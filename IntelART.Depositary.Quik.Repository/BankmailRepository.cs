﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System.Collections.Generic;

namespace IntelART.Depositary.Quik.Repository
{
    public class BankmailRepository : BaseRepository
    {
        public BankmailRepository(string connectionString) : base(connectionString)
        {
        }

        public void SaveBankmailMessage(BankmailMessage message, int number, string documentNumber, ref string body)
        {
            body = message.BODY.Replace("$NUMBER$", documentNumber);
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MESSAGE_TYPE", message.MESSAGE_TYPE);
            parameters.Add("NUMBER", number);
            parameters.Add("DOCUMENT_NUMBER", documentNumber);
            parameters.Add("SENDER_BANK", message.SENDER_BANK);
            parameters.Add("RECEIVER_BANK", message.RECEIVER_BANK);
            parameters.Add("BODY", body);
            Execute(parameters, "sp_SaveBankmailMessage");
        }

        public IEnumerable<BankmailMessage> GetNotProcessedMessages(string messageType)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MESSAGE_TYPE", messageType);
            return GetList<BankmailMessage>(parameters, "sp_GetNotProcessedMessages");
        }

        public void ProcessMessage(BankmailMessage message)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("ID", message.ID);
            parameters.Add("MESSAGE_TYPE", message.MESSAGE_TYPE);
            Execute(parameters, "sp_ProcessMessage");
        }
    }
}
