﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System;
using System.Collections.Generic;

namespace IntelART.Depositary.Quik.Repository
{
    public class RTSOutMessageRepository : BaseRepository
    {
        public RTSOutMessageRepository(string connectionString) : base(connectionString)
        {
        }

        public IEnumerable<RTSOutMessage> GetRTSOutMessage(string messageCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MSG_CODE", messageCode);
            return GetList<RTSOutMessage>(parameters, "sp_GetRTSOutMessageUndeposition");
        }

        public IEnumerable<RTSOutMessage> GetRTSOutMessageFinalCalculation(string messageCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MSG_CODE", messageCode);
            return GetList<RTSOutMessage>(parameters, "sp_GetRTSOutMessageFinalCalculation");
        }

        public IEnumerable<DependUndeposition> GetDependUndepositionInfo(string messageCode)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("MSG_CODE", messageCode);
            return GetList<DependUndeposition>(parameters, "sp_GetDependUndepositionInfo");
        }

        public IEnumerable<DependUndeposition> GetDependUndepositionFinalCalculationInfo()
        {
            DynamicParameters parameters = new DynamicParameters();
            return GetList<DependUndeposition>(null, "sp_GetDependUndepositionFinalCalculationInfo");
        }

    }
}
