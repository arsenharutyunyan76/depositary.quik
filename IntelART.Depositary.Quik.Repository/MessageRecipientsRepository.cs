﻿using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class MessageRecipientsRepository : BaseRepository
    {
        public MessageRecipientsRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task SaveMessageRecipients(MessageRecipients messageRecipients)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("RECIPIENT_LIST", messageRecipients.RECIPIENT_LIST);
            parameters.Add("EMAIL_SUBJECT", messageRecipients.EMAIL_SUBJECT);
            parameters.Add("EMAIL_BODY", messageRecipients.EMAIL_BODY);
            AddSystemTableValuedParameter<string>(parameters, "FILES", messageRecipients.FILES);
            await ExecuteAsync(parameters, "sp_SaveMessageRecipients");
        }
    }
}
