﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Repository
{
    public class BankUserRepository : BaseRepository
    {
        public BankUserRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<BankUser>> GetBankUsers()
        {
            return await GetListAsync<BankUser>(new DynamicParameters(), "sp_GetBankUsers");
        }

        public async Task SaveBankUser(BankUser bankUser)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("BANK_CODE", bankUser.BANK_CODE);
            parameters.Add("USER_ID", bankUser.USER_ID);
            parameters.Add("USER_CODE", bankUser.USER_CODE);
            await ExecuteAsync(parameters, "sp_SaveBankUser");
        }
    }
}
