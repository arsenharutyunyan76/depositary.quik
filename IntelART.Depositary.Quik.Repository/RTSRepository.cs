﻿using Dapper;
using IntelART.Depositary.Quik.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.Repository
{
    public class RTSRepository : BaseRepository
    {
        public RTSRepository(string connectionString) : base(connectionString)
        {
        }

        public async Task<IEnumerable<Isin>> GetNotProcessedISIN()
        {
            return await GetListAsync<Isin>(new DynamicParameters(), "sp_GetNotProcessedISIN");
        }

        public void ProcessISIN(Isin isin)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("RTS_CODE", isin.RTS_CODE);
            parameters.Add("ISIN_CODE", isin.ISIN_CODE);
            Execute(parameters, "sp_ProcessISIN");
        }
    }
}
