import './scss/index.scss';

import App from './app/App';
import React from 'react';
import ReactDOM from 'react-dom';
import { unregister } from './registerServiceWorker';
import './app/configs/validationConfig';

const rootElement = document.getElementById('root');

const render = () => {
  ReactDOM.render(<App />, rootElement as HTMLElement);
};

window.onload = () => render();

unregister();
