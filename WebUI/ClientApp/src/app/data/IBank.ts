import { IBankAccounts } from './IBankAccounts';

export interface IBank {
  AS_CODE: string;
  Accounts: IBankAccounts[];
  CODE: string;
  EMAIL: null | string;
  IS_ACTIVE: boolean;
  SHORT_NAME: string;
  NAME_AM: string;
  NAME_EN: string;
}
