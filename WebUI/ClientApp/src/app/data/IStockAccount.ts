export type IStockAccount = {
  CURRENCY: string;
  ACCOUNT: string;
};
