export interface IAnnouncement {
  EMAIL: null | string;
  EMAIL_BODY: string;
  EMAIL_SUBJECT: string;
}
