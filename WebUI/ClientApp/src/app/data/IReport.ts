export interface IReport {
  ID: number;
  NAME_AM: string;
  NAME_EN: string;
  GENERATION_TIME: string;
  LAST_GENERATE_DATE: string | null;
  EMAIL: string | null;
  EMAIL_SUBJECT: string | null;
  EMAIL_BODY: string | null;
}

export interface IReportPostApplication {
  REPORT_ID: number;
  REPORT_FROM_DATE: string;
  REPORT_TO_DATE: string;
}

export interface IReportApplication {
  REPORT_ID: number;
  REPORT_NAME_AM?: string;
  REPORT_FROM_DATE: string | null;
  REPORT_TO_DATE: string | null;
  CREATION_DATE: string;
  GENERATE_DATE: string;
  ID: string;
}

export interface IReportApplicationGetParams {
  stringFromDate: string;
  stringToDate: string;
  isFail: string;
}
