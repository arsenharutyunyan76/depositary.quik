export interface IAuction {
  AUCTION_ID: string;
  AUCTION_TYPE_ID: number;
  ID: number;
  CREATION_DATE: string;
  ISIN: string;
  AUCTION_DATE: string;
  AUCTION_EXTRA_ID: string;
  SETTLEMENT_DATE: string;
  YIELD: number;
  ISSUE_DATE: string;
  ISSUE_AMOUNT: number;
  MATURITY_DATE: string;
  MATURITY_PAYMENT_DATE: string;
  FIRST_PAYMENT_DATE: string;
  AUCTION_YIELD: number | null;
  MINIMUM_AMOUNT: number;
  MAXIMUM_AMOUNT: number;
  AMOUNT: number | null;
  TIME_FROM: string;
  TIME_TO: string;
  ORDER_TIME_TO: string;
  IS_CANCELLED: boolean;
  AUCTION_TYPE_NAME_AM: string;
  AUCTION_TYPE_NAME_EN: string;
}

export interface IAuctionPostParams {
  stringFromDate: string;
  stringToDate: string;
  isin: string;
}
