export interface IActionLog {
  ID: number;
  CREATION_DATE: string;
  ACTION_TYPE_ID: number;
  IS_FAIL: boolean;
  DETAILS: string;
  ACTION_TYPE_NAME_AM: string;
  ACTION_TYPE_NAME_EN: string;
}

export interface IActionLogGetParams {
  stringFromDate: string;
  stringToDate: string;
  isFail: string;
}
