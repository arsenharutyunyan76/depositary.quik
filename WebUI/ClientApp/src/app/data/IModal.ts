import { ModalHeaderProps } from 'react-bootstrap/ModalHeader';
import { ModalProps } from 'react-bootstrap';
import React from 'react';

export interface IModalOptions {
  children: React.ReactNode | string;
  modalProps?: ModalProps;
  headerProps?: ModalHeaderProps;
  footer?: React.ReactNode | string;
  title?: string;
  closeButton?: boolean;
}

export interface IConfirmModal {
  state: boolean;
  message: string | null;
  title: string;
  confirmAction?: () => void;
}
