import { Column, CellProps, Row } from 'react-table';

export type ITableColumnOptions = {
  format?: string;
};

export interface ITableColumnConfig<D extends object = {}> {
  Header: string;
  accessor: keyof D;
  columns?: Array<Column>;
  width?: number;
  minWidth?: number;
  maxWidth?: number;
  customOptions?: ITableColumnOptions;
  CustomCellComponent?: (cell: CellProps<D> & ITableColumnOptions) => JSX.Element;
}

export type PureCellProps<T extends object, K> = CellProps<T, K>;
export type CustomCellProps<T extends object> = CellProps<T, any> & ITableColumnOptions;

export type RowProps<R extends object> = Row<R>;
