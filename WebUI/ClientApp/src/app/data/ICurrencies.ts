export interface ICurrency {
  CODE: string;
  CURRENCY_TYPE: string;
}
