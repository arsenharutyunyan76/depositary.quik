export interface IBankAccounts {
  CODE: string;
  CURRENCY: string;
  ID: number;
  BANK_CODE: string;
  IS_OWN: boolean;
  CUSTOMER_CODE: string;
  SECURITY_ACCOUNT: string;
}
