import { IAuthRoles } from './IAuth';

export interface INavigation {
  id: string;
  title?: string;
  type: 'collapse' | 'item' | 'divider';
  icon?: string;
  url?: string;
  exact?: boolean;
  children?: INavigation[];
  auth?: IAuthRoles[keyof IAuthRoles];
}
