export interface IBankUser {
  BANK_CODE: string;
  USER_ID: number | null;
  USER_CODE: string;
}
