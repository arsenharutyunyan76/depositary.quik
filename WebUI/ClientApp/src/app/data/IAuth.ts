export interface IAuthRoles {
  superAdmin: ['superAdmin', 'admin', 'moderator'];
  admin: ['admin', 'moderator'];
  moderator: ['moderator'];
}
