export interface IAuctionTypeChartData {
  data: number[];
  backgroundColor?: string[];
  hoverBackgroundColor?: string[];
}

export interface IAuctionTypeChart {
  labels: string[];
  datasets: IAuctionTypeChartData[];
}
