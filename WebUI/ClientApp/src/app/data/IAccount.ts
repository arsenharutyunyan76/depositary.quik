export interface IAccount {
  ID: number;
  BANK_CODE: string;
  CODE: string;
  CURRENCY: string;
}
