import * as AppTypes from 'AppTypes';

import React, { useEffect, useState, ReactNode } from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import { matchRoutes } from 'react-router-config';
import { routes } from '../configs/routes';
import { useSelector } from 'react-redux';

const Authorization: React.FC<{ children: ReactNode }> = props => {
  const location = useLocation();
  const history = useHistory();
  const userRole = useSelector<AppTypes.ReducerState, string>(({ user }) => user.userInfo.role);
  const [hasRouteAccess, setHasRouteAccess] = useState<boolean>(true);

  useEffect(() => {
    const { pathname } = location;
    const matched = matchRoutes(routes, pathname)[0];

    const hasRouteAccess =
      matched && matched.route.auth && matched.route.auth.length > 0 ? matched.route.auth.includes(userRole) : true;

    setHasRouteAccess(hasRouteAccess);
  }, [location, userRole]);

  useEffect(() => {
    !hasRouteAccess && history.push('/');
  }, [hasRouteAccess]);

  return hasRouteAccess ? <React.Fragment key={String(hasRouteAccess)}>{props.children}</React.Fragment> : null;
};

export default Authorization;
