import React, { memo, useMemo } from 'react';
import { ISelectOptions } from '../../Domain';
import CreatableSelect from 'react-select/creatable';
import { SelectStyle } from './style';
import FormControl from 'react-bootstrap/FormControl';

export interface IProps {
  items: ISelectOptions[];
  selected: string;
  isValid?: boolean;
  isInvalid?: boolean;
  menuListHeight?: number | string;
  isLoading: boolean;
  error?: string;
  setValue: (value: string) => void;
  onCreate: (currency: string) => void;
}

const CustomCreatableSelect: React.FC<IProps> = props => {
  const { items, isValid, isInvalid, menuListHeight, error, isLoading, selected, setValue, onCreate, ...rest } = props;

  const selectProps = useMemo(() => {
    return {
      placeholder: 'Ընտրել',
      isSearchable: true,
      isClearable: true,
      styles: SelectStyle({ isValid, isInvalid, menuListHeight }),
      isLoading: isLoading && !items.length,
      noOptionsMessage: () => 'Չկա ընտրություն',
      loadingMessage: () => `Բեռնվում է...`,
      ...rest
    };
  }, [isValid, isInvalid, menuListHeight, isLoading, items.length, rest]);

  const handleChange = (newValue: any) => {
    const value = newValue ? newValue.value : '';
    setValue(value);
  };

  const value = useMemo(() => {
    return items.find(i => i.value === selected);
  }, [selected, items]);

  const onCreateOption = (option: string) => {
    onCreate(option);
    setValue(option);
  };

  return (
    <>
      <CreatableSelect
        {...selectProps}
        onChange={handleChange}
        options={items}
        value={value}
        onCreateOption={onCreateOption}
      />
      {error && (
        <FormControl.Feedback type="invalid" className="d-block">
          {error}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default memo(CustomCreatableSelect);
