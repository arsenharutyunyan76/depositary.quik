import React, { useMemo } from 'react';
import Select from 'react-select';
import FormControl from 'react-bootstrap/FormControl';
import { SelectStyle } from './style';
import { ISelectOptions } from 'app/Domain';

export interface CustomSelectProps {
  items: ISelectOptions[];
  name: string;
  selected: string | number;
  isValid?: boolean;
  isInvalid?: boolean;
  menuListHeight?: number | string;
  isLoading: boolean;
  error?: string;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
}

const CustomSelect: React.FC<CustomSelectProps> = ({
  items,
  name,
  selected,
  setFieldValue,
  isValid,
  isInvalid,
  menuListHeight,
  isLoading,
  ...rest
}) => {
  const onChange = (selected: any) => {
    const value = selected ? selected.value : '';
    return setFieldValue(name, value);
  };

  const selectValue = useMemo(() => {
    const value = selected ? items.find(o => o.value === selected) : null;
    return value && { value };
  }, [selected, items]);

  const selectProps = {
    placeholder: 'Ընտրել',
    isSearchable: false,
    isClearable: false,
    styles: SelectStyle({ isValid, isInvalid, menuListHeight }),
    isLoading: isLoading && !items.length,
    noOptionsMessage: () => 'Չկա ընտրություն',
    loadingMessage: () => `Բեռնվում է...`,
    ...rest
  };

  return (
    <>
      <Select options={items} {...selectProps} {...selectValue} onChange={onChange} />

      {selectProps.error && (
        <FormControl.Feedback type="invalid" className="d-block">
          {selectProps.error}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default CustomSelect;
