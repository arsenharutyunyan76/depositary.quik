export const SelectStyle = (props: any) => ({
  menuPortal: (styles: any) => ({
    ...styles,
    zIndex: 999
  }),
  menu: (styles: any) => ({
    ...styles,
    backgroundColor: '#ffffff'
  }),
  menuList: (styles: any) => ({
    ...styles,
    height: props.menuListHeight || 'auto'
  }),
  container: (styles: any, state: any) => ({
    ...styles,
    backgroundColor: state.isDisabled ? '#eee' : 'transparent'
  }),
  control: (styles: any, state: any) => ({
    ...styles,
    minHeight: 35,
    boxShadow: state.isFocused ? `inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, .6)` : '',
    borderColor: state.isFocused
      ? `rgba(102, 175, 233, .6);`
      : props.isValid
      ? '#3DA08B;'
      : props.isInvalid
      ? '#e10003;'
      : '#ccc'
  }),
  dropdownIndicator: (styles: any) => ({
    ...styles,
    padding: '6px 8px',
    color: props.isValid ? '#3DA08B;' : props.isInvalid ? `#e10003;` : ''
  }),
  clearIndicator: (styles: any) => ({
    ...styles,
    cursor: 'pointer',
    padding: '6px'
  })
});
