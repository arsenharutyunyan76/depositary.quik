import React from 'react';
import * as yup from 'yup';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import { ReducerState } from 'AppTypes';
import { IStockAccount, ISelectOptions } from 'app/Domain';
import { Formik, Form, Field, getIn } from 'formik';
import { useDispatch } from 'react-redux';
import { Col, Row } from 'react-bootstrap';
import { TextInput } from '../Form';
import { saveStockAccount } from 'app/api/StockAccount';
import CustomSelect from '../Select/CustomSelect';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';

export interface StockAccountFormProps {
  data: IStockAccount;
  onSaveCallback: (values: IStockAccount) => void;
  currencies: ISelectOptions[];
}

const validationSchema = yup.object().shape({
  CURRENCY: yup.string().required()
});

const StockAccountForm: React.FC<StockAccountFormProps> = ({ data, onSaveCallback, currencies }) => {
  const dispatch = useDispatch<ThunkDispatch<ReducerState, {}, Action<string>>>();

  const onValidSubmit = async (values: IStockAccount) => {
    try {
      await dispatch(saveStockAccount(values));
      onSaveCallback(values);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Formik initialValues={data} enableReinitialize={true} onSubmit={onValidSubmit} validationSchema={validationSchema}>
      {({ handleSubmit, touched, errors, values, setFieldValue }) => (
        <Form className="stock-account-form" noValidate={true} onSubmit={handleSubmit} id="stockAccountForm">
          <FormGroup as={Row} controlId="account">
            <FormLabel column={true} sm="3">
              Հաշիվ
            </FormLabel>
            <Col sm="8">
              <Field name="ACCOUNT" component={TextInput} />
            </Col>
          </FormGroup>

          <FormGroup as={Row} controlId="currency">
            <FormLabel column={true} sm="3">
              Արժույթ
            </FormLabel>
            <Col sm="8">
              {data.CURRENCY ? (
                <Field name="CURRENCY" component={TextInput} disabled={true} />
              ) : (
                <CustomSelect
                  name="CURRENCY"
                  isLoading={false}
                  items={currencies}
                  setFieldValue={setFieldValue}
                  selected={values.CURRENCY}
                  isInvalid={getIn(touched, 'CURRENCY') && !!getIn(errors, 'CURRENCY')}
                />
              )}
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  );
};

export default StockAccountForm;
