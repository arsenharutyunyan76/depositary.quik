import * as AppTypes from 'AppTypes';
import React, { useCallback } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker from 'react-datepicker';
import FormGroup from 'react-bootstrap/FormGroup';
import FormControl from 'react-bootstrap/FormControl';
import moment from 'moment';
import { IAuctionPostParams } from 'app/Domain';
import { getAuctions } from 'app/api/Auctions';
import { setAuctionsParams } from 'app/store/reducers/auctions/actions';

const TableFilter: React.FC = () => {
  const dispatch = useDispatch();
  const params = useSelector<AppTypes.ReducerState, IAuctionPostParams>(({ auctions: { params } }) => params);
  const setDates = (name: string) => (date: Date) => {
    dispatch(setAuctionsParams({ ...params, [name]: moment(date).format('YYYYMMDD') }));
  };
  const stringFromDate = moment(params.stringFromDate).toDate();
  const stringToDate = moment(params.stringToDate).toDate();

  const handleChangeParam = (e: React.SyntheticEvent<any>) => {
    const { name, value } = e.currentTarget;
    dispatch(setAuctionsParams({ ...params, [name]: value }));
  };

  const updateAuctions = useCallback(() => {
    dispatch(getAuctions());
  }, []);

  return (
    <div className="auction-table-filter table-filter">
      <Button onClick={updateAuctions} size="lg" variant="link" className="mr-2">
        <i className="amx-refresh"></i>
      </Button>
      <FormGroup className="mb-0">
        <FormControl value={params.isin} onChange={handleChangeParam} name="isin" size="sm" placeholder="ISIN" />
      </FormGroup>
      <div className="date-range">
        <DatePicker
          className="start-date"
          showYearDropdown={true}
          selected={stringFromDate}
          onChange={setDates('stringFromDate')}
          selectsStart
          startDate={stringFromDate}
          endDate={stringToDate}
          customInput={<FormControl size="sm" name="stringFromDate" />}
        />
        <i className="amx-calendar"></i>
        <DatePicker
          showYearDropdown={true}
          className="end-date"
          selected={stringToDate}
          onChange={setDates('stringToDate')}
          selectsEnd
          startDate={stringFromDate}
          endDate={stringToDate}
          minDate={stringFromDate}
          customInput={<FormControl size="sm" name="stringToDate" />}
        />
      </div>
    </div>
  );
};

export default TableFilter;
