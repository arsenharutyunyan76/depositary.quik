import React from 'react';
import { ReducerState } from 'AppTypes';
import { Formik, Form, Field } from 'formik';
import DatePicker from 'react-datepicker';
import { Col, Row } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { formatDate, getSelectedTime, toCurrency } from 'app/services/utils';
import { setDatePickerValue } from 'app/services/utils';
import { saveAuction } from 'app/api/Auctions';
import { TextInput, CheckboxInput } from 'app/components/Form';

import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import FormControl from 'react-bootstrap/FormControl';
import { IAuction } from 'app/Domain';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';

export interface AuctionProps {
  data: IAuction;
  onSaveCallback: () => void;
}

const Auction: React.FC<AuctionProps> = ({ data, onSaveCallback }) => {
  const dispatch = useDispatch<ThunkDispatch<ReducerState, {}, Action<string>>>();

  const onValidSubmit = async (values: IAuction) => {
    try {
      await dispatch(saveAuction(values));
      onSaveCallback && onSaveCallback();
      return values;
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="auction-info">
      <Formik initialValues={{ ...data }} enableReinitialize={true} onSubmit={onValidSubmit}>
        {({ handleSubmit, setFieldValue, values }) => (
          <Form className="auction-form" noValidate={true} onSubmit={handleSubmit} id="auctionForm">
            <Row>
              <Col sm={{ span: 6 }}>
                {/* AUCTION_TYPE_NAME_AM */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի տեսակ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{values.AUCTION_TYPE_NAME_AM}</p>
                  </Col>
                </FormGroup>

                {/* AUCTION_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.AUCTION_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* SETTLEMENT_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Վերջնահաշվարկի ամսաթիվը
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.SETTLEMENT_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* CREATION_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Հայտարարության ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.CREATION_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* ISIN */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    ISIN
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{values.ISIN}</p>
                  </Col>
                </FormGroup>

                {/* AUCTION ID */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի նույնացուցիչ
                  </FormLabel>
                  <Col sm={{ span: 4, offset: 1 }}>
                    <Field name="AUCTION_ID" component={TextInput} />
                  </Col>
                </FormGroup>

                {/* TIME_FROM */}
                <FormGroup as={Row} controlId="formTimeFrom">
                  <FormLabel column={true} sm="6">
                    Աճուրդի սկիզբ
                  </FormLabel>
                  <Col sm={{ span: 3, offset: 2 }} className="text-right">
                    <DatePicker
                      selected={getSelectedTime(data.AUCTION_DATE, values.TIME_FROM)}
                      onChange={setDatePickerValue(setFieldValue, 'TIME_FROM', 'HH:mm')}
                      showTimeSelect
                      showTimeSelectOnly
                      timeIntervals={1}
                      timeCaption="Ժամ"
                      dateFormat="HH:mm"
                      name="TIME_FROM"
                      customInput={<FormControl />}
                    />
                  </Col>
                </FormGroup>

                {/* TIME_TO */}
                <FormGroup as={Row} controlId="formTimeTo">
                  <FormLabel column={true} sm="6">
                    Աճուրդի ավարտ
                  </FormLabel>
                  <Col sm={{ span: 3, offset: 2 }} className="text-right">
                    <DatePicker
                      selected={getSelectedTime(data.AUCTION_DATE, values.TIME_TO)}
                      onChange={setDatePickerValue(setFieldValue, 'TIME_TO', 'HH:mm')}
                      showTimeSelect
                      showTimeSelectOnly
                      timeIntervals={1}
                      timeCaption="Ժամ"
                      dateFormat="HH:mm"
                      name="TIME_TO"
                      customInput={<FormControl />}
                    />
                  </Col>
                </FormGroup>

                {/* ORDER_TIME_TO */}
                <FormGroup as={Row} controlId="formOrderTimeTo">
                  <FormLabel column={true} sm="6">
                    Հայտերի ընդունման ավարտ
                  </FormLabel>
                  <Col sm={{ span: 3, offset: 2 }} className="text-right">
                    <DatePicker
                      selected={getSelectedTime(data.AUCTION_DATE, values.ORDER_TIME_TO)}
                      onChange={setDatePickerValue(setFieldValue, 'ORDER_TIME_TO', 'HH:mm')}
                      showTimeSelect
                      showTimeSelectOnly
                      timeIntervals={1}
                      timeCaption="Ժամ"
                      dateFormat="HH:mm"
                      name="ORDER_TIME_TO"
                      customInput={<FormControl />}
                    />
                  </Col>
                </FormGroup>

                {/* IS_CANCELLED */}
                <FormGroup as={Row} controlId="formIsCanceled">
                  <FormLabel column={true} sm="6">
                    Դադարեցված է
                  </FormLabel>
                  <Col sm={{ span: 3, offset: 2 }} className="d-flex align-items-center">
                    <Field name="IS_CANCELLED" component={CheckboxInput} />
                  </Col>
                </FormGroup>
              </Col>
              <Col sm={{ span: 6 }}>
                {/* AUCTION_EXTRA_ID */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի տարբերակիչ ծածկագիրը
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{values.AUCTION_EXTRA_ID}</p>
                  </Col>
                </FormGroup>

                {/* YIELD */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Արժեկտրոնի տարեկան եկամտաբերությունը
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{values.YIELD}</p>
                  </Col>
                </FormGroup>

                {/* AUCTION_YIELD */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի եկամտաբերություն
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{values.AUCTION_YIELD || '-'}</p>
                  </Col>
                </FormGroup>

                {/* MATURITY_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Մարման ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.MATURITY_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* FIRST_PAYMENT_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Արժեկտրոնի առաջին վճարման ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.FIRST_PAYMENT_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* MATURITY_PAYMENT_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Մարման վճարման ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.MATURITY_PAYMENT_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* ISSUE_DATE */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Թողարկման ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{formatDate(values.ISSUE_DATE)}</p>
                  </Col>
                </FormGroup>

                {/* AMOUNT */}
                {values.AMOUNT && (
                  <FormGroup as={Row}>
                    <FormLabel column={true} sm="6">
                      Թողարկման ծավալ
                    </FormLabel>
                    <Col sm="5" className="text-right">
                      <p className="form-control-plaintext">{toCurrency(values.AMOUNT)}</p>
                    </Col>
                  </FormGroup>
                )}

                {/* ISSUE_AMOUNT */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Թողարկման ամսաթիվ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{toCurrency(values.ISSUE_AMOUNT)}</p>
                  </Col>
                </FormGroup>

                {/* MINIMUM_AMOUNT */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի ենթակա նվազագույն ծավալ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{toCurrency(values.MINIMUM_AMOUNT)}</p>
                  </Col>
                </FormGroup>

                {/* MAXIMUM_AMOUNT */}
                <FormGroup as={Row}>
                  <FormLabel column={true} sm="6">
                    Աճուրդի ենթակա առավելագույն ծավալ
                  </FormLabel>
                  <Col sm="5" className="text-right">
                    <p className="form-control-plaintext">{toCurrency(values.MAXIMUM_AMOUNT)}</p>
                  </Col>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Auction;
