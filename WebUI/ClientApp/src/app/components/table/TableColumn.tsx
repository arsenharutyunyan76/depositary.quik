import React from 'react';
import { ColumnInstance } from 'react-table';
import SortHandler from './SortHandler';

export interface Props<T extends object = {}> {
  children?: React.ReactNode;
  value: string;
  column: ColumnInstance<T>;
}

const TableColumn = <K extends object = {}>({ value, column }: Props<K>) => {
  return (
    <span>
      {value} <SortHandler isSorted={column.isSorted} isSortedDesc={column.isSortedDesc} />
    </span>
  );
};

export default TableColumn;
