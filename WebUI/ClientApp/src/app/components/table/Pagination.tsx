import { Pagination } from 'react-bootstrap';
import { UsePaginationInstanceProps } from 'react-table';
import React from 'react';

type Props<P extends object = {}> = UsePaginationInstanceProps<P> & {
  className?: string;
  pageIndex: number;
};

const TablePagination = <T extends object>(props: Props<T>) => {
  const { gotoPage, canPreviousPage, previousPage, nextPage, canNextPage, pageCount, pageIndex, className } = props;

  return (
    <Pagination className={className}>
      <Pagination.First onClick={() => gotoPage(0)} disabled={!canPreviousPage} />
      <Pagination.Prev onClick={() => previousPage()} disabled={!canPreviousPage} />
      <li className="page-item d-flex align-items-center mr-2 ml-2">{`Էջ ${pageIndex + 1} / ${pageCount}`}</li>
      <Pagination.Next onClick={() => nextPage()} disabled={!canNextPage} />
      <Pagination.Last onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage} />
    </Pagination>
  );
};

export default TablePagination;
