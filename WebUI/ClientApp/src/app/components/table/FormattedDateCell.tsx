import React from 'react';
import { formatDate } from '../../services/utils';
import { CustomCellProps } from 'app/Domain';

const FormattedDateCell = <K extends object>({ value, format }: CustomCellProps<K>) => {
  return <>{formatDate(value, format)}</>;
};

export default FormattedDateCell;
