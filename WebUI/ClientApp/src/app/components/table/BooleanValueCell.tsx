import React from 'react';
import { CustomCellProps } from 'app/Domain';

const BooleanValueCell = <K extends object>({ value }: CustomCellProps<K>) => {
  return (
    <span className="text-center d-block">
      {value ? <i className="amx-check text-success"></i> : <i className="amx-times text-danger"></i>}
    </span>
  );
};

export default BooleanValueCell;
