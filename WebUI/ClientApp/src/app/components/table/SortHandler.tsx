import React from 'react';
import clsx from 'clsx';

export interface SortHandlerProps {
  isSorted: boolean;
  isSortedDesc: boolean | undefined;
}

const SortHandler: React.FC<SortHandlerProps> = ({ isSorted, isSortedDesc }) => {
  return (
    <span
      className={clsx('table-sorting', {
        'no-dirty': !isSorted
      })}
    >
      {isSorted ? isSortedDesc ? <i className="amx-sorting-down"></i> : <i className="amx-sorting-up"></i> : ''}
    </span>
  );
};

export default SortHandler;
