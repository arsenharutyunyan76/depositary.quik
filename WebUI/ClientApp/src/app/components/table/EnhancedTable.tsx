import React from 'react';
import { useTable, Row, usePagination, useSortBy, UseTableOptions, TableState } from 'react-table';
import { Spinner, Table, TableProps } from 'react-bootstrap';
import TablePagination from './Pagination';

interface Props<K extends object = {}> extends UseTableOptions<K> {
  loading?: boolean;
  onFetchData?: (props: Partial<TableState<K>>) => void;
  tableProps?: TableProps;
  title?: string;
  paginate?: boolean;
  getRowProps?: (row: Row<K>) => React.HTMLAttributes<HTMLTableRowElement>;
}

function applyDefaultsTableOpt(props: TableProps = {}) {
  const { size = 'sm', striped = true, borderless = true, hover = true, ...rest } = props;
  return { ...rest, size, striped, borderless, hover };
}

const EnhancedTable = <K extends object = {}>({
  data,
  columns,
  initialState,
  getRowProps,
  loading,
  paginate = true,
  tableProps = {},
  ...rest
}: Props<K>) => {
  tableProps = applyDefaultsTableOpt(tableProps);
  const tableInstance = useTable<K>({ columns, data, initialState }, useSortBy, usePagination);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageCount,
    pageOptions,
    canPreviousPage,
    canNextPage,
    setPageSize,
    gotoPage,
    previousPage,
    nextPage,
    rows,
    state: { pageIndex }
  } = tableInstance;

  return (
    <div>
      <Table {...getTableProps()} className="enhanced-table" {...tableProps}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps(column.getSortByToggleProps())}>{column.render('Header')}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.length ? (
            page.map((row: Row<K>) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps(getRowProps ? getRowProps(row) : {})}>
                  {row.cells.map(cell => (
                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  ))}
                </tr>
              );
            })
          ) : (
            <tr>
              <td colSpan={headerGroups.length && headerGroups[0].headers.length} className="text-center p-5 bg-white">
                {loading ? (
                  <Spinner animation="border" role="status" variant="primary">
                    <span className="sr-only">Loading...</span>
                  </Spinner>
                ) : (
                  `${rest.title || 'Տվյալներ'} չկան`
                )}
              </td>
            </tr>
          )}
        </tbody>
      </Table>

      {paginate && (
        <div className="d-flex justify-content-between align-items-center">
          <div>{`Ընդհանուր ${rows.length} տող`}</div>
          <TablePagination
            page={page}
            pageCount={pageCount}
            pageOptions={pageOptions}
            canPreviousPage={canPreviousPage}
            canNextPage={canNextPage}
            gotoPage={gotoPage}
            previousPage={previousPage}
            nextPage={nextPage}
            setPageSize={setPageSize}
            pageIndex={pageIndex}
          />
        </div>
      )}
    </div>
  );
};
export default EnhancedTable;
