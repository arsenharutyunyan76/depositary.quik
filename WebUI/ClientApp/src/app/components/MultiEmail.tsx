import React from 'react';
import { ReactMultiEmail, isEmail } from 'react-multi-email';

export interface MultiEmailProps {
  value: string | null;
  placeholder?: string;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
}

const MultiEmail: React.FC<MultiEmailProps> = ({ value = '', setFieldValue, placeholder = 'Էլ. փոստի հասցեներ' }) => {
  const initialValue = value ? value.split(',') : undefined;
  return (
    <ReactMultiEmail
      placeholder={placeholder}
      emails={initialValue}
      onChange={(emails: string[]) => {
        setFieldValue('EMAIL', emails.join(','));
      }}
      validateEmail={email => {
        return isEmail(email); // return boolean
      }}
      getLabel={(email: string, index: number, removeEmail: (index: number) => void) => {
        return (
          <div data-tag key={index}>
            {email}
            <span data-tag-handle onClick={() => removeEmail(index)}>
              ×
            </span>
          </div>
        );
      }}
    />
  );
};

export default MultiEmail;
