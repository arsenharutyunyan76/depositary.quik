import React, { useEffect, useState } from 'react';
import { getRecentFailedAction } from '../api/RecentFailedAction';
import { IRecentFailedAction } from '../Domain';
import { Alert } from 'react-bootstrap';
import moment from 'moment';

const RecentFailedAction: React.FC = () => {
  const [data, setData] = useState<IRecentFailedAction[]>([]);

  const getData = async () => {
    const data = await getRecentFailedAction();
    setData(data);
  };

  useEffect(() => {
    getData();
    const timer = setInterval(getData, 8000);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <>
      {data.map(d => (
        <Alert key={d.ID} variant={d.IS_FAIL ? 'danger' : 'success'}>
          <h6>{d.ACTION_TYPE_NAME_AM}</h6>
          <small>{`${moment(d.CREATION_DATE).format('DD/MM/YYYY HH:mm:ss')} - ${d.DETAILS}`}</small>
        </Alert>
      ))}
    </>
  );
};

export default RecentFailedAction;
