import React, { memo } from 'react';
import { getIn, FieldProps } from 'formik';
import BaseInput from './BaseInput';

export interface TextInputProps extends FieldProps {
  label?: string;
  regexp?: RegExp;
  maxLength?: number;
  type?: string;
  asField?: React.ElementType<any>;
}

const TextInput: React.FC<TextInputProps> = props => {
  const { field, form, label, regexp, asField = 'input', ...rest } = props;
  const { name, value = '', onChange } = field;
  const error = getIn(form.errors, name);
  const isTouched = getIn(form.touched, name);
  const isNumeric = rest.type === 'number';

  const onChangeReques = (event: React.FormEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value;

    if (regexp && !regexp.test(value)) {
      return false;
    }
    if (isNumeric && rest.maxLength && value.length > rest.maxLength) {
      return false;
    }
    onChange(event);
  };

  return (
    <BaseInput
      as={asField}
      value={value || ''}
      name={name}
      onChange={onChangeReques}
      error={error}
      isTouched={isTouched}
      label={label}
      {...rest}
    />
  );
};

export default memo(TextInput);
