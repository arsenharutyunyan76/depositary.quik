import React, { memo } from 'react';
import FormControl from 'react-bootstrap/FormControl';
import FormLabel from 'react-bootstrap/FormLabel';
import { FormControlElement, ISelectOptions } from '../../Domain';

export interface BaseInputProps {
  name: string;
  value: string;
  label?: string;
  regexp?: RegExp;
  as?: React.ElementType<any>;
  error: string | undefined;
  isTouched: boolean;
  onChange: React.FormEventHandler<FormControlElement>;
  options?: ISelectOptions[];
  type?: string;
  className?: string;
}

function renderOptions(options: ISelectOptions[]) {
  return (
    <>
      <option value="">Ընտրել</option>
      {options.map(option => (
        <option value={option.label} key={option.value}>
          {option.value}
        </option>
      ))}
    </>
  );
}

const BaseInput: React.FC<BaseInputProps> = props => {
  const { isTouched, error, label, name, value, onChange, as, options, ...res } = props;
  const hasError = isTouched && !!error;

  return (
    <>
      {label && <FormLabel>{label}</FormLabel>}
      <FormControl as={as} isInvalid={hasError} {...{ name, value, onChange, ...res }}>
        {options && renderOptions(options)}
      </FormControl>
      {hasError && <FormControl.Feedback type="invalid">{error}</FormControl.Feedback>}
    </>
  );
};

export default memo(BaseInput);
