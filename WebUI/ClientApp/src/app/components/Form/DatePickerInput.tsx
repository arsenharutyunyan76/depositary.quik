import React, { memo } from 'react';
import FormControl from 'react-bootstrap/FormControl';
import FormLabel from 'react-bootstrap/FormLabel';
import { getIn, FieldProps } from 'formik';
import DatePicker, { ReactDatePickerProps } from 'react-datepicker';

export interface IProps extends FieldProps, ReactDatePickerProps {
  label?: string;
}

const DatePickerInput: React.FC<IProps> = ({ field: { name, value = null }, form, label, ...props }) => {
  const error = getIn(form.errors, name);
  const touched = getIn(form.touched, name);
  const hasError = touched && !!error;

  const handleChange = (date: Date | null) => {
    form.setFieldValue(name, date);
  };

  return (
    <>
      {label && <FormLabel>{label}</FormLabel>}
      <DatePicker
        wrapperClassName="d-block calendar-input"
        dateFormat="dd/MM/yyyy"
        inline={false}
        selected={value ? new Date(value) : null}
        autoComplete="false"
        customInput={<FormControl isInvalid={hasError} />}
        {...props}
        onChange={handleChange}
      />
      {hasError && (
        <FormControl.Feedback type="invalid" className="d-block">
          {error}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default memo(DatePickerInput);
