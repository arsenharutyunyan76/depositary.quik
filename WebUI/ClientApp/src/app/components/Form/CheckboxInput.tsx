import React, { memo } from 'react';
import { getIn, FieldProps } from 'formik';
import FormCheck from 'react-bootstrap/FormCheck';

export interface TextInputProps extends FieldProps {
  label?: React.ReactNode;
  type?: 'switch' | 'checkbox' | 'radio';
}

const TextInput: React.FC<TextInputProps> = props => {
  const { field, form, label, type = 'checkbox', ...rest } = props;
  const { name, value, onChange } = field;
  const error = getIn(form.errors, name);
  const isTouched = getIn(form.touched, name);
  const hasError = isTouched && !!error;

  return (
    <>
      <FormCheck
        type={type}
        label={label}
        checked={!!value}
        value={value}
        name={name}
        isInvalid={hasError}
        onChange={onChange}
        feedback={error}
        {...rest}
      />
    </>
  );
};

export default memo(TextInput);
