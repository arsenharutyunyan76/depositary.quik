import React, { memo } from 'react';
import { getIn, FieldProps } from 'formik';
import { ISelectOptions } from '../../Domain';
import BaseInput from './BaseInput';
import clsx from 'clsx';

export interface SelectInputProps extends FieldProps {
  label?: string;
  options: ISelectOptions[];
  loading?: boolean;
}

const SelectInput: React.FC<SelectInputProps> = ({
  field: { name, value, onChange },
  form,
  label,
  loading,
  options
}) => {
  const error = getIn(form.errors, name);
  const isTouched = getIn(form.touched, name);

  return (
    <BaseInput
      as="select"
      value={value || ''}
      name={name}
      onChange={onChange}
      error={error}
      isTouched={isTouched}
      label={label}
      options={options}
      className={clsx(loading && 'loading')}
    />
  );
};

export default memo(SelectInput);
