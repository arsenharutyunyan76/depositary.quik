import { IIsin } from 'app/Domain';

import { Col, Row } from 'react-bootstrap';
import React from 'react';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';

import { Formik, Form, Field } from 'formik';
import { saveIsin } from 'app/api/Isin';
import { TextInput } from 'app/components/Form';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';

export interface IsinFormProps {
  data: IIsin;
  onSaveCallback: () => void;
}

const validationSchema = yup.object().shape({
  RTS_CODE: yup.string(),
  ISIN_CODE: yup.string().nullable().required()
});

const IsinForm: React.FC<IsinFormProps> = ({ data, onSaveCallback }) => {
  const dispatch = useDispatch();

  const onValidSubmit = (values: IIsin) => {
    try {
      dispatch(saveIsin(values));
    } finally {
      onSaveCallback && onSaveCallback();
    }
  };

  return (
    <Formik
      initialValues={{ ...data }}
      enableReinitialize={true}
      onSubmit={onValidSubmit}
      validationSchema={validationSchema}
    >
      {({ handleSubmit }) => (
        <Form className="auction-form" noValidate={true} onSubmit={handleSubmit} id="isinForm">
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              ISIN կոդ
            </FormLabel>
            <Col sm="6" className="text-right">
              <Field name="ISIN_CODE" component={TextInput} disabled={!!data.ISIN_CODE} type="text" />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              RTS կոդ
            </FormLabel>
            <Col sm="6" className="text-right">
              <Field name="RTS_CODE" component={TextInput} type="text" />
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  );
};

export default IsinForm;
