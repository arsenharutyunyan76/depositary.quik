import * as AppTypes from 'AppTypes';
import * as Domain from '../../Domain';

import { useDispatch, useSelector } from 'react-redux';

import DatePicker from 'react-datepicker';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import FormCheck from 'react-bootstrap/FormCheck';
import FormControl from 'react-bootstrap/FormControl';
import React from 'react';
import moment from 'moment';
import { setActionLogParams } from '../../store/reducers/actionLog/actions';

const ActionLogTableFilter: React.FC = () => {
  const dispatch = useDispatch();
  const params = useSelector<AppTypes.ReducerState, Domain.IActionLogGetParams>(({ actionLog: { params } }) => params);
  const setDates = (name: string) => (date: Date) => {
    dispatch(setActionLogParams({ ...params, [name]: moment(date).format('YYYYMMDD') }));
  };
  const stringFromDate = moment(params.stringFromDate).toDate();
  const stringToDate = moment(params.stringToDate).toDate();

  const handleChangeParam = (e: React.SyntheticEvent<any>) => {
    const { name, checked } = e.currentTarget;
    dispatch(setActionLogParams({ ...params, [name]: checked }));
  };

  return (
    <div className="actionLog-table-filter table-filter">
      <FormGroup className="mb-0 mr-3" controlId="isFailAction">
        <FormLabel className="mr-2 mb-0">Ձախողված</FormLabel>
        <FormCheck
          custom
          type={'checkbox'}
          id="isFailAction"
          label={''}
          value={String(params.isFail)}
          checked={Boolean(params.isFail)}
          onChange={handleChangeParam}
          name="isFail"
        />
      </FormGroup>
      <div className="date-range">
        <DatePicker
          className="start-date"
          showYearDropdown={true}
          selected={stringFromDate}
          onChange={setDates('stringFromDate')}
          selectsStart
          startDate={stringFromDate}
          endDate={stringToDate}
          customInput={<FormControl size="sm" name="stringFromDate" />}
        />
        <i className="amx-calendar"></i>
        <DatePicker
          showYearDropdown={true}
          className="end-date"
          selected={stringToDate}
          onChange={setDates('stringToDate')}
          selectsEnd
          startDate={stringFromDate}
          endDate={stringToDate}
          minDate={stringFromDate}
          customInput={<FormControl size="sm" name="stringToDate" />}
        />
      </div>
    </div>
  );
};

export default ActionLogTableFilter;
