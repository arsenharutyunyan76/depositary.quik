import React from 'react';
import { Formik, Form, Field } from 'formik';
import { Col, Row } from 'react-bootstrap';
import { Button, Spinner } from 'react-bootstrap';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import MultiEmail from '../MultiEmail';
import { TextInput } from '../Form';

import * as Domain from '../../Domain';

export interface AnnouncementFormProps {
  initialValues: Domain.IAnnouncement;
  onSaveRequest: (values: Domain.IAnnouncement) => void;
  isLoading: boolean;
}

const AnnouncementForm: React.FC<AnnouncementFormProps> = ({ initialValues, onSaveRequest, isLoading }) => {
  return (
    <Formik initialValues={initialValues} enableReinitialize={true} onSubmit={onSaveRequest}>
      {({ handleSubmit, setFieldValue, values }) => (
        <Form className="auction-form" noValidate={true} onSubmit={handleSubmit} id="bankUserForm">
          <FormGroup as={Row} controlId="last_generate_date">
            <FormLabel column={true} sm="3">
              Էլ. փոստ
            </FormLabel>
            <Col sm="8">
              <MultiEmail value={values.EMAIL} setFieldValue={setFieldValue} placeholder={'Էլ. փոստի հասցեներ'} />
            </Col>
          </FormGroup>

          <FormGroup as={Row} controlId="last_generate_date">
            <FormLabel column={true} sm="3">
              Թեմա
            </FormLabel>
            <Col sm="8">
              <Field name="EMAIL_SUBJECT" component={TextInput} as="textarea" rows="3" />
            </Col>
          </FormGroup>
          <FormGroup as={Row} controlId="last_generate_date">
            <FormLabel column={true} sm="3">
              Հաղորդագրություն
            </FormLabel>
            <Col sm="8">
              <Field name="EMAIL_BODY" component={TextInput} asField={'textarea'} rows="6" />
            </Col>
          </FormGroup>
          <FormGroup as={Row} controlId="last_generate_date">
            <Col sm="12">
              <Button type="submit" variant="primary" disabled={isLoading}>
                {isLoading && <Spinner as="span" animation="border" size="sm" role="status" className="mr-1" />}
                Պահպանել
              </Button>
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  );
};

export default AnnouncementForm;
