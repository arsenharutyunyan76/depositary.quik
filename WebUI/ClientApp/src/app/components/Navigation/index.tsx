import * as AppTypes from 'AppTypes';
import * as Domain from '../../Domain';

import { Nav, NavDropdown } from 'react-bootstrap';

import NavCollapse from './NavCollapse';
import NavItem from './NavItem';
import React from 'react';
import { useSelector } from 'react-redux';

const Navigation: React.FC = () => {
  const navigation = useSelector<AppTypes.ReducerState, Domain.INavigation[]>(({ settings }) => settings.navigation);

  return (
    <Nav className="navigation flex-column">
      {navigation.map(item => (
        <React.Fragment key={item.id}>
          {item.type === 'collapse' && <NavCollapse item={item}></NavCollapse>}
          {item.type === 'item' && <NavItem item={item}></NavItem>}
          {item.type === 'divider' && <NavDropdown.Divider />}
        </React.Fragment>
      ))}
    </Nav>
  );
};

export default Navigation;
