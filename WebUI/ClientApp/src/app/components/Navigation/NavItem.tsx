import * as AppTypes from 'AppTypes';
import * as Domain from '../../Domain';

import { Link } from 'react-router-dom';
import { Nav } from 'react-bootstrap';
import React from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

export interface NavItemProps {
  item: Domain.INavigation;
}

const NavItem: React.FC<NavItemProps> = ({ item: { title, id, url, icon, auth } }) => {
  const location = useLocation();
  const userRole = useSelector<AppTypes.ReducerState, string>(({ user }) => user.userInfo.role);

  return auth && !auth.find(role => role === userRole) ? null : (
    <Nav.Item>
      <Nav.Link eventKey={id} as={Link} to={url || ''} active={location.pathname === url}>
        {icon && <i className={icon}></i>} {title}
      </Nav.Link>
    </Nav.Item>
  );
};

export default NavItem;
