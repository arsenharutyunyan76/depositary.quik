import { INavigation } from '../../Domain';

import { Button, Collapse, Nav } from 'react-bootstrap';
import React, { useEffect, useCallback, useState } from 'react';

import NavItem from './NavItem';
import _ from 'lodash';
import { useLocation } from 'react-router-dom';

export interface NavCollapseProps {
  item: INavigation;
}

const NavCollapse: React.FC<NavCollapseProps> = ({ item: { id, title, children, icon } }) => {
  const [open, setOpen] = useState(false);

  const toggleCollapse = useCallback(() => {
    setOpen(!open);
  }, []);

  const location = useLocation();

  useEffect(() => {
    const isOpen = _.find(children, ['url', location.pathname]) ? true : false;
    setOpen(isOpen);
  }, [children, location.pathname]);

  return (
    <>
      <Button
        variant="link"
        block={true}
        onClick={toggleCollapse}
        aria-controls={id}
        aria-expanded={open}
        as={Nav.Link}
        eventKey={id}
      >
        <i className={icon}></i>
        {title}
        <span className="amx-chevron-down"></span>
      </Button>
      <Collapse in={open} timeout={100}>
        <div id={id} className="collapse-children">
          {children && children.map(subItem => <NavItem key={subItem.id} item={subItem} />)}
        </div>
      </Collapse>
    </>
  );
};

export default NavCollapse;
