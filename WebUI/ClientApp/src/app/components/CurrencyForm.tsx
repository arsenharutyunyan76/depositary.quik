import React from 'react';
import * as yup from 'yup';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import { ReducerState } from 'AppTypes';
import { ICurrency } from 'app/Domain';
import { Formik, Form, Field, getIn } from 'formik';
import { useDispatch } from 'react-redux';
import { Col, Row } from 'react-bootstrap';
import { TextInput } from './Form';
import CustomSelect from 'app/components/Select/CustomSelect';
import { saveCurrency } from 'app/api/Currencies';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { CURRENCY_TYPES } from 'app/constants/CurrencyTypes';

export interface CurrencyFormProps {
  data: ICurrency;
  onSaveCallback: (values: ICurrency) => void;
}

const validationSchema = yup.object().shape({
  CODE: yup.string().required(),
  CURRENCY_TYPE: yup.string().required()
});

const CurrencyForm: React.FC<CurrencyFormProps> = ({ data, onSaveCallback }) => {
  const dispatch = useDispatch<ThunkDispatch<ReducerState, {}, Action<string>>>();

  const onValidSubmit = async (values: ICurrency) => {
    try {
      await dispatch(saveCurrency(values));
      onSaveCallback(values);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Formik initialValues={data} enableReinitialize={true} onSubmit={onValidSubmit} validationSchema={validationSchema}>
      {({ handleSubmit, setFieldValue, touched, errors, values }) => (
        <Form className="stock-account-form" noValidate={true} onSubmit={handleSubmit} id="currencyForm">
          <FormGroup as={Row} controlId="account">
            <FormLabel column={true} sm="3">
              Կոդ
            </FormLabel>
            <Col sm="8">
              <Field name="CODE" component={TextInput} disabled={!!data.CODE} />
            </Col>
          </FormGroup>

          <FormGroup as={Row} controlId="currency">
            <FormLabel column={true} sm="3">
              Փոխարժեքի տեսակ
            </FormLabel>
            <Col sm="8">
              <CustomSelect
                name={`CURRENCY_TYPE`}
                isLoading={false}
                items={CURRENCY_TYPES}
                setFieldValue={setFieldValue}
                selected={values.CURRENCY_TYPE}
                isInvalid={getIn(touched, 'CURRENCY_TYPE') && !!getIn(errors, 'CURRENCY_TYPE')}
              />
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  );
};

export default CurrencyForm;
