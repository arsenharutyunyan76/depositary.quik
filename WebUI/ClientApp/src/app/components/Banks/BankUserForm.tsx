import React, { useMemo, useEffect } from 'react';
import * as yup from 'yup';
import { ReducerState } from 'AppTypes';
import { Col, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import { Formik, Form, Field, getIn } from 'formik';
import { IBankUser } from 'app/Domain';
import { IStoreBanks } from 'app/store/reducers/banks/initialState';
import { TextInput } from '../Form';
import { getBanks } from '../../api/Bank';
import { saveBankUser } from '../../api/BankUsers';
import CustomSelect from '../Select/CustomSelect';

export interface BankUserFormProps {
  data: IBankUser;
  onSaveCallback: () => void;
}

const validationSchema = yup.object().shape({
  BANK_CODE: yup.string().required(),
  USER_ID: yup.number().nullable().required(),
  USER_CODE: yup.string().required()
});

const BankUserForm: React.FC<BankUserFormProps> = ({ data, onSaveCallback }) => {
  const dispatch = useDispatch();

  const { items: banks, isLoading: banksLoading } = useSelector<ReducerState, IStoreBanks>(({ banks }) => banks);

  const onValidSubmit = (values: IBankUser) => {
    try {
      dispatch(saveBankUser(values));
    } finally {
      onSaveCallback && onSaveCallback();
    }
  };

  useEffect(() => {
    if (!banks.length) {
      dispatch(getBanks());
    }
  }, [banks, dispatch]);

  const bankOptions = useMemo(() => {
    return banks.map(({ CODE: value, NAME_AM: label }) => ({ value, label }));
  }, [banks]);

  return (
    <Formik
      initialValues={{ ...data }}
      enableReinitialize={true}
      onSubmit={onValidSubmit}
      validationSchema={validationSchema}
    >
      {({ handleSubmit, setFieldValue, values, touched, errors }) => (
        <Form className="auction-form" noValidate={true} onSubmit={handleSubmit} id="bankUserForm">
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Բանկ
            </FormLabel>
            <Col sm="6">
              <CustomSelect
                name="BANK_CODE"
                menuListHeight={150}
                isLoading={banksLoading}
                items={bankOptions}
                setFieldValue={setFieldValue}
                selected={values.BANK_CODE}
                isInvalid={getIn(touched, 'BANK_CODE') && !!getIn(errors, 'BANK_CODE')}
              />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Օգտատերի համար
            </FormLabel>
            <Col sm="6" className="text-right">
              <Field name="USER_ID" component={TextInput} type="number" />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Օգտատերի կոդ
            </FormLabel>
            <Col sm="6" className="text-right">
              <Field name="USER_CODE" component={TextInput} />
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  );
};

export default BankUserForm;
