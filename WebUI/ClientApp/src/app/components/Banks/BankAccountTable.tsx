import React, { memo } from 'react';
import { getIn, useFormikContext } from 'formik';
import { Table, Spinner } from 'react-bootstrap';
import { IBankAccounts } from 'app/Domain';
import BankAccountRow from './BankAccountRow';

export interface BankAccountTableProps {
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
  loading: boolean;
  remove: <T>(index: number) => T | undefined;
}

const BankAccountTable: React.FC<BankAccountTableProps> = ({ remove, setFieldValue, loading }) => {
  const { values } = useFormikContext();
  const data: IBankAccounts[] = getIn(values, 'Accounts');

  return (
    <Table bordered={true} size="sm" className="bank-accounts-table">
      <thead>
        <tr>
          <th>Կոդ</th>
          <th>Արժույթ</th>
          <th>Սեփական</th>
          <th>Հաճախորդի կոդ</th>
          <th>Արժեթղթի հաշիվ</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {data && data.length > 0 ? (
          data.map((account, index) => (
            <BankAccountRow
              key={index}
              {...{
                ...account,
                remove,
                setFieldValue,
                index
              }}
            />
          ))
        ) : (
          <tr>
            <td colSpan={4} className="text-center">
              {loading ? (
                <Spinner animation="border" role="status" variant="primary">
                  <span className="sr-only">Loading...</span>
                </Spinner>
              ) : (
                'Չկան Հաշիվներ'
              )}
            </td>
          </tr>
        )}
      </tbody>
    </Table>
  );
};

export default memo(BankAccountTable);
