import React, { useMemo, useState, useCallback } from 'react';
import { ReducerState } from 'AppTypes';
import { IBankAccounts, ICurrency } from 'app/Domain';
import { Button } from 'react-bootstrap';
import { getIn, useFormikContext, Field } from 'formik';
import { TextInput, CheckboxInput } from 'app/components/Form';
import CustomSelect from 'app/components/Select/CustomSelect';
import { useSelector } from 'react-redux';

export interface BankAccountRowProps extends IBankAccounts {
  index: number;
  remove: <T>(index: number) => T | undefined;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
}

const BankAccountRow: React.FC<BankAccountRowProps> = ({ index, CODE, IS_OWN, CURRENCY, CUSTOMER_CODE, SECURITY_ACCOUNT, remove, setFieldValue }) => {
  const [editable, setEditable] = useState(!CODE);

  const currencies = useSelector<ReducerState, ICurrency[]>(({ currencies }) => currencies.items);

  const currenciesOptions = useMemo(() => {
    return currencies.map(({ CODE: value }) => ({ value, label: value }));
  }, [currencies]);

  const { touched, errors } = useFormikContext();

  const handleSetEditable = useCallback(() => {
    setEditable(prevState => !prevState);
  }, []);

  const isCurrencyInvalid =
    getIn(touched, `Accounts[${index}].CURRENCY`) && getIn(errors, `Accounts[${index}].CURRENCY`);

  return (
    <tr>
      <td>
        {editable ? (
          <Field name={`Accounts[${index}].CODE`} component={TextInput} />
        ) : (
          <p className="form-control-sm form-control-plaintext mb-0">{CODE}</p>
        )}
      </td>
      <td>
        {editable ? (
          <CustomSelect
            name={`Accounts[${index}].CURRENCY`}
            isLoading={false}
            items={currenciesOptions}
            setFieldValue={setFieldValue}
            selected={CURRENCY}
            isInvalid={isCurrencyInvalid}
          />
        ) : (
          <p className="form-control-sm form-control-plaintext mb-0">{CURRENCY}</p>
        )}
      </td>
      <td>
        {editable ? (
          <Field name={`Accounts[${index}].IS_OWN`} component={CheckboxInput} />
        ) : (
          <p className="form-control-sm form-control-plaintext mb-0">
            {IS_OWN ? <i className="amx-check text-success"></i> : <i className="amx-times text-danger"></i>}
          </p>
        )}
      </td>
      <td>
        {editable ? (
            <Field name={`Accounts[${index}].CUSTOMER_CODE`} component={TextInput} />
        ) : (
                <p className="form-control-sm form-control-plaintext mb-0">{CUSTOMER_CODE}</p>
            )}
      </td>
      <td>
        {editable ? (
            <Field name={`Accounts[${index}].SECURITY_ACCOUNT`} component={TextInput} />
        ) : (
                <p className="form-control-sm form-control-plaintext mb-0">{SECURITY_ACCOUNT}</p>
            )}
      </td>
      <td>
        <Button onClick={handleSetEditable} variant="link" size="sm">
          {editable ? <i className="amx-save text-danger"></i> : <i className="amx-pencil text-danger"></i>}
        </Button>
        <Button onClick={() => remove(index)} variant="link" size="sm">
          <i className="amx-trash text-danger"></i>
        </Button>
      </td>
    </tr>
  );
};

export default BankAccountRow;
