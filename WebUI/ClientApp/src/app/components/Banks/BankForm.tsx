import { ReducerState } from 'AppTypes';
import * as yup from 'yup';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Col, Row, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Formik, Form, FieldArray, Field } from 'formik';
import BankAccountTable from './BankAccountTable';
import { IBank } from 'app/Domain';
import { IStoreBanks } from 'app/store/reducers/banks/initialState';
import { TextInput, CheckboxInput } from '../Form';
import { saveBank, getBankAccounts } from 'app/api/Bank';
import MultiEmail from '../MultiEmail';

import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';

export interface BankFormProps {
  bankCode?: string;
  onSaveCallback: () => void;
}

const initialAccount = {
  CODE: '',
  CURRENCY: '',
  ID: 0,
  BANK_CODE: '',
  IS_OWN: false,
  CUSTOMER_CODE: '',
  SECURITY_ACCOUNT : ''
};

const initialModalData = {
  CODE: '',
  SHORT_NAME: '',
  NAME_AM: '',
  NAME_EN: '',
  IS_ACTIVE: false,
  AS_CODE: '',
  EMAIL: null,
  Accounts: []
};

const validationSchema = yup.object().shape({
  CODE: yup.string().required(),
  Accounts: yup.array().of(
    yup.object().shape({
      CURRENCY: yup.string().required(),
      CODE: yup.string().required()
    })
  )
});

const BankForm: React.FC<BankFormProps> = ({ bankCode, onSaveCallback }) => {
  const dispatch = useDispatch();
  const [formData, setFormData] = useState<IBank>(initialModalData);

  const { items: banks, fetchAccountsLoading } = useSelector<ReducerState, IStoreBanks>(({ banks }) => banks);

  const onValidSubmit = (values: IBank) => {
    try {
      dispatch(saveBank(values));
    } finally {
      onSaveCallback && onSaveCallback();
    }
  };

  useEffect(() => {
    if (bankCode) {
      dispatch(getBankAccounts(bankCode));
    }
  }, [bankCode]);

  useEffect(() => {
    const bank = banks.find(bank => bank.CODE === bankCode);
    if (bank) {
      setFormData(bank);
    }
  }, [banks]);

  return (
    <Formik
      initialValues={formData}
      enableReinitialize={true}
      onSubmit={onValidSubmit}
      validationSchema={validationSchema}
    >
      {({ handleSubmit, setFieldValue, values }) => (
        <Form className="auction-form" noValidate={true} onSubmit={handleSubmit} id="bankUserForm">
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Կոդ
            </FormLabel>
            <Col sm="5" className="text-right">
              <Field name="CODE" component={TextInput} />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Կարճ անվանում
            </FormLabel>
            <Col sm="5" className="text-right">
              <Field name="SHORT_NAME" component={TextInput} />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Հայերեն անվանում
            </FormLabel>
            <Col sm="5" className="text-right">
              <Field name="NAME_AM" component={TextInput} />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Անգլերեն անվանում
            </FormLabel>
            <Col sm="5" className="text-right">
              <Field name="NAME_EN" component={TextInput} />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Հաշվապահական ծրագրում կոդ
            </FormLabel>
            <Col sm="5" className="text-right">
              <Field name="AS_CODE" component={TextInput} />
            </Col>
          </FormGroup>
          <FormGroup as={Row}>
            <FormLabel column={true} sm="6">
              Էլ. Փոստ
            </FormLabel>
            <Col sm="5" className="text-right">
              <MultiEmail value={values.EMAIL} setFieldValue={setFieldValue} placeholder={'Էլ. փոստի հասցեներ'} />
            </Col>
          </FormGroup>

          <FormGroup as={Row} controlId="IsActiveBank">
            <FormLabel column={true} sm="6">
              Աճուրդի մասնակից է
            </FormLabel>
            <Col sm="5" className="d-flex align-items-center text-left">
              <Field name="IS_ACTIVE" component={CheckboxInput} id="IsActiveBank" />
            </Col>
          </FormGroup>
          <FieldArray name="Accounts">
            {arrayHelpers => (
              <div className="mt-4">
                <div className="d-flex justify-content-between mb-2">
                  <h4>Բանկի հաշիվներ</h4>
                  <OverlayTrigger placement="top" overlay={<Tooltip id="createBank">Ավելացնել նոր հաշիվ</Tooltip>}>
                    <Button
                      variant="link"
                      onClick={() =>
                        arrayHelpers.push({
                          ...initialAccount,
                          BANK_CODE: bankCode
                        })
                      }
                    >
                      <i className="amx-plus-o"></i>
                    </Button>
                  </OverlayTrigger>
                </div>
                <BankAccountTable
                  remove={arrayHelpers.remove}
                  setFieldValue={setFieldValue}
                  loading={fetchAccountsLoading}
                />
              </div>
            )}
          </FieldArray>
        </Form>
      )}
    </Formik>
  );
};

export default BankForm;
