import * as AppTypes from 'AppTypes';
import * as Domain from '../../../Domain';

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Doughnut } from 'react-chartjs-2';
import _ from 'lodash';
import { getData } from './data';
import { getAuctions } from '../../../api/Auctions';

const AuctionTypes: React.FC = () => {
  const dispatch = useDispatch();
  const [auctionsCount, setAuctionsCount] = useState<{ [key: number]: number }>({});
  const auctions = useSelector<AppTypes.ReducerState, Domain.IAuction[]>(({ auctions: { items } }) => items);

  useEffect(() => {
    dispatch(getAuctions());
  }, []);

  useEffect(() => {
    if (auctions.length) {
      setAuctionsCount(_.countBy(auctions, value => value.AUCTION_TYPE_ID));
    }
  }, [auctions]);

  return (
    <>
      <p className="mt-0 text-left">{`Ընդհանուր ${auctions.length} աճուրդ`}</p>
      <Doughnut {...getData(auctionsCount)}></Doughnut>
    </>
  );
};

export default AuctionTypes;
