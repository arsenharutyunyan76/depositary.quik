import _ from 'lodash';

type Props = { [key: number]: number };

export const getData = (counts: Props) => ({
  type: 'doughnut',
  data: {
    datasets: [
      {
        data: _.values(counts),
        backgroundColor: ['#3DA3E8', '#FD6585', '#996CFB'],
        label: ''
      }
    ],
    labels: ['Տեղաբաշխման', 'Հետգնման', 'Լրացուցիչ տեղաբաշխման']
  },
  options: {
    responsive: true,
    legend: { position: 'right' },
    animation: { animateScale: true, animateRotate: true }
  }
});
