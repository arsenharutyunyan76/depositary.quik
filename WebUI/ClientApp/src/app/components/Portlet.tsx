import { Button, Collapse } from 'react-bootstrap';
import React, { useState } from 'react';

import clsx from 'clsx';

export interface PortletProps {
  title: string;
  className?: boolean;
  collapsable?: boolean;
  actions?: React.ReactNode;
}

const Portlet: React.FC<PortletProps> = ({ title, className, collapsable, children, actions }) => {
  const [open, setOpen] = useState(true);
  const toggleCollapse = () => setOpen(!open);

  return (
    <div className={clsx('portlet', className)}>
      <div className="portlet-title">
        <h2 className="caption">{title}</h2>
        <div className="action">
          {actions}
          {collapsable && (
            <Button
              variant="link"
              size="sm"
              onClick={toggleCollapse}
              className={clsx('ml-1 d-flex toggle-collapse', {
                show: open
              })}
            >
              <i className="amx-chevron-down"></i>
            </Button>
          )}
        </div>
      </div>
      {collapsable ? (
        <Collapse in={open}>
          <div>
            <div className="portlet-body">{children}</div>
          </div>
        </Collapse>
      ) : (
        <div className="portlet-body">{children}</div>
      )}
    </div>
  );
};

export default Portlet;
