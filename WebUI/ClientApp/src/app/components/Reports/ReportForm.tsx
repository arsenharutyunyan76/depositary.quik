import { IReport } from 'app/Domain';

import { Col, Row } from 'react-bootstrap';
import { getSelectedDate, getSelectedTime, setDatePickerValue } from 'app/services/utils';

import DatePicker from 'react-datepicker';
import { Formik, Form, Field } from 'formik';
import React from 'react';
import moment from 'moment';
import { saveReportTime } from 'app/api/Reports';
import { useDispatch } from 'react-redux';
import MultiEmail from 'app/components/MultiEmail';
import { TextInput } from 'app/components/Form';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import FormControl from 'react-bootstrap/FormControl';

export interface ReportProps {
  data: IReport;
  onSaveCallback: () => void;
}

const Report: React.FC<ReportProps> = ({ data, onSaveCallback }) => {
  const dispatch = useDispatch();

  const onValidSubmit = (values: IReport) => {
    try {
      dispatch(saveReportTime(values));
    } finally {
      onSaveCallback && onSaveCallback();
    }
  };

  return (
    <div className="report-info">
      <Formik initialValues={{ ...data }} enableReinitialize={true} onSubmit={onValidSubmit}>
        {({ handleSubmit, setFieldValue, values }) => (
          <Form className="report-form" noValidate={true} onSubmit={handleSubmit} id="reportForm">
            <FormGroup as={Row} controlId="generation_time">
              <FormLabel column={true} sm="3">
                Պլանավորված ժամանակը
              </FormLabel>
              <Col sm="5">
                <DatePicker
                  selected={getSelectedTime(moment().format('YYYY-MM-DD'), values.GENERATION_TIME)}
                  onChange={setDatePickerValue(setFieldValue, 'GENERATION_TIME', 'HH:mm')}
                  showTimeSelect
                  showTimeSelectOnly
                  timeIntervals={15}
                  timeCaption="Ժամ"
                  dateFormat="HH:mm"
                  dateFormatCalendar="HH:mm"
                  name="GENERATION_TIME"
                  customInput={<FormControl />}
                />
              </Col>
            </FormGroup>
            <FormGroup as={Row} controlId="last_generate_date">
              <FormLabel column={true} sm="3">
                Վերջին արտահանման ամսաթիվ
              </FormLabel>
              <Col sm="5">
                <DatePicker
                  selected={getSelectedDate(values.LAST_GENERATE_DATE)}
                  onChange={setDatePickerValue(setFieldValue, 'LAST_GENERATE_DATE', 'YYYY-MM-DD')}
                  dateFormat="dd/MM/yyyy"
                  name="LAST_GENERATE_DATE"
                  customInput={<FormControl />}
                />
              </Col>
            </FormGroup>

            <FormGroup as={Row} controlId="last_generate_date">
              <FormLabel column={true} sm="3">
                Էլ. փոստ
              </FormLabel>
              <Col sm="8">
                <MultiEmail value={values.EMAIL} setFieldValue={setFieldValue} placeholder={'Էլ. փոստի հասցեներ'} />
              </Col>
            </FormGroup>

            <FormGroup as={Row} controlId="last_generate_date">
              <FormLabel column={true} sm="3">
                Թեմա
              </FormLabel>
              <Col sm="8">
                <Field name="EMAIL_SUBJECT" component={TextInput} asField="textarea" rows="3" />
              </Col>
            </FormGroup>
            <FormGroup as={Row} controlId="last_generate_date">
              <FormLabel column={true} sm="3">
                Հաղորդագրություն
              </FormLabel>
              <Col sm="8">
                <Field name="EMAIL_BODY" component={TextInput} asField="textarea" rows="6" />
              </Col>
            </FormGroup>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Report;
