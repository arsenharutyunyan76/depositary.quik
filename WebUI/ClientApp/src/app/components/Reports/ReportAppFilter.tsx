import * as AppTypes from 'AppTypes';
import { IReportApplicationGetParams } from 'app/Domain';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker from 'react-datepicker';
import React from 'react';
import moment from 'moment';
import { setReportApplicatonsParams } from 'app/store/reducers/reportApplications/actions';
import FormControl from 'react-bootstrap/FormControl';

const TableFilter: React.FC = () => {
  const dispatch = useDispatch();
  const params = useSelector<AppTypes.ReducerState, IReportApplicationGetParams>(
    ({ reportApplications: { params } }) => params
  );
  const setDates = (name: string) => (date: Date) => {
    dispatch(
      setReportApplicatonsParams({
        ...params,
        [name]: moment(date).format('YYYYMMDD')
      })
    );
  };
  const stringFromDate = moment(params.stringFromDate).toDate();
  const stringToDate = moment(params.stringToDate).toDate();

  return (
    <div className="actionLog-table-filter table-filter">
      <div className="date-range">
        <DatePicker
          className="start-date"
          showYearDropdown={true}
          selected={stringFromDate}
          onChange={setDates('stringFromDate')}
          selectsStart
          startDate={stringFromDate}
          endDate={stringToDate}
          customInput={<FormControl size="sm" name="stringFromDate" />}
        />
        <i className="amx-calendar"></i>
        <DatePicker
          showYearDropdown={true}
          className="end-date"
          selected={stringToDate}
          onChange={setDates('stringToDate')}
          selectsEnd
          startDate={stringFromDate}
          endDate={stringToDate}
          minDate={stringFromDate}
          customInput={<FormControl size="sm" name="stringToDate" />}
        />
      </div>
    </div>
  );
};

export default TableFilter;
