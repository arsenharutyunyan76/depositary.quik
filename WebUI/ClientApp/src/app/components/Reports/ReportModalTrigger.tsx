import * as AppTypes from 'AppTypes';

import { Button, OverlayTrigger, Spinner, Tooltip } from 'react-bootstrap';
import React, { useCallback } from 'react';
import { closeModal, openModal } from '../../store/reducers/settings/modal/actions';
import { useDispatch, useSelector } from 'react-redux';

import ReportAppForm from 'app/components/Reports/ReportAppForm';

export interface ReportModalTriggerProps {
  id?: number;
}

const SubmitAuctionBtn: React.FC = () => {
  const isLoading = useSelector<AppTypes.ReducerState, boolean>(
    ({ reports: { reportAppSaveLoading } }) => reportAppSaveLoading
  );
  return (
    <Button type="submit" form="reportAppForm" variant="primary" disabled={isLoading}>
      {isLoading && <Spinner as="span" animation="border" size="sm" role="status" className="mr-1" />}
      Ստեղծել
    </Button>
  );
};

const ReportModalTrigger: React.FC<ReportModalTriggerProps> = ({ id }) => {
  const dispatch = useDispatch();

  const handleRowClick = useCallback(
    e => {
      e.stopPropagation();
      const onSaveCallback = () => {
        dispatch(closeModal());
      };

      dispatch(
        openModal({
          children: <ReportAppForm id={id} onSaveCallback={onSaveCallback} />,
          title: `Ստեղծել հաշվետվության դիմում`,
          footer: <SubmitAuctionBtn />
        })
      );
    },
    [id, dispatch]
  );

  return (
    <>
      <OverlayTrigger placement="top" overlay={<Tooltip id="createReportApp">Ստեղծել Հաշվետվության դիմում</Tooltip>}>
        <Button size="sm" variant="link" className="table-action-btn" onClick={handleRowClick}>
          <i className="amx-plus-o text-success"></i>
        </Button>
      </OverlayTrigger>
    </>
  );
};

export default ReportModalTrigger;
