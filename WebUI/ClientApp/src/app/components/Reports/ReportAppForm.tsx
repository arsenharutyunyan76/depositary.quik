import React, { useMemo, useState, useEffect } from 'react';
import { ReducerState } from 'AppTypes';
import { Col, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import FormGroup from 'react-bootstrap/FormGroup';
import FormLabel from 'react-bootstrap/FormLabel';
import FormControl from 'react-bootstrap/FormControl';
import { useDispatch, useSelector } from 'react-redux';
import { Formik, Form, getIn } from 'formik';
import { IReportPostApplication } from 'app/Domain';

import { getSelectedDate, setDatePickerValue } from 'app/services/utils';

import { setReportApplication } from 'app/api/Reports';

import { IStoreReports } from 'app/store/reducers/reports/initialState';
import { getReports } from 'app/api/Reports';
import CustomSelect from 'app/components/Select/CustomSelect';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';

interface IErrors {
  REPORT_FROM_DATE?: string;
  REPORT_TO_DATE?: string;
}
const validation = (values: IReportPostApplication) => {
  const errors: IErrors = {};

  if (!values.REPORT_FROM_DATE) {
    errors.REPORT_FROM_DATE = 'Required';
  }

  if (!values.REPORT_TO_DATE) {
    errors.REPORT_TO_DATE = 'Required';
  }

  return errors;
};

interface IReportAppFormProps {
  id?: number;
  onSaveCallback: () => void;
}

const ReportAppForm: React.FC<IReportAppFormProps> = ({ id = 0, onSaveCallback }) => {
  const dispatch = useDispatch<ThunkDispatch<ReducerState, {}, Action<string>>>();

  const { items: reports, reportsLoading, reportsLoaded } = useSelector<ReducerState, IStoreReports>(
    ({ reports }) => reports
  );

  const reportsOptions = useMemo(() => {
    return reports.map(({ ID: value, NAME_AM: label }) => ({ value, label }));
  }, [reports]);

  const [initialValues] = useState<IReportPostApplication>({
    REPORT_ID: id,
    REPORT_FROM_DATE: '',
    REPORT_TO_DATE: ''
  });

  useEffect(() => {
    if (!reports.length && !reportsLoaded) {
      dispatch(getReports());
    }
  }, [dispatch, reports.length, reportsLoaded]);

  const onValidSubmit = async (values: IReportPostApplication) => {
    try {
      await dispatch(setReportApplication(values));
      onSaveCallback();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Formik initialValues={initialValues} enableReinitialize={true} onSubmit={onValidSubmit} validate={validation}>
      {({ handleSubmit, setFieldValue, values, touched, errors }) => (
        <Form className="report-application-form" noValidate={true} onSubmit={handleSubmit} id="reportAppForm">
          <FormGroup as={Row}>
            <FormLabel column sm="6">
              Հաշվետվություն
            </FormLabel>
            <Col sm="5">
              <CustomSelect
                name="REPORT_ID"
                menuListHeight={150}
                isLoading={reportsLoading}
                items={reportsOptions}
                setFieldValue={setFieldValue}
                selected={values.REPORT_ID || 0}
                isInvalid={getIn(touched, 'REPORT_ID') && !!getIn(errors, 'REPORT_ID')}
              />
            </Col>
          </FormGroup>

          <FormGroup as={Row} controlId="report_from_date">
            <FormLabel column={true} sm="6">
              Հաշվետվության սկիզբ
            </FormLabel>
            <Col sm="5">
              <DatePicker
                selected={getSelectedDate(values.REPORT_FROM_DATE)}
                onChange={setDatePickerValue(setFieldValue, 'REPORT_FROM_DATE', 'YYYY-MM-DD')}
                dateFormat="dd/MM/yyyy"
                name="REPORT_FROM_DATE"
                customInput={
                  <FormControl
                    isValid={touched.REPORT_FROM_DATE && !errors.REPORT_FROM_DATE}
                    isInvalid={touched.REPORT_FROM_DATE && !!errors.REPORT_FROM_DATE}
                  />
                }
              />
            </Col>
          </FormGroup>
          <FormGroup as={Row} controlId="report_to_date">
            <FormLabel column={true} sm="6">
              Հաշվետվության ավարտ
            </FormLabel>
            <Col sm="5">
              <DatePicker
                selected={getSelectedDate(values.REPORT_TO_DATE)}
                onChange={setDatePickerValue(setFieldValue, 'REPORT_TO_DATE', 'YYYY-MM-DD')}
                dateFormat="dd/MM/yyyy"
                name="REPORT_TO_DATE"
                minDate={getSelectedDate(values.REPORT_FROM_DATE)}
                customInput={
                  <FormControl
                    isValid={touched.REPORT_TO_DATE && !errors.REPORT_TO_DATE}
                    isInvalid={touched.REPORT_TO_DATE && !!errors.REPORT_TO_DATE}
                  />
                }
              />
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  );
};

export default ReportAppForm;
