import * as React from 'react';

import Authorization from './auth/Authorization';
import Layout from './layout/Layout';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import history from '../browserHistory';
import store from './store';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Authorization>
          <Layout />
        </Authorization>
      </Router>
    </Provider>
  );
};

export default App;
