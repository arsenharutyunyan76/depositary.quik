export const CURRENCY_TYPES = [
  { value: 1, label: 'ՀՀ արժույթ' },
  { value: 2, label: 'Արտարժույթ' },
  { value: 3, label: 'Արժեթուղթ' }
];
