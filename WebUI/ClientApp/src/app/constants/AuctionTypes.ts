export const AUCTIONTYPES = [
  {
    ID: 1,
    NAME_AM: 'Տեղաբաշխման աճուրդ',
    NAME_EN: 'Allocation auction'
  },
  {
    ID: 2,
    NAME_AM: 'Հետգնման աճուրդ',
    NAME_EN: 'Buyback auction'
  },
  {
    ID: 3,
    NAME_AM: 'Լրացուցիչ տեղաբաշխման աճուրդ',
    NAME_EN: 'Additional allocation auction'
  }
];
