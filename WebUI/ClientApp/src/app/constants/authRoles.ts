import * as Domain from '../Domain';

export const AUTH_ROLES: Domain.IAuthRoles = {
  superAdmin: ['superAdmin', 'admin', 'moderator'],
  admin: ['admin', 'moderator'],
  moderator: ['moderator']
};
