import React from 'react';
import { Redirect } from 'react-router-dom';
import { RouteConfig } from 'react-router-config';
import { pagesConfigs } from '../pages/pagesConfigs';

export const routes: RouteConfig[] = [
  ...pagesConfigs,
  {
    path: '/',
    exact: true,
    component: () => <Redirect to="/" />
  },
  {
    component: () => <Redirect to="/errors/404" />
  }
];
