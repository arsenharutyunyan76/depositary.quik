/* eslint-disable no-template-curly-in-string */
import { setLocale } from 'yup';

setLocale({
  mixed: {
    default: 'Պարտադիր լրացման դաշտ',
    required: 'Պարտադիր լրացման դաշտ',
    oneOf: 'Պարտադիր նշման դաշտ'
  },
  number: {
    min: 'Պարտադիր լրացման դաշտ'
  }
});
