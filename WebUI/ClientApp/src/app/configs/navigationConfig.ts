import * as Domain from '../Domain';

import { AUTH_ROLES } from '../constants/authRoles';

export const navigationConfig: Domain.INavigation[] = [
  {
    id: 'monitoring',
    title: 'Մոնիտորինգ',
    type: 'item',
    icon: 'amx-monitor',
    url: '/monitoring',
    auth: AUTH_ROLES.moderator
  },
  {
    id: 'informer',
    title: 'Տեղեկատուներ',
    type: 'collapse',
    icon: 'amx-file-system',
    url: '/informer',
    children: [
      {
        id: 'informer_auctions',
        title: 'Աճուրդներ',
        type: 'item',
        url: '/informer/auctions',
        icon: 'amx-auction'
      },
      {
        id: 'informer_auction_announcement',
        title: 'Աճուրդի հայտարարություն',
        type: 'item',
        url: '/informer/auction-announcement',
        icon: 'amx-announcement'
      },
      {
        id: 'informer_currencies',
        title: 'Արժույթներ',
        type: 'item',
        url: '/informer/currencies',
        icon: 'amx-announcement'
      },
      {
        id: 'informer_bank',
        title: 'Բանկեր',
        type: 'item',
        url: '/informer/bank',
        icon: 'amx-bank'
      },
      {
        id: 'informer_bankusers',
        title: 'Բանկի օգտատերեր',
        type: 'item',
        url: '/informer/bankusers',
        icon: 'amx-bank-users'
      },
      {
        id: 'informer_isin',
        title: 'ISIN',
        type: 'item',
        url: '/informer/isin',
        icon: 'amx-staff-estate'
      },
      {
        id: 'informer_stockAccount',
        title: 'Բորսայի հաշիվներ',
        type: 'item',
        url: '/informer/stock-account',
        icon: 'amx-staff-estate'
      }
    ]
  },
  {
    id: 'reports',
    title: 'Հաշվետվություններ ',
    type: 'collapse',
    icon: 'amx-reporting',
    children: [
      {
        id: 'generate_reports',
        title: 'Ժամանակացույց',
        type: 'item',
        url: '/reports/timetable',
        icon: 'amx-timetable'
      },
      {
        id: 'report_announcement',
        title: 'Հայտեր',
        type: 'item',
        url: '/reports/applications',
        icon: 'amx-announcement'
      }
    ]
  }
];
