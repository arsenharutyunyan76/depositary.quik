// This is an example, to be removed
export * from './data/IPerson';
export * from './data/IAuction';
export * from './data/IAuth';
export * from './data/INavigation';
export * from './data/IModal';
export * from './data/IReport';
export * from './data/IBank';
export * from './data/IBankAccounts';
export * from './data/IBankUser';
export * from './data/ISelect';
export * from './data/IActionLog';
export * from './data/IChart';
export * from './data/IAnnouncement';
export * from './data/IAccount';
export * from './data/IFormControl';
export * from './data/IRecentFailedAction';
export * from './data/IIsin';
export * from './data/ITableProps';
export * from './data/IStockAccount';
export * from './data/ICurrencies';
