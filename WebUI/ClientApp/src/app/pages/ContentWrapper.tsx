import React from 'react';

export interface ContentWrapperProps {
  title?: string;
}

const ContentWrapper: React.FC<ContentWrapperProps> = ({ title, children }) => {
  return (
    <div className="page-content-wrapper">
      <div className="page-content-heading">{title && <h2 className="page-title">{title}</h2>}</div>
      <div className="page-content-container">{children}</div>
    </div>
  );
};

export default ContentWrapper;
