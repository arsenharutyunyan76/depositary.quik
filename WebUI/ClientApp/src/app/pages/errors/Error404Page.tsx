import { Link } from 'react-router-dom';
import React from 'react';

const Error404Page: React.FC = () => {
  return (
    <div className="page-wrapper">
      <div className="page error-404-page">
        <h1 className="text-center mb-5 mt-5 error-404">404</h1>
        <h3 className="text-center mb-5">Նման էջ գոյություն չունի։</h3>
        <div className="text-center">
          <Link className="btn btn-primary btn-rounded" to="/">
            Վերադառնալ
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Error404Page;
