import { ReducerState } from 'AppTypes';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, IActionLog } from 'app/Domain';
import { IStoreActionLog } from 'app/store/reducers/actionLog/initialState';
import useTableColumns from 'app/hooks/useTableColumns';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import FormattedDateCell from 'app/components/table/FormattedDateCell';
import BooleanValueCell from 'app/components/table/BooleanValueCell';
import ActionLogTableFilter from 'app/components/ActionLogs/ActionLogTableFilter';
import RecentFailedAction from 'app/components/RecentFailedAction';
import useDebounce from 'app/hooks/useDebounce';
import { getActionLog } from 'app/api/ActionLog';
import { resetAuctionLog } from 'app/store/reducers/actionLog/actions';

const columnsConfig: ITableColumnConfig<IActionLog>[] = [
  {
    Header: 'Ամսաթիվ',
    accessor: 'CREATION_DATE',
    CustomCellComponent: FormattedDateCell,
    customOptions: { format: 'DD/MM/YYYY HH:mm:ss' }
  },
  { Header: 'Գործողության անուն', accessor: 'ACTION_TYPE_NAME_AM' },
  { Header: 'Նկարագրություն', accessor: 'DETAILS' },
  { Header: 'Ձախողված', accessor: 'IS_FAIL', width: 100, CustomCellComponent: BooleanValueCell }
];

const Monitoring: React.FC = () => {
  const dispatch = useDispatch();
  const { columns } = useTableColumns<IActionLog>({ columnsConfig });
  const { items: data, actionLogLoading: isLoading, params } = useSelector<ReducerState, IStoreActionLog>(
    ({ actionLog }) => actionLog
  );
  const debounceParams = useDebounce<typeof params>(params, 500);

  useEffect(() => {
    dispatch(getActionLog(debounceParams));
  }, [debounceParams]);

  useEffect(() => {
    return () => {
      dispatch(resetAuctionLog());
    };
  }, []);

  return (
    <ContentWrapper title="Մոնիտորինգ">
      <div className="monitoring-page">
        <RecentFailedAction />
        <Portlet title="Աճուրդներ" collapsable={true} actions={<ActionLogTableFilter />}>
          <EnhancedTable
            loading={isLoading}
            title="Աճուրդներ"
            paginate={true}
            data={data}
            columns={columns}
            initialState={{ pageSize: 8 }}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Monitoring;
