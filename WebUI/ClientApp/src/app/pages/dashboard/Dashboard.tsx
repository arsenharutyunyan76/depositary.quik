import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

import { AuctionTypes } from 'app/components/Charts';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';

const Dashboard: React.FC = () => {
  return (
    <ContentWrapper title="Dashboard">
      <Container className="dashboard-page">
        <Row>
          <Col>
            <Portlet title="Աճուրդներ ըստ խմբերի" collapsable={true}>
              <AuctionTypes />
            </Portlet>
          </Col>
        </Row>
      </Container>
    </ContentWrapper>
  );
};

export default Dashboard;
