import { AUTH_ROLES } from '../constants/authRoles';
import React from 'react';

export const pagesConfigs = [
  {
    path: '/',
    exact: true,
    component: React.lazy(() => import('./dashboard/Dashboard'))
  },
  {
    path: '/profile',
    exact: true,
    component: React.lazy(() => import('./profile/Profile'))
  },
  {
    path: '/monitoring',
    exact: true,
    component: React.lazy(() => import('./monitoring/Monitoring')),
    auth: AUTH_ROLES.moderator
  },
  {
    path: ['/informer/auctions'],
    exact: true,
    component: React.lazy(() => import('./informer/Auctions'))
  },
  {
    path: '/informer/auction-announcement',
    exact: true,
    component: React.lazy(() => import('./informer/Announcement'))
  },
  {
    path: '/informer/currencies',
    exact: true,
    component: React.lazy(() => import('./informer/Currencies'))
  },
  {
    path: '/informer/bank',
    exact: true,
    component: React.lazy(() => import('./informer/Bank'))
  },
  {
    path: '/informer/bankusers',
    exact: true,
    component: React.lazy(() => import('./informer/BankUsers'))
  },

  {
    path: '/informer/isin',
    exact: true,
    component: React.lazy(() => import('./informer/Isin'))
  },
  {
    path: '/informer/stock-account',
    exact: true,
    component: React.lazy(() => import('./informer/StockAccount'))
  },
  {
    path: '/reports/timetable',
    exact: true,
    component: React.lazy(() => import('./reports/TimeTable'))
  },
  {
    path: '/reports/applications',
    exact: true,
    component: React.lazy(() => import('./reports/Applications'))
  },
  {
    path: '/errors/404',
    exact: true,
    component: React.lazy(() => import('./errors/Error404Page'))
  }
];

export interface Isettings {
  header: boolean;
}
