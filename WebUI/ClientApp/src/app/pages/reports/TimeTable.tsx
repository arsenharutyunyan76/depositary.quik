import * as AppTypes from 'AppTypes';
import React, { useMemo, useCallback, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { IReport, ITableColumnConfig, PureCellProps, RowProps } from 'app/Domain';
import useTableColumns from 'app/hooks/useTableColumns';
import ContentWrapper from '../ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import FormattedDateCell from 'app/components/table/FormattedDateCell';
import { openModal, closeModal } from 'app/store/reducers/settings/modal/actions';
import { IStoreReports } from 'app/store/reducers/reports/initialState';
import { getReports } from 'app/api/Reports';
import { resetReports } from 'app/store/reducers/reports/actions';
import ReportForm from 'app/components/Reports/ReportForm';
import ReportModalTrigger from 'app/components/Reports/ReportModalTrigger';

const columnsConfig: ITableColumnConfig<IReport>[] = [
  {
    Header: 'Անուն',
    accessor: 'NAME_AM',
    width: 130
  },
  {
    Header: 'Պլանավորված ժամանակը',
    accessor: 'GENERATION_TIME',
    width: 100
  },
  {
    Header: 'Վերջին արտահանման ամսաթիվ',
    accessor: 'LAST_GENERATE_DATE',
    CustomCellComponent: FormattedDateCell,
    width: 100,
    customOptions: {
      format: 'DD/MM/YYYY HH:mm:ss'
    }
  },
  {
    Header: '',
    accessor: 'ID',
    width: 50
  }
];

const Reports: React.FC = () => {
  const dispatch = useDispatch();
  const { columns } = useTableColumns<IReport>({ columnsConfig });
  const { items: data, reportsLoading: isLoading } = useSelector<AppTypes.ReducerState, IStoreReports>(
    ({ reports }) => reports
  );

  const tableColumns = useMemo<typeof columns>(() => {
    return columns.map(column => {
      switch (column.accessor) {
        case 'ID':
          return {
            ...column,
            Cell: ({ row: { original } }: PureCellProps<IReport, any>) => {
              return <ReportModalTrigger id={original.ID} />;
            }
          };
        default:
          return column;
      }
    });
  }, [columns]);

  const handleRowClick = useCallback(
    (row: RowProps<IReport>) => {
      dispatch(
        openModal({
          children: <ReportForm data={row.original} onSaveCallback={() => dispatch(closeModal())} />,
          title: `Աճուրդ`,
          footer: (
            <Button type="submit" form="reportForm" variant="primary">
              Պահպանել
            </Button>
          ),
          modalProps: {
            size: 'xl'
          }
        })
      );
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(getReports());
    return () => {
      dispatch(resetReports());
    };
  }, [dispatch]);

  return (
    <ContentWrapper title="Հաշվետվություն">
      <div className="reports-page">
        <Portlet title="Հաշվետվությունների ժամանակացույց" collapsable={true}>
          <EnhancedTable
            loading={isLoading}
            title="Հաշվետվություններ"
            paginate={true}
            data={data}
            columns={tableColumns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleRowClick(row)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Reports;
