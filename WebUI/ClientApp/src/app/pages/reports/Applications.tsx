import * as AppTypes from 'AppTypes';
import React, { useMemo, useCallback, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, IReport, IReportApplication, PureCellProps } from 'app/Domain';
import useTableColumns from 'app/hooks/useTableColumns';
import useDebounce from 'app/hooks/useDebounce';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import FormattedDateCell from 'app/components/table/FormattedDateCell';
import { IStoreReportApplication } from 'app/store/reducers/reportApplications/initialState';
import { resetReportApplicatons } from 'app/store/reducers/reportApplications/actions';
import { openConfirmModal, closeConfirmModal } from 'app/store/reducers/settings/modal/actions';

import { getReportApplications, getReports, deleteReportApplication } from 'app/api/Reports';
import ReportAppFilter from 'app/components/Reports/ReportAppFilter';
import ReportModalTrigger from 'app/components/Reports/ReportModalTrigger';

const format = `DD/MM/YYYY HH:mm:ss`;

const columnsConfig: ITableColumnConfig<IReportApplication>[] = [
  { Header: 'Հաշվետվություն', accessor: 'REPORT_NAME_AM', width: 100 },
  {
    Header: 'Ստեղծման ամսաթիվ',
    accessor: 'GENERATE_DATE',
    CustomCellComponent: FormattedDateCell,
    customOptions: { format }
  },
  {
    Header: 'Հաշվետվության սկիզբ',
    accessor: 'REPORT_FROM_DATE',
    CustomCellComponent: FormattedDateCell,
    customOptions: { format }
  },
  {
    Header: 'Հաշվետվության ավարտ',
    accessor: 'REPORT_TO_DATE',
    CustomCellComponent: FormattedDateCell,
    customOptions: { format }
  },
  { Header: '', accessor: 'REPORT_ID', width: 50 }
];

const Announcements: React.FC = () => {
  const dispatch = useDispatch();

  const { columns } = useTableColumns<IReportApplication>({ columnsConfig });
  const reports = useSelector<AppTypes.ReducerState, IReport[]>(({ reports: { items } }) => items);
  const { items: applications, params, isLoading } = useSelector<AppTypes.ReducerState, IStoreReportApplication>(
    ({ reportApplications }) => reportApplications
  );
  const debounceParams = useDebounce<typeof params>(params, 500);

  const data = useMemo<IReportApplication[]>(() => {
    return applications.map(item => ({
      ...item,
      REPORT_NAME_AM: reports.find(r => r.ID === item.REPORT_ID)?.NAME_AM
    }));
  }, [applications, reports]);

  const tableColumns = useMemo<typeof columns>(() => {
    return columns.map(column => {
      switch (column.accessor) {
        case 'REPORT_ID':
          return {
            ...column,
            Cell: ({ row: { original } }: PureCellProps<IReportApplication, any>) => {
              if (original.GENERATE_DATE) return '';
              return (
                <Button
                  size="sm"
                  variant="link"
                  className="table-action-btn"
                  onClick={handleDeleteRow(original.ID, original.REPORT_NAME_AM)}
                >
                  <i className="amx-times"></i>
                </Button>
              );
            }
          };
        default:
          return column;
      }
    });
  }, [columns]);

  const handleDeleteRow = useCallback(
    (id: string, name: string | undefined = '') => (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
      e.stopPropagation();
      dispatch(
        openConfirmModal({
          title: `${name}`,
          message: `Դուք համոզված ե՞ք, որ ցանկանում եք ջնջել տվյալ Հայտարարությունը `,
          confirmAction: () => {
            dispatch(deleteReportApplication(id));
            dispatch(closeConfirmModal());
          }
        })
      );
    },
    []
  );

  useEffect(() => {
    dispatch(getReportApplications(debounceParams));
  }, [debounceParams]);

  useEffect(() => {
    dispatch(getReports());
    return () => {
      dispatch(resetReportApplicatons());
    };
  }, [dispatch]);

  return (
    <ContentWrapper title="Հաշվետվություն">
      <div className="reports-page">
        <Portlet
          title="Հաշվետվությունների հայտեր"
          collapsable={true}
          actions={
            <>
              <ReportAppFilter />
              <ReportModalTrigger />
            </>
          }
        >
          <EnhancedTable
            loading={isLoading}
            title="Հաշվետվություններ"
            paginate={true}
            data={data}
            columns={tableColumns}
            initialState={{ pageSize: 8 }}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Announcements;
