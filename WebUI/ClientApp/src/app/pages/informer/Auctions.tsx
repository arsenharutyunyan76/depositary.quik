import * as AppTypes from 'AppTypes';
import React, { useMemo, useCallback, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, RowProps, IAuction, PureCellProps } from 'app/Domain';
import { IStoreAuctions } from 'app/store/reducers/auctions/initialState';
import useTableColumns from 'app/hooks/useTableColumns';
import useDebounce from 'app/hooks/useDebounce';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import FormattedDateCell from 'app/components/table/FormattedDateCell';
import { closeModal, openModal, openConfirmModal, closeConfirmModal } from 'app/store/reducers/settings/modal/actions';

import Auction from 'app/components/Auctions/Auction';
import AuctionTableFilter from 'app/components/Auctions/Filter';
import { getAuctions, deleteAuction } from 'app/api/Auctions';

const columnsConfig: ITableColumnConfig<IAuction>[] = [
  { Header: 'Աճուրդի նույնացուցիչ', accessor: 'AUCTION_ID' },
  { Header: 'Հայտարարության ամսաթիվ', accessor: 'CREATION_DATE', CustomCellComponent: FormattedDateCell },
  { Header: 'Աճուրդի ամսաթիվ', accessor: 'AUCTION_DATE', CustomCellComponent: FormattedDateCell },
  { Header: 'Վերջնահաշվարկի ամսաթիվը', accessor: 'SETTLEMENT_DATE', CustomCellComponent: FormattedDateCell },
  { Header: 'Աճուրդի տեսակ', accessor: 'AUCTION_TYPE_NAME_AM' },
  { Header: '', accessor: 'ID', width: 50 }
];

const Auctions: React.FC = () => {
  const dispatch = useDispatch();
  const { items: data, params, auctionsLoading: isLoading } = useSelector<AppTypes.ReducerState, IStoreAuctions>(
    ({ auctions }) => auctions
  );
  const debounceParams = useDebounce<typeof params>(params, 500);
  const { columns } = useTableColumns<IAuction>({ columnsConfig });

  const tableColumns = useMemo<typeof columns>(() => {
    return columns.map(column => {
      switch (column.accessor) {
        case 'ID':
          return {
            ...column,
            Cell: ({ row: { original } }: PureCellProps<IAuction, any>) => (
              <Button
                size="sm"
                variant="link"
                className="table-action-btn"
                onClick={handleDeleteRow(original.AUCTION_ID, original.ID)}
              >
                <i className="amx-times"></i>
              </Button>
            )
          };
        default:
          return column;
      }
    });
  }, [columns]);

  const handleRowClick = useCallback(
    (row: RowProps<IAuction>) => {
      dispatch(
        openModal({
          children: <Auction data={row.original} onSaveCallback={() => dispatch(closeModal())} />,
          title: `Աճուրդ`,
          footer: (
            <Button type="submit" form="auctionForm" variant="primary">
              Պահպանել
            </Button>
          ),
          modalProps: {
            size: 'xl'
          }
        })
      );
    },
    [data, dispatch]
  );

  const handleDeleteRow = useCallback(
    (auctionId: string, id: number) => (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
      e.stopPropagation();
      dispatch(
        openConfirmModal({
          title: `Աճուրդ ${auctionId}`,
          message: `Դուք համոզված ե՞ք, որ ցանկանում եք ջնջել տվյալ աճուրդը՞ `,
          confirmAction: () => {
            dispatch(deleteAuction(id));
            dispatch(closeConfirmModal());
          }
        })
      );
    },
    []
  );

  useEffect(() => {
    dispatch(getAuctions(debounceParams));
  }, [debounceParams]);

  return (
    <ContentWrapper title="Աճուրդներ">
      <div className="informer-auctions-page">
        <Portlet title="Աճուրդներ" collapsable={true} actions={<AuctionTableFilter />}>
          <EnhancedTable
            loading={isLoading}
            title="Աճուրդներ"
            paginate={true}
            data={data}
            columns={tableColumns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleRowClick(row)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Auctions;
