import { ReducerState } from 'AppTypes';
import React, { useMemo, useCallback, useEffect } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, IBank, IBankUser, PureCellProps } from 'app/Domain';
import { IStoreBankUsers } from 'app/store/reducers/bankUsers/initialState';
import useTableColumns from 'app/hooks/useTableColumns';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import { closeModal, openModal } from 'app/store/reducers/settings/modal/actions';

import { getBankUsers } from 'app/api/BankUsers';
import { getBanks } from 'app/api/Bank';
import { resetBankUsers } from 'app/store/reducers/bankUsers/actions';
import BankUserForm from 'app/components/Banks/BankUserForm';

const columnsConfig: ITableColumnConfig<IBankUser>[] = [
  {
    Header: 'Բանկ',
    accessor: 'BANK_CODE',
    width: 130
  },
  {
    Header: 'Օգտատերի համար',
    accessor: 'USER_ID',
    width: 100
  },
  {
    Header: 'Օգտատերի կոդ',
    accessor: 'USER_CODE',
    width: 100
  }
];

const BankUsers: React.FC = () => {
  const dispatch = useDispatch();
  const banks = useSelector<ReducerState, IBank[]>(({ banks }) => banks.items);
  const { items: data, isLoading } = useSelector<ReducerState, IStoreBankUsers>(({ bankUsers }) => bankUsers);
  const { columns } = useTableColumns<IBankUser>({ columnsConfig });

  const tableColumns = useMemo(() => {
    return columns.map(column => {
      switch (column.accessor) {
        case 'BANK_CODE':
          return {
            ...column,
            Cell: ({ value }: PureCellProps<IBankUser, any>) => (
              <span>{banks.find(({ CODE }) => value === CODE)?.NAME_AM}</span>
            )
          };
        default:
          return column;
      }
    });
  }, [columns, banks]);

  const handleClick = useCallback(
    (data: IBankUser) => {
      dispatch(
        openModal({
          children: <BankUserForm data={data} onSaveCallback={() => dispatch(closeModal())} />,
          title: `Ավելացնել նոր օգտատեր`,
          footer: (
            <Button type="submit" form="bankUserForm">
              Ավելացնել
            </Button>
          )
        })
      );
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(getBankUsers());
    dispatch(getBanks());
    return () => {
      dispatch(resetBankUsers());
    };
  }, [dispatch]);

  return (
    <ContentWrapper title="Բանկի օգտատերեր">
      <div className="informer-bankusers-page">
        <Portlet
          title="Օգտատերեր"
          collapsable={true}
          actions={
            <OverlayTrigger placement="top" overlay={<Tooltip id="createBankUser">Ստեղծել Նոր օգտատեր</Tooltip>}>
              <Button
                size="sm"
                variant="link"
                onClick={() => handleClick({ BANK_CODE: '', USER_ID: null, USER_CODE: '' })}
              >
                <i className="amx-plus-o"></i>
              </Button>
            </OverlayTrigger>
          }
        >
          <EnhancedTable
            loading={isLoading}
            title="Աճուրդներ"
            paginate={true}
            data={data}
            columns={tableColumns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleClick(row.original)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default BankUsers;
