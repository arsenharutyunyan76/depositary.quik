import { ReducerState } from 'AppTypes';
import React, { useState, useCallback, useEffect } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, ICurrency } from 'app/Domain';
import { IStoreCurrencies } from 'app/store/reducers/currencies/initialState';
import useTableColumns from 'app/hooks/useTableColumns';
import CurrencyForm from 'app/components/CurrencyForm';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import { closeModal, openModal } from 'app/store/reducers/settings/modal/actions';
import { getCurrencies } from 'app/api/Currencies';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import { CURRENCY_TYPES } from 'app/constants/CurrencyTypes';

const columnsConfig: ITableColumnConfig<ICurrency>[] = [
  {
    Header: 'Կոդ',
    accessor: 'CODE',
    width: 100
  },
  {
    Header: 'Փոխարժեքի տեսակ',
    accessor: 'CURRENCY_TYPE',
    width: 130,
    CustomCellComponent: ({ value }) => <>{CURRENCY_TYPES.find(i => i.value === value)?.label}</>
  }
];

const Currencies: React.FC = () => {
  const dispatch = useDispatch<ThunkDispatch<ReducerState, {}, Action<string>>>();
  const [isFetchLoading, setIsFetchLoading] = useState(false);
  const { items: data } = useSelector<ReducerState, IStoreCurrencies>(({ currencies }) => currencies);
  const { columns } = useTableColumns<ICurrency>({ columnsConfig });

  useEffect(() => {
    setIsFetchLoading(true);
    dispatch(getCurrencies()).finally(() => setIsFetchLoading(false));
  }, [dispatch]);

  const handleClick = useCallback(
    (currency: ICurrency) => {
      dispatch(
        openModal({
          children: (
            <CurrencyForm
              data={currency}
              onSaveCallback={() => {
                dispatch(closeModal());
                dispatch(getCurrencies());
              }}
            />
          ),
          title: !currency.CODE ? `Ավելացնել նոր Արժույթ` : 'Փոփոխել արժույթը',
          footer: (
            <Button type="submit" form="currencyForm">
              {!currency.CODE ? `Ավելացնել` : `Փոփոխել`}
            </Button>
          )
        })
      );
    },
    [dispatch]
  );

  return (
    <ContentWrapper title="Արժույթներ">
      <div className="informer-bankusers-page">
        <Portlet
          title="Արժույթներ"
          collapsable={true}
          actions={
            <OverlayTrigger placement="top" overlay={<Tooltip id="createBankUser">Ստեղծել Նոր Արժույթ</Tooltip>}>
              <Button size="sm" variant="link" onClick={() => handleClick({ CURRENCY_TYPE: '', CODE: '' })}>
                <i className="amx-plus-o"></i>
              </Button>
            </OverlayTrigger>
          }
        >
          <EnhancedTable
            loading={isFetchLoading}
            title="Արժույթներ"
            paginate={true}
            data={data}
            columns={columns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleClick(row.original)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Currencies;
