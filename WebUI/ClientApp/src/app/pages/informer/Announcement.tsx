import ContentWrapper from '../ContentWrapper';
import Portlet from '../../components/Portlet';
import React, { useEffect } from 'react';
import AnnouncementForm from '../../components/Announcement/AnnouncementForm';
import { useSelector, useDispatch } from 'react-redux';
import * as Domain from '../../Domain';
import * as AppTypes from 'AppTypes';
import { getAnnouncement, saveAnnouncement } from '../../api/Announcement';

const Announcement: React.FC = () => {
  const dispatch = useDispatch();
  const announcement = useSelector<AppTypes.ReducerState, Domain.IAnnouncement>(({ announcement: { data } }) => data);
  const isSaving = useSelector<AppTypes.ReducerState, boolean>(({ announcement: { isSaving } }) => isSaving);

  const handleSubmit = (values: Domain.IAnnouncement) => {
    dispatch(saveAnnouncement(values));
  };

  useEffect(() => {
    dispatch(getAnnouncement());
  }, []);

  return (
    <ContentWrapper title="Աճուրդի հայտարարություն">
      <div className="informer-auctions-page">
        <Portlet title="Հայտարարություն" collapsable={false}>
          <AnnouncementForm onSaveRequest={handleSubmit} initialValues={announcement} isLoading={isSaving} />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Announcement;
