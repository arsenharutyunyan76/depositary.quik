import { ReducerState } from 'AppTypes';
import React, { useState, useMemo, useCallback, useEffect } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, IStockAccount, ISelectOptions, ICurrency, PureCellProps } from 'app/Domain';
import { IStoreStockAccounts } from 'app/store/reducers/stockAccounts/initialState';
import useTableColumns from 'app/hooks/useTableColumns';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import { closeModal, openModal } from 'app/store/reducers/settings/modal/actions';
import { getStockAccount } from 'app/api/StockAccount';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'typesafe-actions';
import StockAccountForm from 'app/components/stockAccount/StockAccountForm';
import { getCurrencies } from 'app/api/Currencies';
import { CURRENCY_TYPES } from 'app/constants/CurrencyTypes';

const columnsConfig: ITableColumnConfig<IStockAccount>[] = [
  {
    Header: 'Հաշիվ',
    accessor: 'ACCOUNT',
    width: 100
  },
  {
    Header: 'Արժույթ',
    accessor: 'CURRENCY',
    width: 130
  }
];

const StockAccount: React.FC = () => {
  const dispatch = useDispatch<ThunkDispatch<ReducerState, {}, Action<string>>>();
  const [fetchLoading, setFetchLoading] = useState(false);

  const { items: data } = useSelector<ReducerState, IStoreStockAccounts>(({ stockAccounts }) => stockAccounts);
  const currencies = useSelector<ReducerState, ICurrency[]>(({ currencies }) => currencies.items);

  const { columns } = useTableColumns<IStockAccount>({ columnsConfig });

  const savedCurrencies = useMemo(() => data.map(({ CURRENCY }) => CURRENCY.trim()), [data]);

  const currenciesOptions = useMemo<ISelectOptions[]>(() => {
    return currencies.reduce((acc: ISelectOptions[], { CODE }) => {
      if (savedCurrencies.includes(CODE)) return [...acc];
      return [...acc, { value: CODE, label: CODE }];
    }, []);
  }, [currencies, savedCurrencies]);

  const tableColumns = useMemo(() => {
    return columns.map(col => {
      switch (col.accessor) {
        case 'CURRENCY':
          return {
            ...col,
            Cell: ({ value }: PureCellProps<IStockAccount, any>) => {
              value = value.trim();
              const typeCode = currencies.find(currency => currency.CODE === value)?.CURRENCY_TYPE;
              const type = CURRENCY_TYPES.find(t => t.value === Number(typeCode))?.label;
              return `${value} / ${type}`;
            }
          };
        default:
          return col;
      }
    });
  }, [columns, currencies, CURRENCY_TYPES]);

  const handleClick = useCallback(
    (data: IStockAccount) => {
      dispatch(
        openModal({
          children: (
            <StockAccountForm
              data={data}
              onSaveCallback={() => {
                dispatch(closeModal());
                dispatch(getStockAccount());
              }}
              currencies={currenciesOptions}
            />
          ),
          title: !data.CURRENCY ? 'Ավելացնել նոր հաշիվ' : 'Փոփոխել հաշիվը',
          footer: (
            <Button type="submit" form="stockAccountForm">
              {!data.CURRENCY ? `Ավելացնել` : `Պահպանել`}
            </Button>
          )
        })
      );
    },
    [dispatch, currenciesOptions]
  );

  useEffect(() => {
    setFetchLoading(true);
    dispatch(getStockAccount()).finally(() => {
      setFetchLoading(false);
    });
    dispatch(getCurrencies());
  }, [dispatch]);

  return (
    <ContentWrapper title="Բորսայի հաշիվներ">
      <div className="informer-inner-page">
        <Portlet
          title="Բորսայի հաշիվներ"
          collapsable={true}
          actions={
            <OverlayTrigger placement="top" overlay={<Tooltip id="createBankUser">Ստեղծել նոր հաշիվ</Tooltip>}>
              <Button size="sm" variant="link" onClick={() => handleClick({ CURRENCY: '', ACCOUNT: '' })}>
                <i className="amx-plus-o"></i>
              </Button>
            </OverlayTrigger>
          }
        >
          <EnhancedTable
            loading={fetchLoading}
            title="հաշիվներ"
            paginate={true}
            data={data}
            columns={tableColumns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleClick(row.original)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default StockAccount;
