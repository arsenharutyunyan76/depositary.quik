import { ReducerState } from 'AppTypes';
import React, { useCallback, useEffect } from 'react';
import { ITableColumnConfig, IBank } from 'app/Domain';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import ContentWrapper from 'app/pages/ContentWrapper';
import useTableColumns from 'app/hooks/useTableColumns';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import BooleanValueCell from 'app/components/table/BooleanValueCell';
import BankForm from 'app/components/Banks/BankForm';
import { getBanks } from 'app/api/Bank';
import { getCurrencies } from 'app/api/Currencies';
import { resetBanks } from 'app/store/reducers/banks/actions';
import { IStoreBanks } from 'app/store/reducers/banks/initialState';
import { openModal, closeModal } from 'app/store/reducers/settings/modal/actions';

const columnsConfig: ITableColumnConfig<IBank>[] = [
  { Header: 'Կոդ', accessor: 'CODE' },
  { Header: 'Կարճ անվանում', accessor: 'SHORT_NAME' },
  { Header: 'Հայերեն անվանում', accessor: 'NAME_AM' },
  { Header: 'Անգլերեն անվանում', accessor: 'NAME_EN' },
  { Header: 'Հաշվապահական ծրագրում կոդ', accessor: 'AS_CODE' },
  { Header: 'Աճուրդի մասնակից է', accessor: 'IS_ACTIVE', width: 50, CustomCellComponent: BooleanValueCell }
];

const Banks: React.FC = () => {
  const dispatch = useDispatch();
  const { items: data, isLoading } = useSelector<ReducerState, IStoreBanks>(({ banks }) => banks);
  const { columns } = useTableColumns<IBank>({ columnsConfig });

  const handleClick = useCallback(
    (code?: string) => {
      dispatch(
        openModal({
          children: <BankForm bankCode={code} onSaveCallback={() => dispatch(closeModal())} />,
          title: code ? `Փոփոխել բանկի տվյալները` : `Ավելացնել նոր բանկ`,
          modalProps: {
            size: 'lg'
          },
          footer: (
            <Button type="submit" form="bankUserForm">
              {code ? `Փոփոխել` : `Ավելացնել`}
            </Button>
          )
        })
      );
    },
    [data, dispatch]
  );

  useEffect(() => {
    dispatch(getBanks());
    dispatch(getCurrencies());
    return () => {
      dispatch(resetBanks());
    };
  }, [dispatch]);

  return (
    <ContentWrapper title="Բանկեր">
      <div className="informer-bankusers-page">
        <Portlet
          title="Բանկեր"
          collapsable={true}
          actions={
            <OverlayTrigger placement="top" overlay={<Tooltip id="createBank">Ստեղծել Նոր բանկ</Tooltip>}>
              <Button size="sm" variant="link" onClick={() => handleClick()}>
                <i className="amx-plus-o"></i>
              </Button>
            </OverlayTrigger>
          }
        >
          <EnhancedTable
            loading={isLoading}
            title="Բանկեր"
            paginate={true}
            data={data}
            columns={columns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleClick(row.original.CODE)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Banks;
