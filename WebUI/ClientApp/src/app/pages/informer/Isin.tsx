import { ReducerState } from 'AppTypes';
import React, { useCallback, useEffect } from 'react';
import { Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { ITableColumnConfig, IIsin } from 'app/Domain';
import { IStoreIsin } from 'app/store/reducers/isin/initialState';
import useTableColumns from 'app/hooks/useTableColumns';
import IsinForm from 'app/components/Isin/IsinForm';
import ContentWrapper from 'app/pages/ContentWrapper';
import Portlet from 'app/components/Portlet';
import EnhancedTable from 'app/components/table/EnhancedTable';
import { closeModal, openModal } from 'app/store/reducers/settings/modal/actions';
import { getIsin } from 'app/api/Isin';
import { resetIsin } from 'app/store/reducers/isin/actions';

const columnsConfig: ITableColumnConfig<IIsin>[] = [
  {
    Header: 'ISIN կոդ',
    accessor: 'ISIN_CODE',
    width: 100
  },
  {
    Header: 'RTS կոդ',
    accessor: 'RTS_CODE',
    width: 130
  }
];

const Isin: React.FC = () => {
  const dispatch = useDispatch();
  const { items: data, isLoading } = useSelector<ReducerState, IStoreIsin>(({ isin }) => isin);
  const { columns } = useTableColumns<IIsin>({ columnsConfig });

  useEffect(() => {
    dispatch(getIsin());
    return () => {
      dispatch(resetIsin());
    };
  }, [dispatch]);

  const handleClick = useCallback(
    (data: IIsin) => {
      dispatch(
        openModal({
          children: <IsinForm data={data} onSaveCallback={() => dispatch(closeModal())} />,
          title: `Ավելացնել նոր ISIN`,
          footer: (
            <Button type="submit" form="isinForm">
              Ավելացնել
            </Button>
          )
        })
      );
    },
    [dispatch]
  );

  return (
    <ContentWrapper title="ISIN">
      <div className="informer-bankusers-page">
        <Portlet
          title="ISIN"
          collapsable={true}
          actions={
            <OverlayTrigger placement="top" overlay={<Tooltip id="createBankUser">Ստեղծել Նոր ISIN կոդ</Tooltip>}>
              <Button size="sm" variant="link" onClick={() => handleClick({ RTS_CODE: '', ISIN_CODE: '' })}>
                <i className="amx-plus-o"></i>
              </Button>
            </OverlayTrigger>
          }
        >
          <EnhancedTable
            loading={isLoading}
            title="Աճուրդներ"
            paginate={true}
            data={data}
            columns={columns}
            initialState={{ pageSize: 8 }}
            getRowProps={row => ({
              onClick: () => handleClick(row.original)
            })}
          />
        </Portlet>
      </div>
    </ContentWrapper>
  );
};

export default Isin;
