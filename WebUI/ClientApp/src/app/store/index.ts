import { applyMiddleware, compose, createStore } from 'redux';

import rootReducer from './reducers/root-reducers';
import thunk from 'redux-thunk';

const composeEnhancers =
  (process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

// compose enhancers
const enhancer = composeEnhancers(applyMiddleware(thunk));
const initialState = {};
const store = createStore(rootReducer, initialState, enhancer);

export default store;
