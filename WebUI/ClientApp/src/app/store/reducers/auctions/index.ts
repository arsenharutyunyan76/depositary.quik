import * as AppTypes from 'AppTypes';

import initialState, { IStoreAuctions } from './initialState';

import _ from 'lodash';
import acTypes from './acTypes';

export const auctions = (stateStore: IStoreAuctions = initialState, action: AppTypes.RootAction): IStoreAuctions => {
  switch (action.type) {
    case acTypes.AUCTIONS_FETCH_REQUEST: {
      return {
        ...stateStore,
        auctionsLoading: true,
        auctionsLoadError: false
      };
    }

    case acTypes.AUCTIONS_FETCH_SUCCESS: {
      return {
        ...stateStore,
        auctionsLoading: false,
        auctionsLoaded: true,
        items: action.payload
      };
    }

    case acTypes.AUCTIONS_FETCH_FAIL: {
      return {
        ...stateStore,
        auctionsLoading: false,
        auctionsLoadError: true
      };
    }

    case acTypes.SET_AUCTIONS_PARAMS: {
      return {
        ...stateStore,
        params: {
          ...stateStore.params,
          ...action.payload
        }
      };
    }

    case acTypes.AUCTION_SAVE_REQUEST: {
      return {
        ...stateStore,
        auctionSaveLoading: true
      };
    }

    case acTypes.AUCTION_SAVE_SUCESS: {
      const index = _.findIndex(stateStore.items, a => a.ID === action.payload.ID);
      return {
        ...stateStore,
        auctionSaveLoading: false,
        items: [...stateStore.items.slice(0, index), action.payload, ...stateStore.items.slice(index + 1)]
      };
    }
    case acTypes.AUCTION_SAVE_FAIL: {
      return {
        ...stateStore,
        auctionSaveLoading: false
      };
    }

    case acTypes.DELETE_AUCTION_REQUEST: {
      return {
        ...stateStore,
        auctionDeleteLoading: true
      };
    }

    case acTypes.DELETE_AUCTION_SUCCESS: {
      return {
        ...stateStore,
        auctionDeleteLoading: false,
        items: stateStore.items.filter(auction => auction.ID !== Number(action.payload))
      };
    }

    case acTypes.DELETE_AUCTION_FAIL: {
      return {
        ...stateStore,
        auctionDeleteLoading: false
      };
    }

    case acTypes.RESET_AUCTIONS: {
      return {
        ...stateStore,
        auctionsLoaded: false,
        items: initialState.items
      };
    }

    default:
      return stateStore;
  }
};
