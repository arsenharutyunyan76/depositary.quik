export default {
  AUCTIONS_FETCH_REQUEST: '@Auctions/AUCTIONS_FETCH_REQUEST',
  AUCTIONS_FETCH_SUCCESS: '@Auctions/AUCTIONS_FETCH_SUCCESS',
  AUCTIONS_FETCH_FAIL: '@Auctions/AUCTIONS_FETCH_FAIL',

  AUCTION_SAVE_REQUEST: '@Auctions/AUCTION_SAVE_REQUEST',
  AUCTION_SAVE_FAIL: '@Auctions/AUCTION_SAVE_FAIL',
  AUCTION_SAVE_SUCESS: '@Auctions/AUCTION_SAVE_SUCESS',

  DELETE_AUCTION_REQUEST: '@Auctions/DELETE_AUCTIONS_REQUEST',
  DELETE_AUCTION_SUCCESS: '@Auctions/DELETE_AUCTION_SUCCESS',
  DELETE_AUCTION_FAIL: '@Auctions/DELETE_AUCTIONS_FAIL',

  SET_AUCTIONS_PARAMS: '@Auctions/SET_AUCTIONS_PARAMS',
  RESET_AUCTIONS: '@Auctions/RESET_AUCTIONS'
} as const;
