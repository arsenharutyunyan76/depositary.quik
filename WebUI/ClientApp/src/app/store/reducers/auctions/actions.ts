import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const auctionsFetchRequest = () => {
  return action(acTypes.AUCTIONS_FETCH_REQUEST);
};

export const auctionsFetchSuccess = (data: Domain.IAuction[]) => {
  return action(acTypes.AUCTIONS_FETCH_SUCCESS, data);
};

export const auctionsFetchFail = () => {
  return action(acTypes.AUCTIONS_FETCH_FAIL);
};

export const setAuctionsParams = (params: Domain.IAuctionPostParams) => {
  return action(acTypes.SET_AUCTIONS_PARAMS, params);
};

export const auctionSaveRequest = () => {
  return action(acTypes.AUCTION_SAVE_REQUEST);
};

export const auctionSaveFail = () => {
  return action(acTypes.AUCTION_SAVE_FAIL);
};

export const auctionSaveSuccess = (data: Domain.IAuction) => {
  return action(acTypes.AUCTION_SAVE_SUCESS, data);
};

export const auctionDeleteRequest = () => {
  return action(acTypes.DELETE_AUCTION_REQUEST);
};

export const auctionDeleteFail = () => {
  return action(acTypes.DELETE_AUCTION_FAIL);
};

export const auctionDeleteSuccess = (id: number) => {
  return action(acTypes.DELETE_AUCTION_SUCCESS, id);
};

export const resetAuctions = () => {
  return action(acTypes.RESET_AUCTIONS);
};
