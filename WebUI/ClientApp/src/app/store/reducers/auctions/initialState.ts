import * as Domain from '../../../Domain';

import moment from 'moment';

export interface IStoreAuctions {
  auctionsLoading: boolean;
  auctionsLoaded: boolean;
  auctionsLoadError: boolean;
  auctionSaveLoading: boolean;
  auctionDeleteLoading: boolean;

  params: Domain.IAuctionPostParams;
  items: Domain.IAuction[];
}

const initialState: IStoreAuctions = {
  auctionsLoading: false,
  auctionsLoaded: false,
  auctionsLoadError: false,
  auctionSaveLoading: false,
  auctionDeleteLoading: false,
  params: {
    stringFromDate: moment().subtract(1, 'years').format('YYYYMMDD'),
    stringToDate: moment().format('YYYYMMDD'),
    isin: ''
  },
  items: []
};

export default initialState;
