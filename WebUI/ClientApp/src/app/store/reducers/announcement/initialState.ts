import * as Domain from '../../../Domain';

export interface IStoreAnnouncement {
  isLoading: boolean;
  isLoaded: boolean;
  IsError: boolean;
  isSaving: boolean;
  data: Domain.IAnnouncement;
}

const initialState: IStoreAnnouncement = {
  isLoading: false,
  isLoaded: false,
  IsError: false,
  isSaving: false,
  data: {
    EMAIL: '',
    EMAIL_BODY: '',
    EMAIL_SUBJECT: ''
  }
};

export default initialState;
