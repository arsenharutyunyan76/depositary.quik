import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const announcementFetchRequest = () => {
  return action(acTypes.ANNOUNCEMENT_FETCH_REQUEST);
};

export const announcementSaveRequest = () => {
  return action(acTypes.ANNOUNCEMENT_SAVE_REQUEST);
};

export const announcementFetchSuccess = (data: Domain.IAnnouncement) => {
  return action(acTypes.ANNOUNCEMENT_FETCH_SUCCESS, data);
};

export const announcementFetchFail = () => {
  return action(acTypes.ANNOUNCEMENT_FETCH_FAIL);
};
