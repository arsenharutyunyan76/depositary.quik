import * as AppTypes from 'AppTypes';
import initialState, { IStoreAnnouncement } from './initialState';
import acTypes from './acTypes';

export const announcement = (
  stateStore: IStoreAnnouncement = initialState,
  action: AppTypes.RootAction
): IStoreAnnouncement => {
  switch (action.type) {
    case acTypes.ANNOUNCEMENT_FETCH_REQUEST: {
      return {
        ...stateStore,
        isLoading: true,
        IsError: false
      };
    }

    case acTypes.ANNOUNCEMENT_SAVE_REQUEST: {
      return {
        ...stateStore,
        isSaving: true
      };
    }

    case acTypes.ANNOUNCEMENT_FETCH_SUCCESS: {
      return {
        ...stateStore,
        isLoading: false,
        isLoaded: true,
        isSaving: false,
        data: action.payload
      };
    }

    case acTypes.ANNOUNCEMENT_FETCH_FAIL: {
      return {
        ...stateStore,
        isLoading: false,
        IsError: true
      };
    }

    default:
      return stateStore;
  }
};
