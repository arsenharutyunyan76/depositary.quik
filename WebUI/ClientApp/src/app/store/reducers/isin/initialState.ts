import * as Domain from '../../../Domain';

export interface IStoreIsin {
  isLoading: boolean;
  isLoaded: boolean;
  isLoadError: boolean;
  saveLoading: boolean;
  items: Domain.IIsin[];
}

const initialState: IStoreIsin = {
  isLoading: false,
  isLoaded: false,
  isLoadError: false,
  saveLoading: false,
  items: []
};

export default initialState;
