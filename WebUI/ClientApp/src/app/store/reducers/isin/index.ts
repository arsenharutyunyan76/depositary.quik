import * as AppTypes from 'AppTypes';

import initialState, { IStoreIsin } from './initialState';

import _ from 'lodash';
import acTypes from './acTypes';

export const isin = (stateStore: IStoreIsin = initialState, action: AppTypes.RootAction): IStoreIsin => {
  switch (action.type) {
    case acTypes.ISIN_FETCH_REQUEST: {
      return {
        ...stateStore,
        isLoading: true,
        isLoadError: false
      };
    }

    case acTypes.ISIN_FETCH_SUCCESS: {
      return {
        ...stateStore,
        isLoading: false,
        isLoaded: true,
        items: action.payload
      };
    }

    case acTypes.ISIN_FETCH_FAIL: {
      return {
        ...stateStore,
        isLoading: false,
        isLoadError: true
      };
    }

    case acTypes.ISIN_SAVE_REQUEST: {
      return {
        ...stateStore,
        saveLoading: true
      };
    }

    case acTypes.ISIN_SAVE_SUCCESS: {
      const index = _.findIndex(stateStore.items, a => a.ISIN_CODE === action.payload.ISIN_CODE);
      return {
        ...stateStore,
        saveLoading: false,
        items:
          index !== -1
            ? [...stateStore.items.slice(0, index), action.payload, ...stateStore.items.slice(index + 1)]
            : [action.payload, ...stateStore.items]
      };
    }

    case acTypes.ISIN_SAVE_FAIL: {
      return {
        ...stateStore,
        saveLoading: false
      };
    }

    case acTypes.RESET_ISIN: {
      return {
        ...stateStore,
        isLoaded: false,
        items: initialState.items
      };
    }

    default:
      return stateStore;
  }
};
