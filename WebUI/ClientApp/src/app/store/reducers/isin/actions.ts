import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const isinFetchRequest = () => {
  return action(acTypes.ISIN_FETCH_REQUEST);
};

export const isinFetchSuccess = (data: Domain.IIsin[]) => {
  return action(acTypes.ISIN_FETCH_SUCCESS, data);
};

export const isinFetchFail = () => {
  return action(acTypes.ISIN_FETCH_FAIL);
};

export const isinSaveRequest = () => {
  return action(acTypes.ISIN_SAVE_REQUEST);
};

export const isinSaveSuccess = (data: Domain.IIsin) => {
  return action(acTypes.ISIN_SAVE_SUCCESS, data);
};

export const isinSaveFail = () => {
  return action(acTypes.ISIN_SAVE_FAIL);
};

export const resetIsin = () => {
  return action(acTypes.RESET_ISIN);
};
