import * as AppTypes from 'AppTypes';

import initialState, { IStoreStockAccounts } from './initialState';

import _ from 'lodash';
import acTypes from './acTypes';

export const stockAccounts = (
  stateStore: IStoreStockAccounts = initialState,
  action: AppTypes.RootAction
): IStoreStockAccounts => {
  switch (action.type) {
    case acTypes.STOCK_ACCOUNT_FETCH_SUCCESS: {
      return {
        ...stateStore,
        items: action.payload
      };
    }

    case acTypes.STOCK_ACCOUNT_SAVE_SUCESS: {
      const index = _.findIndex(stateStore.items, a => a.CURRENCY === action.payload.CURRENCY);
      return {
        ...stateStore,
        items: [...stateStore.items.slice(0, index), action.payload, ...stateStore.items.slice(index + 1)]
      };
    }

    default:
      return stateStore;
  }
};
