import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const stockAccountsFetchSuccess = (data: Domain.IStockAccount[]) => {
  return action(acTypes.STOCK_ACCOUNT_FETCH_SUCCESS, data);
};

export const stockAccountsSaveSuccess = (data: Domain.IStockAccount) => {
  return action(acTypes.STOCK_ACCOUNT_SAVE_SUCESS, data);
};
