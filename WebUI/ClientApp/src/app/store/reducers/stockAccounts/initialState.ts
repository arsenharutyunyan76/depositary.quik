import * as Domain from '../../../Domain';

export interface IStoreStockAccounts {
  items: Domain.IStockAccount[];
}

const initialState: IStoreStockAccounts = {
  items: []
};

export default initialState;
