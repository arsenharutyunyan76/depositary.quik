import * as AppTypes from 'AppTypes';

import initialState, { IStoreNavbar } from './initialState';

import acTypes from './acTypes';

export const navbar = (stateStore: IStoreNavbar = initialState, action: AppTypes.RootAction): IStoreNavbar => {
  switch (action.type) {
    case acTypes.TOGGLE_FOLDED_NAVBAR: {
      localStorage.setItem('navbarFolded', String(!stateStore.foldedOpen));
      return {
        ...stateStore,
        foldedOpen: !stateStore.foldedOpen
      };
    }
    case acTypes.OPEN_FOLDED_NAVBAR: {
      return {
        ...stateStore,
        foldedOpen: true
      };
    }
    case acTypes.CLOSE_FOLDED_NAVBAR: {
      return {
        ...stateStore,
        foldedOpen: false
      };
    }
    default:
      return stateStore;
  }
};
