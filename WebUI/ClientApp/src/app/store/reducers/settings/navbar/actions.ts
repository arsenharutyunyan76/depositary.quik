import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export function navbarToggleFolded() {
  return action(acTypes.TOGGLE_FOLDED_NAVBAR);
}

export function navbarOpenFolded() {
  return action(acTypes.OPEN_FOLDED_NAVBAR);
}

export function navbarCloseFolded() {
  return action(acTypes.CLOSE_FOLDED_NAVBAR);
}
