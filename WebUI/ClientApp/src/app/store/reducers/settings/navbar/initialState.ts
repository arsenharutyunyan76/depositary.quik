const isFolded = localStorage.getItem('navbarFolded') === 'true';

const initialState: IStoreNavbar = {
  foldedOpen: isFolded || false
};

export default initialState;

export interface IStoreNavbar {
  foldedOpen: boolean;
}
