import * as Domain from '../../../../Domain';

const initialState: IStoreModal = {
  state: false,
  options: {
    children: ''
  },
  confirmModal: {
    state: false,
    message: null,
    title: ''
  }
};

export default initialState;

export interface IStoreModal {
  confirmModal: Domain.IConfirmModal;
  state: boolean;
  options: Domain.IModalOptions;
}
