import * as Domain from '../../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const openModal = (options: Domain.IModalOptions) => {
  return action(acTypes.OPEN_MODAL, options);
};

export const closeModal = () => {
  return action(acTypes.CLOSE_MODAL);
};

export const openConfirmModal = (options: { message: string; title: string; confirmAction?: () => void }) => {
  return action(acTypes.OPEN_CONFIRM_MODAL, options);
};

export const closeConfirmModal = () => {
  return action(acTypes.CLOSE_CONFIRM_MODAL);
};
