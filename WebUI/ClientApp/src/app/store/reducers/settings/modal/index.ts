import * as AppTypes from 'AppTypes';

import initialState, { IStoreModal } from './initialState';

import acTypes from './acTypes';

export const modal = (stateStore: IStoreModal = initialState, action: AppTypes.RootAction): IStoreModal => {
  switch (action.type) {
    case acTypes.OPEN_MODAL: {
      return {
        ...stateStore,
        state: true,
        options: {
          closeButton: true,
          ...action.payload
        }
      };
    }

    case acTypes.OPEN_CONFIRM_MODAL: {
      return {
        ...stateStore,
        confirmModal: {
          state: true,
          ...action.payload
        }
      };
    }

    case acTypes.CLOSE_CONFIRM_MODAL: {
      return {
        ...stateStore,
        confirmModal: {
          ...initialState.confirmModal
        }
      };
    }

    case acTypes.CLOSE_MODAL: {
      return {
        ...initialState,
        state: false
      };
    }

    default:
      return stateStore;
  }
};
