export default {
  OPEN_MODAL: '@Settings/OPEN_MODAL',
  CLOSE_MODAL: '@Settings/CLOSE_MODAL',

  OPEN_CONFIRM_MODAL: '@Settings/OPEN_CONFIRM_MODAL',
  CLOSE_CONFIRM_MODAL: '@Settings/CLOSE_CONFIRM_MODAL'
} as const;
