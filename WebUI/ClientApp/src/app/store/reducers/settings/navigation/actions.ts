import * as Domain from '../../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export function getNavigation() {
  return action(acTypes.GET_NAVIGATION);
}

export function setNavigation(navigation: Domain.INavigation[]) {
  return action(acTypes.SET_NAVIGATION, navigation);
}

export function resetNavigation() {
  return action(acTypes.RESET_NAVIGATION);
}
