export default {
  GET_NAVIGATION: '@Settings/GET_NAVIGATION',
  SET_NAVIGATION: '@Settings/SET_NAVIGATION',
  RESET_NAVIGATION: '@Settings/RESET_NAVIGATION'
} as const;
