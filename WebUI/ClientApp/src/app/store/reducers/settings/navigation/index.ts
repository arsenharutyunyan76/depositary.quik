import * as AppTypes from 'AppTypes';
import * as Domain from '../../../../Domain';

import acTypes from './acTypes';
import initialState from './initialState';

export const navigation = (stateStore = initialState, action: AppTypes.RootAction): Domain.INavigation[] => {
  switch (action.type) {
    case acTypes.GET_NAVIGATION: {
      return [...stateStore];
    }
    case acTypes.SET_NAVIGATION: {
      return [...action.payload];
    }
    case acTypes.RESET_NAVIGATION: {
      return [...initialState];
    }
    default:
      return stateStore;
  }
};
