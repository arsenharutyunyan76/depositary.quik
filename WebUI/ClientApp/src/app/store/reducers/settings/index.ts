import { combineReducers } from 'redux';
import { modal } from './modal';
import { navbar } from './navbar';
import { navigation } from './navigation';

const directory = combineReducers({
  modal,
  navbar,
  navigation
});

export default directory;
