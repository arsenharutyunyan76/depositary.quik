import * as Domain from '../../../Domain';

export interface IStoreReports {
  reportsLoading: boolean;
  reportsLoaded: boolean;
  reportsLoadError: boolean;
  reportSaveLoading: boolean;
  reportAppSaveLoading: boolean;
  items: Domain.IReport[];
}

const initialState: IStoreReports = {
  reportsLoading: false,
  reportsLoaded: false,
  reportsLoadError: false,
  reportSaveLoading: false,
  reportAppSaveLoading: false,
  items: []
};

export default initialState;
