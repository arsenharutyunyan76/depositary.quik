import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const reportsFetchRequest = () => {
  return action(acTypes.REPORTS_FETCH_REQUEST);
};

export const reportsFetchSuccess = (data: Domain.IReport[]) => {
  return action(acTypes.REPORTS_FETCH_SUCCESS, data);
};

export const reportsFetchFail = () => {
  return action(acTypes.REPORTS_FETCH_FAIL);
};

export const reportSaveRequest = () => {
  return action(acTypes.REPORT_SAVE_REQUEST);
};

export const reportSaveFail = () => {
  return action(acTypes.REPORT_SAVE_FAIL);
};

export const reportSaveSuccess = (data: Domain.IReport) => {
  return action(acTypes.REPORT_SAVE_SUCESS, data);
};

export const resetReports = () => {
  return action(acTypes.RESET_REPORTS);
};
