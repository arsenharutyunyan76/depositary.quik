import * as AppTypes from 'AppTypes';

import initialState, { IStoreReports } from './initialState';

import _ from 'lodash';
import acTypes from './acTypes';

export const reports = (stateStore: IStoreReports = initialState, action: AppTypes.RootAction): IStoreReports => {
  switch (action.type) {
    case acTypes.REPORTS_FETCH_REQUEST: {
      return {
        ...stateStore,
        reportsLoading: true,
        reportsLoadError: false
      };
    }

    case acTypes.REPORTS_FETCH_SUCCESS: {
      return {
        ...stateStore,
        reportsLoading: false,
        reportsLoaded: true,
        items: action.payload
      };
    }

    case acTypes.REPORTS_FETCH_FAIL: {
      return {
        ...stateStore,
        reportsLoading: false,
        reportsLoadError: true
      };
    }

    case acTypes.REPORT_SAVE_REQUEST: {
      return {
        ...stateStore,
        reportSaveLoading: true
      };
    }

    case acTypes.REPORT_SAVE_SUCESS: {
      const index = _.findIndex(stateStore.items, a => a.ID === action.payload.ID);
      return {
        ...stateStore,
        reportSaveLoading: false,
        items: [...stateStore.items.slice(0, index), action.payload, ...stateStore.items.slice(index + 1)]
      };
    }

    case acTypes.REPORT_SAVE_FAIL: {
      return {
        ...stateStore,
        reportSaveLoading: false
      };
    }

    case acTypes.RESET_REPORTS: {
      return {
        ...stateStore,
        reportsLoaded: false,
        items: initialState.items
      };
    }

    default:
      return stateStore;
  }
};
