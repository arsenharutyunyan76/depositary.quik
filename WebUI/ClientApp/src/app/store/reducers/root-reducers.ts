import { actionLog } from './actionLog';
import { auctions } from './auctions';
import { bankUsers } from './bankUsers';
import { banks } from './banks';
import { combineReducers } from 'redux';
import { reports } from './reports';
import settings from './settings';
import { user } from './user';
import { announcement } from './announcement';
import { currencies } from './currencies';
import { reportApplications } from './reportApplications';
import { isin } from './isin';
import { stockAccounts } from './stockAccounts';

const rootReducers = combineReducers({
  user,
  settings,
  actionLog,
  auctions,
  reports,
  reportApplications,
  banks,
  bankUsers,
  announcement,
  currencies,
  isin,
  stockAccounts
});

export default rootReducers;
