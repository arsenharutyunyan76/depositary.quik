import * as Domain from '../../../Domain';

export interface IStoreBanks {
  isLoading: boolean;
  isLoaded: boolean;
  IsError: boolean;
  saveBankLoading: boolean;
  fetchAccountsLoading: boolean;
  items: Domain.IBank[];
}

const initialState: IStoreBanks = {
  isLoading: false,
  isLoaded: false,
  IsError: false,
  fetchAccountsLoading: false,
  saveBankLoading: false,
  items: []
};

export default initialState;
