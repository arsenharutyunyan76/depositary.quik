import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const bankFetchRequest = () => {
  return action(acTypes.BANKS_FETCH_REQUEST);
};

export const bankFetchSuccess = (data: Domain.IBank[]) => {
  return action(acTypes.BANKS_FETCH_SUCCESS, data);
};

export const bankFetchFail = () => {
  return action(acTypes.BANKS_FETCH_FAIL);
};

export const bankAccountsFetchRequest = () => {
  return action(acTypes.BANK_ACCOUNTS_FETCH_REQUEST);
};
export const bankAccountsFetchSuccess = (data: Domain.IBankAccounts[], bankCode: string) => {
  return action(acTypes.BANK_ACCOUNTS_FETCH_SUCCESS, { data, bankCode });
};

export const bankAccountsFetchFail = () => {
  return action(acTypes.BANK_ACCOUNTS_FETCH_FAIL);
};

export const bankSaveRequest = () => {
  return action(acTypes.BANK_SAVE_REQUEST);
};

export const bankSaveSuccess = (data: Domain.IBank) => {
  return action(acTypes.BANK_SAVE_SUCCESS, data);
};

export const bankSaveFail = () => {
  return action(acTypes.BANK_SAVE_FAIL);
};

export const resetBanks = () => {
  return action(acTypes.RESET_BANKS);
};
