export default {
  BANKS_FETCH_REQUEST: '@Banks/BANKS_FETCH_REQUEST',
  BANKS_FETCH_SUCCESS: '@Banks/BANKS_FETCH_SUCCESS',
  BANKS_FETCH_FAIL: '@Banks/BANKS_FETCH_FAIL',

  BANK_ACCOUNTS_FETCH_REQUEST: '@Banks/BANK_ACCOUNTS_FETCH_REQUEST',
  BANK_ACCOUNTS_FETCH_SUCCESS: '@Banks/BANK_ACCOUNTS_FETCH_SUCCESS',
  BANK_ACCOUNTS_FETCH_FAIL: '@bank/BANK_ACCOUNTS_FETCH_FAIL',

  BANK_SAVE_REQUEST: '@Banks/BANK_SAVE_REQUEST',
  BANK_SAVE_SUCCESS: '@Banks/BANK_SAVE_SUCCESS',
  BANK_SAVE_FAIL: '@Banks/BANK_SAVE_FAIL',

  RESET_BANKS: '@Banks/RESET_BANKS'
} as const;
