import * as AppTypes from 'AppTypes';
import { IBank } from '../../../Domain';

import initialState, { IStoreBanks } from './initialState';

import _ from 'lodash';
import acTypes from './acTypes';

const findBankIndex = (arr: IBank[], code: string) => _.findIndex(arr, a => a.CODE === code);

export const banks = (stateStore: IStoreBanks = initialState, action: AppTypes.RootAction): IStoreBanks => {
  switch (action.type) {
    case acTypes.BANKS_FETCH_REQUEST: {
      return {
        ...stateStore,
        isLoading: true,
        IsError: false
      };
    }

    case acTypes.BANKS_FETCH_SUCCESS: {
      return {
        ...stateStore,
        isLoading: false,
        isLoaded: true,
        items: action.payload
      };
    }

    case acTypes.BANKS_FETCH_FAIL: {
      return {
        ...stateStore,
        isLoading: false,
        IsError: true
      };
    }

    case acTypes.BANK_ACCOUNTS_FETCH_REQUEST: {
      return {
        ...stateStore,
        fetchAccountsLoading: true
      };
    }

    case acTypes.BANK_ACCOUNTS_FETCH_SUCCESS: {
      const { bankCode, data } = action.payload;
      const index = findBankIndex(stateStore.items, bankCode);
      const bank = stateStore.items[index];
      return {
        ...stateStore,
        fetchAccountsLoading: false,
        items: [...stateStore.items.slice(0, index), { ...bank, Accounts: data }, ...stateStore.items.slice(index + 1)]
      };
    }

    case acTypes.BANK_ACCOUNTS_FETCH_FAIL: {
      return {
        ...stateStore,
        fetchAccountsLoading: false
      };
    }

    case acTypes.BANK_SAVE_REQUEST: {
      return {
        ...stateStore,
        saveBankLoading: true
      };
    }

    case acTypes.BANK_SAVE_SUCCESS: {
      const index = findBankIndex(stateStore.items, action.payload.CODE);

      return {
        ...stateStore,
        saveBankLoading: false,
        items:
          index !== -1
            ? [...stateStore.items.slice(0, index), action.payload, ...stateStore.items.slice(index + 1)]
            : [...stateStore.items, action.payload]
      };
    }

    case acTypes.BANK_SAVE_FAIL: {
      return {
        ...stateStore,
        saveBankLoading: false
      };
    }

    case acTypes.RESET_BANKS: {
      return {
        ...stateStore,
        isLoaded: false,
        items: initialState.items
      };
    }

    default:
      return stateStore;
  }
};
