export default {
  REPORT_APPLICATIONS_FETCH_REQUEST: '@Reports/REPORT_APPLICATIONS_FETCH_REQUEST',
  REPORT_APPLICATIONS_FETCH_SUCCESS: '@Reports/REPORT_APPLICATIONS_FETCH_SUCCESS',
  REPORT_APPLICATIONS_FETCH_FAIL: '@Reports/REPORT_APPLICATIONS_FETCH_FAIL',

  SET_REPORT_APPLICATIONS_PARAMS: '@Reports/SET_REPORT_APPLICATIONS_PARAMS',

  DELETE_REPORT_APPLICATION_SUCCESS: '@Reports/DELETE_REPORT_APPLICATION_SUCCESS',

  CREATE_REPORT_APP_REQUEST: '@Reports/CREATE_REPORT_APP_REQUEST',
  CREATE_REPORT_APP_SUCCESS: '@Reports/CREATE_REPORT_APP_SUCCESS',

  RESET_REPORT_APPLICATIONS: '@Reports/RESET_REPORT_APPLICATIONS'
} as const;
