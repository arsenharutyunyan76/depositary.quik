import * as AppTypes from 'AppTypes';

import initialState, { IStoreReportApplication } from './initialState';
import acTypes from './acTypes';

export const reportApplications = (
  stateStore: IStoreReportApplication = initialState,
  action: AppTypes.RootAction
): IStoreReportApplication => {
  switch (action.type) {
    case acTypes.REPORT_APPLICATIONS_FETCH_REQUEST: {
      return {
        ...stateStore,
        isLoading: true,
        isFail: false
      };
    }

    case acTypes.REPORT_APPLICATIONS_FETCH_SUCCESS: {
      return {
        ...stateStore,
        isLoading: false,
        isLoaded: true,
        items: action.payload
      };
    }

    case acTypes.REPORT_APPLICATIONS_FETCH_FAIL: {
      return {
        ...stateStore,
        isLoading: false,
        isFail: true
      };
    }

    case acTypes.SET_REPORT_APPLICATIONS_PARAMS: {
      return {
        ...stateStore,
        params: {
          ...stateStore.params,
          ...action.payload
        }
      };
    }

    case acTypes.DELETE_REPORT_APPLICATION_SUCCESS: {
      return {
        ...stateStore,
        items: stateStore.items.filter(app => app.ID !== action.payload)
      };
    }

    case acTypes.RESET_REPORT_APPLICATIONS: {
      return {
        ...stateStore,
        isLoaded: false,
        items: initialState.items
      };
    }

    case acTypes.CREATE_REPORT_APP_REQUEST: {
      return {
        ...stateStore,
        isSaveing: true
      };
    }

    case acTypes.CREATE_REPORT_APP_SUCCESS: {
      return {
        ...stateStore,
        isSaveing: false,
        items: [action.payload, ...stateStore.items]
      };
    }

    default:
      return stateStore;
  }
};
