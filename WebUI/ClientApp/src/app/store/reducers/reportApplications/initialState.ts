import * as Domain from '../../../Domain';

import moment from 'moment';

export interface IStoreReportApplication {
  isLoading: boolean;
  isLoaded: boolean;
  isFail: boolean;
  isSaveing: boolean;
  params: Domain.IReportApplicationGetParams;
  items: Domain.IReportApplication[];
}

const initialState: IStoreReportApplication = {
  isLoading: false,
  isLoaded: false,
  isFail: false,
  isSaveing: false,
  params: {
    stringFromDate: moment().subtract(1, 'years').format('YYYYMMDD'),
    stringToDate: moment().format('YYYYMMDD'),
    isFail: ''
  },
  items: []
};

export default initialState;
