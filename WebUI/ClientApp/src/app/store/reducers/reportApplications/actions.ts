import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const reportApplicatonsFetchRequest = () => {
  return action(acTypes.REPORT_APPLICATIONS_FETCH_REQUEST);
};

export const reportApplicatonsFetchSuccess = (data: Domain.IReportApplication[]) => {
  return action(acTypes.REPORT_APPLICATIONS_FETCH_SUCCESS, data);
};

export const reportApplicatonsFetchFail = () => {
  return action(acTypes.REPORT_APPLICATIONS_FETCH_FAIL);
};

export const setReportApplicatonsParams = (params: Domain.IReportApplicationGetParams) => {
  return action(acTypes.SET_REPORT_APPLICATIONS_PARAMS, params);
};

export const reportApplicatonDeleteSuccess = (id: string) => {
  return action(acTypes.DELETE_REPORT_APPLICATION_SUCCESS, id);
};

export const resetReportApplicatons = () => {
  return action(acTypes.RESET_REPORT_APPLICATIONS);
};

export const reportAppSaveRequest = () => {
  return action(acTypes.CREATE_REPORT_APP_REQUEST);
};

export const reportAppSaveSuccess = (data: Domain.IReportApplication) => {
  return action(acTypes.CREATE_REPORT_APP_SUCCESS, data);
};
