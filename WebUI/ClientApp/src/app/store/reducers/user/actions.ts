import acTypes from './acTypes';
import { action } from 'typesafe-actions';

// USER INFO FETCH ACTION CREATORS
export const userInfoFetchRequest = () => {
  return action(acTypes.USER_INFO_FETCH_REQUEST);
};

export const userInfoFetchSuccess = (data: Record<string, unknown>) => {
  return action(acTypes.USER_INFO_FETCH_SUCCESS, data);
};

export const userInfoFetchFail = () => {
  return action(acTypes.USER_INFO_FETCH_FAIL);
};

// USER LOGOUT ACTION CREATORS
export const userLogout = () => {
  return action(acTypes.USER_LOGOUT);
};
