import * as AppTypes from 'AppTypes';

import initialState, { IStoreUser } from './initialState';

import acTypes from './acTypes';

export const user = (stateStore: IStoreUser = initialState, action: AppTypes.RootAction): IStoreUser => {
  switch (action.type) {
    //USER INFO PART
    case acTypes.USER_INFO_FETCH_REQUEST: {
      return {
        ...stateStore,
        userInfoLoading: true,
        userInfoLoadError: false
      };
    }

    case acTypes.USER_INFO_FETCH_SUCCESS: {
      return {
        ...stateStore,
        userInfoLoading: false,
        userInfoLoaded: true
      };
    }

    case acTypes.USER_INFO_FETCH_FAIL: {
      return {
        ...stateStore,
        userInfoLoading: false,
        userInfoLoadError: true
      };
    }

    case acTypes.USER_LOGOUT: {
      return {
        ...initialState
      };
    }

    default:
      return stateStore;
  }
};
