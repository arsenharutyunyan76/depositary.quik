export interface IStoreUser {
  userInfoLoading: boolean;
  userInfoLoaded: boolean;
  userInfoLoadError: boolean;
  userInfo: {
    role: string;
    name: string;
  };
}

const initialState: IStoreUser = {
  userInfoLoading: false,
  userInfoLoaded: false,
  userInfoLoadError: false,
  userInfo: {
    role: 'moderator',
    name: 'Պողոս Պողոսյան'
  }
};

export default initialState;
