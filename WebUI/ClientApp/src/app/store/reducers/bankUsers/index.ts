import * as AppTypes from 'AppTypes';

import initialState, { IStoreBankUsers } from './initialState';

import _ from 'lodash';
import acTypes from './acTypes';

export const bankUsers = (stateStore: IStoreBankUsers = initialState, action: AppTypes.RootAction): IStoreBankUsers => {
  switch (action.type) {
    case acTypes.BANK_USERS_FETCH_REQUEST: {
      return {
        ...stateStore,
        isLoading: true,
        isLoadError: false
      };
    }

    case acTypes.BANK_USERS_FETCH_SUCCESS: {
      return {
        ...stateStore,
        isLoading: false,
        isLoaded: true,
        items: action.payload
      };
    }

    case acTypes.BANK_USERS_FETCH_FAIL: {
      return {
        ...stateStore,
        isLoading: false,
        isLoadError: true
      };
    }

    case acTypes.BANK_USERS_SAVE_REQUEST: {
      return {
        ...stateStore,
        saveLoading: true
      };
    }

    case acTypes.BANK_USERS_SAVE_SUCESS: {
      const index = _.findIndex(stateStore.items, a => a.BANK_CODE === action.payload.BANK_CODE);
      return {
        ...stateStore,
        saveLoading: false,
        items:
          index !== -1
            ? [...stateStore.items.slice(0, index), action.payload, ...stateStore.items.slice(index + 1)]
            : [action.payload, ...stateStore.items]
      };
    }

    case acTypes.BANK_USERS_SAVE_FAIL: {
      return {
        ...stateStore,
        saveLoading: false
      };
    }

    case acTypes.RESET_BANK_USERS: {
      return {
        ...stateStore,
        isLoaded: false,
        items: initialState.items
      };
    }

    default:
      return stateStore;
  }
};
