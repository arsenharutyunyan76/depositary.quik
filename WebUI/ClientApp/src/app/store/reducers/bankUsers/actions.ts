import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const bankUsersFetchRequest = () => {
  return action(acTypes.BANK_USERS_FETCH_REQUEST);
};

export const bankUsersFetchSuccess = (data: Domain.IBankUser[]) => {
  return action(acTypes.BANK_USERS_FETCH_SUCCESS, data);
};

export const bankUsersFetchFail = () => {
  return action(acTypes.BANK_USERS_FETCH_FAIL);
};

export const bankUsersSaveRequest = () => {
  return action(acTypes.BANK_USERS_SAVE_REQUEST);
};

export const bankUsersSaveSuccess = (data: Domain.IBankUser) => {
  return action(acTypes.BANK_USERS_SAVE_SUCESS, data);
};

export const bankUsersSaveFail = () => {
  return action(acTypes.BANK_USERS_SAVE_FAIL);
};

export const resetBankUsers = () => {
  return action(acTypes.RESET_BANK_USERS);
};
