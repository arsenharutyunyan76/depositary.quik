import * as Domain from '../../../Domain';

export interface IStoreBankUsers {
  isLoading: boolean;
  isLoaded: boolean;
  isLoadError: boolean;
  saveLoading: boolean;
  items: Domain.IBankUser[];
}

const initialState: IStoreBankUsers = {
  isLoading: false,
  isLoaded: false,
  isLoadError: false,
  saveLoading: false,
  items: []
};

export default initialState;
