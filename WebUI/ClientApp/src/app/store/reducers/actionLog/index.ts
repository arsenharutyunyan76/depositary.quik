import { RootAction } from 'AppTypes';
import initialState, { IStoreActionLog } from './initialState';
import acTypes from './acTypes';

export const actionLog = (stateStore: IStoreActionLog = initialState, action: RootAction): IStoreActionLog => {
  switch (action.type) {
    case acTypes.ACTION_LOG_FETCH_REQUEST: {
      return {
        ...stateStore,
        actionLogLoading: true,
        actionLogLoadError: false
      };
    }

    case acTypes.ACTION_LOG_FETCH_SUCCESS: {
      return {
        ...stateStore,
        actionLogLoading: false,
        actionLogLoaded: true,
        items: action.payload
      };
    }

    case acTypes.ACTION_LOG_FETCH_FAIL: {
      return {
        ...stateStore,
        actionLogLoading: false,
        actionLogLoadError: true
      };
    }

    case acTypes.SET_ACTION_LOG_PARAMS: {
      return {
        ...stateStore,
        params: {
          ...stateStore.params,
          ...action.payload
        }
      };
    }

    case acTypes.RESET_ACTION_LOG: {
      return {
        ...stateStore,
        actionLogLoaded: false,
        items: initialState.items
      };
    }

    default:
      return stateStore;
  }
};
