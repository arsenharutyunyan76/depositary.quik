import * as Domain from '../../../Domain';

import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const actionLogFetchRequest = () => {
  return action(acTypes.ACTION_LOG_FETCH_REQUEST);
};

export const actionLogFetchSuccess = (data: Domain.IActionLog[]) => {
  return action(acTypes.ACTION_LOG_FETCH_SUCCESS, data);
};

export const actionLogFetchFail = () => {
  return action(acTypes.ACTION_LOG_FETCH_FAIL);
};

export const setActionLogParams = (params: Domain.IActionLogGetParams) => {
  return action(acTypes.SET_ACTION_LOG_PARAMS, params);
};

export const resetAuctionLog = () => {
  return action(acTypes.RESET_ACTION_LOG);
};
