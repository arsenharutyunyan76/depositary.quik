import * as Domain from '../../../Domain';

import moment from 'moment';

export interface IStoreActionLog {
  actionLogLoading: boolean;
  actionLogLoaded: boolean;
  actionLogLoadError: boolean;

  params: Domain.IActionLogGetParams;
  items: Domain.IActionLog[];
}

const initialState: IStoreActionLog = {
  actionLogLoading: false,
  actionLogLoaded: false,
  actionLogLoadError: false,
  params: {
    stringFromDate: moment().subtract(1, 'years').format('YYYYMMDD'),
    stringToDate: moment().format('YYYYMMDD'),
    isFail: ''
  },
  items: []
};

export default initialState;
