import { ICurrency } from 'app/Domain';
export interface IStoreCurrencies {
  items: ICurrency[];
}

const initialState: IStoreCurrencies = {
  items: []
};

export default initialState;
