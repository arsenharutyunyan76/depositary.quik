import { ICurrency } from 'app/Domain';
import acTypes from './acTypes';
import { action } from 'typesafe-actions';

export const currenciesFetchSuccess = (data: ICurrency[]) => {
  return action(acTypes.CURRENCIES_FETCH_SUCCESS, data);
};

export const currenciesSaveSuccess = (data: ICurrency) => {
  return action(acTypes.CURRENCIES_SAVE_SUCCESS, data);
};
