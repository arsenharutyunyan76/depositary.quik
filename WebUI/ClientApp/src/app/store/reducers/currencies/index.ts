import * as AppTypes from 'AppTypes';

import initialState, { IStoreCurrencies } from './initialState';

import acTypes from './acTypes';

export const currencies = (
  stateStore: IStoreCurrencies = initialState,
  action: AppTypes.RootAction
): IStoreCurrencies => {
  switch (action.type) {
    case acTypes.CURRENCIES_FETCH_SUCCESS: {
      return {
        ...stateStore,
        items: action.payload
      };
    }

    case acTypes.CURRENCIES_SAVE_SUCCESS: {
      return {
        ...stateStore
      };
    }

    default:
      return stateStore;
  }
};
