import moment from 'moment';

export const formatDate = (date: string | null, format = 'DD/MM/YYYY') => {
  return date ? moment(date).format(format) : '';
};

export const d2String = (date: Date | string) => moment(date).format('YYYYMMDD');

export const toCurrency = (numberString: string | number) => {
  const number = parseFloat(String(numberString));
  return `${number.toLocaleString('AMD')}`;
};

export const toDateTime = (date: string, time: string) => {
  const actualDate = moment(date).format('YYYY-MM-DD');
  return moment(`${actualDate} ${time}`, 'YYYY-MM-DD HH:mm').toDate();
};

export const setDatePickerValue = (
  setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void,
  name: string,
  format: string
) => (date: Date) => date && setFieldValue(name, moment(date).format(format));

export const getSelectedTime = (date: string, value: string) => toDateTime(date, value);

export const getSelectedDate = (date: string | null) => {
  return date ? moment(date).toDate() : null;
};
