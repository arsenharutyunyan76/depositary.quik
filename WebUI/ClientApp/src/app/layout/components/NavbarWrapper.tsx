import * as AppTypes from 'AppTypes';

import { Link } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';
import Navigation from '../../components/Navigation/';
import React from 'react';
import clsx from 'clsx';
import { useSelector } from 'react-redux';

const NavbarHeader: React.FC = () => {
  return (
    <Navbar bg="primary" className="page-toolbar navbar-toolbar">
      <Navbar.Brand as={Link} to="/">
        <img src="/assets/images/logo-withe.svg" alt="Ֆոնդային բորսա" className="logo mr-0" height="25" width="88" />
      </Navbar.Brand>
    </Navbar>
  );
};

const NavbarLayout: React.FC = () => {
  return (
    <div className="d-flex flex-column overflow-hidden h-full">
      <NavbarHeader />
      <div className="navbar-content">
        <Navigation />
      </div>
    </div>
  );
};

const NavbarWrapper: React.FC = () => {
  const folded = useSelector<AppTypes.ReducerState, boolean>(({ settings }) => settings.navbar.foldedOpen);
  return (
    <div className={clsx('page-navbar', { folded: folded })}>
      <div className={clsx('navbar-wrapper', { folded: folded })}>
        {/*TODO: check mobile  and use drawer effect for navbarLayout in mobile*/}
        <NavbarLayout />
      </div>
    </div>
  );
};

export default NavbarWrapper;
