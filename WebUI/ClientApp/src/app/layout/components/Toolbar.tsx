import * as AppTypes from 'AppTypes';

import { Button, Nav, NavDropdown, Navbar } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

import { Link } from 'react-router-dom';
import React from 'react';
import clsx from 'clsx';
import { navbarToggleFolded } from '../../store/reducers/settings/navbar/actions';

const Toolbar: React.FC = () => {
  const dispatch = useDispatch();

  const toggleNavbar = () => {
    dispatch(navbarToggleFolded());
  };
  const folded = useSelector<AppTypes.ReducerState, boolean>(({ settings }) => settings.navbar.foldedOpen);
  const userName = useSelector<AppTypes.ReducerState, string>(({ user }) => user.userInfo.name);

  return (
    <Navbar bg="primary" variant="dark" className="page-toolbar">
      <Button className={clsx('navbar-toggle-btn', { folded: folded })} variant="link" onClick={toggleNavbar}>
        <i className="icon"></i>
      </Button>
      <Nav className="ml-auto">
        <NavDropdown title={userName} id="user-dropdown" className="user-dropdown">
          <NavDropdown.Item to="/profile" as={Link}>
            <i className="amx-avatar"></i>Հաշիվ
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/logout">
            <i className="amx-logout"></i>Ելք
          </NavDropdown.Item>
        </NavDropdown>
      </Nav>
    </Navbar>
  );
};

export default Toolbar;
