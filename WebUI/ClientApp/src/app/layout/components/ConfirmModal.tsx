import * as AppTypes from 'AppTypes';
import * as Domain from '../../Domain';

import { Button, Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

import React from 'react';
import { closeModal } from '../../store/reducers/settings/modal/actions';

const ConfirmModal: React.FC = () => {
  const dispatch = useDispatch();
  const { state, message, title, confirmAction } = useSelector<AppTypes.ReducerState, Domain.IConfirmModal>(
    ({ settings: { modal } }) => modal.confirmModal
  );

  const handleClose = () => dispatch(closeModal());

  return (
    <Modal show={state} onHide={handleClose}>
      <Modal.Header closeButton={true}>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p className="">{message}</p>
      </Modal.Body>
      <Modal.Footer>
        {confirmAction && (
          <Button variant="danger" onClick={confirmAction}>
            Հաստատել
          </Button>
        )}

        <Button variant="secondary" onClick={handleClose}>
          Փակել
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ConfirmModal;
