import * as AppTypes from 'AppTypes';
import * as Domain from '../../Domain';

import { Button, Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import ModalHeader from 'react-bootstrap/ModalHeader';

import React from 'react';
import { closeModal } from '../../store/reducers/settings/modal/actions';

const ModalContainer: React.FC = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector<AppTypes.ReducerState, boolean>(({ settings: { modal } }) => modal.state);

  const { children, modalProps, footer, title, closeButton } = useSelector<AppTypes.ReducerState, Domain.IModalOptions>(
    ({ settings: { modal } }) => modal.options
  );

  const handleClose = () => dispatch(closeModal());

  return (
    <Modal show={isOpen} onHide={handleClose} {...modalProps}>
      <ModalHeader closeButton={true}>
        <Modal.Title>{title}</Modal.Title>
      </ModalHeader>
      <Modal.Body>{children}</Modal.Body>
      <Modal.Footer>
        {footer}
        {closeButton && (
          <Button variant="secondary" onClick={handleClose}>
            Փակել
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
};

export default ModalContainer;
