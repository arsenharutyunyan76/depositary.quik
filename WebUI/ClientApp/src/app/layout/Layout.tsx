import React, { Suspense } from 'react';

import ConfirmModal from './components/ConfirmModal';
import Loading from '../components/Loading';
import Modal from './components/Modal';
import NavbarWrapper from './components/NavbarWrapper';
import Toolbar from './components/Toolbar';
import { renderRoutes } from 'react-router-config';
import { routes } from '../configs/routes';
import { ToastContainer } from 'react-toastify';

const Layout: React.FC = ({ children }) => {
  return (
    <>
      <main className="page-layout" id="main-layout">
        <div className="d-flex flex-grow-1 flex-shrink-1 flex-column overflow-hidden position-relative">
          <div className="page-wrapper">
            <NavbarWrapper />
            <div className="page-content">
              <Toolbar />
              <Suspense fallback={<Loading />}>{renderRoutes(routes)}</Suspense>
              {children}
            </div>
          </div>
        </div>
      </main>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={true}
        newestOnTop={false}
        closeOnClick={true}
        pauseOnHover={true}
      />
      <Modal />
      <ConfirmModal />
    </>
  );
};

export default Layout;
