import React, { useState, useEffect } from 'react';
import { Column, HeaderProps, CellProps } from 'react-table';
import { ITableColumnConfig } from '../Domain';
import TableColumn from '../components/table/TableColumn';

type Props<T extends object> = {
  columnsConfig: ITableColumnConfig<T>[];
};

const useTableColumns = <T extends object = {}>({ columnsConfig }: Props<T>) => {
  const [columns, setColumns] = useState<Column<T>[]>([]);

  useEffect(() => {
    const columns = generateColumns<T>(columnsConfig);
    setColumns(columns);
  }, [columnsConfig]);

  return { columns };
};

export default useTableColumns;

function generateColumns<T extends object = {}>(columnsConfig: ITableColumnConfig<T>[]): Column<T>[] {
  const newArray: Column<T>[] = columnsConfig.map(columnConfig => {
    const { CustomCellComponent, customOptions, ...config } = columnConfig;

    const options = {
      Header: ({ column }: HeaderProps<T>) => {
        return <TableColumn key={column.id} value={config.Header} column={column} />;
      }
    };

    if (CustomCellComponent) {
      Object.assign(options, {
        Cell: (props: CellProps<T, any>) => {
          return <CustomCellComponent {...{ ...props, ...customOptions }} />;
        }
      });
    }

    return { ...config, ...options };
  });

  return newArray;
}
