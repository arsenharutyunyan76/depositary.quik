import { IActionLogGetParams } from '../Domain';

import { actionLogActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';

export const getActionLog = (params?: IActionLogGetParams) => async (dispatch: Dispatch<any>, getState: any) => {
  dispatch(Actions.actionLogFetchRequest());

  const initialParams = getState().auctions.params;
  const dates = params || initialParams;
  try {
    const response = await ApiClient.GetActionLog(dates);
    dispatch(Actions.actionLogFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.actionLogFetchFail());
    return Promise.reject(resData);
  }
};
