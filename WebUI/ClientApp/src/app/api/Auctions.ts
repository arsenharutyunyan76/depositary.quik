import { IAuction, IAuctionPostParams } from '../Domain';

import { auctionsActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';
import { toast } from 'react-toastify';
import { DEFAULT_ERROR_MSG } from '../locale/global.json';

export const getAuctions = (params?: IAuctionPostParams) => async (dispatch: Dispatch<any>, getState: any) => {
  dispatch(Actions.auctionsFetchRequest());

  const initialParams = getState().auctions.params;
  const dates = params || initialParams;
  try {
    const response = await ApiClient.GetAuctions(dates);
    dispatch(Actions.auctionsFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.auctionsFetchFail());
    return Promise.reject(resData);
  }
};

export const saveAuction = (data: IAuction) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.auctionSaveRequest());
  try {
    await ApiClient.SetAuctionDate(data);
    toast.success('Աճուրդի տվյալները հաջողությամբ պահպանվեց');
    dispatch(Actions.auctionSaveSuccess(data));
    return data;
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || DEFAULT_ERROR_MSG);
    dispatch(Actions.auctionSaveFail());
    return Promise.reject(resData);
  }
};

export const deleteAuction = (id: number) => {
  return async (dispatch: Dispatch<any>): Promise<any> => {
    dispatch(Actions.auctionDeleteRequest());
    try {
      await ApiClient.DeleteAuction(id);
      toast.success('Աճուրդը հաջողությամբ հեռացվեց');
      dispatch(Actions.auctionDeleteSuccess(id));
      return Promise.resolve(true);
    } catch (error) {
      const resData = error.response && error.response.data;
      dispatch(Actions.auctionDeleteFail());
      return Promise.reject(resData);
    }
  };
};
