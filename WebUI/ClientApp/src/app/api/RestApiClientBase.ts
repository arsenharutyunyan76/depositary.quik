﻿class HttpVerb {
  private name: string;
  private constructor(name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }

  public static Get: HttpVerb = new HttpVerb('GET');
  public static Post: HttpVerb = new HttpVerb('POST');
  public static Put: HttpVerb = new HttpVerb('PUT');
  public static Delete: HttpVerb = new HttpVerb('DELETE');
}

export abstract class RestApiClientBase {
  private baseUrl: string | null;
  private token: string | null;
  private tokenPromise: Promise<string> | null = null;

  public constructor() {
    this.baseUrl = null;
    this.token = null;
  }

  private getToken(): Promise<string> {
    if (this.tokenPromise) return this.tokenPromise;
    this.tokenPromise = fetch('/api/session/token')
      .then(response => response.text())
      .then(token => {
        this.token = token;
        return token;
      })
      .finally(() => {
        this.tokenPromise = null;
      });

    return this.tokenPromise;
  }

  private checkToken(): Promise<string> {
    if (this.token) {
      return new Promise<string>(accept => accept(this.token as string));
    } else {
      return this.getToken();
    }
  }

  private async getServiceEndpoints(): Promise<string> {
    if (this.baseUrl) {
      return new Promise<string>(accept => accept(this.baseUrl as string));
    } else {
      const response = await fetch('api/configuration/settings');
      const result = await response.json();
      this.baseUrl = result.RestApiBaseUrl as string;
      return this.baseUrl;
    }
  }

  protected httpGet<T>(resource: string, queryString: string | undefined = undefined) {
    return this.httpSend<T>(resource, queryString, HttpVerb.Get);
  }

  protected httpGetRaw(resource: string, queryString: string | undefined = undefined) {
    return this.httpSendRaw(resource, queryString, HttpVerb.Get);
  }

  protected httpPost<T>(resource: string, body: object, queryString: string | undefined = undefined) {
    return this.httpSend<T>(resource, queryString, HttpVerb.Post, body);
  }

  protected httpPostRaw(
    resource: string,
    body: object | string | undefined,
    queryString: string | undefined = undefined
  ) {
    return this.httpSendRaw(resource, queryString, HttpVerb.Post, body);
  }

  protected httpPutRaw(
    resource: string,
    body: object | string | undefined,
    queryString: string | undefined = undefined
  ) {
    return this.httpSendRaw(resource, queryString, HttpVerb.Put, body);
  }

  protected httpDeleteRaw(
    resource: string,
    queryString: string | undefined = undefined,
    body: string | object | undefined = undefined
  ) {
    return this.httpSendRaw(resource, queryString, HttpVerb.Delete, body);
  }

  private async httpSend<T>(
    resource: string,
    queryString: string | undefined,
    method: HttpVerb,
    body: object | undefined = undefined
  ) {
    const response = await this.httpSendRaw(resource, queryString, method, body);
    return await (response.status === 204
      ? new Promise<T>(resolve => resolve(undefined))
      : (response.json() as Promise<T>));
  }

  protected async httpDownload(
    resource: string,
    queryString: string | undefined,
    body: object | undefined = undefined
  ) {
    const response = await this.httpSendRaw(resource, queryString, HttpVerb.Get, body);
    let result: Promise<void>;
    if (response && response.ok && response.status != 204) {
      const contentDisposition = response.headers.get('content-disposition');
      const r = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDisposition!);
      const fileName = r ? r[1] : undefined;
      result = response.blob().then(blob => {
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        const clickHandler = function () {
          setTimeout(() => {
            URL.revokeObjectURL(url);
            a.removeEventListener('click', clickHandler);
          }, 150);
        };
        a.href = url;
        a.download = fileName || 'download';
        a.addEventListener('click', clickHandler);
        a.click();
      });
    } else {
      result = new Promise<void>(resolve => resolve(undefined));
    }
    return result;
  }

  protected httpPostForm<T>(resource: string, body: string | Blob, queryString: string | undefined = undefined) {
    return this.httpSendForm<T>(resource, queryString, HttpVerb.Post, body);
  }

  protected async httpSendForm<T>(
    resource: string,
    queryString: string | undefined,
    method: HttpVerb,
    body: string | Blob
  ) {
    const response = await this.httpSendFormRaw(resource, queryString, method, body);
    return await (response.status == 204
      ? new Promise<T>(resolve => resolve(undefined))
      : (response.json() as Promise<T>));
  }

  protected async httpSendFormRaw(
    resource: string,
    queryString: string | undefined,
    method: HttpVerb,
    body: string | Blob
  ) {
    const baseUrl = await this.getServiceEndpoints();
    const token = await this.checkToken();
    let url = baseUrl + resource;
    if (queryString) {
      url = url + queryString;
    }
    const headers: HeadersInit = new Headers();
    if (token) {
      headers.set('Authorization', `Bearer ${token}`);
    }
    const request: RequestInit = {
      method: method.getName(),
      headers: new Headers(headers)
    };
    if (body) {
      const b = new FormData();
      b.append('file', body);
      request['body'] = b;
    }
    const response = await fetch(url, request);
    if (!response.ok) {
      return new Promise<Response>((resolve, reject) => {
        response
          .json()
          .then(result_3 => reject(result_3))
          .catch(error => reject(error));
      });
    }
    return response;
  }

  protected async httpSendRaw(
    resource: string,
    queryString: string | undefined,
    method: HttpVerb,
    body: object | string | undefined = undefined
  ): Promise<Response> {
    const baseUrl = await this.getServiceEndpoints();
    const token = await this.checkToken();
    let url = baseUrl + resource;
    if (queryString) {
      url = url + queryString;
    }
    const headers: HeadersInit = new Headers();
    if (body) {
      headers.set('Content-Type', `application/json`);
    }
    if (token) {
      headers.set('Authorization', `Bearer ${token}`);
    }
    const request: RequestInit = {
      method: method.getName(),
      headers: new Headers(headers)
    };
    if (body) {
      request['body'] = JSON.stringify(body);
    }
    const response = await fetch(url, request);
    if (!response.ok) {
      return new Promise<Response>((resolve, reject) => {
        response
          .json()
          .then(result_3 => reject(result_3))
          .catch(error => reject(error));
      });
    }
    return response;
  }
}
