﻿import * as Domain from '../Domain';

import { RestApiClientBase } from './RestApiClientBase';
import qs from 'qs';

class RestApiClient extends RestApiClientBase {
  public GetPerson(personId: string): Promise<Domain.IPerson> {
    return this.httpGet<Domain.IPerson>(`/persons/${encodeURIComponent(personId)}`);
  }

  public GetPersons(): Promise<Domain.IPerson[]> {
    return this.httpGet<Domain.IPerson[]>(`/entities/persons`);
  }

  // AUCTIONS
  public GetAuctions(params: Domain.IAuctionPostParams): Promise<Domain.IAuction[]> {
    return this.httpGet(`/Auction?`, qs.stringify(params));
  }

  public GetActionLog(params: Domain.IActionLogGetParams): Promise<Domain.IActionLog[]> {
    return this.httpGet(`/ActionLog?`, qs.stringify(params));
  }

  public SetAuctionDate(data: Domain.IAuction): Promise<Domain.IAuction[]> {
    return this.httpPost(`/Auction/SetAuctionDate`, data);
  }

  public DeleteAuction(id: number) {
    return this.httpPostRaw(`/Auction/DeleteAuction`, String(id));
  }

  // REPORTS
  public GetReports(): Promise<Domain.IReport[]> {
    return this.httpGet(`/Report`);
  }

  public GetReportApplications(params: Domain.IReportApplicationGetParams): Promise<Domain.IReportApplication[]> {
    return this.httpGet(`/ReportApplication?`, qs.stringify(params));
  }

  public DeleteReportApplication(id: string) {
    return this.httpDeleteRaw(`/ReportApplication/${id}`);
  }

  public SetReportTime(data: Domain.IReport): Promise<Domain.IReport[]> {
    return this.httpPost(`/Report/SetReportTime`, data);
  }

  public SetReportApplication(data: Domain.IReportPostApplication) {
    return this.httpPost<string>(`/ReportApplication/CreateReportApplication`, data);
  }

  ///BANK
  public GetBanks(): Promise<Domain.IBank[]> {
    return this.httpGet<Domain.IBank[]>(`/Bank`);
  }

  public GetBankAccounts(bankCode: string): Promise<Domain.IBankAccounts[]> {
    return this.httpGet<Domain.IBankAccounts[]>(`/Account?`, qs.stringify({ bankCode: bankCode }));
  }

  public SaveBank(data: Domain.IBank): Promise<Domain.IBank[]> {
    return this.httpPost(`/Bank/SaveBank`, data);
  }

  ///BANKUSERS
  public GetBankUsers(): Promise<Domain.IBankUser[]> {
    return this.httpGet<Domain.IBankUser[]>(`/BankUser`);
  }

  public SaveBankUser(data: Domain.IBankUser): Promise<Domain.IBankUser[]> {
    return this.httpPost(`/BankUser/SaveBankUser`, data);
  }

  ///AuctionAnnouncementController
  public GetAnnouncement(): Promise<Domain.IAnnouncement> {
    return this.httpGet<Domain.IAnnouncement>(`/AuctionAnnouncement`);
  }

  public SaveAnnouncement(data: Domain.IAnnouncement): Promise<Domain.IAnnouncement> {
    return this.httpPost(`/AuctionAnnouncement/SaveAuctionAnnouncement`, data);
  }

  // Currencies
  public GetCurrencies(): Promise<Domain.ICurrency[]> {
    return this.httpGet<Domain.ICurrency[]>(`/Currency`);
  }

  public SaveCurrency(data: Domain.ICurrency) {
    return this.httpPostRaw(`/Currency/SaveCurrency`, data);
  }

  //RecentFailedActio
  public GetRecentFailedAction(): Promise<Domain.IRecentFailedAction[]> {
    return this.httpGet<Domain.IRecentFailedAction[]>(`/RecentFailedAction`);
  }

  // Isin
  public GetIsin(code?: string): Promise<Domain.IIsin[]> {
    const url = code ? `/Isin?isinCode=${code}` : '/Isin';
    return this.httpGet<Domain.IIsin[]>(url);
  }

  public SaveIsin(data: Domain.IIsin): Promise<Domain.IIsin[]> {
    return this.httpPost(`/Isin/SaveISIN`, data);
  }

  // Isin
  public GetStockAccount(): Promise<Domain.IStockAccount[]> {
    return this.httpGet<Domain.IStockAccount[]>(`/StockAccount`);
  }

  public SaveStockAccount(data: Domain.IStockAccount): Promise<boolean> {
    return this.httpPost(`/StockAccount/SaveStockAccount`, data);
  }
}

export const ApiClient = new RestApiClient();
