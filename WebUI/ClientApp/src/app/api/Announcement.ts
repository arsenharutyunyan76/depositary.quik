import { IAnnouncement } from '../Domain';

import { announcementActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';
import { toast } from 'react-toastify';
import { DEFAULT_ERROR_MSG } from '../locale/global.json';

export const getAnnouncement = () => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.announcementFetchRequest());
  try {
    const response = await ApiClient.GetAnnouncement();
    dispatch(Actions.announcementFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.announcementFetchFail());
    return Promise.reject(resData);
  }
};

export const saveAnnouncement = (data: IAnnouncement) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.announcementSaveRequest());
  try {
    await ApiClient.SaveAnnouncement(data);
    toast.success('Հայտարարությունը հաջողությամբ պահպանվեց');
    dispatch(Actions.announcementFetchSuccess(data));
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || DEFAULT_ERROR_MSG);
    dispatch(Actions.announcementFetchFail());
    return Promise.reject(resData);
  }
};
