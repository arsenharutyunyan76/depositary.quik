import { IIsin } from '../Domain';

import { isinActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';
import { toast } from 'react-toastify';
import { DEFAULT_ERROR_MSG } from '../locale/global.json';

export const getIsin = () => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.isinFetchRequest());
  try {
    const response = await ApiClient.GetIsin();
    dispatch(Actions.isinFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.isinFetchFail());
    return Promise.reject(resData);
  }
};

export const saveIsin = (data: IIsin) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.isinSaveRequest());
  try {
    await ApiClient.SaveIsin(data);
    toast.success('ISIN կոդը հաջողությամբ պահպանվեց');
    dispatch(Actions.isinSaveSuccess(data));
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || error.Message || DEFAULT_ERROR_MSG);
    dispatch(Actions.isinSaveFail());
    return Promise.reject(resData);
  }
};
