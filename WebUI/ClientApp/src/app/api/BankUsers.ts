import { IBankUser } from '../Domain';

import { bankUsersActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';
import { toast } from 'react-toastify';
import { DEFAULT_ERROR_MSG } from '../locale/global.json';

export const getBankUsers = () => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.bankUsersFetchRequest());
  try {
    const response = await ApiClient.GetBankUsers();
    dispatch(Actions.bankUsersFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.bankUsersFetchFail());
    return Promise.reject(resData);
  }
};

export const saveBankUser = (data: IBankUser) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.bankUsersSaveRequest());
  try {
    await ApiClient.SaveBankUser(data);
    toast.success('Բանկի օգտատիրոջ տվյալները հաջողությամբ պահպանվեց');
    dispatch(Actions.bankUsersSaveSuccess(data));
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || error.Message || DEFAULT_ERROR_MSG);
    dispatch(Actions.bankUsersSaveFail());
    return Promise.reject(resData);
  }
};
