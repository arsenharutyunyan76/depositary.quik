import { IReport, IReportPostApplication, IReportApplicationGetParams } from '../Domain';

import { reportsActions as Actions } from '../store/reducers/root-actions';
import { reportApplicationsActions as applicationActions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';
import { toast } from 'react-toastify';
import { DEFAULT_ERROR_MSG } from '../locale/global.json';

export const getReports = () => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.reportsFetchRequest());
  try {
    const response = await ApiClient.GetReports();
    dispatch(Actions.reportsFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.reportsFetchFail());
    return Promise.reject(resData);
  }
};

export const saveReportTime = (data: IReport) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.reportSaveRequest());
  try {
    await ApiClient.SetReportTime(data);
    toast.success('հաշվետվության տվյալները հաջողությամբ պահպանվեց');
    dispatch(Actions.reportSaveSuccess(data));
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || DEFAULT_ERROR_MSG);
    dispatch(Actions.reportSaveFail());
    return Promise.reject(resData);
  }
};

export const setReportApplication = (data: IReportPostApplication) => async (dispatch: Dispatch<any>) => {
  dispatch(applicationActions.reportAppSaveRequest());
  try {
    const id = await ApiClient.SetReportApplication(data);
    dispatch(
      applicationActions.reportAppSaveSuccess({
        ...data,
        ID: id,
        CREATION_DATE: '',
        GENERATE_DATE: ''
      })
    );
    return id;
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || DEFAULT_ERROR_MSG);
    return Promise.reject(resData);
  }
};

export const getReportApplications = (params?: IReportApplicationGetParams) => async (
  dispatch: Dispatch<any>,
  getState: any
) => {
  dispatch(applicationActions.reportApplicatonsFetchRequest());

  const initialParams = getState().auctions.params;
  const dates = params || initialParams;
  try {
    const response = await ApiClient.GetReportApplications(dates);
    dispatch(applicationActions.reportApplicatonsFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(applicationActions.reportApplicatonsFetchFail());
    return Promise.reject(resData);
  }
};

export const deleteReportApplication = (id: string) => async (dispatch: Dispatch<any>) => {
  try {
    await ApiClient.DeleteReportApplication(id);
    toast.success('Աճուրդը հաջողությամբ հեռացվեց');
    dispatch(applicationActions.reportApplicatonDeleteSuccess(id));
  } catch (error) {
    const resData = error.response && error.response.data;
    return Promise.reject(resData);
  }
};
