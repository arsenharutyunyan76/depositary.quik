import { ApiClient } from './RestApiClient';

export const getRecentFailedAction = async () => {
  try {
    return await ApiClient.GetRecentFailedAction();
  } catch (error) {
    const resData = error.response && error.response.data;
    return Promise.reject(resData);
  }
};
