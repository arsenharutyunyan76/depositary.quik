import { ICurrency } from 'app/Domain';
import { currenciesActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';

export const getCurrencies = () => async (dispatch: Dispatch<any>) => {
  try {
    const response = await ApiClient.GetCurrencies();
    dispatch(Actions.currenciesFetchSuccess(response));
    return response;
  } catch (error) {
    const resData = error.response && error.response.data;
    return Promise.reject(resData);
  }
};

export const saveCurrency = (data: ICurrency) => async (dispatch: Dispatch<any>) => {
  try {
    await ApiClient.SaveCurrency(data);
    dispatch(Actions.currenciesSaveSuccess(data));
    return data;
  } catch (error) {
    const resData = error.response && error.response.data;
    return Promise.reject(resData);
  }
};
