import { IBank } from '../Domain';

import { banksActions as Actions } from '../store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'react';
import { toast } from 'react-toastify';
import { DEFAULT_ERROR_MSG } from '../locale/global.json';

export const getBanks = () => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.bankFetchRequest());
  try {
    const response = await ApiClient.GetBanks();
    dispatch(Actions.bankFetchSuccess(response));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.bankFetchFail());
    return Promise.reject(resData);
  }
};

export const getBankAccounts = (bankCode: string) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.bankAccountsFetchRequest());
  try {
    const response = await ApiClient.GetBankAccounts(bankCode);
    dispatch(Actions.bankAccountsFetchSuccess(response, bankCode));
  } catch (error) {
    const resData = error.response && error.response.data;
    dispatch(Actions.bankAccountsFetchFail());
    return Promise.reject(resData);
  }
};

export const saveBank = (data: IBank) => async (dispatch: Dispatch<any>) => {
  dispatch(Actions.bankSaveRequest());
  try {
    await ApiClient.SaveBank(data);
    toast.success('Բանկի տվյալները հաջողությամբ պահպանվեց');
    dispatch(Actions.bankSaveSuccess(data));
  } catch (error) {
    const resData = error.response && error.response.data;
    toast.error(resData || DEFAULT_ERROR_MSG);
    dispatch(Actions.bankSaveFail());
    return Promise.reject(resData);
  }
};
