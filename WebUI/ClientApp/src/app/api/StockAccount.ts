import { IStockAccount } from '../Domain';

import { stockAccountsActions as Actions } from 'app/store/reducers/root-actions';
import { ApiClient } from './RestApiClient';
import { Dispatch } from 'redux';

export const getStockAccount = () => async (dispatch: Dispatch<any>) => {
  try {
    const response = await ApiClient.GetStockAccount();
    dispatch(Actions.stockAccountsFetchSuccess(response));
    return true;
  } catch (error) {
    const resData = error.response && error.response.data;
    return Promise.reject(resData);
  }
};

export const saveStockAccount = (data: IStockAccount) => async (dispatch: Dispatch<any>) => {
  try {
    await ApiClient.SaveStockAccount(data);
    dispatch(Actions.stockAccountsSaveSuccess(data));
    return data;
  } catch (error) {
    const resData = error.response && error.response.data;
    return Promise.reject(resData);
  }
};
