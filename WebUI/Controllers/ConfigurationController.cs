using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebUI.Controllers
{
    [Authorize]
    [Route("api/configuration")]
    public class ConfigurationController : Controller
    {
        private IConfiguration configuration;

        public ConfigurationController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpGet("settings")]
        public async Task<ConfigurationSettings> GetToken()
        {
            return await Task.FromResult(new ConfigurationSettings(this.configuration));
        }

        public class ConfigurationSettings
        {
            public string RestApiBaseUrl { get; }
            public string NotificationsApiBaseUrl { get; }

            public ConfigurationSettings(IConfiguration configuration)
            {
                string restApiBaseUrl = configuration["RestApi:BaseUrl"];
                string notificationsApiBaseUrl = configuration["NotificationsApi:BaseUrl"];
                if (string.IsNullOrEmpty(notificationsApiBaseUrl))
                {
                    notificationsApiBaseUrl = restApiBaseUrl.Replace("http://", "ws://");
                    notificationsApiBaseUrl = notificationsApiBaseUrl.Replace("https://", "wss://");
                }
                this.RestApiBaseUrl = restApiBaseUrl;
                this.NotificationsApiBaseUrl = notificationsApiBaseUrl;
            }
        }
    }
}
