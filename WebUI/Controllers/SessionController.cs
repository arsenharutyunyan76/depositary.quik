using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    [Authorize]
    [Route("api/session")]
    public class SessionController : Controller
    {
        [HttpGet("token")]
        public async Task<string> GetToken()
        {
            return await this.HttpContext.GetTokenAsync("access_token");
        }

        [HttpGet("roles")]
        public async Task<IEnumerable<string>> GetRoles()
        {
            return await Task.FromResult(this.User.Claims.Where(c => c.Type == "role").Select(c => c.Value));
        }
    }
}
