using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth.Claims;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            string authority = Configuration.GetSection("Authentication")["Authority"];

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            })
            .AddCookie("Cookies")
            .AddOpenIdConnect("oidc", options =>
            {
                options.SignInScheme = "Cookies";

                options.Authority = authority;
                options.RequireHttpsMetadata = false; // TODO: Change to 'true', leave as 'false' only in development mode

                options.ResponseType = "code id_token";

                options.ClientId = "clientUi";
                options.ClientSecret = "secret";
                options.SaveTokens = true;
                options.Scope.Add("adminApi");
                options.Scope.Add("offline_access");
                options.GetClaimsFromUserInfoEndpoint = true;
                options.TokenValidationParameters.NameClaimType = "name";
                options.TokenValidationParameters.RoleClaimType = "role";
                options.ClaimActions.Add(new RolesResolver());
                options.Events = new OpenIdConnectEvents
                {
                    OnRemoteFailure = ctx => {
                        ctx.HandleResponse();
                        ctx.Response.Redirect("/");
                        return Task.CompletedTask;
                    }
                };
            });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // TODO: Disabled HTTPS redirection for now, needs to be reenabled later.
            ////app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.Use(async (context, next) =>
            {
                if (!context.User.Identity.IsAuthenticated)
                {
                    await context.ChallengeAsync();
                }
                else
                {
                    await next();
                }
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }

        private class RolesResolver : ClaimAction
        {
            public RolesResolver()
                : base("role", null)
            {
            }

            public override void Run(JObject userData, ClaimsIdentity identity, string issuer)
            {
                var values = userData?["role"];
                if (values is JValue)
                {
                    identity.AddClaim(new Claim(ClaimType, values.ToString(), ValueType, issuer));
                }
                else if (values is JArray)
                {
                    foreach (var value in values)
                    {
                        identity.AddClaim(new Claim(ClaimType, value.ToString(), ValueType, issuer));
                    }
                }
            }
        }
    }
}
