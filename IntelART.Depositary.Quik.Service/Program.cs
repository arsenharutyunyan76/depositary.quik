﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using IntelART.Depositary.Quik.MessageProcessor;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.Service
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var currentDirectory = new FileInfo(typeof(Program).Assembly.Location).Directory.FullName;
            var isService = !(Debugger.IsAttached || args.Contains("--console"));
            var builder = new HostBuilder()
                .ConfigureAppConfiguration(config =>
                {
                    config.SetBasePath(currentDirectory)
                        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                        .AddEnvironmentVariables();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    // Injecting service dependencies
                    services.AddSingleton<IListenerConfiguration>(new ListenerConfiguration(hostContext.Configuration));

                    // Registering hosted services that will actually run
                    services.AddHostedService<TradeListenerService>();
                    services.AddHostedService<HT575GenerationListenerService>();
                    services.AddHostedService<HT535ListenerService>();
                    services.AddHostedService<HTPaymentListenerService>();
                    services.AddHostedService<HTPaymentOutMessageService>();
                    services.AddHostedService<OrderListenerService>();
                    services.AddHostedService<DependListenerService>();
                    services.AddHostedService<RTSFileGenerationService>();
                    services.AddHostedService<QuikFileGenerationService>();
                    services.AddHostedService<ISINUpdater>();
                    services.AddHostedService<DependOutListenerService>();
                    services.AddHostedService<DependUndepositionListenerService>();
                    services.AddHostedService<DependUndepositionFinalCalculationListenerService>();
                    services.AddHostedService<RTSPaymentOutMessageListenerService>();
                    services.AddHostedService<RTSOutDataListenerService>();
                    services.AddHostedService<AuctionParamListenerService>();
                    services.AddHostedService<ISINFRFUpdater>(); 
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.ClearProviders();
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddLog4Net($"{currentDirectory}/log4net.config");
                    if (!isService)
                    {
                        logging.AddConsole();
                    }
                });
            if (isService)
            {
                await builder.RunAsServiceAsync();
            }
            else
            {
                await builder.RunConsoleAsync();
            }
        }

        private class ListenerConfiguration : IListenerConfiguration
        {
            private IConfiguration configuration { get; }

            public string ConnectionString => this.configuration.GetConnectionString("Quik");
            public string IntegrationConnectionString => this.configuration.GetConnectionString("QuikIntegration");
            public string BankmailConnectionString => this.configuration.GetConnectionString("Bankmail");
            public string BankmailPaymentConnectionString => this.configuration.GetConnectionString("BankmailPayment");
            public string RTSConnectionString => this.configuration.GetConnectionString("RTS");
            public string RTSGatewayConnectionString => this.configuration.GetConnectionString("RTSGateway");
            public string OperationConnectionString => this.configuration.GetConnectionString("QuikOperation");

            public Settings AppSettings { get; set; }

            public ListenerConfiguration(IConfiguration configuration)
            {
                AppSettings = new Settings();
                configuration.GetSection("Settings").Bind(AppSettings);

                this.configuration = configuration;
            }
        }
    }
}
