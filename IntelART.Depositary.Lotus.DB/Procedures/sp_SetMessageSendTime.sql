﻿if exists (select * from sys.objects where name='sp_SetMessageSendTime' and type='P')
	drop procedure sp_SetMessageSendTime
GO
create procedure sp_SetMessageSendTime(
	@ID			int,
	@SEND_DATE	datetime
)
AS
	update MESSAGE_RECIPIENTS
	set SEND_DATE = @SEND_DATE
	where ID=@ID
GO
