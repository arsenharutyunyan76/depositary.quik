﻿if exists (select * from sys.objects where name='sp_SaveMessageRecipients' and type='P')
	drop procedure sp_SaveMessageRecipients
GO
create procedure sp_SaveMessageRecipients(
	@RECIPIENT_LIST	nvarchar(max),
	@EMAIL_SUBJECT	nvarchar(200),
	@EMAIL_BODY		nvarchar(max),
	@FILES			MessageRecipientFiles readonly
)
AS
	DECLARE @messageRecipientID integer
	insert into MESSAGE_RECIPIENTS ([RECIPIENT_LIST], [EMAIL_SUBJECT], [EMAIL_BODY])
		values (@RECIPIENT_LIST, @EMAIL_SUBJECT, @EMAIL_BODY)
		
	SET @messageRecipientID = SCOPE_IDENTITY() 

	insert into MESSAGE_RECIPIENT_FILES (MESSAGE_RECIPIENT_ID,FILE_NAME)
	select @messageRecipientID, FILE_NAME from @FILES
GO
