﻿if exists (select * from sys.objects where name='sp_GetMessageRecipients' and type='P')
	drop procedure sp_GetMessageRecipients
GO

create procedure sp_GetMessageRecipients
AS
	select [ID]
	     , [RECIPIENT_LIST]
	     , [EMAIL_SUBJECT]
		 , [EMAIL_BODY]
		 , [SEND_DATE]
	from MESSAGE_RECIPIENTS
	where [SEND_DATE] IS NULL
GO
