﻿if exists (select * from sys.objects where name='sp_GetMessageRecipientFiles' and type='P')
	drop procedure sp_GetMessageRecipientFiles
GO

create procedure sp_GetMessageRecipientFiles(
@RECIPIENT_ID	integer
)
AS
	SELECT [FILE_NAME] FROM MESSAGE_RECIPIENT_FILES
	WHERE MESSAGE_RECIPIENT_ID = @RECIPIENT_ID
GO
