use msdb
GO

ALTER DATABASE msdb set TRUSTWORTHY ON;
GO
EXECUTE dbo.sp_changedbowner @loginame = N'sa', @map = false
GO
sp_configure 'show advanced options', 1;
GO

EXECUTE sp_configure 'xp_cmdshell', 1;
GO
RECONFIGURE;
GO

EXECUTE dbo.sp_add_job @job_name=N'Send Messages',
		@owner_login_name=N'sa'
GO
EXECUTE dbo.sp_add_jobstep @job_name = 'Send Messages',
		@step_name=N'Send Messages',
		@subsystem=N'TSQL',
		@command=N'DECLARE @command varchar(8000);
SET @command = ''"C:\IntelART.Depositary.Quik\Lotus\IntelART.Depositary.Quik.Lotus.exe"'';
EXECUTE master..xp_cmdshell @command'
GO
EXECUTE dbo.sp_add_jobschedule @job_name = 'Send Messages',
		@name=N'Send Messages',
		@freq_type=4,
		@freq_interval=1,
		@freq_subday_type=2,
		@freq_subday_interval=30,
		@active_start_date=20200423,
		@active_end_date=99991231,
		@active_start_time=0,
		@active_end_time=235959
GO
EXECUTE dbo.sp_add_jobserver @job_name = 'Send Messages',
    @server_name = '(local)'
GO
