﻿if (not exists (select * from sys.types where name='MessageRecipientFiles'))
	CREATE TYPE MessageRecipientFiles AS TABLE
	(
		FILE_NAME	nvarchar(200)	NOT NULL
	)
GO