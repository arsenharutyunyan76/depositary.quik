﻿if exists (select * from sys.objects where name='MESSAGE_RECIPIENTS' and type='U')
	drop table MESSAGE_RECIPIENTS
GO

CREATE TABLE MESSAGE_RECIPIENTS
(
	ID				int				identity(1, 1),
	CREATION_DATE	datetime		NOT NULL default getdate(),
	RECIPIENT_LIST	nvarchar(max)	NOT NULL,
	EMAIL_SUBJECT	nvarchar(200)	NULL,
	EMAIL_BODY		nvarchar(max)	NOT NULL,
	SEND_DATE		datetime		NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iMESSAGE_RECIPIENTS1 ON dbo.MESSAGE_RECIPIENTS(ID)
GO
