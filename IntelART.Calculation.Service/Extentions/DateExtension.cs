﻿using System;

namespace IntelART.Calculation.Service.Extentions
{
    public static class DateExtension
    {
        public static int MonthDifference(this DateTime lValue, DateTime rValue)
        {
            return (lValue.Month - rValue.Month) + 12 * (lValue.Year - rValue.Year);
        }

        public static int DaysDifference(this DateTime end, DateTime start)
        {
            return (int)(Convert.ToDateTime(end.ToString("yyyy-MM-dd")) - Convert.ToDateTime(start.ToString("yyyy-MM-dd"))).TotalDays;
        }
    }
}
