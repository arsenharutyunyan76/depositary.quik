﻿namespace IntelART.Calculation.Service.Enums
{
    public enum ActionType
    {
        Yield = 0,
        CleanPrice = 1
    }
}
