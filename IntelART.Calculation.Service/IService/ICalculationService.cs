﻿using IntelART.Calculation.Service.Entity;
using System;

namespace IntelART.Calculation.Service.IService
{
    public interface ICalculationService: IDisposable
    {
        CalculationResultViewModel Calculate(PayloadModel model);
    }
}
