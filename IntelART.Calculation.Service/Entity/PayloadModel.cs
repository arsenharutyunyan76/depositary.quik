﻿using IntelART.Calculation.Service.Enums;
using System;

namespace IntelART.Calculation.Service.Entity
{
    public class PayloadModel
    {
        public string Isin { get; set; }
        public DateTime Date { get; set; }
        public decimal CleanPrice { get; set; }
        public decimal Yield { get; set; }
        public ActionType ActionType { get; set; }
        public Instrument Instrument { get; set; }
    }
}
