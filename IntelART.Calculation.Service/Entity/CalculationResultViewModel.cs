﻿using IntelART.Calculation.Service.Enums;

namespace IntelART.Calculation.Service.Entity
{
    public class CalculationResultViewModel
    {
        public decimal Yield { get; set; }
        public decimal CleanPrice { get; set; }
        public ActionType ActionType { get; set; }
        public double AccruedInterest { get; set; }
        public double DirtyPrice { get; set; }
    }
}
