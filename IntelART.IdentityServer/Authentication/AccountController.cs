﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using IntelART.IdentityManagement;

namespace IntelART.IdentityServer.Authentication
{
    public class AccountController : Controller
    {
        private IMembershipProvider membershipProvider;

        public AccountController(IMembershipProvider membershipProvider)
        {
            this.membershipProvider = membershipProvider;
        }

        [HttpGet()]
        public async Task<IActionResult> CreatePassword(string returnUrl, string username, string token)
        {
            CreatePasswordModel model = new CreatePasswordModel()
            {
                ReturnUrl = returnUrl,
                Username = username,
                PasswordChangeRequestToken = token,
            };
            return View(model);
        }

        [HttpPost()]
        public async Task<IActionResult> CreatePassword(CreatePasswordModel model)
        {
            IActionResult result;
            if (string.IsNullOrWhiteSpace(model.NewPassword)
                || model.NewPassword != model.ConfirmNewPassword)
            {
                result = View(model);
            }
            else
            {
                if (await this.membershipProvider.ValidatePassword(model.Username, model.PasswordChangeRequestToken))
                {
                    IUserInfo user = await this.membershipProvider.GetUserByUsername(model.Username);
                    if (user == null)
                    {
                        result = this.BadRequest(string.Format("Unknown user '{0}'.", model.Username));
                    }
                    else
                    {
                        await this.membershipProvider.ChangeUserPassword(user.Id.ToString(), model.PasswordChangeRequestToken, model.NewPassword);
                        result = Redirect(model.ReturnUrl);
                    }
                }
                else
                {
                    result = Redirect(model.ReturnUrl);
                }
            }
            return result;
        }

        [Authorize]
        [HttpGet()]
        public async Task<IActionResult> ChangePassword()
        {
            if (HttpContext.User != null
                && HttpContext.User.Identity != null
                && HttpContext.User.Identity.IsAuthenticated)
            {
                string returnUrl = HttpContext.Request.Headers["referer"];
                ChangePasswordModel model = new ChangePasswordModel();
                model.Username = HttpContext.User.Identity.Name;
                if (!string.IsNullOrWhiteSpace(returnUrl))
                {
                    model.ReturnUrl = returnUrl;
                }
                return View(model);
            }
            else
            {
                return Unauthorized();
            }
        }

        private class ChangePasswordResult
        {
            public bool IsSuccess { get; private set; }
            public string Message { get; private set; }

            public ChangePasswordResult(bool isSuccess, string message)
            {
                this.IsSuccess = isSuccess;
                this.Message = message;
            }
        }

        private async Task<ChangePasswordResult> ChangePasswordImpl(ChangePasswordModel model)
        {
            bool success = false;
            string message = null;

            Claim subjectClaim = HttpContext.User.FindFirst("sub");
            if (subjectClaim != null)
            {
                string userId = subjectClaim.Value;
                if (model != null
                    && model.NewPassword == model.ConfirmNewPassword)
                {
                    if (await this.membershipProvider.ValidatePassword(HttpContext.User.Identity.Name, model.OldPassword))
                    {
                        try
                        {
                            await this.membershipProvider.ChangeUserPassword(userId, model.OldPassword, model.NewPassword);
                            success = true;
                        }
                        catch (Exception e)
                        {
                            // TODO: Tigran: Log exception message here
                            message = "Համակարգային սխալ գաղտնաբառը փոխելիս";
                        }
                    }
                    else
                    {
                        message = "Ընթացիկ գաղտնաբառը սխալ է մուտքագրված";
                    }
                }
                else
                {
                    message = "Նոր գաղտնաբառը և նոր գաղտնաբառի հաստատումը իրար չեն համապատասխանում";
                }
            }
            else
            {
                message = "Անհնար է որոշել ընթացիկ օգտագործողին";
            }

            return new ChangePasswordResult(success, message);
        }

        [Authorize]
        [HttpPost()]
        public async Task<IActionResult> ChangePasswordRpc(ChangePasswordModel model)
        {
            IActionResult result;

            ChangePasswordResult r = await this.ChangePasswordImpl(model);

            if(r.IsSuccess)
            {
                result = Ok();
            }
            else
            {
                result = BadRequest(r.Message);
            }

            return result;
        }

        [Authorize]
        [HttpPost()]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model)
        {
            IActionResult result;

            ChangePasswordResult r = await this.ChangePasswordImpl(model);

            if (r.IsSuccess)
            {
                // TODO: Tigran: Should logout here. Also, the redirect URL should be the logout
                // endpoint of the clinet application, so that the user will be logged out on the
                // client application scope as well.
                result = Redirect(model.ReturnUrl);
            }
            else
            {
                if (r.Message != null)
                {
                    ModelState.AddModelError("", r.Message);
                }
                model.Username = HttpContext.User.Identity.Name;
                result = View(model);
            }

            return result;
        }
    }
}
