﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using IntelART.IdentityManagement;
using IntelART.IdentityManagement.ActiveDirectory;

namespace IntelART.IdentityServer
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            IClientStore clientStoreDataProvider = new ConfigurationClientStore(this.Configuration.GetSection("IdentityServer:ClientApplications"));
            IResourceStore resourceStore = new ConfigurationResourceStore(this.Configuration.GetSection("IdentityServer:Resources"));
            services.AddMvc();
            services.AddIdentityServer(options =>
            {
                options.UserInteraction.LoginUrl = "/Authentication/Login";
                options.UserInteraction.LogoutUrl = "/Authentication/Logout";
            })
                .AddResourceStore<ResourceStoreProxy>()
                .AddClientStore<ClientStoreProxy>()
                .AddCustomTokenRequestValidator<ClientClaimsHandler>()
                .AddDeveloperSigningCredential() // Replace with .AddSigningCredential()
                .AddResourceOwnerValidator<ResourceOwnerPasswordValidator>();

            ////IMembershipProvider membershipProvider = new SqlMembershipProvider(Configuration.GetSection("ConnectionStrings")["MembershipDB"]);
            IMembershipProvider membershipProvider = null;
            IConfiguration membershipStoreConfiguration = Configuration.GetSection("MembershipStore");
            string storeType = null;

            if (membershipStoreConfiguration != null)
            {
                storeType = membershipStoreConfiguration["Type"];
            }

            if (storeType == "Dummy")
            {
                membershipProvider = new DummyUserStore();
            }
            else if (storeType == "MSAM")
            {
                ActiveDirectoryUserStore userStore = new ActiveDirectoryUserStore("local");
                userStore.Initialize();
                membershipProvider = userStore;
            }
            else if (storeType == "ActiveDirectory")
            {
                string domain = membershipStoreConfiguration["Domain"];
                ActiveDirectoryUserStore userStore = new ActiveDirectoryUserStore("ActiveDirectory", domain);
                userStore.Initialize();
                membershipProvider = userStore;
            }

            services.AddSingleton<IUserStore>(membershipProvider);
            services.AddSingleton(this.Configuration);
            services.AddSingleton<IMembershipProvider>(membershipProvider);
            ////services.AddTransient<IUserStore>((sp) => new TestUserStore(new[] { new UserInfo() { Id = 1, Username = "administrator" } }));

            services.AddSingleton<IClientStore>(clientStoreDataProvider);
            services.AddSingleton<IResourceStore>(resourceStore);

            services.AddTransient<IProfileService, ProfileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }
    }
}
