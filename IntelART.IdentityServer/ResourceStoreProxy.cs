﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace IntelART.IdentityServer
{
    public class ResourceStoreProxy : IResourceStore
    {
        private IResourceStore resourceStore;

        public ResourceStoreProxy(IResourceStore resourceStore)
        {
            this.resourceStore = resourceStore;
        }

        public Task<ApiResource> FindApiResourceAsync(string name)
        {
            return this.resourceStore.FindApiResourceAsync(name);
        }

        public Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return this.resourceStore.FindApiResourcesByScopeAsync(scopeNames);
        }

        public Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return this.resourceStore.FindIdentityResourcesByScopeAsync(scopeNames);
        }

        public Task<Resources> GetAllResourcesAsync()
        {
            return this.resourceStore.GetAllResourcesAsync();
        }
    }
}
