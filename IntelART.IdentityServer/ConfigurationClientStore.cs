﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace IntelART.IdentityServer
{
    public class ConfigurationClientStore : IClientStore
    {
        private IConfiguration configuration;
        private string[] defaultScopes;

        public ConfigurationClientStore(IConfiguration configuration)
        {
            this.defaultScopes = new[] {
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
            };
            this.configuration = configuration;
        }

        public async Task<Client> FindClientByIdAsync(string clientId)
        {
            Client client = null;
            if (configuration != null)
            {
                string oidcEndpoint = "/signin-oidc";
                IConfigurationSection child = configuration.GetSection(clientId);
                if (child != null)
                {
                    string clientUrl = child["Url"];
                    string secret = child["Secret"] ?? "secret";
                    int accessTokenLifetime = Convert.ToInt32(child["AccessTokenLifetime"]);
                    if (accessTokenLifetime == 0)
                    {
                        accessTokenLifetime = 3600;
                    }

                    List<string> scopes = new List<string>(this.defaultScopes);
                    IConfiguration scopesSection = child.GetSection("Scopes");
                    if (scopesSection != null)
                    {
                        foreach (IConfigurationSection scopeItem in scopesSection.GetChildren())
                        {
                            scopes.Add(scopeItem.Value);
                        }
                    }
                    client = new Client
                    {
                        ClientId = clientId,
                        ClientName = clientId,
                        AllowedGrantTypes = GrantTypes.ClientCredentials,

                        ClientSecrets = { new Secret(secret.Sha256()) },

                        AllowedScopes = scopes,

                        RequireConsent = false,
                        AllowOfflineAccess = true,
                        AccessTokenLifetime = accessTokenLifetime,
                    };

                    if (clientUrl != null)
                    {
                        string clientOidcEndpoint = string.Format("{0}{1}", clientUrl, oidcEndpoint);
                        client.RedirectUris = new[] { clientOidcEndpoint };
                        client.PostLogoutRedirectUris = new[] { clientOidcEndpoint };
                        client.AllowedCorsOrigins = new[] { clientUrl };
                        client.AllowedGrantTypes = GrantTypes.HybridAndClientCredentials;
                    }
                }
            }

            return client;
        }
    }
}
