﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IntelART.IdentityManagement;

namespace IntelART.IdentityServer
{
    public class DummyUserStore : IMembershipProvider
    {
        public Task ChangeUserPassword(string id, string oldPassword, string newPassword)
        {
            return Task.CompletedTask;
        }

        public Task<IUserInfo> GetUserById(string id)
        {
            IUserInfo result = null;
            if (id == "admin")
            {
                result = new UserInfo
                {
                    Id = "admin",
                    Username = "admin",
                    Email = "admin@example.com",
                    FullName = "Administrator",
                    IsActive = true,
                };
            }
            return Task.FromResult(result);
        }

        public Task<IUserInfo> GetUserByUsername(string username)
        {
            return this.GetUserById(username);
        }

        public Task<IEnumerable<string>> GetUserRolesById(string id)
        {
            return Task.FromResult(new[] { "admin" } as IEnumerable<string>);
        }

        public Task<bool> ValidatePassword(string username, string password)
        {
            return Task.FromResult(username == "admin" && password == "admin");
        }

        private class UserInfo : IUserInfo
        {
            public string Id { get; set; }

            public string Username { get; set; }

            public string Email { get; set; }

            public string FullName { get; set; }

            public bool IsActive { get; set; }
        }
    }
}
