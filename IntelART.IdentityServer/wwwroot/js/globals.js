var DEFAULT_DATE_FORMAT = "dd/mm/yyyy";
var REQUIRED_FIELD_ERROR = "Պարտադիր լրացման դաշտ";
var EMAIL_FORMAT_ERROR = "Էլ․ հասցեի ֆորմատը սխալ է";
var DATE_FORMAT_ERROR = "Ամսաթվի ֆորմատը սխալ է";
var NUMBER_FORMAT_ERROR = "Թվի ֆորմատը սխալ է";
var PASSWORDS_DONT_MATCH = "Գաղտնաբառերը չեն համընկնում ";
