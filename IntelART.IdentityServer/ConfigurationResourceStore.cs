﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using IdentityServer4.Models;
using IdentityServer4.Stores;

namespace IntelART.IdentityServer
{
    public class ConfigurationResourceStore : IResourceStore
    {
        private Resources resources;
        private IConfiguration configuration;

        public ConfigurationResourceStore(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.Initialize();
            ChangeToken.OnChange(this.configuration.GetReloadToken, this.Initialize);
        }

        private void Initialize()
        {
            this.resources = new Resources();
            if (this.configuration != null)
            {
                IConfiguration apiResourcesConfiguration = this.configuration.GetSection("Api");
                foreach (IConfigurationSection child in apiResourcesConfiguration.GetChildren())
                {
                    string name = child.Key;
                    string displayName = child["DisplayName"];

                    this.resources.ApiResources.Add(new ApiResource(name, displayName));
                }
            }
            this.resources.IdentityResources.Add(new IdentityResources.OpenId());
            this.resources.IdentityResources.Add(new IdentityResources.Profile());
        }

        public async Task<ApiResource> FindApiResourceAsync(string name)
        {
            return this.resources.ApiResources.Where(r => r.Name == name).FirstOrDefault();
        }

        public async Task<IEnumerable<ApiResource>> FindApiResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return this.resources.ApiResources.Where(r => r.Scopes.Where(s => scopeNames.Contains(s.Name)).Count() > 0);
        }

        public async Task<IEnumerable<IdentityResource>> FindIdentityResourcesByScopeAsync(IEnumerable<string> scopeNames)
        {
            return this.resources.IdentityResources.Where(r => scopeNames.Contains(r.Name));
        }

        public async Task<Resources> GetAllResourcesAsync()
        {
            return this.resources;
        }
    }
}
