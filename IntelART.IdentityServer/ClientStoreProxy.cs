﻿using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;


namespace IntelART.IdentityServer
{
    public class ClientStoreProxy : IClientStore
    {
        private IClientStore dataProvider;

        public ClientStoreProxy(IClientStore dataProvider)
        {
            this.dataProvider = dataProvider;
        }

        public Task<Client> FindClientByIdAsync(string clientId)
        {
            return this.dataProvider.FindClientByIdAsync(clientId);
        }
    }
}
