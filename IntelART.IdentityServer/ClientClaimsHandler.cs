﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using IdentityServer4.Validation;

namespace IntelART.IdentityServer
{
    public class ClientClaimsHandler : ICustomTokenRequestValidator
    {
        private IConfiguration configuration;

        public ClientClaimsHandler(IConfigurationRoot configuration)
        {
            this.configuration = configuration;
        }

        public async Task ValidateAsync(CustomTokenRequestValidationContext context)
        {
            string clientName = context.Result.ValidatedRequest.Client.ClientId;
            IConfiguration applicationRolesConfiguration = this.configuration.GetSection($"IdentityServer:ClientApplications:{clientName}:ClientRoles");
            if (applicationRolesConfiguration != null)
            {
                context.Result.ValidatedRequest.Client.AlwaysSendClientClaims = true;
                foreach (IConfigurationSection role in applicationRolesConfiguration.GetChildren())
                {
                    context.Result.ValidatedRequest.ClientClaims.Add(new Claim("role", role.Value));
                    context.Result.ValidatedRequest.ClientClaims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role.Value));
                }
                context.Result.ValidatedRequest.Client.ClientClaimsPrefix = "";
            }
        }
    }
}
