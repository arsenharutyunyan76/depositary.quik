﻿using System.Text;

namespace IntelART.Utilities
{
    public class Transliteration
    {
        public static string ConvertRU2EN(string source)
        {
            char[] sourceCharacters = source.Trim().Replace("'", string.Empty).ToCharArray();
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < sourceCharacters.Length; i++)
            {
                char symbol = sourceCharacters[i];
                switch (symbol.ToString().ToLower())
                {
                    case "а":
                        result.Append('A');
                        break;
                    case "б":
                        result.Append('B');
                        break;
                    case "в":
                        result.Append('V');
                        break;
                    case "г":
                        result.Append('G');
                        break;
                    case "д":
                        result.Append('D');
                        break;
                    case "е":
                        result.Append('E');
                        break;
                    case "ж":
                        result.Append('J');
                        break;
                    case "з":
                        result.Append('Z');
                        break;
                    case "и":
                        result.Append('I');
                        break;
                    case "й":
                        result.Append('i');
                        break;
                    case "к":
                        result.Append('K');
                        break;
                    case "л":
                        result.Append('L');
                        break;
                    case "м":
                        result.Append('M');
                        break;
                    case "н":
                        result.Append('N');
                        break;
                    case "о":
                        result.Append('O');
                        break;
                    case "п":
                        result.Append('P');
                        break;
                    case "р":
                        result.Append('R');
                        break;
                    case "с":
                        result.Append('S');
                        break;
                    case "т":
                        result.Append('T');
                        break;
                    case "у":
                        result.Append('U');
                        break;
                    case "ф":
                        result.Append('F');
                        break;
                    case "х":
                        result.Append('H');
                        break;
                    case "ц":
                        result.Append('C');
                        break;
                    case "ч":
                        result.Append('c');
                        break;
                    case "ш":
                        result.Append('Q');
                        break;
                    case "щ":
                        result.Append('q');
                        break;
                    case "ъ":
                        result.Append('x');
                        break;
                    case "ы":
                        result.Append('Y');
                        break;
                    case "ь":
                        result.Append('X');
                        break;
                    case "э":
                        result.Append('e');
                        break;
                    case "ю":
                        result.Append('u');
                        break;
                    case "я":
                        result.Append('a');
                        break;
                    case "'":
                        break;
                    default:
                        if (IsEnglishLetter(symbol))
                        {
                            char previousSymbol;
                            if (i == 0)
                                previousSymbol = ' ';
                            else
                                previousSymbol = sourceCharacters[i - 1];
                            if (previousSymbol == ' ' || !IsEnglishLetter(previousSymbol))
                                result.Append("'");

                            result.Append(symbol);

                            char nextSymbol;
                            if (i >= source.Length - 1)
                                nextSymbol = ' ';
                            else
                                nextSymbol = sourceCharacters[i + 1];
                            if (nextSymbol == ' ' || !IsEnglishLetter(nextSymbol))
                                result.Append("'");
                        }
                        else
                            result.Append(symbol);
                        break;
                }
            }
            return result.ToString();
        }

        public static string ConvertAM2EN(string source)
        {
            char[] sourceCharacters = source.Trim().Replace("'", string.Empty).ToCharArray();
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < sourceCharacters.Length; i++)
            {
                char symbol = sourceCharacters[i];

                char previousSymbol;
                if (i == 0)
                    previousSymbol = ' ';
                else
                    previousSymbol = sourceCharacters[i - 1];

                char nextSymbol;
                if (i >= source.Length - 1)
                    nextSymbol = ' ';
                else
                    nextSymbol = sourceCharacters[i + 1];

                switch (symbol.ToString().ToLower())
                {
                    case "ա":
                        result.Append('A');
                        break;
                    case "բ":
                        result.Append('B');
                        break;
                    case "գ":
                        result.Append('G');
                        break;
                    case "դ":
                        result.Append('D');
                        break;
                    case "ե":
                    case "է":
                        result.Append('E');
                        break;
                    case "զ":
                        result.Append('Z');
                        break;
                    case "ը":
                        result.Append('Y');
                        break;
                    case "թ":
                    case "տ":
                        result.Append('T');
                        break;
                    case "ժ":
                        result.Append("ZH");
                        break;
                    case "ի":
                        result.Append('I');
                        break;
                    case "լ":
                        result.Append('L');
                        break;
                    case "խ":
                        result.Append("KH");
                        break;
                    case "ծ":
                        result.Append("TS");
                        break;
                    case "կ":
                    case "ք":
                        result.Append('K');
                        break;
                    case "հ":
                        result.Append('H');
                        break;
                    case "ձ":
                        result.Append("DZ");
                        break;
                    case "ղ":
                        result.Append("GH");
                        break;
                    case "ճ":
                        result.Append("TCH");
                        break;
                    case "մ":
                        result.Append('M');
                        break;
                    case "յ":
                        result.Append('Y');
                        break;
                    case "ն":
                        result.Append('N');
                        break;
                    case "շ":
                        result.Append("SH");
                        break;
                    case "չ":
                        result.Append("CH");
                        break;
                    case "պ":
                    case "փ":
                        result.Append('P');
                        break;
                    case "ջ":
                        result.Append('J');
                        break;
                    case "ռ":
                    case "ր":
                        result.Append('R');
                        break;
                    case "ս":
                        result.Append('S');
                        break;
                    case "վ":
                        result.Append('V');
                        break;
                    case "ց":
                        result.Append('C');
                        break;
                    case "և":
                        result.Append("EV");
                        break;
                    case "օ":
                        result.Append('O');
                        break;
                    case "ֆ":
                        result.Append('F');
                        break;
                    case "ո":
                        if (nextSymbol.ToString().ToLower() == "ւ")
                            result.Append('U');
                        else if (previousSymbol == ' ')
                            result.Append("VO");
                        else
                            result.Append('O');
                        break;
                    case "ւ":
                        if (previousSymbol == 'ե')
                            result.Append('V');
                        break;
                    default:
                        result.Append(symbol);
                        break;
                }
            }
            return result.ToString();
        }

        private static bool Between(int value, int from, int to)
        {
            return (value >= from && value <= to);
        }

        private static bool IsEnglishLetter(char symbol)
        {
            int asciiCode = (int)symbol;
            return (Between(asciiCode, 97, 122) || Between(asciiCode, 65, 90));
        }
    }
}
