﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with StockAccounts
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class StockAccountController : RepositoryControllerBase<StockAccountRepository>
    {
        public StockAccountController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new StockAccountRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /StockAccount
        /// Returns the list of StockAccounts
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<StockAccount>> GetStockAccounts()
        {
            IEnumerable<StockAccount> stockAccounts = await Repository.GetStockAccounts();
            return stockAccounts;
        }

        /// <summary>
        /// Implements POST /StockAccount/SaveStockAccount
        /// Creates/updates a StockAccount
        /// </summary>
        [HttpPost("SaveStockAccount")]
        public async Task<bool> Post([FromBody] StockAccount stockAccount)
        {
            await Repository.SaveStockAccount(stockAccount);
            return true;
        }
    }
}
