﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for retrieving
    /// recent fails
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class RecentFailedActionController : RepositoryControllerBase<ActionLogRepository>
    {
        public RecentFailedActionController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new ActionLogRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /RecentFailedAction
        /// Returns the action log since last call
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<ActionLog>> GetRecentFailedActions()
        {
            IEnumerable<ActionLog> ActionLogs = await Repository.GetRecentFailedActions();
            return ActionLogs;
        }
    }
}
