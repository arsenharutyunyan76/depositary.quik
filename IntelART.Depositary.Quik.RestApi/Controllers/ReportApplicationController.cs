﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with reports
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class ReportApplicationController : RepositoryControllerBase<ReportRepository>
    {
        public ReportApplicationController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new ReportRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /ReportApplication
        /// Returns the list of report applications
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<ReportApplication>> GetReportApplicationsByDate([FromQuery]string stringFromDate, [FromQuery]string stringToDate)
        {
            IEnumerable<ReportApplication> reports = await Repository.GetReportApplicationsByDate(DeserializeDate(stringFromDate), DeserializeDate(stringToDate));
            return reports;
        }

        /// <summary>
        /// Implements POST /ReportApplication/CreateReportApplication
        /// Creates application for report creation
        /// </summary>
        [HttpPost("CreateReportApplication")]
        public async Task<Guid> Post([FromBody]ReportApplication application)
        {
            Guid id = await Repository.CreateReportApplication(application.REPORT_ID, application.REPORT_FROM_DATE, application.REPORT_TO_DATE);
            return id;
        }

        /// <summary>
        /// Implements DELETE /ReportApplication/{id}
        /// Deletes a report application with the given id
        /// </summary>
        [HttpDelete("{id}")]
        public async Task Delete(Guid id)
        {
            await Repository.DeleteReportApplication(id);
        }
    }
}

