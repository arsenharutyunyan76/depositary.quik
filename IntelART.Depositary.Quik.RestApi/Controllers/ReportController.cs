﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with reports
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class ReportController : RepositoryControllerBase<ReportRepository>
    {
        public ReportController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new ReportRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /Report
        /// Returns the list of reports
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<ReportSchedule>> GetReportSchedules()
        {
            IEnumerable<ReportSchedule> reports = await Repository.GetReportSchedules();
            return reports;
        }

        /// <summary>
        /// Implements POST /Report/SetReportTime
        /// Sets generation scheduled time and last date
        /// </summary>
        [HttpPost("SetReportTime")]
        public async Task<bool> Post([FromBody]ReportSchedule report)
        {
            await Repository.SetReportTime(report);
            return true;
        }
    }
}
