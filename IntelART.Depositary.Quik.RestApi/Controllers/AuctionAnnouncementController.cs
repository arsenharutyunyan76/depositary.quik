﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with auction announcement
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class AuctionAnnouncementController : RepositoryControllerBase<ReportRepository>
    {
        public AuctionAnnouncementController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new ReportRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /AuctionAnnouncement
        /// Returns the auction announcement
        /// </summary>
        [HttpGet]
        public async Task<AuctionAnnouncement> GetAuctionAnnouncement()
        {
            AuctionAnnouncement announcement = await Repository.GetAuctionAnnouncement();
            return announcement;
        }

        /// <summary>
        /// Implements POST /AuctionAnnouncement/SaveAuctionAnnouncement
        /// Saves the auction announcement
        /// </summary>
        [HttpPost("SaveAuctionAnnouncement")]
        public async Task<bool> Post([FromBody]AuctionAnnouncement announcement)
        {
            await Repository.SaveAuctionAnnouncement(announcement);
            return true;
        }
    }
}
