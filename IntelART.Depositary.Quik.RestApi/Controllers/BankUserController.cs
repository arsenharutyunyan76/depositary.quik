﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with BankUsers
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class BankUserController : RepositoryControllerBase<BankUserRepository>
    {
        public BankUserController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new BankUserRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /BankUser
        /// Returns the list of BankUsers
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<BankUser>> GetBankUsers()
        {
            IEnumerable<BankUser> BankUsers = await Repository.GetBankUsers();
            return BankUsers;
        }

        /// <summary>
        /// Implements POST /BankUser/SaveBankUser
        /// Creates/updates a BankUser
        /// </summary>
        [HttpPost("SaveBankUser")]
        public async Task<bool> Post([FromBody]BankUser BankUser)
        {
            await Repository.SaveBankUser(BankUser);
            return true;
        }
    }
}
