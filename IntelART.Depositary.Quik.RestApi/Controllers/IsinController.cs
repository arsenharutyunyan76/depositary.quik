﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with ISINs
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class IsinController :  RepositoryControllerBase<IsinRepository>
    {
        public IsinController(IConfigurationRoot Configuration)
           : base(Configuration, (connectionString) => new IsinRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /Isin
        /// Returns the list of ISINs
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Isin>> GetIsins()
        {
            IEnumerable<Isin> isins = await Repository.GetIsins();
            return isins;
        }

        /// <summary>
        /// Implements POST /Isin/SaveISIN
        /// Creates/updates an ISIN
        /// </summary>
        [HttpPost("SaveISIN")]
        public async Task<bool> Post([FromBody]Isin isin)
        {
            await Repository.SaveIsin(isin);
            return true;
        }
    }
}