﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for retrieving
    /// action log data
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class ActionLogController : RepositoryControllerBase<ActionLogRepository>
    {
        public ActionLogController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new ActionLogRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /ActionLog?stringFromDate={stringFromDate}&stringToDate={stringToDate}isFail={isFail}
        /// Returns the action log for the given time period
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<ActionLog>> GetActionLogs([FromQuery]string stringFromDate, [FromQuery]string stringToDate, [FromQuery]string isFail)
        {
            IEnumerable<ActionLog> ActionLogs = await Repository.GetActionLog(DeserializeDate(stringFromDate), DeserializeDate(stringToDate), DeserializeBoolean(isFail));
            return ActionLogs;
        }
    }
}
