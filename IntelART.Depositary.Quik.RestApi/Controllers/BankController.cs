﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with banks
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class BankController : RepositoryControllerBase<BankRepository>
    {
        public BankController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new BankRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /Bank
        /// Returns the list of banks
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Bank>> GetBanks()
        {
            IEnumerable<Bank> banks = await Repository.GetBanks();
            return banks;
        }

        /// <summary>
        /// Implements POST /Bank/SaveBank
        /// Creates/updates a bank
        /// </summary>
        [HttpPost("SaveBank")]
        public async Task<bool> Post([FromBody]Bank bank)
        {
            await Repository.SaveBank(bank);
            return true;
        }
    }
}
