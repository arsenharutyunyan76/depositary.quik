﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with accounts
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class AccountController : RepositoryControllerBase<AccountRepository>
    {
        public AccountController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new AccountRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /account
        /// Returns the list of accounts
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Account>> GetAccounts([FromQuery]string bankCode)
        {
            IEnumerable<Account> account = await Repository.GetAccounts(bankCode);
            return account;
        }
    }
}
