﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Repository;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for operations with currencys
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class CurrencyController : RepositoryControllerBase<CurrencyRepository>
    {
        public CurrencyController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new CurrencyRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /Currency
        /// Returns the list of currencies
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Currency>> GetCurrencies()
        {
            IEnumerable<Currency> currencies = await Repository.GetCurrencies();
            return currencies;
        }

        /// <summary>
        /// Implements POST /Currency/SaveCurrency
        /// Creates/updates a currency
        /// </summary>
        [HttpPost("SaveCurrency")]
        public async Task<bool> Post([FromBody] Currency currency)
        {
            await Repository.SaveCurrency(currency);
            return true;
        }
    }
}
