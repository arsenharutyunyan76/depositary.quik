﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.RestApi.Controllers
{
    /// <summary>
    /// Controller class to implement the API methods required for retrieving
    /// auction data
    /// </summary>
    ////[Authorize(Roles ="User,PowerUser")]
    [Route("[controller]")]
    public class AuctionController : RepositoryControllerBase<AuctionRepository>
    {
        public AuctionController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new AuctionRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements GET /Auction?stringFromDate={stringFromDate}&stringToDate={stringToDate}&isin={isin}
        /// Returns the list of auctions for the given time period
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<Auction>> GetAuctions([FromQuery]string stringFromDate, [FromQuery]string stringToDate, [FromQuery]string isin)
        {
            IEnumerable<Auction> auctions = await Repository.GetAuctions(DeserializeDate(stringFromDate), DeserializeDate(stringToDate), isin);
            return auctions;
        }

        /// <summary>
        /// Implements POST /Auction/SetAuctionDate
        /// Sets auction details, not imported from announcement
        /// </summary>
        [HttpPost("SetAuctionDate")]
        public async Task<bool> Post([FromBody]Auction auction)
        {
            await Repository.SetAuctionDate(auction);
            return true;
        }

        /// <summary>
        /// Implements POST /Auction/DeleteAuction
        /// Deletes auction details with given ID
        /// </summary>
        [HttpPost("DeleteAuction")]
        public async Task<bool> Post([FromBody]int id)
        {
            await Repository.DeleteAuction(id);
            return true;
        }
    }
}
