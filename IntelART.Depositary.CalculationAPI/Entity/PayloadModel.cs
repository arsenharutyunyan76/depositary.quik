﻿using IntelART.Depositary.CalculationAPI.Enums;
using System;

namespace IntelART.Depositary.CalculationAPI.Entity
{
    public class PayloadModel
    {
        public string Isin { get; set; }
        public DateTime Date { get; set; }
        public decimal DirtyPrice { get; set; }
        public decimal Yield { get; set; }
        public ActionType ActionType { get; set; }
        public Instrument Instrument { get; set; }
    }
}
