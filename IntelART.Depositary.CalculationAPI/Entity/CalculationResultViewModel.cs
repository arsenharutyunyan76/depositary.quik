﻿using IntelART.Depositary.CalculationAPI.Enums;

namespace IntelART.Depositary.CalculationAPI.Entity
{
    public class CalculationResultViewModel
    {
        public decimal Yield { get; set; }
        public decimal CleanPrice { get; set; }
        public ActionType ActionType { get; set; }
        public double AccruedInterest { get; set; }
        public double DirtyPrice { get; set; }
    }
}
