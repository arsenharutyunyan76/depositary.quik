﻿using System;

namespace IntelART.Depositary.CalculationAPI.Entity
{
    public class Instrument
    {
        public decimal FaceValue { get; set; }
        public string FormulaType { get; set; }
        public decimal? CouponRate { get; set; }
        public int? CouponFrequency { get; set; }
        public string Currency { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime MaturityDate { get; set; }
    }
}
