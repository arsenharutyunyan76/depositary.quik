﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using IntelART.Depositary.CalculationAPI.Entity;
using IntelART.Depositary.CalculationAPI.Enums;
using IntelART.Depositary.CalculationAPI.Extentions;
using System;

namespace IntelART.Depositary.CalculationAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CalculationController : Controller
    {
        /// <summary>
        /// Implements POST /Calculation
        /// Creates/updates a bank
        /// </summary>
        [HttpPost]
        public CalculationResultViewModel Post([FromBody]PayloadModel model)
        {
            CalculationResultViewModel resultViewModel = new CalculationResultViewModel();
            var instrument = model.Instrument;
            string nextdate = "";
            int dsc = 0;
            int e = 0;
            int a = 0;
            if (instrument.FormulaType != "ACT/360")
            {
                int count = GetFrequencyCount(instrument.CouponFrequency ?? 1);
                int diffDateInMonth = instrument.MaturityDate.MonthDifference(instrument.IssueDate);
                int TotalCouponsNumber;
                if (diffDateInMonth != 0)
                {
                    TotalCouponsNumber = (int)diffDateInMonth / count;
                }
                else
                {
                    TotalCouponsNumber = 0;
                }
                double? z = 0;
                for (int i = 0; i < TotalCouponsNumber; i++)
                {
                    if (nextdate == "")
                        nextdate = instrument.IssueDate.AddMonths(count).ToShortDateString();
                    else
                        nextdate = Convert.ToDateTime(nextdate).AddMonths(count).ToShortDateString();
                    if (Convert.ToDateTime(nextdate) > model.Date)
                    {
                        switch (instrument.FormulaType)
                        {
                            case "ACT/ACT":
                                dsc = Convert.ToDateTime(nextdate).DaysDifference(model.Date);
                                e = Convert.ToDateTime(nextdate).DaysDifference(Convert.ToDateTime(nextdate).AddMonths(count * (-1)));
                                a = e - dsc;
                                break;
                            case "30E/360":
                                var prevCoupon = Convert.ToDateTime(nextdate).AddMonths(-1 * count).ToString();
                                var nextCouponParse = Convert.ToDateTime(nextdate).ToString("yyyy-MM-dd").Split('-');
                                var prevCouponParse = Convert.ToDateTime(prevCoupon).ToString("yyyy-MM-dd").Split('-');
                                var todayDateParse = model.Date.ToString("yyyy-MM-dd").Split('-');
                                var nYear = nextCouponParse[0];
                                var nMonth = nextCouponParse[1];
                                var nDay = nextCouponParse[2];
                                var pYear = prevCouponParse[0];
                                var pMonth = prevCouponParse[1];
                                var pDay = prevCouponParse[2];
                                var tYear = todayDateParse[0];
                                var tMonth = todayDateParse[1];
                                var tDay = todayDateParse[2];
                                e = (Convert.ToInt32(nYear) - Convert.ToInt32(pYear)) * 360 + (Convert.ToInt32(nMonth) - Convert.ToInt32(pMonth)) * 30 + (Convert.ToInt32(nDay) - Convert.ToInt32(pDay));
                                a = (Convert.ToInt32(tYear) - Convert.ToInt32(pYear)) * 360 + (Convert.ToInt32(tMonth) - Convert.ToInt32(pMonth)) * 30 + (Convert.ToInt32(tDay) - Convert.ToInt32(pDay));
                                dsc = (Convert.ToInt32(nYear) - Convert.ToInt32(tYear)) * 360 + (Convert.ToInt32(nMonth) - Convert.ToInt32(tMonth)) * 30 + (Convert.ToInt32(nDay) - Convert.ToInt32(tDay));
                                break;
                        }
                        int couponNumber = TotalCouponsNumber - i;
                        double t = (double)dsc / e;
                        decimal price = (decimal)0.000;
                        var par_value = instrument.FaceValue;
                        var couponPercent = instrument.CouponRate;
                        double accruedInterest = (double)instrument.FaceValue * (double)instrument.CouponRate * a / e / (double)instrument.CouponFrequency;
                        double cleanPrice = (double)model.DirtyPrice - accruedInterest;
                        double dirtyPrice = (double)model.DirtyPrice;

                        switch (model.ActionType)
                        {
                            case ActionType.CleanPrice:
                                {
                                    for (var j = 0.5; (double)price < dirtyPrice;)
                                    {
                                        j -= 0.01;
                                        var nominator = (double)par_value * ((double)couponPercent * (double)Math.Pow((double)(1 + j / instrument.CouponFrequency), (double)couponNumber) - (double)couponPercent + (double)j);
                                        var denominator = j * Math.Pow((1 + j / instrument.CouponFrequency ?? 1), (couponNumber - 1 + t));
                                        price = (decimal)(nominator / denominator);
                                        if ((double)price >= dirtyPrice)
                                        {
                                            for (var x = j + 0.01; j < 0.5;)
                                            {
                                                x -= 0.0001;
                                                nominator = (double)par_value * ((double)couponPercent * Math.Pow((double)(1 + x / instrument.CouponFrequency), (double)couponNumber) - (double)couponPercent + (double)x);
                                                denominator = x * Math.Pow((double)(1 + x / instrument.CouponFrequency), (couponNumber - 1 + t));

                                                price = (decimal)(nominator / denominator);
                                                if ((double)price >= dirtyPrice)
                                                {
                                                    for (z = x + 0.0001; j < 0.5;)
                                                    {
                                                        z -= 0.000000001;
                                                        nominator = (double)par_value * ((double)couponPercent * Math.Pow((double)(1 + z / instrument.CouponFrequency), (double)couponNumber) - (double)couponPercent + (double)z);
                                                        denominator = (double)z * Math.Pow((double)(1 + z / instrument.CouponFrequency), (couponNumber - 1 + t));
                                                        price = (decimal)(nominator / denominator);
                                                        if ((double)price >= dirtyPrice)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    //resultViewModel.ActionType = Data.Enums.ActionType.CleanPrice;
                                    //resultViewModel.Yield = (decimal)(z ?? 0) * 100;
                                    return new CalculationResultViewModel
                                    {
                                        Yield = Math.Round((decimal)(z ?? 0) * 100, 4),
                                        ActionType = ActionType.CleanPrice,
                                        AccruedInterest = accruedInterest,
                                        DirtyPrice = dirtyPrice,
                                        CleanPrice = (decimal)cleanPrice
                                    };
                                    //break;
                                }
                            case ActionType.Yield:
                                {
                                    double nominator = (double)par_value * ((double)couponPercent * Math.Pow((double)(1 + model.Yield / 100 / instrument.CouponFrequency), (double)couponNumber) - (double)couponPercent + (double)model.Yield / 100);
                                    double denominator = (double)model.Yield / 100 * Math.Pow((double)(1 + model.Yield / 100 / instrument.CouponFrequency), (couponNumber - 1 + t));
                                    price = (decimal)(nominator / denominator);
                                    return new CalculationResultViewModel
                                    {
                                        DirtyPrice = (double)price,
                                        ActionType = ActionType.Yield,
                                        AccruedInterest = accruedInterest,
                                        CleanPrice = Math.Round((decimal)price - (decimal)accruedInterest, 4),
                                        Yield = model.Yield
                                    };
                                    //resultViewModel.ActionType = Data.Enums.ActionType.Yield;
                                    //resultViewModel.Yield = price;
                                    //break;
                                }
                        }
                    }
                }
                return resultViewModel;
            }
            else
            {
                int dsn = instrument.MaturityDate.DaysDifference(model.Date);
                int dcc = 360;
                double price = 0;
                switch (model.ActionType)
                {

                    case ActionType.Yield:
                        {
                            double CleanPrice = (double)instrument.FaceValue / (1 + ((double)model.Yield / 100 * dsn / dcc));
                            //resultViewModel.ActionType = Data.Enums.ActionType.Yield;
                            //resultViewModel.CleanPrice = (decimal)CleanPrice;
                            return new CalculationResultViewModel
                            {
                                CleanPrice = Math.Round((decimal)CleanPrice, 4),
                                ActionType = ActionType.Yield,
                                DirtyPrice = Math.Round(CleanPrice, 4),
                                Yield = model.Yield,
                                AccruedInterest = 0,
                            };
                            //break;
                        }
                    case ActionType.CleanPrice:
                        {
                            double CleanPrice = (double)model.DirtyPrice;
                            double l = 0;
                            for (double i = 0.5; price < CleanPrice;)
                            {
                                i -= 0.01;
                                price = (double)instrument.FaceValue / (1 + (i * dsn / dcc));
                                if (price >= CleanPrice)
                                {
                                    for (l = i + 0.01; l < 0.5;)
                                    {
                                        l -= 0.0000001;
                                        price = (double)instrument.FaceValue / (1 + (l * dsn / dcc));
                                        if (price >= (double)CleanPrice)
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                }

                            }
                            //resultViewModel.Yield = (decimal)l * 100;
                            //resultViewModel.ActionType = Data.Enums.ActionType.CleanPrice;
                            //break;
                            return new CalculationResultViewModel
                            {
                                Yield = Math.Round((decimal)l * 100, 4),
                                ActionType = ActionType.CleanPrice,
                                CleanPrice = (decimal)CleanPrice,
                                DirtyPrice = (double)model.DirtyPrice,
                                AccruedInterest = 0,
                            };
                        }
                }
                return resultViewModel;
            }
        }

        private int GetFrequencyCount(int CouponFrequency)
        {
            return 12 / CouponFrequency;
        }
    }
}
