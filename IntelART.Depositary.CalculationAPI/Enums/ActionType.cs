﻿namespace IntelART.Depositary.CalculationAPI.Enums
{
    public enum ActionType
    {
        Yield = 0,
        CleanPrice = 1
    }
}
