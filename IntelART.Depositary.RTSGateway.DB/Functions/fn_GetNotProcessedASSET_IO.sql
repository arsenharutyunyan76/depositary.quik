﻿if exists (select * from sys.objects where name='fn_GetNotProcessedASSET_IO' and type='TF')
	drop function fn_GetNotProcessedASSET_IO
GO

CREATE FUNCTION fn_GetNotProcessedASSET_IO()

RETURNS @tmp
TABLE (
	id				numeric(10, 0) ,
	rev				numeric(10, 0) ,
	moment			datetime ,
	firm_name		varchar(7) ,
	settl_pair		varchar(7) ,
	depo_accounts	varchar(3) ,
	name			varchar(7) ,
	i_code			varchar(12) ,
	active_type		char(1) ,
	code			varchar(7) ,
	acct_num		varchar(23) ,
	free			numeric(26, 2) ,
	opcode			char(1) ,
	ext_id			numeric(10, 0) ,
	status			tinyint 
)
AS
BEGIN
	INSERT INTO @tmp(id,rev,moment,firm_name,settl_pair,depo_accounts,name,i_code,active_type,code,acct_num,free,opcode,ext_id,status)
	SELECT   X.id 				
			,X.rev 				
			,X.moment 			
			,X.firm_name 		
			,X.settl_pair 		
			,X.depo_accounts 	
			,X.name 				
			,X.i_code 			
			,X.active_type 		
			,X.code 				
			,X.acct_num 			
			,X.free 				
			,X.opcode 			
			,X.ext_id 			
			,X.status 			
	FROM [Armex_9.5_Live].dbo.AssetIO AS X
	LEFT JOIN dbo.PROCESSED_ASSET_IO g with (nolock)
		ON X.id=g.ID
	WHERE g.ID is null

    RETURN
END
GO
