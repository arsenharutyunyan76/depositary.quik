﻿if exists (select * from sys.objects where name='sp_GetNotProcessedTRADE_HIST' and type='P')
	drop procedure sp_GetNotProcessedTRADE_HIST
GO

create procedure sp_GetNotProcessedTRADE_HIST
AS

	SELECT  X.id				AS ID,
			X.status			AS STATUS,
			X.issue_name		AS ISSUE_NAME,
			X.price				AS PRICE,
			X.history_id		AS HISTORY_ID,
			X.history_action	AS HISTORY_ACTION,
			X.history_moment	AS HISTORY_MOMENT,
			X.lstg				AS LSTG,
			X.qty				AS QTY,
			X.volume			AS VOLUME,
			X.type_ext			AS TYPE_EXT,
			X.rate				AS RATE,
			X.delivery_date		AS DELIVERY_DATE,
			X.new_delivery_date	AS NEW_DELIVERY_DATE,
			X.init_name			AS INIT_NAME,
			X.init_wks			AS INIT_WKS,
			X.init_action		AS INIT_ACTION,
			X.init_pair			AS INIT_PAIR,
			X.conf_name			AS CONF_NAME,
			X.conf_quoteID		AS CONF_QUOTEID,
			X.trade_ref			AS TRADE_REF,
			X.repo_action		AS REPO_ACTION,
			X.conf_action		AS CONF_ACTION,
			X.conf_pair			AS CONF_PAIR,
			X.trade_moment		AS TRADE_MOMENT,
			X.price_currency	AS PRICE_CURRENCY,
			X.type				AS TYPE,
			X.conf_wks			AS CONF_WKS,
			X.price_rate		AS PRICE_RATE,
			X.history_event		AS HISTORY_EVENT,
			X.depo_accounts		AS DEPO_ACCOUNTS
	FROM [Armex_9.5_Live].dbo.Trade_hist AS X
	LEFT JOIN dbo.PROCESSED_TRADE_HIST g with (nolock)
		ON X.id=g.ID
	WHERE g.ID is null
GO
