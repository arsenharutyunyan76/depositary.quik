﻿if exists (select * from sys.objects where name='sp_ProcessASSET_IO' and type='P')
	drop procedure sp_ProcessASSET_IO
GO

create procedure sp_ProcessASSET_IO( @ID varchar(50) )

AS
	INSERT INTO dbo.PROCESSED_ASSET_IO(ID)
	VALUES (@ID)
GO
