﻿if exists (select * from sys.objects where name='sp_GetNotProcessedQUOTE_HIST' and type='P')
	drop procedure sp_GetNotProcessedQUOTE_HIST
GO

create procedure sp_GetNotProcessedQUOTE_HIST
AS

	SELECT  X.id				AS ID,
			X.status			AS STATUS,
			X.issue_name		AS ISSUE_NAME,
			X.price				AS PRICE,
			X.history_id		AS HISTORY_ID,
			X.history_action	AS HISTORY_ACTION,
			X.history_moment	AS HISTORY_MOMENT,
			X.lstg				AS LSTG,
			X.init_qty			AS INIT_QTY,
			X.moment			AS MOMENT,
			X.firm_name			AS FIRM_NAME,
			X.wks				AS WKS,
			X.settl_pair		AS SETTL_PAIR,
			X.order_type		AS ORDER_TYPE,
			X.all_non			AS ALL_NON,
			X.leave				AS LEAVE,
			X.catalyst			AS CATALYST,
			X.type				AS TYPE,
			X.yield				AS YIELD,
			X.history_event		AS HISTORY_EVENT,
			X.depo_accounts		AS DEPO_ACCOUNTS,
			X.qty				AS QUOTE_QTY
	FROM [Armex_9.5_Live].dbo.Quote_hist AS X
	LEFT JOIN dbo.PROCESSED_QUOTE_HIST g with (nolock)
		ON X.id=g.ID
	WHERE g.ID is null
GO
