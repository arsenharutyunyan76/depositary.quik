﻿if exists (select * from sys.objects where name='sp_ProcessQUOTE_HIST' and type='P')
	drop procedure sp_ProcessQUOTE_HIST
GO

create procedure sp_ProcessQUOTE_HIST( @ID	INT )

AS
	INSERT INTO dbo.PROCESSED_QUOTE_HIST(ID)
	VALUES (@ID)
GO
