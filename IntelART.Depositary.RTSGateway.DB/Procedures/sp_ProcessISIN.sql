﻿if exists (select * from sys.objects where name='sp_ProcessISIN' and type='P')
	drop procedure sp_ProcessISIN
GO

create procedure sp_ProcessISIN(
	@RTS_CODE		varchar(20),
	@ISIN_CODE		varchar(20)
)
AS
	INSERT INTO PROCESSED_ISIN
		(RTS_CODE
		,ISIN_CODE)
	VALUES
		(@RTS_CODE
		,@ISIN_CODE)
GO
