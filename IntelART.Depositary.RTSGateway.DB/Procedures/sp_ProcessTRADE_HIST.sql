﻿if exists (select * from sys.objects where name='sp_ProcessTRADE_HIST' and type='P')
	drop procedure sp_ProcessTRADE_HIST
GO

create procedure sp_ProcessTRADE_HIST( @ID	INT )

AS
	INSERT INTO dbo.PROCESSED_TRADE_HIST(ID)
	VALUES (@ID)
GO
