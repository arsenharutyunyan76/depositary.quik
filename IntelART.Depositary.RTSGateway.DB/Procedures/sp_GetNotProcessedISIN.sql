﻿if exists (select * from sys.objects where name='sp_GetNotProcessedISIN' and type='P')
	drop procedure sp_GetNotProcessedISIN
GO

create procedure sp_GetNotProcessedISIN
AS
	select distinct X.RTS_CODE
		,X.ISIN_CODE
		,X.CURRENCY
		,X.NOMINAL
		,X.REGISTRATION_NUMBER
		,X.ISSUE_DATE
		,X.MATURITY_DATE
	from
	(
	SELECT  name		AS RTS_CODE,
			ISIN		AS ISIN_CODE,
			currency	AS CURRENCY,
			nominal		AS NOMINAL,
			registration_number AS REGISTRATION_NUMBER,
			listlogdate	AS ISSUE_DATE,
			cutoffdate	AS MATURITY_DATE
	FROM [Armex_9.5_Live].dbo.Issue
	--WHERE convert(date,cutoffdate)>=convert(date,getdate())

	union all

	SELECT  name		AS RTS_CODE,
			ISIN		AS ISIN_CODE,
			currency	AS CURRENCY,
			nominal		AS NOMINAL,
			registration_number AS REGISTRATION_NUMBER,
			listlogdate	AS ISSUE_DATE,
			cutoffdate	AS MATURITY_DATE
	FROM [Armex_9.5_Live].dbo.Issue_hist
	--WHERE convert(date,cutoffdate)>=convert(date,getdate())
	) as X
	LEFT JOIN dbo.PROCESSED_ISIN g with (nolock)
		ON X.RTS_CODE=g.RTS_CODE COLLATE SQL_Latin1_General_CP1_CS_AS and X.ISIN_CODE=g.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
	WHERE g.ISIN_CODE is null

GO
