@echo off

set ConfigFile=config.ini
SET DatabaseName=RTSGateway
for /F "usebackq eol=;" %%s in ("%ConfigFile%") do set %%s

SET SQLEXEC=sqlcmd

IF DEFINED DB_ServerName SET SQLEXEC=%SQLEXEC% -S %DB_ServerName%
IF DEFINED DB_Username SET SQLEXEC=%SQLEXEC% -U %DB_Username%
IF DEFINED DB_Password SET SQLEXEC=%SQLEXEC% -P %DB_Password%
IF DEFINED DB_Name SET SQLEXEC=%SQLEXEC% -v DatabaseName=%DB_Name%

ECHO %SQLEXEC%

IF DEFINED RecreateDB %SQLEXEC% -Q "DROP DATABASE %DB_Name%"
IF DEFINED RecreateDB %SQLEXEC% -i Database.sql

for %%G in (Tables/*.sql) do %SQLEXEC% -d %DB_Name% -i "Tables/%%G"
for %%G in (Procedures/*.sql) do %SQLEXEC% -d %DB_Name% -i "Procedures/%%G"
