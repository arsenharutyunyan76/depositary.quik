﻿if exists (select * from sys.objects where name='PROCESSED_TRADE_HIST' and type='U')
	drop table dbo.PROCESSED_TRADE_HIST
GO

CREATE TABLE dbo.PROCESSED_TRADE_HIST(
	ID				INT	NOT NULL,
	PROCESSED_DATE	datetime	NOT NULL default getdate()
)
GO

CREATE UNIQUE INDEX iTRADE_HIST1 ON dbo.PROCESSED_TRADE_HIST(ID)
GO
