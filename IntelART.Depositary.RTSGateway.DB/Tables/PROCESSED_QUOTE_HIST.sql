﻿if exists (select * from sys.objects where name='PROCESSED_QUOTE_HIST' and type='U')
	drop table dbo.PROCESSED_QUOTE_HIST
GO

CREATE TABLE dbo.PROCESSED_QUOTE_HIST(
	ID				INT	NOT NULL,
	PROCESSED_DATE	datetime	NOT NULL default getdate()
)
GO

CREATE UNIQUE INDEX iQUOTE_HIST1 ON dbo.PROCESSED_QUOTE_HIST(ID)
GO
