﻿if exists (select * from sys.objects where name='PROCESSED_ASSET_IO' and type='U')
	drop table dbo.PROCESSED_ASSET_IO
GO

CREATE TABLE dbo.PROCESSED_ASSET_IO(
	ID				varchar(50)	NOT NULL,
	PROCESSED_DATE	datetime	NOT NULL default getdate()
)
GO

CREATE UNIQUE INDEX iASSET_IO1 ON dbo.PROCESSED_ASSET_IO(ID)
GO
