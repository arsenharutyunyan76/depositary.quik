﻿if exists (select * from sys.objects where name='PROCESSED_ISIN' and type='U')
	drop table dbo.PROCESSED_ISIN
GO

CREATE TABLE dbo.PROCESSED_ISIN(
	RTS_CODE		varchar(20) COLLATE SQL_Latin1_General_CP1_CS_AS	NOT NULL,
	ISIN_CODE		varchar(20) COLLATE SQL_Latin1_General_CP1_CS_AS	NOT NULL,
	PROCESSED_DATE	datetime	NOT NULL default getdate()
)
GO

CREATE UNIQUE INDEX iISIN ON dbo.PROCESSED_ISIN(RTS_CODE,ISIN_CODE)
GO
