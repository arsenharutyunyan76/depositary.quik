﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.LotusAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : RepositoryControllerBase<MessageRecipientsRepository>
    {
        public MessageController(IConfigurationRoot Configuration)
            : base(Configuration, (connectionString) => new MessageRecipientsRepository(connectionString))
        {
        }

        /// <summary>
        /// Implements POST /Message/SaveMessage
        /// Creates/updates a bank
        /// </summary>
        [HttpPost("SaveMessage")]
        public async Task<bool> Post([FromBody]MessageRecipients message)
        {
            await Repository.SaveMessageRecipients(message);
            return true;
        }
    }
}
