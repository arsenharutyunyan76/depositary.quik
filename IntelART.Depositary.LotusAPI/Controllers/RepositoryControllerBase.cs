﻿using System;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Configuration;

namespace IntelART.Depositary.LotusAPI.Controllers
{
    public abstract class RepositoryControllerBase<T> : ControllerBase
        where T : BaseRepository
    {
        private Lazy<T> repositoryFactory;
        private string connectionString;
        protected string languageCode;

        protected T Repository
        {
            get
            {
                return this.repositoryFactory.Value;
            }
        }

        public RepositoryControllerBase(IConfigurationRoot Configuration, Func<string, T> repositoryInitializer)
        {
            this.connectionString = Configuration.GetSection("ConnectionStrings")["IntegrationDB"];
            this.repositoryFactory = new Lazy<T>(() => repositoryInitializer(this.connectionString), true);
            this.languageCode = this.GetLanguageCode();
        }

        protected DateTime DeserializeDate(string stringDate)
        {
            return new DateTime(int.Parse(stringDate.Substring(0, 4)), int.Parse(stringDate.Substring(4, 2)), int.Parse(stringDate.Substring(6, 2)));
        }

        protected bool? DeserializeBoolean(string stringBoolean)
        {
            if (string.IsNullOrWhiteSpace(stringBoolean))
                return null;
            else if (stringBoolean == "0" || stringBoolean.ToLower() == "false")
                return false;
            else
                return true;
        }
    }
}
