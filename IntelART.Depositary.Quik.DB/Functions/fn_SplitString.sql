﻿if exists (select * from sys.objects where name='fn_SplitString' and type='TF')
	drop function fn_SplitString
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION fn_SplitString
(
    @delimitedString NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
)
RETURNS @tmp
TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
	DECLARE @xml XML = N'<t>' + REPLACE(@delimitedString, @delimiter,'</t><t>') + '</t>'
    INSERT INTO @tmp(val)
    SELECT RTRIM(LTRIM(r.value('.','varchar(MAX)'))) as item
    FROM @xml.nodes('/t') as records(r)
    RETURN
END
GO
