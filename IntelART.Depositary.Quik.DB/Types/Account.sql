﻿if (not exists (select * from sys.types where name='Account'))
	CREATE TYPE Account AS TABLE
	(
		ID					int,
		BANK_CODE			char(5),
		CODE				varchar(16),
		CURRENCY			varchar(4),
		IS_OWN				bit,
		CUSTOMER_CODE		varchar(20),
		SECURITY_ACCOUNT	varchar(16)
	)
GO
