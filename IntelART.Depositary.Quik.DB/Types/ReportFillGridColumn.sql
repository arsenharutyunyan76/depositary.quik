﻿if (not exists (select * from sys.types where name='ReportFillGridColumn'))
	CREATE TYPE ReportFillGridColumn AS TABLE
	(
		CODE				nvarchar(50)	NOT NULL,
		COLUMN_NUMBER		int				NOT NULL
	)
GO
