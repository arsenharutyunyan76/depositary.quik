﻿IF (NOT EXISTS (SELECT * FROM sys.types WHERE NAME='IsinCompact'))
	CREATE TYPE IsinCompact AS TABLE
	(	
		ID					int,
		ISIN_CODE			varchar(20) COLLATE SQL_Latin1_General_CP1_CS_AS, 
		REGISTRATION_NUMBER	varchar(50),	
		FREQUENCY			int,			
		RATE				money,		
		FORMULA				varchar(20)	
	)
GO
