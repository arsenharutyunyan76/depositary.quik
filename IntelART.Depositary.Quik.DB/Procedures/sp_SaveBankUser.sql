﻿if exists (select * from sys.objects where name='sp_SaveBankUser' and type='P')
	drop procedure sp_SaveBankUser
GO

create procedure sp_SaveBankUser(
	@BANK_CODE	char(5),
	@USER_ID	int,
	@USER_CODE	varchar(20)
)
AS
	if exists (select USER_CODE from BANK_USER where BANK_CODE=@BANK_CODE and USER_ID=@USER_ID)
		update BANK_USER
		set USER_CODE=@USER_CODE
		where BANK_CODE=@BANK_CODE and USER_ID=@USER_ID
	else
		insert into BANK_USER (BANK_CODE,USER_ID,USER_CODE)
			values (@BANK_CODE,@USER_ID,@USER_CODE)
GO
