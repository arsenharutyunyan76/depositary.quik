﻿if exists (select * from sys.objects where name='sp_GetTradesForRegisters' and type='P')
	drop procedure sp_GetTradesForRegisters
GO

create procedure sp_GetTradesForRegisters(
	@FromDate	date,
	@ToDate		date
)
AS
	select ID
		,TRADE_NUMBER
		,TRADE_DATE
		,ISIN
		,AUCTION_TYPE_ID
		,OPERATION_TYPE_ID
		,VALUE
		,YIELD
		,PRICE
		,QUANTITY
		,SETTLE_DATE
		,BANK_CODE
		,CP_BANK_CODE
		,CLIENT_CODE
		,BROKER_REFERENCE
		,USER_ID
	from TRADE
	where convert(date,TRADE_DATE) between isnull(@FromDate,'2001-01-01') and @ToDate
	order by TRADE_DATE
GO
