﻿if exists (select * from sys.objects where name='sp_GetSameAuctionTradesCount' and type='P')
	drop procedure sp_GetSameAuctionTradesCount
GO

create procedure sp_GetSameAuctionTradesCount(
	@TRADE_DATE			date,
	@ISIN				char(12),
	@AUCTION_TYPE_ID	tinyint
)
AS
	SELECT count(ID)
	FROM TRADE
	WHERE PROCESS_STATUS_ID=1
		and convert(date,TRADE_DATE)=@TRADE_DATE
		and ISIN=@ISIN
		and AUCTION_TYPE_ID=@AUCTION_TYPE_ID
GO
