﻿if exists (select * from sys.objects where name='sp_SetBankRegisterNumber' and type='P')
	drop procedure sp_SetBankRegisterNumber
GO

create procedure sp_SetBankRegisterNumber(
	@CODE				char(5),
	@REGISTER_NUMBER	int
)
AS
	UPDATE BANK
	SET REGISTER_NUMBER=@REGISTER_NUMBER
	WHERE CODE=@CODE
GO
