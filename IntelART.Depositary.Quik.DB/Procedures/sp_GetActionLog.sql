﻿if exists (select * from sys.objects where name='sp_GetActionLog' and type='P')
	drop procedure sp_GetActionLog
GO

create procedure sp_GetActionLog(
	@DATE_FROM	date,
	@DATE_TO	date,
	@IS_FAIL	bit = null
)
AS
	select a.CREATION_DATE,a.ID,a.ACTION_TYPE_ID,a.IS_FAIL,a.DETAILS
		,t.NAME_AM as ACTION_TYPE_NAME_AM,t.NAME_EN as ACTION_TYPE_NAME_EN
	from ACTION_LOG a
	join ACTION_TYPE t
		on a.ACTION_TYPE_ID=t.ID
	where a.CREATION_DATE between @DATE_FROM and @DATE_TO
		and isnull(@IS_FAIL,a.IS_FAIL)=a.IS_FAIL
	order by a.CREATION_DATE desc
GO
