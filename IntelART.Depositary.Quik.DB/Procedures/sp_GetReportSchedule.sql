﻿if exists (select * from sys.objects where name='sp_GetReportSchedule' and type='P')
	drop procedure sp_GetReportSchedule
GO

create procedure sp_GetReportSchedule(@ID	tinyint)
AS
	select ID
		,NAME_AM
		,NAME_EN
		,GENERATION_TIME
		,LAST_GENERATE_DATE
		,EMAIL
		,EMAIL_SUBJECT
		,EMAIL_BODY
	from REPORT_SCHEDULE
	where ID=@ID
GO
