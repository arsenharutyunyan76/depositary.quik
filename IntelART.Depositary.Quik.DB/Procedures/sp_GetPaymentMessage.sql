﻿if exists (select * from sys.objects where name='sp_GetPaymentMessage' and type='P')
	drop procedure sp_GetPaymentMessage
GO

create procedure sp_GetPaymentMessage(
	@MESSAGE_DATE_FROM	date,
	@MESSAGE_DATE_TO	date
)
AS
	select ID,
			MESSAGE_TYPE,
			DOCUMENT_NUMBER,
			SECURITY_ID,
			CURRENCY,
			AMOUNT,
			PAYER,
			RECEIVER
	from PAYMENT_MESSAGE
	where convert(date,CREATION_DATE) between ISNULL(@MESSAGE_DATE_FROM,convert(date,CREATION_DATE)) and ISNULL(@MESSAGE_DATE_TO,convert(date,CREATION_DATE))
			AND PROCESSED=0
GO
