﻿if exists (select * from sys.objects where name='sp_SaveReportFillGrid' and type='P')
	drop procedure sp_SaveReportFillGrid
GO
create procedure sp_SaveReportFillGrid(
	@REPORT_ID		tinyint,
	@EXCEL_SHEET	tinyint,
	@CODE			nvarchar(50),
	@ROW_NUMBER		int,
	@COLUMNS		ReportFillGridColumn	readonly
)
AS
	declare @ID int
	select @ID=ID from REPORT_FILL_GRID where REPORT_ID=@REPORT_ID and CODE=@CODE
	if @ID is null
		insert into REPORT_FILL_GRID (REPORT_ID,EXCEL_SHEET,CODE,ROW_NUMBER)
		values (@REPORT_ID,@EXCEL_SHEET,@CODE,@ROW_NUMBER)
	else
	begin
		update REPORT_FILL_GRID
		set	EXCEL_SHEET = @EXCEL_SHEET
			,ROW_NUMBER = @ROW_NUMBER
		where ID=@ID

		delete from REPORT_FILL_GRID_COLUMN
		where REPORT_FILL_GRID_ID=@ID
	end

	insert into REPORT_FILL_GRID_COLUMN (REPORT_FILL_GRID_ID,CODE,COLUMN_NUMBER)
	select @ID,CODE,COLUMN_NUMBER from @COLUMNS
GO
