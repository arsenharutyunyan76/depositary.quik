﻿if exists (select * from sys.objects where name='sp_SaveStockAccount' and type='P')
	drop procedure sp_SaveStockAccount
GO

create procedure sp_SaveStockAccount(
	@CURRENCY	varchar(4),
	@ACCOUNT	varchar(16)
)
AS
	if exists (select * from STOCK_ACCOUNT where CURRENCY=@CURRENCY)
		update STOCK_ACCOUNT
		set ACCOUNT=@ACCOUNT
		where CURRENCY=@CURRENCY
	else
		insert into STOCK_ACCOUNT (CURRENCY,ACCOUNT)
			values (@CURRENCY,@ACCOUNT)
GO
