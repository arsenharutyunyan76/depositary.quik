﻿if exists (select * from sys.objects where name='sp_SaveBank' and type='P')
	drop procedure sp_SaveBank
GO

create procedure sp_SaveBank(
	@CODE		char(5),
	@SHORT_NAME	varchar(20),
	@IS_ACTIVE	bit,
	@NAME_AM	nvarchar(50),
	@NAME_EN	varchar(50),
	@AS_CODE	varchar(8),
	@EMAIL		nvarchar(max) = null,
	@ACCOUNTS	Account	readonly
)
AS
	BEGIN TRANSACTION
	BEGIN TRY
		if exists (select CODE from BANK where CODE=@CODE)
			update BANK
			set SHORT_NAME=@SHORT_NAME
				,IS_ACTIVE=@IS_ACTIVE
				,NAME_AM=@NAME_AM
				,NAME_EN=@NAME_EN
				,AS_CODE=@AS_CODE
				,EMAIL=@EMAIL
			where CODE=@CODE
		else
			insert into BANK (CODE,SHORT_NAME,IS_ACTIVE,NAME_AM,NAME_EN,AS_CODE,EMAIL)
				values (@CODE,@SHORT_NAME,@IS_ACTIVE,@NAME_AM,@NAME_EN,@AS_CODE,@EMAIL)

		delete from ACCOUNT where BANK_CODE=@CODE
		insert into ACCOUNT
			(BANK_CODE, CODE, CURRENCY, IS_OWN, CUSTOMER_CODE, SECURITY_ACCOUNT)
		select @CODE, CODE, CURRENCY, IS_OWN, CUSTOMER_CODE, SECURITY_ACCOUNT
			from @ACCOUNTS
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		declare @ErrorMessage varchar(4000)
		set @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 17, 1)
		RETURN
	END CATCH
	COMMIT TRANSACTION
GO
