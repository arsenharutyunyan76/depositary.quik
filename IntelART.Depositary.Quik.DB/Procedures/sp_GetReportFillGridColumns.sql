﻿if exists (select * from sys.objects where name='sp_GetReportFillGridColumns' and type='P')
	drop procedure sp_GetReportFillGridColumns
GO
create procedure sp_GetReportFillGridColumns(
	@REPORT_FILL_GRID_ID int
)
AS
	select	CODE,
			COLUMN_NUMBER
	from REPORT_FILL_GRID_COLUMN
	where REPORT_FILL_GRID_ID = @REPORT_FILL_GRID_ID
GO
