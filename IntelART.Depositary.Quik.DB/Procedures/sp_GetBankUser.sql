﻿if exists (select * from sys.objects where name='sp_GetBankUser' and type='P')
	drop procedure sp_GetBankUser
GO

create procedure sp_GetBankUser(
	@BANK_CODE	char(5),
	@USER_ID	int
)
AS
	select USER_CODE
	from BANK_USER
	where BANK_CODE=@BANK_CODE
		and USER_ID=@USER_ID
GO
