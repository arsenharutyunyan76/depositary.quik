﻿if exists (select * from sys.objects where name='sp_SaveBankIsinDiscount' and type='P')
	drop procedure sp_SaveBankIsinDiscount
GO

create procedure sp_SaveBankIsinDiscount(
	@BANK_CODE	char(5),
	@ISIN_CODE	varchar(20),
	@DISCOUNT	decimal(8,5),
	@IS_ACTIVE	bit
)
AS
	if exists (select * from BANK_ISIN_DISCOUNT where BANK_CODE=@BANK_CODE and ISIN_CODE=@ISIN_CODE)
		update BANK_ISIN_DISCOUNT
		set DISCOUNT=@DISCOUNT ,
			IS_ACTIVE=@IS_ACTIVE
		where BANK_CODE=@BANK_CODE and ISIN_CODE=@ISIN_CODE
	else
		insert into BANK_ISIN_DISCOUNT (BANK_CODE,ISIN_CODE,DISCOUNT,IS_ACTIVE)
			values (@BANK_CODE,@ISIN_CODE,@DISCOUNT,@IS_ACTIVE)

GO
