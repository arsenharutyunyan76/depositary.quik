﻿IF EXISTS (SELECT * FROM sys.objects WHERE NAME='sp_UpdateActionTypeLastOperationDate' and TYPE='P')
	DROP PROCEDURE sp_UpdateActionTypeLastOperationDate
GO


CREATE PROCEDURE sp_UpdateActionTypeLastOperationDate(
	@Id	AS	smallint
)
AS
	UPDATE ACTION_TYPE 
	SET LAST_OPERATION_DATE = GETDATE()
	WHERE ID = @Id
GO