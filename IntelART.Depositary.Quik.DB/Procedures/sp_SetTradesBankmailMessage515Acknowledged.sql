﻿if exists (select * from sys.objects where name='sp_SetTradesBankmailMessage515Acknowledged' and type='P')
	drop procedure sp_SetTradesBankmailMessage515Acknowledged
GO

create procedure sp_SetTradesBankmailMessage515Acknowledged(
	@BANKMAIL_MESSAGE_515_ID	uniqueidentifier
)
AS
	update TRADE
	set PROCESS_STATUS_ID=4
	where BANKMAIL_MESSAGE_515_ID=@BANKMAIL_MESSAGE_515_ID
GO
