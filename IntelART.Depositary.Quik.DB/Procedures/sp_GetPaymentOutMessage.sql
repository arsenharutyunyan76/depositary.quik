﻿if exists (select * from sys.objects where name='sp_GetPaymentOutMessage' and type='P')
	drop procedure sp_GetPaymentOutMessage
GO

create procedure sp_GetPaymentOutMessage(
	@MESSAGE_DATE_FROM	date,
	@MESSAGE_DATE_TO	date
)
AS
	select ID,
			MESSAGE_TYPE,
			DOCUMENT_NUMBER,
			SECURITY_ID,
			CURRENCY,
			INITIAL_AMOUNT,
			AMOUNT,
			PAYER,
			RECEIVER,
			RECEIVER_NAME,
			IS_FINAL_CALCULATION,
			OPCODE,
			CODE,
			MOMENT
	from PAYMENT_OUT_MESSAGE
	where convert(date,CREATION_DATE) between ISNULL(@MESSAGE_DATE_FROM,convert(date,CREATION_DATE)) and ISNULL(@MESSAGE_DATE_TO,convert(date,CREATION_DATE))
			AND PROCESSED=0
GO
