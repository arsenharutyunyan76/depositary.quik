﻿if exists (select * from sys.objects where name='sp_SetTradeBankmailMessage515' and type='P')
	drop procedure sp_SetTradeBankmailMessage515
GO

create procedure sp_SetTradeBankmailMessage515(
	@ID							uniqueidentifier,
	@BANKMAIL_MESSAGE_515_ID	uniqueidentifier
)
AS
	update TRADE
	set BANKMAIL_MESSAGE_515_ID=@BANKMAIL_MESSAGE_515_ID
		,PROCESS_STATUS_ID=3
	where ID=@ID
GO
