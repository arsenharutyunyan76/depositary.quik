﻿if exists (select * from sys.objects where name='sp_GetTradesForAuction' and type='P')
	drop procedure sp_GetTradesForAuction
GO

create procedure sp_GetTradesForAuction(@AUCTION_ID	varchar(16))
AS
	select t.ID
		,t.TRADE_NUMBER
		,t.TRADE_DATE
		,t.ISIN
		,t.AUCTION_TYPE_ID
		,t.OPERATION_TYPE_ID
		,t.VALUE
		,t.YIELD
		,t.PRICE
		,t.QUANTITY
		,t.SETTLE_DATE
		,t.BANK_CODE
		,t.CP_BANK_CODE
		,t.CLIENT_CODE
		,t.BROKER_REFERENCE
		,t.USER_ID
		,a.TIME_FROM
		,a.TIME_TO
		,a.ORDER_TIME_TO
	from TRADE t
	join AUCTION a
		on a.AUCTION_DATE=convert(date,t.TRADE_DATE) and a.ISIN=t.ISIN and a.AUCTION_TYPE_ID=t.AUCTION_TYPE_ID
	where a.AUCTION_ID=@AUCTION_ID
GO
