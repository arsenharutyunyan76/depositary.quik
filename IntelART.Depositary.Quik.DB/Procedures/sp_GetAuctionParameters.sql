﻿if exists (select * from sys.objects where name='sp_GetAuctionParameters' and type='P')
	drop procedure sp_GetAuctionParameters
GO

create procedure sp_GetAuctionParameters(
	@ClassCode	varchar(12),
	@SecCode	char(12),
	@TradeDate	date
)
AS
	select TRADE_QUANTITY
		,AVERAGE_YIELD
		,MINIMUM_YIELD
		,MAXIMUM_YIELD
		,AUCTION_YIELD
		,PARTICIPANT_COUNT
	from PARAM
	where CLASS_CODE=@ClassCode
		and SEC_CODE=@SecCode
		and TRADE_DATE=@TradeDate
GO