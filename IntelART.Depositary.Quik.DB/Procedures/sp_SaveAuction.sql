﻿if exists (select * from sys.objects where name='sp_SaveAuction' and type='P')
	drop procedure sp_SaveAuction
GO

create procedure sp_SaveAuction(
	@ISIN					char(12),
	@AUCTION_ID				varchar(16),
	@AUCTION_EXTRA_ID		varchar(16),
	@AUCTION_DATE			date,
	@SETTLEMENT_DATE		date,
	@AUCTION_TYPE_ID		tinyint,
	@YIELD					money,
	@ISSUE_DATE				date,
	@ISSUE_AMOUNT			money,
	@MATURITY_DATE			date,
	@MATURITY_PAYMENT_DATE	date,
	@FIRST_PAYMENT_DATE		date,
	@AUCTION_YIELD			money,
	@MINIMUM_AMOUNT			money,
	@MAXIMUM_AMOUNT			money,
	@AMOUNT					money,
	@TIME_FROM				char(5),
	@TIME_TO				char(5),
	@ORDER_TIME_TO			char(5))
AS
	insert into AUCTION
	(
		ISIN,
		AUCTION_ID,
		AUCTION_EXTRA_ID,
		AUCTION_DATE,
		SETTLEMENT_DATE,
		AUCTION_TYPE_ID,
		YIELD,
		ISSUE_DATE,
		ISSUE_AMOUNT,
		MATURITY_DATE,
		MATURITY_PAYMENT_DATE,
		FIRST_PAYMENT_DATE,
		AUCTION_YIELD,
		MINIMUM_AMOUNT,
		MAXIMUM_AMOUNT,
		AMOUNT,
		TIME_FROM,
		TIME_TO,
		ORDER_TIME_TO
	)
	values
	(
		@ISIN,
		@AUCTION_ID,
		@AUCTION_EXTRA_ID,
		@AUCTION_DATE,
		@SETTLEMENT_DATE,
		@AUCTION_TYPE_ID,
		@YIELD,
		@ISSUE_DATE,
		@ISSUE_AMOUNT,
		@MATURITY_DATE,
		@MATURITY_PAYMENT_DATE,
		@FIRST_PAYMENT_DATE,
		@AUCTION_YIELD,
		@MINIMUM_AMOUNT,
		@MAXIMUM_AMOUNT,
		@AMOUNT,
		@TIME_FROM,
		@TIME_TO,
		@ORDER_TIME_TO
	)
GO
