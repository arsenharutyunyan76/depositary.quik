﻿if exists (select * from sys.objects where name='sp_SaveParam' and type='P')
	drop procedure sp_SaveParam
GO

create procedure sp_SaveParam(
	@AUCTION_YIELD		decimal(28, 4),
	@AVERAGE_YIELD		decimal(28, 4),
	@MAXIMUM_YIELD		decimal(28, 4),
	@MINIMUM_YIELD		decimal(28, 4),
	@PARTICIPANT_COUNT	int,
	@TRADE_QUANTITY		decimal(28, 4),
	@CLASS_CODE			varchar(12),
	@SEC_CODE			varchar(12),
	@TRADE_DATE			date,
	@ID					uniqueidentifier	output)
AS
	set @ID = newid()
	insert into PARAM
	(
		AUCTION_YIELD,		
		AVERAGE_YIELD,		
		MAXIMUM_YIELD,		
		MINIMUM_YIELD,		
		PARTICIPANT_COUNT,	
		TRADE_QUANTITY,	
		CLASS_CODE,
		SEC_CODE,
		TRADE_DATE,
		ID
	)
	values
	(
		@AUCTION_YIELD,		
		@AVERAGE_YIELD,		
		@MAXIMUM_YIELD,		
		@MINIMUM_YIELD,		
		@PARTICIPANT_COUNT,
		@TRADE_QUANTITY,		
		@CLASS_CODE,
		@SEC_CODE,
		@TRADE_DATE,
		@ID		
	)
GO
