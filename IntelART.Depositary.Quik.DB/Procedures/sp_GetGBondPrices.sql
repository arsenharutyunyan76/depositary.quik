﻿if exists (select * from sys.objects where name='sp_GetGBondPrices' and type='P')
	drop procedure sp_GetGBondPrices
GO

create procedure sp_GetGBondPrices(
	@DATE_FROM	date,
	@DATE_TO	date
)
AS
---------------------------------------
	declare @QuoteHist_A table(ISIN_CODE varchar(20), TYPE char(1), MAX_PRICE numeric(16,5), MIN_PRICE numeric(16,5))
	declare @QuoteHist_B table(ISIN_CODE varchar(20), TYPE char(1), MAX_PRICE numeric(16,5), MIN_PRICE numeric(16,5))
	declare @QuoteHist2 table(ISIN_CODE varchar(20), BEST_BID numeric(16,5), BEST_ASK numeric(16,5))


	insert into @QuoteHist_A(ISIN_CODE, TYPE, MAX_PRICE, MIN_PRICE)
	select	i.ISIN_CODE,
			qh.TYPE,
			MAX(qh.PRICE),
			MIN(qh.PRICE)
	from dbo.QUOTE_HIST qh
	LEFT JOIN dbo.ISIN i ON i.RTS_CODE=qh.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
	where convert(date,qh.HISTORY_MOMENT) between ISNULL(@DATE_FROM,convert(date,qh.HISTORY_MOMENT)) and ISNULL(@DATE_TO,convert(date,qh.HISTORY_MOMENT))
		AND qh.LSTG IN ('Abond','Bbond','Cbond', 'GBondB', 'GBondN', 'GBondT', 'GBondE')
		AND qh.HISTORY_ACTION='U' 
		AND qh.STATUS IN (4,6)
		AND qh.TYPE='A'
	group by i.ISIN_CODE, qh.TYPE


	insert into @QuoteHist_B(ISIN_CODE, TYPE, MAX_PRICE, MIN_PRICE)
	select	i.ISIN_CODE,
			qh.TYPE,
			MAX(qh.PRICE),
			MIN(qh.PRICE)
	from dbo.QUOTE_HIST qh
	LEFT JOIN dbo.ISIN i ON i.RTS_CODE=qh.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
	where convert(date,qh.HISTORY_MOMENT) between ISNULL(@DATE_FROM,convert(date,qh.HISTORY_MOMENT)) and ISNULL(@DATE_TO,convert(date,qh.HISTORY_MOMENT))
		AND qh.LSTG IN ('Abond','Bbond','Cbond', 'GBondB', 'GBondN', 'GBondT', 'GBondE')
		AND qh.HISTORY_ACTION='U' 
		AND qh.STATUS IN (4,6)
		AND qh.TYPE='B'
	group by i.ISIN_CODE, qh.TYPE


	insert into @QuoteHist2(ISIN_CODE, BEST_BID, BEST_ASK)
	select  ISNULL(qhA.ISIN_CODE, qhB.ISIN_CODE),
			qhB.MAX_PRICE,
			qhA.MIN_PRICE
	from @QuoteHist_A qhA
	full outer join @QuoteHist_B qhB on qhA.ISIN_CODE=qhB.ISIN_CODE 

---------------------------------------
	declare @TradeHist1 table(ROW_NUM int identity(1,1), ISIN_CODE varchar(20), HISTORY_MOMENT datetime, PRICE numeric(16,5))
	declare @TradeHist2 table(ISIN_CODE varchar(20), MAX_ROW_NUM int)
	declare @TradeHist3 table(ISIN_CODE varchar(20), CLOSING_PRICE numeric(16,5))


	insert into @TradeHist1(ISIN_CODE, HISTORY_MOMENT, PRICE)
	select	i.ISIN_CODE,
			th.HISTORY_MOMENT,
			th.PRICE
	from dbo.TRADE_HIST th
	LEFT JOIN dbo.ISIN i ON i.RTS_CODE=th.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
	where convert(date,th.HISTORY_MOMENT) between ISNULL(@DATE_FROM,convert(date,th.HISTORY_MOMENT)) and ISNULL(@DATE_TO,convert(date,th.HISTORY_MOMENT))
		AND th.TYPE='A' AND th.TYPE_EXT<>'R'
	order by i.ISIN_CODE, th.HISTORY_MOMENT


	insert into @TradeHist2(ISIN_CODE, MAX_ROW_NUM)
	select	th.ISIN_CODE,
			MAX(th.ROW_NUM)
	from @TradeHist1 th
	group by th.ISIN_CODE


	insert into @TradeHist3(ISIN_CODE, CLOSING_PRICE)
	select	th1.ISIN_CODE,
			th1.PRICE
	from @TradeHist1 th1
	inner join @TradeHist2 th2 on th1.ROW_NUM=th2.MAX_ROW_NUM

---------------------------------------

	select	@DATE_FROM							as DATE, 
			ISNULL(qh.ISIN_CODE, th.ISIN_CODE)	as ISIN, 
			ISNULL(qh.BEST_BID,0)				as BEST_BID, 
			ISNULL(qh.BEST_ASK,0)				as BEST_ASK, 
			ISNULL(th.CLOSING_PRICE, 0)			as CLOSING_PRICE
	from @QuoteHist2 qh
	full join @TradeHist3 th on qh.ISIN_CODE= th.ISIN_CODE
	ORDER BY DATE, ISNULL(qh.ISIN_CODE, th.ISIN_CODE)

---------------------------------------

GO
