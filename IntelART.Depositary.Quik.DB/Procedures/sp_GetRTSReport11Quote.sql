﻿if exists (select * from sys.objects where name='sp_GetRTSReport11Quote' and type='P')
	drop procedure sp_GetRTSReport11Quote
GO

create procedure sp_GetRTSReport11Quote(
	@QUOTE_DATE_FROM	date,
	@QUOTE_DATE_TO		date
)
AS
	select
		qh.HISTORY_ID,
		b.CODE		AS BANK_CODE,
		qh.WKS,
		a.IS_OWN	AS ACCOUNT_TYPE,
		qh.FIRM_NAME,
		i.ISIN_CODE,
		qh.LSTG,
		i.CURRENCY,
		qh.PRICE,
		qh.YIELD,
		qh.INIT_QTY,
		th.QTY,
		i.NOMINAL,
		th.VOLUME,
		qh.ORDER_TYPE,
		qh.ALL_NON,
		qh.LEAVE,
		qh.CATALYST,
		qh.TYPE,
		qh.STATUS,
		qh.HISTORY_ACTION,
		qh.HISTORY_MOMENT,
		i.REGISTRATION_NUMBER,
		qh.DEPO_ACCOUNTS,
		qh.SETTL_PAIR,
		qh.QUOTE_QTY
	from dbo.QUOTE_HIST qh
	LEFT JOIN dbo.BANK b ON b.SHORT_NAME=qh.FIRM_NAME
	LEFT JOIN dbo.ACCOUNT a ON a.CODE=qh.SETTL_PAIR
	LEFT JOIN dbo.ISIN i ON i.RTS_CODE=qh.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
	LEFT JOIN dbo.TRADE_HIST th ON th.HISTORY_EVENT=qh.HISTORY_EVENT AND qh.STATUS in (3,22) AND qh.HISTORY_ACTION='U'
	WHERE convert(date,qh.HISTORY_MOMENT) between ISNULL(@QUOTE_DATE_FROM,convert(date,qh.HISTORY_MOMENT)) and ISNULL(@QUOTE_DATE_TO,convert(date,qh.HISTORY_MOMENT))
		AND qh.LSTG IN ('IPO', 'IPO_BOND', 'A', 'B', 'C', 'GBondB', 'GBondN', 'GBondT', 'GBondE', 'Abond', 'Bbond', 'CBond')
		AND ((qh.HISTORY_ACTION='U' and qh.STATUS in (1,3,4,6,8,10,22)) OR (qh.HISTORY_ACTION='A' and qh.STATUS=1))
	order by qh.HISTORY_ID,qh.HISTORY_MOMENT
GO
