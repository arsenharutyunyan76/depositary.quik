﻿if exists (select * from sys.objects where name='sp_GetAuctionNumber' and type='P')
	drop procedure sp_GetAuctionNumber
GO

create procedure sp_GetAuctionNumber(@AUCTION_DATE	date,
									 @AUCTION_TYPE	bit)
AS
	declare @NUMBER int

	select @NUMBER = NUMBER
	from AUCTION_PARAMETER
	where AUCTION_DATE = @AUCTION_DATE
		and AUCTION_TYPE = @AUCTION_TYPE

	select isnull(@NUMBER,0) as Number
GO
