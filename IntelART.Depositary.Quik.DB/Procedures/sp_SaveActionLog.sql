﻿if exists (select * from sys.objects where name='sp_SaveActionLog' and type='P')
	drop procedure sp_SaveActionLog
GO

create procedure sp_SaveActionLog(
	@ACTION_TYPE_ID	smallint,
	@IS_FAIL		bit,
	@DETAILS		nvarchar(max)
)
AS
	insert into ACTION_LOG
	(
		ACTION_TYPE_ID,
		IS_FAIL,
		DETAILS
	)
	values
	(
		@ACTION_TYPE_ID,
		@IS_FAIL,
		@DETAILS
	)
GO
