﻿if exists (select * from sys.objects where name='sp_SetFileGenerationTime' and type='P')
	drop procedure sp_SetFileGenerationTime
GO

create procedure sp_SetFileGenerationTime(@FILE_TYPE tinyint, @GENERATE_DATE datetime)
AS
	update FILE_GENERATION_TIME
	set GENERATE_DATE=@GENERATE_DATE
	where FILE_TYPE=@FILE_TYPE
GO
