﻿if exists (select * from sys.objects where name='sp_AcknowledgeBankmailMessage' and type='P')
	drop procedure sp_AcknowledgeBankmailMessage
GO

create procedure sp_AcknowledgeBankmailMessage(
	@ID				uniqueidentifier,
	@MESSAGE_RESULT	bit)
AS
	update BANKMAIL_MESSAGE
	set BANKMAIL_STATUS_ID=case @MESSAGE_RESULT when 1 then 2 else 3 end
	where ID=@ID
GO
