﻿if exists (select * from sys.objects where name='sp_GetStockAccountByCurrency' and type='P')
	drop procedure sp_GetStockAccountByCurrency
GO

create procedure sp_GetStockAccountByCurrency(@CURRENCY	char(4))

AS
	select ACCOUNT
	from STOCK_ACCOUNT
	where CURRENCY=@CURRENCY
GO
