﻿if exists (select * from sys.objects where name='sp_GetAccountPairs' and type='P')
	drop procedure sp_GetAccountPairs
GO

create procedure sp_GetAccountPairs	
AS
	select   ac1.ID			as BANK_ID
			,ac1.BANK_CODE	as BANK_CODE
			,b.SHORT_NAME	as BANK_SHORT_NAME
			,ac1.CUSTOMER_CODE		as ACCOUNT_CUSTOMER_CODE
			,ac1.SECURITY_ACCOUNT	as ACCOUNT_CODE_1
			,ac1.CURRENCY			as CURRENCY_1
			,ac2.SECURITY_ACCOUNT	as ACCOUNT_CODE_2
			,ac2.CURRENCY			as CURRENCY_2
	from ACCOUNT ac1
	INNER JOIN ACCOUNT ac2 ON ac2.BANK_CODE=ac1.BANK_CODE AND ac2.IS_OWN=ac1.IS_OWN
	INNER JOIN BANK b on b.CODE=ac1.BANK_CODE
	WHERE ac1.CURRENCY='AMD' AND  ac2.CURRENCY='DEPO'
	
GO
