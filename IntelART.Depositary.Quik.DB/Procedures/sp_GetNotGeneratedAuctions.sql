﻿if exists (select * from sys.objects where name='sp_GetNotGeneratedAuctions' and type='P')
	drop procedure sp_GetNotGeneratedAuctions
GO

create procedure sp_GetNotGeneratedAuctions
AS
	select ID,
		ISIN,
		AUCTION_ID,
		AUCTION_EXTRA_ID,
		AUCTION_DATE,
		SETTLEMENT_DATE,
		AUCTION_TYPE_ID,
		YIELD,
		ISSUE_DATE,
		ISSUE_AMOUNT,
		MATURITY_DATE,
		MATURITY_PAYMENT_DATE,
		FIRST_PAYMENT_DATE,
		AUCTION_YIELD,
		MINIMUM_AMOUNT,
		MAXIMUM_AMOUNT,
		AMOUNT,
		TIME_FROM,
		TIME_TO,
		ORDER_TIME_TO
	from AUCTION
	where GENERATE_DATE is null
	order by CREATION_DATE
GO
