﻿if exists (select * from sys.objects where name='sp_SetDependFileGenerationStatus' and type='P')
	drop procedure sp_SetDependFileGenerationStatus
GO

create procedure sp_SetDependFileGenerationStatus(
	@ATSORDER_ID int,
	@IS_FILE_GENERATED bit
)
AS
	update DEPEND_LIST_ORDER
	set IS_FILE_GENERATED=@IS_FILE_GENERATED
	where ATSORDER_ID=@ATSORDER_ID
GO
