﻿if exists (select * from sys.objects where name='sp_SaveReportFillValue' and type='P')
	drop procedure sp_SaveReportFillValue
GO
create procedure sp_SaveReportFillValue(
	@REPORT_ID		tinyint,
	@EXCEL_SHEET	tinyint,
	@CODE			nvarchar(50),
	@ROW_NUMBER		int,
	@COLUMN_NUMBER	int
)
AS
	declare @ID int
	select @ID=ID from REPORT_FILL_VALUE where REPORT_ID=@REPORT_ID and CODE=@CODE
	if @ID is null
		insert into REPORT_FILL_VALUE (REPORT_ID,EXCEL_SHEET,CODE,ROW_NUMBER,COLUMN_NUMBER)
		values (@REPORT_ID,@EXCEL_SHEET,@CODE,@ROW_NUMBER,@COLUMN_NUMBER)
	else
		update REPORT_FILL_VALUE
		set	EXCEL_SHEET = @EXCEL_SHEET
			,ROW_NUMBER = @ROW_NUMBER
			,COLUMN_NUMBER = @COLUMN_NUMBER
		where REPORT_ID=@REPORT_ID and CODE=@CODE
GO
