﻿if exists (select * from sys.objects where name='sp_SaveBankmailMessage' and type='P')
	drop procedure sp_SaveBankmailMessage
GO

create procedure sp_SaveBankmailMessage(
	@MESSAGE_TYPE		char(3),
	@SENDER_BANK		char(5),
	@RECEIVER_BANK		char(5),
	@NUMBER				int					output,
	@DOCUMENT_NUMBER	varchar(16)			output,
	@ID					uniqueidentifier	output)
AS
	set @ID = newid()
	declare @CURRENT_DATE date = getdate()
	declare @LAST_SEND_DATE date,@LAST_SEND_NUMBER int
	select @LAST_SEND_DATE=LAST_SEND_DATE
		,@LAST_SEND_NUMBER=LAST_SEND_NUMBER
	from BANKMAIL_PARAMETER with (TABLOCK)

	if @LAST_SEND_DATE is null or @CURRENT_DATE<>@LAST_SEND_DATE
		set @NUMBER=1
	else
		set @NUMBER=isnull(@LAST_SEND_NUMBER,0)+1

	set @DOCUMENT_NUMBER = 'N'+CONVERT(VARCHAR,@CURRENT_DATE,112)+right('0000'+convert(varchar,@NUMBER),4)

	insert into BANKMAIL_MESSAGE
	(
		ID,
		MESSAGE_TYPE,
		DOCUMENT_NUMBER,
		BANKMAIL_STATUS_ID
	)
	values
	(
		@ID,
		@MESSAGE_TYPE,
		@DOCUMENT_NUMBER,
		1
	)

	delete from BANKMAIL_PARAMETER
	insert into BANKMAIL_PARAMETER (LAST_SEND_DATE,LAST_SEND_NUMBER)
	values (@CURRENT_DATE,@NUMBER)
GO
