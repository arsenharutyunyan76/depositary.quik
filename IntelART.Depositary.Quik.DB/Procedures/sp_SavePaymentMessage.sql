﻿if exists (select * from sys.objects where name='sp_SavePaymentMessage' and type='P')
	drop procedure sp_SavePaymentMessage
GO

create procedure sp_SavePaymentMessage(
	@ID					char(16),
	@MESSAGE_TYPE		char(3),
	@DOCUMENT_NUMBER	varchar(20),
	@SECURITY_ID		varchar(20),
	@CURRENCY			varchar(3),
	@AMOUNT				decimal(28,8),
	@PAYER				varchar(16),
	@RECEIVER			varchar(16)
	)
AS
	insert into PAYMENT_MESSAGE
	(
		ID,
		MESSAGE_TYPE,
		DOCUMENT_NUMBER,
		SECURITY_ID,
		CURRENCY,
		AMOUNT,
		PAYER,
		RECEIVER
	)
	values
	(
		@ID,
		@MESSAGE_TYPE,
		@DOCUMENT_NUMBER,
		@SECURITY_ID,
		@CURRENCY,
		@AMOUNT,
		@PAYER,
		@RECEIVER
	)
GO
