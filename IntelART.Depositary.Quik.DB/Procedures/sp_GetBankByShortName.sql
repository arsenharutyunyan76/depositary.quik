﻿if exists (select * from sys.objects where name='sp_GetBankByShortName' and type='P')
	drop procedure sp_GetBankByShortName
GO

create procedure sp_GetBankByShortName(@SHORT_NAME	varchar(20))
AS
	select CODE
	from BANK
	where SHORT_NAME = @SHORT_NAME
GO
