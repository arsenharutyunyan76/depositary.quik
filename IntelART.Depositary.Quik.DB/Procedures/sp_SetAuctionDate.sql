﻿if exists (select * from sys.objects where name='sp_SetAuctionDate' and type='P')
	drop procedure sp_SetAuctionDate
GO

create procedure sp_SetAuctionDate(
	@ID				int,
	@AUCTION_ID		varchar(16),
	@TIME_FROM		char(5),
	@TIME_TO		char(5),
	@ORDER_TIME_TO	char(5),
	@IS_CANCELLED	bit
)
AS
	update AUCTION
	set AUCTION_ID=@AUCTION_ID
		,TIME_FROM=@TIME_FROM
		,TIME_TO=@TIME_TO
		,ORDER_TIME_TO=@ORDER_TIME_TO
		,IS_CANCELLED=@IS_CANCELLED
	where ID=@ID
GO
