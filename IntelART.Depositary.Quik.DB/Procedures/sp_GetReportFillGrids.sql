﻿if exists (select * from sys.objects where name='sp_GetReportFillGrids' and type='P')
	drop procedure sp_GetReportFillGrids
GO
create procedure sp_GetReportFillGrids(
	@REPORT_ID tinyint
)
AS
	select	ID,
			EXCEL_SHEET,
			CODE,
			ROW_NUMBER
	from REPORT_FILL_GRID
	where REPORT_ID = @REPORT_ID
GO