﻿if exists (select * from sys.objects where name='sp_GetRTSReportDataTrade' and type='P')
	drop procedure sp_GetRTSReportDataTrade
GO

create procedure sp_GetRTSReportDataTrade(
	@TRADE_DATE_FROM	date,
	@TRADE_DATE_TO		date
)
AS
	select ID,
			STATUS			,
			ISSUE_NAME		,
			PRICE			,
			HISTORY_ID		,
			HISTORY_ACTION	,
			HISTORY_MOMENT	,
			LSTG			,

			QTY				,
			VOLUME			,
			TYPE_EXT		,
			RATE			,
			DELIVERY_DATE	,
			NEW_DELIVERY_DATE	,
			INIT_NAME		,
			INIT_WKS		,
			INIT_ACTION		,
			INIT_PAIR		,
			CONF_NAME		,
			CONF_QUOTEID	,
			TRADE_REF		,
			REPO_ACTION		,

			CONF_ACTION		,
			CONF_PAIR		,
			TRADE_MOMENT	,
			PRICE_CURRENCY	,
			TYPE			,

			CONF_WKS		,
			PRICE_RATE		,

			HISTORY_EVENT	,
			DEPO_ACCOUNTS
	from TRADE_HIST
	where convert(date,HISTORY_MOMENT) between ISNULL(@TRADE_DATE_FROM,convert(date,HISTORY_MOMENT)) and ISNULL(@TRADE_DATE_TO,convert(date,HISTORY_MOMENT))
GO
