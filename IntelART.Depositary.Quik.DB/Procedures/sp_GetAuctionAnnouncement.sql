﻿if exists (select * from sys.objects where name='sp_GetAuctionAnnouncement' and type='P')
	drop procedure sp_GetAuctionAnnouncement
GO

create procedure sp_GetAuctionAnnouncement
AS
	select EMAIL
		,EMAIL_SUBJECT
		,EMAIL_BODY
	from AUCTION_ANNOUNCEMENT
GO
