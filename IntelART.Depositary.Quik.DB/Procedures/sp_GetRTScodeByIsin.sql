﻿IF exists (SELECT * FROM sys.objects WHERE name='sp_GetRTScodeByIsin' and type='P')
	DROP PROCEDURE sp_GetRTScodeByIsin
GO
CREATE PROCEDURE sp_GetRTScodeByIsin(
	@ISIN_CODE	nvarchar(20)
)
AS
	SELECT TOP 1 RTS_CODE
	FROM dbo.ISIN
	WHERE ISIN_CODE=@ISIN_CODE
GO
