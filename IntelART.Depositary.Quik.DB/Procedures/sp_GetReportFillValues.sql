﻿if exists (select * from sys.objects where name='sp_GetReportFillValues' and type='P')
	drop procedure sp_GetReportFillValues
GO
create procedure sp_GetReportFillValues(
	@REPORT_ID tinyint
)
AS
	select	EXCEL_SHEET,
			CODE,
			ROW_NUMBER,
			COLUMN_NUMBER
	from REPORT_FILL_VALUE
	where REPORT_ID = @REPORT_ID
GO
