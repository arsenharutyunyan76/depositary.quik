﻿if exists (select * from sys.objects where name='sp_GetBankUsers' and type='P')
	drop procedure sp_GetBankUsers
GO

create procedure sp_GetBankUsers
AS
	select BANK_CODE
		,USER_ID
		,USER_CODE
	from BANK_USER
GO
