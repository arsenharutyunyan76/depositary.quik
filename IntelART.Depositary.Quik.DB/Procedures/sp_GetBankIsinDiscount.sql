﻿if exists (select * from sys.objects where name='sp_GetBankIsinDiscount' and type='P')
	drop procedure sp_GetBankIsinDiscount
GO

create procedure sp_GetBankIsinDiscount(
	@BANK_CODE	char(5) = null,
	@ISIN_CODE	nvarchar(20) = null
)
AS
	select	BANK_CODE,
			ISIN_CODE,
			DISCOUNT,
			IS_ACTIVE
	from BANK_ISIN_DISCOUNT
	where isnull(@BANK_CODE,BANK_CODE)=BANK_CODE
		and isnull(@ISIN_CODE,ISIN_CODE)=ISIN_CODE
GO
