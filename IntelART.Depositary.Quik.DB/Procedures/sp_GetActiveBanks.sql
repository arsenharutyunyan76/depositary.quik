﻿if exists (select * from sys.objects where name='sp_GetActiveBanks' and type='P')
	drop procedure sp_GetActiveBanks
GO

create procedure sp_GetActiveBanks
AS
	select CODE
		,SHORT_NAME
		,NAME_AM
		,NAME_EN
		,AS_CODE
	from BANK
GO
