﻿if exists (select * from sys.objects where name='sp_GetTradesForOrder' and type='P')
	drop procedure sp_GetTradesForOrder
GO

create procedure sp_GetTradesForOrder(@ORDER_NUMBER	decimal(19, 0))
AS
	select ID
		,TRADE_NUMBER
		,TRADE_DATE
		,ISIN
		,AUCTION_TYPE_ID
		,OPERATION_TYPE_ID
		,VALUE
		,YIELD
		,PRICE
		,QUANTITY
		,SETTLE_DATE
		,BANK_CODE
		,CP_BANK_CODE
		,CLIENT_CODE
		,BROKER_REFERENCE
		,USER_ID
	from TRADE
	where ORDER_NUMBER=@ORDER_NUMBER
GO
