﻿if exists (select * from sys.objects where name='sp_GetRTSReportDataQuote' and type='P')
	drop procedure sp_GetRTSReportDataQuote
GO

create procedure sp_GetRTSReportDataQuote(
	@QUOTE_DATE_FROM	date,
	@QUOTE_DATE_TO		date
)
AS
	select ID,
			STATUS			,
			ISSUE_NAME		,
			PRICE			,
			HISTORY_ID		,
			HISTORY_ACTION	,
			HISTORY_MOMENT	,
			LSTG			,

			INIT_QTY		,
			MOMENT			,
			FIRM_NAME		,
			WKS				,
			SETTL_PAIR		,
			ORDER_TYPE		,
			ALL_NON			,
			LEAVE			,
			CATALYST		,
			TYPE			,

			HISTORY_EVENT	,
			DEPO_ACCOUNTS	,

			QUOTE_QTY

	from QUOTE_HIST
	where convert(date,HISTORY_MOMENT) between ISNULL(@QUOTE_DATE_FROM,convert(date,HISTORY_MOMENT)) and ISNULL(@QUOTE_DATE_TO,convert(date,HISTORY_MOMENT))
GO
