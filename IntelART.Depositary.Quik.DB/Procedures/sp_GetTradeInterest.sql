﻿if exists (select * from sys.objects where name='sp_GetTradeInterest' and type='P')
	drop procedure sp_GetTradeInterest
GO

create procedure sp_GetTradeInterest
AS
	SELECT INTEREST
		,FIXED_AMOUNT
		,MINIMUM_AMOUNT
		,MAXIMUM_AMOUNT
		,FROM_AMOUNT
		,TO_AMOUNT
	FROM TRADE_INTEREST
GO
