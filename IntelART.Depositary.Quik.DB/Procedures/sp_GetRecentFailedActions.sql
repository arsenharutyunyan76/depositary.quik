﻿if exists (select * from sys.objects where name='sp_GetRecentFailedActions' and type='P')
	drop procedure sp_GetRecentFailedActions
GO

create procedure sp_GetRecentFailedActions(
	@USER_ID	varchar(50) = null
)
AS
	declare @LAST_VIEW_DATE datetime
	select @LAST_VIEW_DATE = LAST_VIEW_DATE
	from ACTION_LOG_LAST_VIEW
	where isnull(USER_ID,'') = isnull(@USER_ID,'')

	declare @CURRENT_DATE datetime = getdate()
	if @LAST_VIEW_DATE is null
		set @LAST_VIEW_DATE=@CURRENT_DATE

	select a.CREATION_DATE,a.ID,a.ACTION_TYPE_ID,a.IS_FAIL,a.DETAILS
		,t.NAME_AM as ACTION_TYPE_NAME_AM,t.NAME_EN as ACTION_TYPE_NAME_EN
	from ACTION_LOG a
	join ACTION_TYPE t
		on a.ACTION_TYPE_ID=t.ID
	where a.CREATION_DATE>@LAST_VIEW_DATE
		and a.CREATION_DATE<=@CURRENT_DATE
		and a.IS_FAIL=1
	order by a.CREATION_DATE desc

	if exists (select * from ACTION_LOG_LAST_VIEW where isnull(USER_ID,'') = isnull(@USER_ID,''))
		update ACTION_LOG_LAST_VIEW
		set LAST_VIEW_DATE = @CURRENT_DATE
		where isnull(USER_ID,'') = isnull(@USER_ID,'')
	else
		insert into ACTION_LOG_LAST_VIEW (USER_ID,LAST_VIEW_DATE)
		values (@USER_ID,@CURRENT_DATE)
GO
