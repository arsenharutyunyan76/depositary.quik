﻿if exists (select * from sys.objects where name='sp_GetCurrencies' and type='P')
	drop procedure sp_GetCurrencies
GO

create procedure sp_GetCurrencies
AS
	select CODE, CURRENCY_TYPE
	from CURRENCY
GO
