﻿if exists (select * from sys.objects where name='sp_SetBankRegistrationsNumber' and type='P')
	drop procedure sp_SetBankRegistrationsNumber
GO

create procedure sp_SetBankRegistrationsNumber(
	@CODE				char(5),
	@REGISTRATIONS_NUMBER	int
)
AS
	UPDATE BANK
	SET REGISTRATIONS_NUMBER=@REGISTRATIONS_NUMBER
	WHERE CODE=@CODE
GO
