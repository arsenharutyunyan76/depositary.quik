﻿if exists (select * from sys.objects where name='sp_GetBanks' and type='P')
	drop procedure sp_GetBanks
GO

create procedure sp_GetBanks
AS
	select CODE
		,SHORT_NAME
		,IS_ACTIVE
		,NAME_AM
		,NAME_EN
		,AS_CODE
		,EMAIL
	from BANK
GO
