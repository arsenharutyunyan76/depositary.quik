﻿if exists (select * from sys.objects where name='sp_SetDependUndepositionStatus' and type='P')
	drop procedure sp_SetDependUndepositionStatus
GO

create procedure sp_SetDependUndepositionStatus(
	@ID int,
	@PROCESSING_STATUS bit
)
AS
	update DEPEND_UNDEPOSITION
	set PROCESSING_STATUS=@PROCESSING_STATUS
	where ID=@ID
GO