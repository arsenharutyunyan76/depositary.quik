﻿if exists (select * from sys.objects where name='sp_GetReportSchedules' and type='P')
	drop procedure sp_GetReportSchedules
GO

create procedure sp_GetReportSchedules
AS
	select ID
		,NAME_AM
		,NAME_EN
		,GENERATION_TIME
		,LAST_GENERATE_DATE
		,EMAIL
		,EMAIL_SUBJECT
		,EMAIL_BODY
	from REPORT_SCHEDULE
GO
