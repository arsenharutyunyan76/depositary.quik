﻿if exists (select * from sys.objects where name='sp_GetBankmailMessage515Trades' and type='P')
	drop procedure sp_GetBankmailMessage515Trades
GO

create procedure sp_GetBankmailMessage515Trades(
	@ID	uniqueidentifier
)
AS
	select ID
		,TRADE_NUMBER
		,TRADE_DATE
		,ISIN
		,AUCTION_TYPE_ID
		,OPERATION_TYPE_ID
		,VALUE
		,YIELD
		,QUANTITY
		,SETTLE_DATE
		,BANK_CODE
		,CP_BANK_CODE
		,CLIENT_CODE
		,BROKER_REFERENCE
		,USER_ID
	from TRADE
	where BANKMAIL_MESSAGE_515_ID=@ID
		and PROCESS_STATUS_ID=3
GO
