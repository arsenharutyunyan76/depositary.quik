﻿if exists (select * from sys.objects where name='sp_GetAccounts' and type='P')
	drop procedure sp_GetAccounts
GO

create procedure sp_GetAccounts	(@BANK_CODE char(5))
AS
	select CODE
		  ,CURRENCY
		  ,IS_OWN
		  ,CUSTOMER_CODE
		  ,SECURITY_ACCOUNT
	from ACCOUNT
	where BANK_CODE = @BANK_CODE
GO
