﻿if exists (select * from sys.objects where name='sp_DeleteReportApplication' and type='P')
	drop procedure sp_DeleteReportApplication
GO

create procedure sp_DeleteReportApplication(
	@ID	uniqueidentifier
)
AS
	delete from REPORT_APPLICATION
	where ID=@ID and GENERATE_DATE is null
GO
