﻿if exists (select * from sys.objects where name='sp_GetReportApplications' and type='P')
	drop procedure sp_GetReportApplications
GO

create procedure sp_GetReportApplications
AS
	select ID
		,REPORT_ID
		,REPORT_FROM_DATE
		,REPORT_TO_DATE
	from REPORT_APPLICATION
	where GENERATE_DATE is null
	order by CREATION_DATE
GO
