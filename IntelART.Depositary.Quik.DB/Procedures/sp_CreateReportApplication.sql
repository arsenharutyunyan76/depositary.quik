﻿if exists (select * from sys.objects where name='sp_CreateReportApplication' and type='P')
	drop procedure sp_CreateReportApplication
GO

create procedure sp_CreateReportApplication(
	@REPORT_ID			tinyint,
	@REPORT_FROM_DATE	date,
	@REPORT_TO_DATE		date,
	@ID					uniqueidentifier	OUTPUT
)
AS
	set @ID=newid()
	insert into REPORT_APPLICATION (ID,REPORT_ID,REPORT_FROM_DATE,REPORT_TO_DATE)
		values (@ID,@REPORT_ID,@REPORT_FROM_DATE,@REPORT_TO_DATE)
GO
