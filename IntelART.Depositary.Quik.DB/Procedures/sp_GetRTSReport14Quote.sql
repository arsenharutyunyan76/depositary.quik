﻿if exists (select * from sys.objects where name='sp_GetRTSReport14Quote' and type='P')
	drop procedure sp_GetRTSReport14Quote
GO

create procedure sp_GetRTSReport14Quote(
	@QUOTE_DATE	date
)
AS

DECLARE @tmpPriceB TABLE(ISIN VARCHAR(20), PRICE_11 FLOAT, PRICE_12 FLOAT, PRICE_13 FLOAT, PRICE_14 FLOAT, PRICE_15 FLOAT)
DECLARE @tmpPriceA TABLE(ISIN VARCHAR(20), PRICE_11 FLOAT, PRICE_12 FLOAT, PRICE_13 FLOAT, PRICE_14 FLOAT, PRICE_15 FLOAT)
------------------------------------------------------------
INSERT INTO @tmpPriceB(ISIN, PRICE_11, PRICE_12, PRICE_13, PRICE_14, PRICE_15)
SELECT	ISNULL(tb11.a, ISNULL(tb12.a, ISNULL(tb13.a, ISNULL(tb14.a, tb15.a)))),
		ISNULL(tb11.b, ISNULL(tb12.b, ISNULL(tb13.b, ISNULL(tb14.b, tb15.b)))),
		ISNULL(tb12.c, ISNULL(tb13.c, ISNULL(tb14.c, tb15.c))),
		ISNULL(tb13.d, ISNULL(tb14.d, tb15.d)),
		ISNULL(tb14.e, tb15.e),
		tb15.f
FROM
(
SELECT
		 i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,MAX(thB.[PRICE]) AS Price_11_10, NULL AS Price_12_00, NULL AS Price_13_00, NULL AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('B')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='11:10:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='11:10:00.000'
GROUP BY i.ISIN_CODE
) AS tb11(a,b,c,d,e,f)
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, MAX(thB.[PRICE]) AS Price_12_00, NULL AS Price_13_00, NULL AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('B')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='12:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='12:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb12(a,b,c,d,e,f) ON tb11.a=tb12.a
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, NULL AS Price_12_00, MAX(thB.[PRICE]) AS Price_13_00, NULL AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('B')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='13:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='13:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb13(a,b,c,d,e,f) ON tb12.a=tb13.a
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, NULL AS Price_12_00, NULL AS Price_14_00, MAX(thB.[PRICE]) AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('B')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='14:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='14:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb14(a,b,c,d,e,f) ON tb13.a=tb14.a
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, NULL AS Price_12_00, NULL AS Price_14_00, NULL AS Price_14_00, MAX(thB.[PRICE]) AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('B')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='15:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='15:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb15(a,b,c,d,e,f) ON tb14.a=tb15.a
------------------------------------------------------------
INSERT INTO @tmpPriceA(ISIN, PRICE_11, PRICE_12, PRICE_13, PRICE_14, PRICE_15)
SELECT	ISNULL(tb11.a, ISNULL(tb12.a, ISNULL(tb13.a, ISNULL(tb14.a, tb15.a)))),
		ISNULL(tb11.b, ISNULL(tb12.b, ISNULL(tb13.b, ISNULL(tb14.b, tb15.b)))),
		ISNULL(tb12.c, ISNULL(tb13.c, ISNULL(tb14.c, tb15.c))),
		ISNULL(tb13.d, ISNULL(tb14.d, tb15.d)),
		ISNULL(tb14.e, tb15.e),
		tb15.f
FROM
(
SELECT
		 i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,MAX(thB.[PRICE]) AS Price_11_10, NULL AS Price_12_00, NULL AS Price_13_00, NULL AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('A')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='11:10:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='11:10:00.000'
GROUP BY i.ISIN_CODE
) AS tb11(a,b,c,d,e,f)
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, MAX(thB.[PRICE]) AS Price_12_00, NULL AS Price_13_00, NULL AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('A')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='12:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='12:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb12(a,b,c,d,e,f) ON tb11.a=tb12.a
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, NULL AS Price_12_00, MAX(thB.[PRICE]) AS Price_13_00, NULL AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('A')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='13:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='13:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb13(a,b,c,d,e,f) ON tb12.a=tb13.a
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, NULL AS Price_12_00, NULL AS Price_14_00, MAX(thB.[PRICE]) AS Price_14_00, NULL AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('A')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='14:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='14:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb14(a,b,c,d,e,f) ON tb13.a=tb14.a
	FULL OUTER JOIN
(
SELECT
i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS
		,NULL AS Price_11_10, NULL AS Price_12_00, NULL AS Price_14_00, NULL AS Price_14_00, MAX(thB.[PRICE]) AS Price_15_00
FROM [dbo].[QUOTE_HIST] thB
LEFT JOIN [dbo].[QUOTE_HIST] thD ON thB.HISTORY_ID=thD.HISTORY_ID AND thD.HISTORY_ACTION='D'
LEFT JOIN [dbo].[ISIN] i ON i.RTS_CODE=thB.ISSUE_NAME COLLATE SQL_Latin1_General_CP1_CS_AS
WHERE thB.LSTG IN ('GbondB','GbondN','GbondT','GbondE') AND thB.[TYPE] IN ('A')
		AND CONVERT(DATE, thB.HISTORY_MOMENT)=@QUOTE_DATE AND CONVERT(TIME, thB.HISTORY_MOMENT)<='15:00:00.000'
		AND NOT CONVERT(TIME, thD.HISTORY_MOMENT)<='15:00:00.000'
GROUP BY i.ISIN_CODE
) AS tb15(a,b,c,d,e,f) ON tb14.a=tb15.a
------------------------------------------------------------
select	ISNULL(tpb.ISIN, tpa.ISIN) AS ISIN_CODE
		,( ISNULL(tpb.PRICE_11,0)+ISNULL(tpb.PRICE_12,0)+ISNULL(tpb.PRICE_13,0)+ISNULL(tpb.PRICE_14,0)+ISNULL(tpb.PRICE_15,0) ) /
			( IIF(tpb.PRICE_11 IS NULL,0,1)+IIF(tpb.PRICE_12 IS NULL,0,1)+IIF(tpb.PRICE_13 IS NULL,0,1)+IIF(tpb.PRICE_14 IS NULL,0,1)+IIF(tpb.PRICE_15 IS NULL,0,1) ) AS PRICE_MAX_B
		,( ISNULL(tpa.PRICE_11,0)+ISNULL(tpa.PRICE_12,0)+ISNULL(tpa.PRICE_13,0)+ISNULL(tpa.PRICE_14,0)+ISNULL(tpa.PRICE_15,0) ) /
			( IIF(tpa.PRICE_11 IS NULL,0,1)+IIF(tpa.PRICE_12 IS NULL,0,1)+IIF(tpa.PRICE_13 IS NULL,0,1)+IIF(tpa.PRICE_14 IS NULL,0,1)+IIF(tpa.PRICE_15 IS NULL,0,1) ) AS PRICE_MIN_A
from @tmpPriceB tpb
FULL OUTER JOIN @tmpPriceA tpa ON tpa.ISIN=tpb.ISIN
------------------------------------------------------------

GO
