﻿if exists (select * from sys.objects where name='sp_GenerateAuction' and type='P')
	drop procedure sp_GenerateAuction
GO

create procedure sp_GenerateAuction(
	@ID	int
)
AS
	update AUCTION
	set GENERATE_DATE=getdate()
	where ID=@ID
GO
