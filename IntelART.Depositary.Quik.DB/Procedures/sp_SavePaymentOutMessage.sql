﻿if exists (select * from sys.objects where name='sp_SavePaymentOutMessage' and type='P')
	drop procedure sp_SavePaymentOutMessage
GO

create procedure sp_SavePaymentOutMessage(
	@ID					varchar(50)	output,
	@MESSAGE_TYPE		char(3),
	@NUMBER				int			output,
	@DOCUMENT_NUMBER	varchar(16)	output,
	@SECURITY_ID		varchar(20),
	@CURRENCY			varchar(3),
	@INITIAL_AMOUNT		decimal(28,8),
	@AMOUNT				decimal(28,8),
	@PAYER				varchar(16),
	@RECEIVER			varchar(16),
	@RECEIVER_NAME		varchar(16),
	@IS_FINAL_CALCULATION	bit,
	@OPCODE				varchar(1),
	@CODE				varchar(7),
	@MOMENT				datetime
	)
AS

	declare @CURRENT_DATE date = getdate()
	declare @LAST_SEND_DATE date,@LAST_SEND_NUMBER int
	select @LAST_SEND_DATE=LAST_SEND_DATE
		,@LAST_SEND_NUMBER=LAST_SEND_NUMBER
	from PAYMENT_OUT_PARAMETER with (TABLOCK)

	if @LAST_SEND_DATE is null or @CURRENT_DATE<>@LAST_SEND_DATE
		set @NUMBER=1
	else
		set @NUMBER=isnull(@LAST_SEND_NUMBER,0)+1

	set @DOCUMENT_NUMBER = 'N'+CONVERT(VARCHAR,@CURRENT_DATE,112)+right('0000'+convert(varchar,@NUMBER),4)
	------------------------------
	IF (@IS_FINAL_CALCULATION=1) SET @ID = CONVERT(varchar(50), CONVERT(date, GETDATE())) + '_' + @ID;
	------------------------------
	IF (NOT EXISTS (SELECT * FROM PAYMENT_OUT_MESSAGE WHERE ID=@ID))
		BEGIN
			INSERT INTO PAYMENT_OUT_MESSAGE
			(
				ID,
				MESSAGE_TYPE,
				DOCUMENT_NUMBER,
				SECURITY_ID,
				CURRENCY,
				INITIAL_AMOUNT,
				AMOUNT,
				PAYER,
				RECEIVER,
				RECEIVER_NAME,
				IS_FINAL_CALCULATION,
				OPCODE,
				CODE,
				MOMENT
			)
			VALUES
			(
				@ID,
				@MESSAGE_TYPE,
				@DOCUMENT_NUMBER,
				@SECURITY_ID,
				@CURRENCY,
				@INITIAL_AMOUNT,
				@AMOUNT,
				@PAYER,
				@RECEIVER,
				@RECEIVER_NAME,
				@IS_FINAL_CALCULATION,
				@OPCODE,
				@CODE,
				@MOMENT
			);

			delete from PAYMENT_OUT_PARAMETER
			insert into PAYMENT_OUT_PARAMETER (LAST_SEND_DATE,LAST_SEND_NUMBER)
			values (@CURRENT_DATE,@NUMBER)
		END
	ELSE
		BEGIN
			UPDATE PAYMENT_OUT_MESSAGE
			SET 
				MESSAGE_TYPE=@MESSAGE_TYPE,
				SECURITY_ID=@SECURITY_ID,
				CURRENCY=@CURRENCY,
				INITIAL_AMOUNT=@INITIAL_AMOUNT,
				AMOUNT=@AMOUNT,
				PAYER=@PAYER,
				RECEIVER=@RECEIVER,
				RECEIVER_NAME=@RECEIVER_NAME,
				IS_FINAL_CALCULATION=@IS_FINAL_CALCULATION,
				OPCODE=@OPCODE,
				CODE=@CODE,
				MOMENT=@MOMENT
			WHERE ID=@ID
		END
GO
