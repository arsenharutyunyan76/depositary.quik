﻿if exists (select * from sys.objects where name='sp_SetProcessedPaymentMessage' and type='P')
	drop procedure sp_SetProcessedPaymentMessage
GO

create procedure sp_SetProcessedPaymentMessage(@ID varchar(50))
AS
	
	UPDATE PAYMENT_MESSAGE
	SET PROCESSED = 1
	WHERE ID=@ID 

GO
