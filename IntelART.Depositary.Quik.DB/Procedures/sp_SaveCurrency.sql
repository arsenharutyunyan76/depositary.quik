﻿if exists (select * from sys.objects where name='sp_SaveCurrency' and type='P')
	drop procedure sp_SaveCurrency
GO

create procedure sp_SaveCurrency(
	@CODE			varchar(4),
	@CURRENCY_TYPE	tinyint
)
AS
	if exists (select CODE from CURRENCY where CODE=@CODE)
		update CURRENCY
		set CURRENCY_TYPE=@CURRENCY_TYPE
		where CODE=@CODE
	else
		insert into CURRENCY (CODE, CURRENCY_TYPE)
			values (@CODE, @CURRENCY_TYPE)
GO
