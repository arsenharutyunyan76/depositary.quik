﻿if exists (select * from sys.objects where name='sp_SetProcessedPaymentOutMessage' and type='P')
	drop procedure sp_SetProcessedPaymentOutMessage
GO

create procedure sp_SetProcessedPaymentOutMessage( @ID	varchar(50) ) 
AS
	
	UPDATE PAYMENT_OUT_MESSAGE
	SET PROCESSED = 1
	WHERE ID=@ID 

GO
