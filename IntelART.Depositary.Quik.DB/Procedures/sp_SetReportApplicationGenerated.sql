﻿if exists (select * from sys.objects where name='sp_SetReportApplicationGenerated' and type='P')
	drop procedure sp_SetReportApplicationGenerated
GO

create procedure sp_SetReportApplicationGenerated(
	@ID	uniqueidentifier
)
AS
	update REPORT_APPLICATION
	set GENERATE_DATE=getdate()
	where ID=@ID
GO
