﻿if exists (select * from sys.objects where name='sp_GetAcknowledgedBankmailMessage' and type='P')
	drop procedure sp_GetAcknowledgedBankmailMessage
GO

create procedure sp_GetAcknowledgedBankmailMessage(
	@MESSAGE_TYPE		char(3),
	@DOCUMENT_NUMBER	varchar(16)
)
AS
	select ID from BANKMAIL_MESSAGE
	where DOCUMENT_NUMBER=@DOCUMENT_NUMBER
		and MESSAGE_TYPE=@MESSAGE_TYPE
GO
