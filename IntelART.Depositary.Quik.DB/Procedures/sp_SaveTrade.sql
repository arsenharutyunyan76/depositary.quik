﻿if exists (select * from sys.objects where name='sp_SaveTrade' and type='P')
	drop procedure sp_SaveTrade
GO

create procedure sp_SaveTrade(
	@TRADE_NUMBER		decimal(19, 0),
	@TRADE_DATE			datetime,
	@ISIN				char(12),
	@AUCTION_TYPE_ID	tinyint,
	@OPERATION_TYPE_ID	tinyint,
	@PRICE				decimal(19, 8),
	@VALUE				decimal(28, 8),
	@YIELD				decimal(19, 4),
	@QUANTITY			decimal(28, 8),
	@SETTLE_DATE		datetime,
	@BANK_CODE			char(5),
	@CP_BANK_CODE		char(5),
	@CLIENT_CODE		varchar(12),
	@BROKER_REFERENCE	varchar(34),
	@USER_ID			int,
	@ORDER_NUMBER		decimal(19, 0),
	@ID					uniqueidentifier	output)
AS
	set @ID = newid()
	insert into TRADE
	(
		TRADE_NUMBER,
		TRADE_DATE,
		ISIN,
		AUCTION_TYPE_ID,
		OPERATION_TYPE_ID,
		PRICE,
		VALUE,
		YIELD,
		QUANTITY,
		SETTLE_DATE,
		BANK_CODE,
		CP_BANK_CODE,
		CLIENT_CODE,
		BROKER_REFERENCE,
		USER_ID,
		ORDER_NUMBER,
		ID,
		PROCESS_STATUS_ID
	)
	values
	(
		@TRADE_NUMBER,
		@TRADE_DATE,
		@ISIN,
		@AUCTION_TYPE_ID,
		@OPERATION_TYPE_ID,
		@PRICE,
		round(@VALUE,1),
		@YIELD,
		round(@QUANTITY,1),
		@SETTLE_DATE,
		@BANK_CODE,
		@CP_BANK_CODE,
		@CLIENT_CODE,
		@BROKER_REFERENCE,
		@USER_ID,
		@ORDER_NUMBER,
		@ID,
		1
	)
GO
