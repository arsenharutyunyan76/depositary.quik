﻿if exists (select * from sys.objects where name='sp_SetReportTime' and type='P')
	drop procedure sp_SetReportTime
GO

create procedure sp_SetReportTime(
	@ID					tinyint,
	@GENERATION_TIME	char(5),
	@LAST_GENERATE_DATE	datetime,
	@EMAIL				nvarchar(max) = null,
	@EMAIL_SUBJECT		nvarchar(500) = null,
	@EMAIL_BODY			nvarchar(max) = null
)
AS
	update REPORT_SCHEDULE
	set GENERATION_TIME=@GENERATION_TIME
		,LAST_GENERATE_DATE=@LAST_GENERATE_DATE
		,EMAIL=@EMAIL
		,EMAIL_SUBJECT=@EMAIL_SUBJECT
		,EMAIL_BODY=@EMAIL_BODY
	where ID=@ID
GO
