﻿if exists (select * from sys.objects where name='sp_GetBank' and type='P')
	drop procedure sp_GetBank
GO

create procedure sp_GetBank(@CODE	char(5))
AS
	select SHORT_NAME
		,IS_ACTIVE
		,NAME_AM
		,NAME_EN
		,AS_CODE
		,REGISTER_NUMBER
		,REGISTRATIONS_NUMBER
		,EMAIL
	from BANK
	where CODE = @CODE
GO
