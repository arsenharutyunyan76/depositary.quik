﻿if exists (select * from sys.objects where name='sp_SetReportGenerated' and type='P')
	drop procedure sp_SetReportGenerated
GO

create procedure sp_SetReportGenerated(
	@ID	tinyint
)
AS
	update REPORT_SCHEDULE
	set LAST_GENERATE_DATE=getdate()
	where ID=@ID
GO
