﻿IF exists (SELECT * FROM sys.objects WHERE name='sp_GetDependIsin' and type='P')
	DROP PROCEDURE sp_GetDependIsin
GO
CREATE PROCEDURE sp_GetDependIsin(
	@ISIN_CODE	nvarchar(20)
)
AS
	SELECT * FROM dbo.DEPEND_LIST_ISIN WHERE ISIN_CODE=@ISIN_CODE
GO