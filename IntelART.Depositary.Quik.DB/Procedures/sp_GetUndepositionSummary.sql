﻿if exists (select * from sys.objects where name='sp_GetUndepositionSummary' and type='P')
	drop procedure sp_GetUndepositionSummary
GO

create procedure sp_GetUndepositionSummary(
	@MESSAGE_DATE_FROM	date,
	@MESSAGE_DATE_TO	date
)
AS
	DECLARE @maxCurID int = (SELECT MAX(ID) FROM CURRENCY)

	DECLARE @tmpRes_1 TABLE(BANK_CODE varchar(16), ACCOUNT_TYPE bit, DEPO_CURR varchar(20),
							INITIAL_AMOUNT decimal(28,8), DEPOSITION_AMOUNT decimal(28,8), UNDEPOSITION_AMOUNT decimal(28,8), FINAL_AMOUNT decimal(28,8),
							CUR_ID int)
	INSERT INTO @tmpRes_1(BANK_CODE, ACCOUNT_TYPE, DEPO_CURR, INITIAL_AMOUNT, DEPOSITION_AMOUNT, UNDEPOSITION_AMOUNT, FINAL_AMOUNT, CUR_ID)
	SELECT	a.BANK_CODE AS BANK_CODE,
			a.IS_OWN AS ACCOUNT_TYPE,
			CASE WHEN pm.CODE='CDV' AND c.CODE IS NULL THEN pm.SECURITY_ID
				 WHEN pm.CODE='BANV' OR (pm.CODE='CDV' AND c.CODE IS NOT NULL) THEN pm.CURRENCY END AS DEPO_CURR,
			pm.INITIAL_AMOUNT AS INITIAL_AMOUNT,
			CASE WHEN pm.OPCODE='i' THEN pm.AMOUNT ELSE 0 END AS DEPOSITION_AMOUNT,
			CASE WHEN pm.OPCODE='o' THEN pm.AMOUNT ELSE 0 END AS UNDEPOSITION_AMOUNT,
			CASE WHEN pm.IS_FINAL_CALCULATION=1 THEN pm.AMOUNT ELSE 0 END AS FINAL_AMOUNT,
			ISNULL(c.ID,@maxCurID+1) AS CUR_ID
	FROM PAYMENT_OUT_MESSAGE AS pm
	JOIN ACCOUNT AS a ON a.CODE=pm.RECEIVER
	LEFT JOIN CURRENCY AS c ON c.CODE=pm.CURRENCY
	WHERE convert(date,pm.CREATION_DATE) between ISNULL(@MESSAGE_DATE_FROM,convert(date,pm.CREATION_DATE)) and ISNULL(@MESSAGE_DATE_TO,convert(date,pm.CREATION_DATE))

	------------------------------------------------------------------------------------------------

	DECLARE @tmpRes_2 TABLE(BANK_CODE varchar(16), ACCOUNT_TYPE bit, DEPO_CURR varchar(20),
							INITIAL_AMOUNT decimal(28,8), DEPOSITION_AMOUNT decimal(28,8), UNDEPOSITION_AMOUNT decimal(28,8), FINAL_AMOUNT decimal(28,8),
							DIFF_AMOUNT decimal(28,8), CUR_ID int
							)
	INSERT INTO @tmpRes_2(BANK_CODE, ACCOUNT_TYPE, DEPO_CURR, INITIAL_AMOUNT, DEPOSITION_AMOUNT, UNDEPOSITION_AMOUNT, FINAL_AMOUNT, DIFF_AMOUNT, CUR_ID)
	SELECT	BANK_CODE AS BANK_CODE,
			ACCOUNT_TYPE AS ACCOUNT_TYPE,
			DEPO_CURR AS DEPO_CURR,
			SUM(INITIAL_AMOUNT) AS INITIAL_AMOUNT,
			SUM(DEPOSITION_AMOUNT) AS DEPOSITION_AMOUNT,
			SUM(UNDEPOSITION_AMOUNT) AS UNDEPOSITION_AMOUNT,
			SUM(FINAL_AMOUNT) AS FINAL_AMOUNT,
			SUM(FINAL_AMOUNT)-SUM(INITIAL_AMOUNT) AS DIFF_AMOUNT,
			MAX(t1.CUR_ID) AS CUR_ID
	FROM @tmpRes_1 AS t1
	GROUP BY BANK_CODE, ACCOUNT_TYPE, DEPO_CURR

	------------------------------------------------------------------------------------------------

	DECLARE @tmpRes_3 TABLE(ID int IDENTITY(1,1),
							BANK_CODE varchar(16), ACCOUNT_TYPE bit, DEPO_CURR varchar(20),
							INITIAL_AMOUNT decimal(28,8), DEPOSITION_AMOUNT decimal(28,8), UNDEPOSITION_AMOUNT decimal(28,8), FINAL_AMOUNT decimal(28,8),
							DIFF_AMOUNT decimal(28,8), IS_CURRENCY bit
							)
	INSERT INTO @tmpRes_3(BANK_CODE, ACCOUNT_TYPE, DEPO_CURR, INITIAL_AMOUNT, DEPOSITION_AMOUNT, UNDEPOSITION_AMOUNT, FINAL_AMOUNT, DIFF_AMOUNT, IS_CURRENCY)
	SELECT	BANK_CODE,
			ACCOUNT_TYPE,
			CASE WHEN t2.CUR_ID<=@maxCurID THEN t2.DEPO_CURR COLLATE SQL_Latin1_General_CP1_CS_AS ELSE i.ISIN_CODE COLLATE SQL_Latin1_General_CP1_CS_AS END AS DEPO_CURR,
			INITIAL_AMOUNT,
			DEPOSITION_AMOUNT,
			UNDEPOSITION_AMOUNT,
			FINAL_AMOUNT,
			DIFF_AMOUNT,
			CASE WHEN t2.CUR_ID<=@maxCurID THEN 1 ELSE 0 END AS IS_CURRENCY
	FROM @tmpRes_2 AS t2
	LEFT JOIN ISIN AS i ON i.RTS_CODE COLLATE SQL_Latin1_General_CP1_CS_AS=t2.DEPO_CURR COLLATE SQL_Latin1_General_CP1_CS_AS
	ORDER BY BANK_CODE, CUR_ID

	------------------------------------------------------------------------------------------------

	SELECT ID, BANK_CODE, ACCOUNT_TYPE, DEPO_CURR, INITIAL_AMOUNT, DEPOSITION_AMOUNT, UNDEPOSITION_AMOUNT, FINAL_AMOUNT, DIFF_AMOUNT, IS_CURRENCY
	FROM @tmpRes_3
GO
