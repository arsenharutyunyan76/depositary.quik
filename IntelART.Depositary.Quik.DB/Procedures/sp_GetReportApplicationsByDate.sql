﻿if exists (select * from sys.objects where name='sp_GetReportApplicationsByDate' and type='P')
	drop procedure sp_GetReportApplicationsByDate
GO

create procedure sp_GetReportApplicationsByDate(
	@DATE_FROM	date,
	@DATE_TO	date
)
AS
	select ID
		,CREATION_DATE
		,REPORT_ID
		,REPORT_FROM_DATE
		,REPORT_TO_DATE
		,GENERATE_DATE
	from REPORT_APPLICATION
	where convert(date,CREATION_DATE) between @DATE_FROM and @DATE_TO
GO
