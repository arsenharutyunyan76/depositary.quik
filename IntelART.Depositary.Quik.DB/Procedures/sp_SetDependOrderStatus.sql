﻿if exists (select * from sys.objects where name='sp_SetDependOrderStatus' and type='P')
	drop procedure sp_SetDependOrderStatus
GO

create procedure sp_SetDependOrderStatus(
	@ATSORDER_ID int,
	@IS_CANCELED bit
)
AS
	update DEPEND_LIST_ORDER
	set IS_CANCELED=@IS_CANCELED
	where ATSORDER_ID=@ATSORDER_ID
GO
