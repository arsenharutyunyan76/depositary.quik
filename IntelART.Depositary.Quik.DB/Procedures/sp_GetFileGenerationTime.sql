﻿if exists (select * from sys.objects where name='sp_GetFileGenerationTime' and type='P')
	drop procedure sp_GetFileGenerationTime
GO

create procedure sp_GetFileGenerationTime(@FILE_TYPE tinyint)
AS
	select FILE_TYPE, GENERATE_DATE
	from FILE_GENERATION_TIME
	where FILE_TYPE = @FILE_TYPE
GO
