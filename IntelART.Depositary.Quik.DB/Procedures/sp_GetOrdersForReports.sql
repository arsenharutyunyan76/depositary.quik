﻿if exists (select * from sys.objects where name='sp_GetOrdersForReports' and type='P')
	drop procedure sp_GetOrdersForReports
GO

create procedure sp_GetOrdersForReports(
	@FromDate	date,
	@ToDate		date
)
AS
	select o.ID
		,o.ORDER_NUMBER
		,o.ORDER_DATE
		,o.ISIN
		,o.AUCTION_TYPE_ID
		,o.OPERATION_TYPE_ID
		,o.PRICE
		,o.VALUE
		,o.YIELD
		,o.QUANTITY
		,o.BALANCE
		,o.WITHDRAW_DATE
		,o.BANK_CODE
		,o.CLIENT_CODE
		,o.BROKER_REFERENCE
		,o.USER_ID
		,o.ORDER_STATUS_ID
		,a.AUCTION_ID
		,a.TIME_FROM
		,a.TIME_TO
		,a.ORDER_TIME_TO
	from [ORDER] o
	join AUCTION a
		on a.AUCTION_DATE=convert(date,o.ORDER_DATE) and a.ISIN=o.ISIN and a.AUCTION_TYPE_ID=o.AUCTION_TYPE_ID
	where convert(date,o.ORDER_DATE) between isnull(@FromDate,'2001-01-01') and @ToDate
	order by o.ORDER_DATE
GO
