﻿if exists (select * from sys.objects where name='sp_SaveAuctionAnnouncement' and type='P')
	drop procedure sp_SaveAuctionAnnouncement
GO

create procedure sp_SaveAuctionAnnouncement(
	@EMAIL			nvarchar(max) = null,
	@EMAIL_SUBJECT	nvarchar(500) = null,
	@EMAIL_BODY		nvarchar(max) = null
)
AS
	update AUCTION_ANNOUNCEMENT
	set EMAIL=@EMAIL
		,EMAIL_SUBJECT=@EMAIL_SUBJECT
		,EMAIL_BODY=@EMAIL_BODY
GO
