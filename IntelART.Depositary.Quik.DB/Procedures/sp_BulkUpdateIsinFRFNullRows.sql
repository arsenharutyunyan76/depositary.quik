﻿IF EXISTS (SELECT * FROM sys.objects WHERE NAME='sp_BulkUpdateIsinFRFNullRows' and TYPE='P')
	DROP PROCEDURE sp_BulkUpdateIsinFRFNullRows
GO

CREATE PROCEDURE sp_BulkUpdateIsinFRFNullRows
	@Isins IsinCompact READONLY
AS
	BEGIN TRANSACTION
	BEGIN TRY
		MERGE ISIN AS dbIsin  
		USING @Isins AS tblIsin  
		ON (dbIsin.ID = tblIsin.ID)  
  
		WHEN MATCHED THEN  
			UPDATE SET dbIsin.FORMULA = tblIsin.FORMULA,   
					   dbIsin.FREQUENCY = tblIsin.FREQUENCY,
					   dbIsin.RATE = tblIsin.RATE;
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessage varchar(4000)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 17, 1)
		RETURN
	END CATCH
	COMMIT TRANSACTION
GO
