﻿if exists (select * from sys.objects where name='sp_GetAuctionType' and type='P')
	drop procedure sp_GetAuctionType
GO

create procedure sp_GetAuctionType(
	@ID	tinyint
)
AS
	select NAME_AM,NAME_EN
	from AUCTION_TYPE
	where ID=@ID
GO
