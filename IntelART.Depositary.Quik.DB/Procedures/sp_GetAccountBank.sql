﻿if exists (select * from sys.objects where name='sp_GetAccountBank' and type='P')
	drop procedure sp_GetAccountBank
GO

create procedure sp_GetAccountBank (@CODE varchar(16))
AS
	select ID, BANK_CODE, CODE, CURRENCY, IS_OWN, CUSTOMER_CODE, SECURITY_ACCOUNT
	from ACCOUNT
	where CODE = @CODE
GO
