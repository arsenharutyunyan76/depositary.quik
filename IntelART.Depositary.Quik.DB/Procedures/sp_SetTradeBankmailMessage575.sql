﻿if exists (select * from sys.objects where name='sp_SetTradeBankmailMessage575' and type='P')
	drop procedure sp_SetTradeBankmailMessage575
GO

create procedure sp_SetTradeBankmailMessage575(
	@ID							uniqueidentifier,
	@BANKMAIL_MESSAGE_575_ID	uniqueidentifier
)
AS
	update TRADE
	set BANKMAIL_MESSAGE_575_ID=@BANKMAIL_MESSAGE_575_ID
		,PROCESS_STATUS_ID=2
	where ID=@ID
GO
