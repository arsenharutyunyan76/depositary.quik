﻿if exists (select * from sys.objects where name='sp_SetAuctionNumber' and type='P')
	drop procedure sp_SetAuctionNumber
GO

create procedure sp_SetAuctionNumber(@AUCTION_DATE	date,
									 @AUCTION_TYPE	bit,
									 @NUMBER		int)
AS
	if exists (select * from AUCTION_PARAMETER where AUCTION_DATE = @AUCTION_DATE and AUCTION_TYPE = @AUCTION_TYPE)
		update AUCTION_PARAMETER
			set NUMBER = @NUMBER
			where AUCTION_DATE = @AUCTION_DATE
				and AUCTION_TYPE = @AUCTION_TYPE
	else
		insert into AUCTION_PARAMETER (AUCTION_DATE,AUCTION_TYPE,NUMBER)
			values (@AUCTION_DATE,@AUCTION_TYPE,@NUMBER)
GO
