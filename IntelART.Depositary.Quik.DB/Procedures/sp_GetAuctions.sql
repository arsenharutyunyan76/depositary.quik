﻿if exists (select * from sys.objects where name='sp_GetAuctions' and type='P')
	drop procedure sp_GetAuctions
GO

create procedure sp_GetAuctions(
	@AUCTION_DATE_FROM	date,
	@AUCTION_DATE_TO	date,
	@ISIN				char(12) = null
)
AS
	if rtrim(@ISIN)=''
		set @ISIN=null
	select a.CREATION_DATE,a.ID,a.AUCTION_ID,a.ISIN,a.AUCTION_EXTRA_ID,a.AUCTION_DATE,a.SETTLEMENT_DATE,a.AUCTION_TYPE_ID
		,a.YIELD,a.ISSUE_DATE,a.ISSUE_AMOUNT,a.MATURITY_DATE,a.MATURITY_PAYMENT_DATE,a.FIRST_PAYMENT_DATE
		,a.MINIMUM_AMOUNT,a.MAXIMUM_AMOUNT,a.AMOUNT,a.AUCTION_YIELD,a.TIME_FROM,a.TIME_TO,a.ORDER_TIME_TO,a.IS_CANCELLED
		,t.NAME_AM as AUCTION_TYPE_NAME_AM,t.NAME_EN as AUCTION_TYPE_NAME_EN
	from AUCTION a
	join AUCTION_TYPE t
		on a.AUCTION_TYPE_ID=t.ID
	where a.AUCTION_DATE between @AUCTION_DATE_FROM and @AUCTION_DATE_TO
		and isnull(@ISIN,a.ISIN)=a.ISIN
GO
