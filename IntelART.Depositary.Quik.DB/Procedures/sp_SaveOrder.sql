﻿if exists (select * from sys.objects where name='sp_SaveOrder' and type='P')
	drop procedure sp_SaveOrder
GO

create procedure sp_SaveOrder(
	@ORDER_NUMBER		decimal(19, 0),
	@ORDER_DATE			datetime,
	@ISIN				char(12),
	@OPERATION_TYPE_ID	tinyint,
	@AUCTION_TYPE_ID	tinyint,
	@PRICE				decimal(19, 8),
	@VALUE				decimal(28, 8),
	@YIELD				decimal(19, 4),
	@QUANTITY			decimal(28, 8),
	@BALANCE			decimal(28, 8),
	@WITHDRAW_DATE		datetime,
	@BANK_CODE			char(5),
	@CLIENT_CODE		varchar(12),
	@BROKER_REFERENCE	varchar(34),
	@USER_ID			int,
	@ORDER_STATUS_ID	tinyint,
	@ID					uniqueidentifier	output)
AS
	set @ID = newid()
	insert into [ORDER]
	(
		ORDER_NUMBER,
		ORDER_DATE,
		ISIN,
		AUCTION_TYPE_ID,
		OPERATION_TYPE_ID,
		PRICE,
		VALUE,
		YIELD,
		QUANTITY,
		BALANCE,
		WITHDRAW_DATE,
		BANK_CODE,
		CLIENT_CODE,
		BROKER_REFERENCE,
		USER_ID,
		ORDER_STATUS_ID,
		ID
	)
	values
	(
		@ORDER_NUMBER,
		@ORDER_DATE,
		@ISIN,
		@AUCTION_TYPE_ID,
		@OPERATION_TYPE_ID,
		@PRICE,
		round(@VALUE,1),
		@YIELD,
		round(@QUANTITY,1),
		@BALANCE,
		@WITHDRAW_DATE,
		@BANK_CODE,
		@CLIENT_CODE,
		@BROKER_REFERENCE,
		@USER_ID,
		@ORDER_STATUS_ID,
		@ID
	)
GO
