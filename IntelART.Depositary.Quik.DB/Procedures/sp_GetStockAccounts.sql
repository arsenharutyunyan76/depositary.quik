﻿if exists (select * from sys.objects where name='sp_GetStockAccounts' and type='P')
	drop procedure sp_GetStockAccounts
GO

create procedure sp_GetStockAccounts
AS
	select CURRENCY, ACCOUNT
	from STOCK_ACCOUNT
GO
