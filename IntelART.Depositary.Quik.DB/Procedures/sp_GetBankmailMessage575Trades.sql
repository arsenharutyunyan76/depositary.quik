﻿if exists (select * from sys.objects where name='sp_GetBankmailMessage575Trades' and type='P')
	drop procedure sp_GetBankmailMessage575Trades
GO

create procedure sp_GetBankmailMessage575Trades(
	@ID	uniqueidentifier
)
AS
	select ID
		,TRADE_NUMBER
		,TRADE_DATE
		,ISIN
		,AUCTION_TYPE_ID
		,OPERATION_TYPE_ID
		,VALUE
		,YIELD
		,QUANTITY
		,SETTLE_DATE
		,BANK_CODE
		,CP_BANK_CODE
		,CLIENT_CODE
		,BROKER_REFERENCE
		,USER_ID
	from TRADE
	where BANKMAIL_MESSAGE_575_ID=@ID
		and PROCESS_STATUS_ID=2
GO
