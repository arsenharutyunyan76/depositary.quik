﻿if exists (select * from sys.objects where name='sp_GetRTSRegistrationsAsset' and type='P')
	drop procedure sp_GetRTSRegistrationsAsset
GO

create procedure sp_GetRTSRegistrationsAsset(
	@MESSAGE_DATE_FROM	date,
	@MESSAGE_DATE_TO	date
)
AS
	select 	convert(date,pom.CREATION_DATE)	AS CREATION_DATE,
			pom.RECEIVER_NAME	AS RECEIVER_NAME,
			a.IS_OWN			AS IS_OWN,
			pom.CODE			AS CODE,
			pom.SECURITY_ID		AS SECURITY_ID,
			pom.CURRENCY		AS CURRENCY,
			SUM(pom.AMOUNT)		AS AMOUNT
	from PAYMENT_OUT_MESSAGE pom
	INNER JOIN ACCOUNT a ON a.CODE=pom.RECEIVER
	where convert(date,CREATION_DATE) between ISNULL(@MESSAGE_DATE_FROM,convert(date,CREATION_DATE)) and ISNULL(@MESSAGE_DATE_TO,convert(date,CREATION_DATE))
			AND IS_FINAL_CALCULATION=1
	group by convert(date,pom.CREATION_DATE), pom.RECEIVER_NAME, a.IS_OWN, pom.CODE, pom.SECURITY_ID, pom.CURRENCY

GO
