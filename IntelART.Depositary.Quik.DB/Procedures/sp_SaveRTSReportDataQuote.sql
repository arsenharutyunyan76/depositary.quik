﻿if exists (select * from sys.objects where name='sp_SaveRTSReportDataQuote' and type='P')
	drop procedure sp_SaveRTSReportDataQuote
GO

create procedure sp_SaveRTSReportDataQuote(
	@ID				INT			,
	@STATUS			TINYINT		,
	@ISSUE_NAME		VARCHAR(7)	,
	@PRICE			NUMERIC(16,5),
	@YIELD			NUMERIC(16,5),
	@HISTORY_ID		INT			,
	@HISTORY_ACTION	VARCHAR(1)	,
	@HISTORY_MOMENT	DATETIME	,
	@LSTG			VARCHAR(7)	,

	@INIT_QTY		INT			,
	@MOMENT			DATETIME	,
	@FIRM_NAME		VARCHAR(7)	,
	@WKS			VARCHAR(31) ,
	@SETTL_PAIR		VARCHAR(7)	,
	@ORDER_TYPE		TINYINT		,
	@ALL_NON		TINYINT		,
	@LEAVE			INT			,
	@CATALYST		TINYINT		,
	@TYPE			VARCHAR(1)	,

	@HISTORY_EVENT	NUMERIC(10,0),
	@DEPO_ACCOUNTS	VARCHAR(3),

	@QUOTE_QTY		INT
	)
AS
	INSERT INTO QUOTE_HIST
	(
		ID				,
		STATUS			,
		ISSUE_NAME		,
		PRICE			,
		YIELD			,
		HISTORY_ID		,
		HISTORY_ACTION	,
		HISTORY_MOMENT	,
		LSTG			,

		INIT_QTY		,
		MOMENT			,
		FIRM_NAME		,
		WKS				,
		SETTL_PAIR		,
		ORDER_TYPE		,
		ALL_NON			,
		LEAVE			,
		CATALYST		,
		TYPE			,

		HISTORY_EVENT	,
		DEPO_ACCOUNTS	,

		QUOTE_QTY
	)
	VALUES
	(
		@ID				,
		@STATUS			,
		@ISSUE_NAME		,
		@PRICE			,
		@YIELD			,
		@HISTORY_ID		,
		@HISTORY_ACTION	,
		@HISTORY_MOMENT	,
		@LSTG			,

		@INIT_QTY		,
		@MOMENT			,
		@FIRM_NAME		,
		@WKS			,
		@SETTL_PAIR		,
		@ORDER_TYPE		,
		@ALL_NON		,
		@LEAVE			,
		@CATALYST		,
		@TYPE			,

		@HISTORY_EVENT	,
		@DEPO_ACCOUNTS	,

		@QUOTE_QTY
	)
GO
