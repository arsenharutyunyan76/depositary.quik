sp_configure 'show advanced options', 1
GO
RECONFIGURE
GO
sp_configure 'clr enabled', 1
GO
RECONFIGURE
GO

USE master
GO

CREATE DATABASE $(DatabaseName)
GO
USE $(DatabaseName)
GO

ALTER DATABASE $(DatabaseName) SET TRUSTWORTHY ON
GO

ALTER DATABASE $(DatabaseName) SET ENABLE_BROKER
GO
