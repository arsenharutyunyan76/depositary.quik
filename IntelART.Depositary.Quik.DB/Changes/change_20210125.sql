﻿IF NOT EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'LAST_OPERATION_DATE'
          AND Object_ID = Object_ID(N'dbo.ACTION_TYPE'))
BEGIN
	ALTER TABLE dbo.ACTION_TYPE
	ADD	LAST_OPERATION_DATE datetime NULL
END