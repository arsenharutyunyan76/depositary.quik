﻿if exists (select * from sys.objects where name='fn_SplitString' and type='TF')
	drop function fn_SplitString
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION fn_SplitString
(
    @delimitedString NVARCHAR(MAX),
    @delimiter NVARCHAR(100)
)
RETURNS @tmp
TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
	DECLARE @xml XML = N'<t>' + REPLACE(@delimitedString, @delimiter,'</t><t>') + '</t>'
    INSERT INTO @tmp(val)
    SELECT RTRIM(LTRIM(r.value('.','varchar(MAX)'))) as item
    FROM @xml.nodes('/t') as records(r)
    RETURN
END
GO



if exists (select * from sys.objects where name='sp_GetRTSOutMessageUndeposition' and type='P')
	drop procedure sp_GetRTSOutMessageUndeposition
GO

create procedure sp_GetRTSOutMessageUndeposition(@MSG_CODE	varchar(max))
AS
	DECLARE @tmp_MSG_CODE_list TABLE (MSG_CODE VARCHAR(20))
	INSERT INTO @tmp_MSG_CODE_list(MSG_CODE)
	SELECT DISTINCT val
	FROM dbo.fn_SplitString(@MSG_CODE, ',')

	declare @CURRENT_DATE date = getdate()

	SELECT a.id as ID, a.rev as REV, a.moment as MOMENT, a.firm_name as FIRM_NAME, a.settl_pair as SETTL_PAIR,
	a.depo_accounts as DEPO_ACCOUNTS, a.name as NAME,
			a.i_code as I_CODE, a.active_type as ACTIVE_TYPE, a.code as CODE, a.acct_num as ACCT_NUM, a.free as FREE,
			a.opcode as OPCODE, a.ext_id as EXT_ID, a.status as STATUS,
			/*a.initial*/ NULL as INITIAL_AMOUNT, /*a.blocked*/ NULL as BLOCKED, /*a.accounts*/ NULL as ACCOUNTS
	FROM RTSGateway.dbo.fn_GetNotProcessedASSET_IO()  -- FROM dbo.AssetIO a with (nolock)
	INNER JOIN @tmp_MSG_CODE_list m ON m.MSG_CODE=a.code
	WHERE a.opcode='o' -- AND convert(Date,a.moment)=@CURRENT_DATE
GO



if exists (select * from sys.objects where name='sp_GetRTSOutMessageFinalCalculation' and type='P')
	drop procedure sp_GetRTSOutMessageFinalCalculation
GO

create procedure sp_GetRTSOutMessageFinalCalculation(@MSG_CODE	varchar(max))
AS
	DECLARE @tmp_MSG_CODE_list TABLE (MSG_CODE VARCHAR(20))
	INSERT INTO @tmp_MSG_CODE_list(MSG_CODE)
	SELECT DISTINCT val
	FROM dbo.fn_SplitString(@MSG_CODE, ',')

	declare @CURRENT_DATE date = getdate()

	SELECT a.id as ID, a.rev as REV, /*a.moment*/ NULL as MOMENT, a.firm_name as FIRM_NAME, /*a.settl_pair*/ NULL as SETTL_PAIR,
			/*a.depo_accounts*/ NULL as DEPO_ACCOUNTS, a.name as NAME,
			a.i_code as I_CODE, a.active_type as ACTIVE_TYPE, a.code as CODE, a.acct_num as ACCT_NUM, a.free as FREE,
			/*a.opcode*/ NULL as OPCODE, /*a.ext_id*/ NULL as EXT_ID, /*a.status*/ NULL as STATUS,
			a.initial as INITIAL_AMOUNT, a.blocked as BLOCKED,	a.accounts as ACCOUNTS
	FROM dbo.Asset a with (nolock)
	INNER JOIN @tmp_MSG_CODE_list m ON m.MSG_CODE=a.code
	WHERE a.free<>0
GO



if exists (select * from sys.objects where name='sp_GetDependUndepositionInfo' and type='P')
	drop procedure sp_GetDependUndepositionInfo
GO

create procedure sp_GetDependUndepositionInfo(@MSG_CODE	varchar(max))
AS
	DECLARE @tmp_MSG_CODE_list TABLE (MSG_CODE VARCHAR(20))
	INSERT INTO @tmp_MSG_CODE_list(MSG_CODE)
	SELECT DISTINCT val
	FROM dbo.fn_SplitString(@MSG_CODE, ',')

	declare @CURRENT_DATE date = getdate()

	SELECT  a.settl_pair as SETTL_PAIR, a.depo_accounts as DEPO_ACCOUNTS, a.name as ISIN_SHORT_NAME, a.firm_name as DEPOPART_CODE,
			a.acct_num as ACCOUNT_REFERENCE, a.free as BALCHANGE_QTY,
			case when a.depo_accounts='DCC' then s.bank_num else s.depo_num end as LINKED_ACCOUNT_REFERENCE
	FROM dbo.AssetIO a with (nolock)
	INNER JOIN @tmp_MSG_CODE_list m ON m.MSG_CODE=a.code
	INNER JOIN dbo.SettlPair s ON a.settl_pair=s.settl_pair
	WHERE convert(Date,a.moment)=@CURRENT_DATE AND a.opcode='o'
GO



if exists (select * from sys.objects where name='sp_GetDependUndepositionFinalCalculationInfo' and type='P')
	drop procedure sp_GetDependUndepositionFinalCalculationInfo
GO

create procedure sp_GetDependUndepositionFinalCalculationInfo

AS
  SELECT  RIGHT(a.acct_num, 7) as SETTL_PAIR, a.accounts as DEPO_ACCOUNTS, a.name as ISIN_SHORT_NAME, a.firm_name as DEPOPART_CODE,
			LEFT(a.acct_num, LEN(a.acct_num)-7)  as ACCOUNT_REFERENCE, a.free as BALCHANGE_QTY,
			case when a.accounts='DCC'
			then LEFT(s.bank_num, LEN(s.bank_num)-7)
			else LEFT(s.depo_num, LEN(s.depo_num)-7)
			end as LINKED_ACCOUNT_REFERENCE
	FROM dbo.Asset a with (nolock)
	INNER JOIN dbo.SettlPair s ON RIGHT(a.acct_num, 7)=s.settl_pair
	WHERE accounts in ('DCC', 'BNK')
GO
