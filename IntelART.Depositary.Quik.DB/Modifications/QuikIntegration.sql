﻿ALTER DATABASE Quik SET ENABLE_BROKER with rollback immediate
GO



alter table Trades
add
	ID				uniqueidentifier	NOT NULL	default newid(),
	PROCESSED_ID	uniqueidentifier	NULL
GO



if exists (select * from sys.objects where name='tTrades1' and type='TR')
	drop trigger dbo.tTrades1
GO

CREATE TRIGGER dbo.tTrades1
   ON dbo.Trades
   AFTER UPDATE
AS
BEGIN
	declare @OldProcessedID uniqueidentifier,@NewProcessedID uniqueidentifier
	select @OldProcessedID=PROCESSED_ID from deleted
	select @NewProcessedID=PROCESSED_ID from inserted
	if @OldProcessedID is not null and @NewProcessedID is not null
		and @OldProcessedID<>@NewProcessedID
	BEGIN
		RAISERROR ('Trade is already processed',10,1)
		ROLLBACK TRANSACTION
	END
END
GO



if exists (select * from sys.objects where name='sp_ReplicateTrade' and type='P')
	drop procedure sp_ReplicateTrade
GO

create procedure sp_ReplicateTrade(
	@ID				uniqueidentifier,
	@PROCESSED_ID	uniqueidentifier
)
AS
	update Trades
	set PROCESSED_ID=@PROCESSED_ID
	where ID=@ID
GO



alter table Orders
add
	ID				uniqueidentifier	NOT NULL	default newid(),
	PROCESSED_ID	uniqueidentifier	NULL
GO



if exists (select * from sys.objects where name='tOrders1' and type='TR')
	drop trigger dbo.tOrders1
GO

CREATE TRIGGER dbo.tOrders1
   ON dbo.Orders
   AFTER UPDATE
AS
BEGIN
	declare @OldProcessedID uniqueidentifier,@NewProcessedID uniqueidentifier
	select @OldProcessedID=PROCESSED_ID from deleted
	select @NewProcessedID=PROCESSED_ID from inserted
	if @OldProcessedID is not null and @NewProcessedID is not null
		and @OldProcessedID<>@NewProcessedID
	BEGIN
		RAISERROR ('Order is already processed',10,1)
		ROLLBACK TRANSACTION
	END
END
GO

if exists (select * from sys.objects where name='sp_ReplicateOrder' and type='P')
	drop procedure sp_ReplicateOrder
GO

create procedure sp_ReplicateOrder(
	@ID				uniqueidentifier,
	@PROCESSED_ID	uniqueidentifier
)
AS
	update Orders
	set PROCESSED_ID=@PROCESSED_ID
	where ID=@ID
GO

----------------------------PARAMS----------------------------------------------------

alter table Params
add
	ID				uniqueidentifier	NOT NULL	default newid(),
	PROCESSED_ID	uniqueidentifier	NULL
GO

if exists (select * from sys.objects where name='tParams1' and type='TR')
	drop trigger dbo.tParams1
GO

CREATE TRIGGER dbo.tParams1
   ON dbo.Params
   AFTER UPDATE
AS
BEGIN
	declare @OldProcessedID uniqueidentifier,@NewProcessedID uniqueidentifier
	select @OldProcessedID=PROCESSED_ID from deleted
	select @NewProcessedID=PROCESSED_ID from inserted
	if @OldProcessedID is not null and @NewProcessedID is not null
		and @OldProcessedID<>@NewProcessedID
	BEGIN
		RAISERROR ('Param is already processed',10,1)
		ROLLBACK TRANSACTION
	END
END
GO

if exists (select * from sys.objects where name='sp_ReplicateAuctionParam' and type='P')
	drop procedure sp_ReplicateAuctionParam
GO

create procedure sp_ReplicateAuctionParam(
	@ID				uniqueidentifier,
	@PROCESSED_ID	uniqueidentifier
)
AS
	update Params
	set PROCESSED_ID=@PROCESSED_ID
	where ID=@ID
GO