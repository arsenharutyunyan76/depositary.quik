﻿if exists (select * from sys.objects where name='sp_GetClientFirms' and type='P')
	drop procedure sp_GetClientFirms
GO

create procedure sp_GetClientFirms
AS
	SELECT DISTINCT C.CLIENT_CODE
		,F.FIRM_ID
	FROM USER_SECC_CLIENT C with (nolock)
	JOIN USER_SECC_FIRM F with (nolock) ON F.USER_ID = C.USER_ID
	WHERE c.CLIENT_CODE NOT IN ('Agent', 'Principal')
GO
