﻿if exists (select * from sys.objects where name='DEPEND_LIST_HOLDER' and type='U')
	drop table dbo.DEPEND_LIST_HOLDER
GO

CREATE TABLE dbo.DEPEND_LIST_HOLDER (
	ID			int			identity(1, 1),
	ACCOUNT_ID	int,
	ACCOUNT_REFERENCE	varchar(50)		NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX DEPEND_LIST_HOLDER ON dbo.DEPEND_LIST_HOLDER(ID)
GO