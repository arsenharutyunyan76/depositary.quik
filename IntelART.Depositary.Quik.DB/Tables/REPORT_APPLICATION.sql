﻿if exists (select * from sys.objects where name='REPORT_APPLICATION' and type='U')
	drop table REPORT_APPLICATION
GO

CREATE TABLE REPORT_APPLICATION
(
	ID					uniqueidentifier	NOT NULL default newid(),
	CREATION_DATE		date				NOT NULL default getdate(),
	REPORT_ID			tinyint				NOT NULL,
	REPORT_FROM_DATE	date				NOT NULL,
	REPORT_TO_DATE		date				NOT NULL,
	GENERATE_DATE		datetime			NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iREPORT_APPLICATION1 ON dbo.REPORT_APPLICATION(ID)
GO

CREATE INDEX iREPORT_APPLICATION2 ON dbo.REPORT_APPLICATION(GENERATE_DATE,CREATION_DATE)
GO
