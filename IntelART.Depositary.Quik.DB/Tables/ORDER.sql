﻿if exists (select * from sys.objects where name='ORDER' and type='U')
	drop table [ORDER]
GO

CREATE TABLE [ORDER](
	ID					uniqueidentifier	NOT NULL,
	CREATION_DATE		datetime			NOT NULL default getdate(),
	ORDER_NUMBER		decimal(19, 0)		NOT NULL,
	ORDER_DATE			datetime			NOT NULL,
	ISIN				char(12)			NOT	NULL,
	OPERATION_TYPE_ID	tinyint				NOT NULL,
	AUCTION_TYPE_ID		tinyint				NOT NULL,
	PRICE				decimal(19, 8)		NOT NULL,
	VALUE				decimal(28, 8)		NOT NULL,
	YIELD				decimal(19, 4)		NOT NULL,
	QUANTITY			decimal(28, 8)		NOT NULL,
	BALANCE				decimal(28, 8)		NOT NULL,
	WITHDRAW_DATE		datetime			NULL,
	BANK_CODE			char(5)				NOT NULL,
	CLIENT_CODE			varchar(12)			NOT NULL,
	BROKER_REFERENCE	varchar(34)			NOT NULL,
	USER_ID				int					NOT NULL,
	ORDER_STATUS_ID		tinyint				NOT NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iORDER1 ON [ORDER] (ID)
GO
