﻿if exists (select * from sys.objects where name='BANKMAIL_STATUS' and type='U')
	drop table BANKMAIL_STATUS
GO

CREATE TABLE BANKMAIL_STATUS
(
	ID		tinyint			identity(1,1),
	NAME_AM	nvarchar(50)	NOT NULL,
	NAME_EN	varchar(50)		NOT NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iBANKMAIL_STATUS1 ON dbo.BANKMAIL_STATUS(ID)
GO



INSERT INTO BANKMAIL_STATUS
(
	NAME_AM,
	NAME_EN
)
VALUES
(N'Ուղարկված', 'Sent'),
(N'Հաստատված', 'Approved'),
(N'Մերժված', 'Rejected');
GO
