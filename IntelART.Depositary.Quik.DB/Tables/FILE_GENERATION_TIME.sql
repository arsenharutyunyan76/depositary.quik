﻿if exists (select * from sys.objects where name='FILE_GENERATION_TIME' and type='U')
	drop table FILE_GENERATION_TIME
GO

CREATE TABLE FILE_GENERATION_TIME
(
	FILE_TYPE		tinyint			NOT NULL,
	GENERATE_DATE	datetime		NULL,
	DESCRIPTION		varchar(200)	NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iFILE_GENERATION_TIME1 ON dbo.FILE_GENERATION_TIME(FILE_TYPE)
GO



insert into FILE_GENERATION_TIME (FILE_TYPE, GENERATE_DATE, DESCRIPTION)
values
	(1, NULL, 'RTS file from Depend is created'),
	(2, NULL, 'RTS file from Bankmail is created'),
	(3, NULL, 'Undeposition action is done'),
	(4, NULL, 'Quik file is created')
GO
