﻿if exists (select * from sys.objects where name='PAYMENT_OUT_PARAMETER' and type='U')
	drop table PAYMENT_OUT_PARAMETER
GO

CREATE TABLE PAYMENT_OUT_PARAMETER(
	LAST_SEND_DATE		date	NULL,
	LAST_SEND_NUMBER	int		NULL
)
GO
