﻿if exists (select * from sys.objects where name='TRADE' and type='U')
	drop table TRADE
GO

CREATE TABLE TRADE(
	ID						uniqueidentifier	NOT NULL,
	CREATION_DATE			datetime			NOT NULL default getdate(),
	TRADE_NUMBER			decimal(19, 0)		NOT NULL,
	TRADE_DATE				datetime			NOT NULL,
	ISIN					char(12)			NOT	NULL,
	AUCTION_TYPE_ID			tinyint				NOT NULL,
	OPERATION_TYPE_ID		tinyint				NOT NULL,
	PRICE					decimal(19, 8)		NULL,
	VALUE					decimal(28, 8)		NOT NULL,
	YIELD					decimal(19, 4)		NOT NULL,
	QUANTITY				decimal(28, 8)		NOT NULL,
	SETTLE_DATE				datetime			NOT NULL,
	BANK_CODE				char(5)				NOT NULL,
	CP_BANK_CODE			char(5)				NOT NULL,
	CLIENT_CODE				varchar(12)			NOT NULL,
	BROKER_REFERENCE		varchar(34)			NOT NULL,
	USER_ID					int					NOT NULL,
	ORDER_NUMBER			decimal(19, 0)		NOT NULL,
	PROCESS_STATUS_ID		tinyint				NOT NULL,
	BANKMAIL_MESSAGE_575_ID	uniqueidentifier	NULL,
	BANKMAIL_MESSAGE_515_ID	uniqueidentifier	NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iTRADE1 ON TRADE (ID)
GO

CREATE INDEX iTRADE2 ON TRADE (PROCESS_STATUS_ID,TRADE_DATE)
GO

CREATE INDEX iTRADE3 ON TRADE (BANKMAIL_MESSAGE_575_ID)
GO

CREATE INDEX iTRADE4 ON TRADE (BANKMAIL_MESSAGE_515_ID)
GO



if exists (select * from sys.objects where name='tTRADE1' and type='TR')
	drop trigger dbo.tTRADE1
GO

CREATE TRIGGER dbo.tTRADE1
   ON dbo.TRADE
   AFTER UPDATE
AS
BEGIN
	declare @OldMTID uniqueidentifier,@NewMTID uniqueidentifier

	select @OldMTID=BANKMAIL_MESSAGE_575_ID from deleted
	select @NewMTID=BANKMAIL_MESSAGE_575_ID from inserted
	if @OldMTID is not null and @NewMTID is not null
		and @OldMTID<>@NewMTID
	BEGIN
		RAISERROR ('MT575 is already created',10,1)
		ROLLBACK TRANSACTION
	END

	select @OldMTID=BANKMAIL_MESSAGE_515_ID from deleted
	select @NewMTID=BANKMAIL_MESSAGE_515_ID from inserted
	if @OldMTID is not null and @NewMTID is not null
		and @OldMTID<>@NewMTID
	BEGIN
		RAISERROR ('MT515 is already created',10,1)
		ROLLBACK TRANSACTION
	END
END
GO
