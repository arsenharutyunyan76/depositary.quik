﻿if exists (select * from sys.objects where name='ACTION_LOG' and type='U')
	drop table dbo.ACTION_LOG
GO

CREATE TABLE dbo.ACTION_LOG (
	ID				bigint			identity(1,1),
	CREATION_DATE	datetime		NOT NULL default getdate(),
	ACTION_TYPE_ID	smallint		NOT NULL,
	IS_FAIL			bit				NOT NULL,
	DETAILS			nvarchar(max)	NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iACTION_LOG1 ON dbo.ACTION_LOG(ID)
GO

CREATE INDEX iACTION_LOG2 ON dbo.ACTION_LOG(CREATION_DATE,ACTION_TYPE_ID,IS_FAIL)
GO
