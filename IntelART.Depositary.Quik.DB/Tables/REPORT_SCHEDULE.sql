﻿if exists (select * from sys.objects where name='REPORT_SCHEDULE' and type='U')
	drop table REPORT_SCHEDULE
GO

CREATE TABLE REPORT_SCHEDULE
(
	ID					tinyint			identity(1,1),
	NAME_AM				nvarchar(50)	NOT NULL,
	NAME_EN				varchar(50)		NOT NULL,
	GENERATION_TIME		char(5)			NOT NULL,
	LAST_GENERATE_DATE	datetime		NULL,
	EMAIL				nvarchar(max)	NULL,
	EMAIL_SUBJECT		nvarchar(500)	NULL,
	EMAIL_BODY			nvarchar(max)	NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iREPORT_SCHEDULE1 ON dbo.REPORT_SCHEDULE(ID)
GO



INSERT INTO REPORT_SCHEDULE
(
	NAME_AM,
	NAME_EN,
	EMAIL_SUBJECT,
	EMAIL_BODY,
	GENERATION_TIME
)
VALUES
(N'Ռեգիստր', 'Register',
N'Պետական պարտատոնսերի աճուրդ_Գրանցման վկայական_|CurrentDate|_Gbond_Auction_Registration_certificate',
N'Կից ներկայացնում ենք «ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ» ԲԲԸ-ում կայացած պետական պարտատոմսերի աճուրդի արդյունքում կնքված գործարքների գրանցման վկայականը:

ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ
ՇՈՒԿԱՅԱԿԱՆ ԳՈՐԾԱՌՆՈՒԹՅՈՒՆՆԵՐԻ ԵՎ ՍՊԱՍԱՐԿՄԱՆ ԴԵՊԱՐՏԱՄԵՆՏ
Հեռ.` 060 69-55-55',
'17:00')

,(N'ԿԲ 11', 'CB 11', null, null, '17:00')

,(N'ԿԲ 15', 'CB 15', null, null, '17:00')

,(N'Աճուրդի արդյունքների վերաբերյալ հայտարարություն', 'Bond auction results announcement',
N'Պետական պարտատոմսերի աճուրդի արդյունքներ_|CurrentDate|_Gbond Auction Results',
N'|AuctionDate|թ.-ին «ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ» ԲԲԸ-ում տեղի է ունեցել
|ISIN| տարբերակիչ ծածկագրով թողարկման պետական պարտատոմսերի |AuctionType|։
Արդյունքները կցվում են:

ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ
ՇՈՒԿԱՅԱԿԱՆ ԳՈՐԾԱՌՆՈՒԹՅՈՒՆՆԵՐԻ ԵՎ ՍՊԱՍԱՐԿՄԱՆ ԴԵՊԱՐՏԱՄԵՆՏ
Հեռ.` 060 69-55-55',
'17:00')

,(N'Ֆինանսների նախարարություն', 'Ministry of finance',
N'Պետական պարտատոմսերի աճուրդի հաշվետվություն_|CurrentDate|_Gbond Auction Report',
N'Ներկայացնում ենք |AuctionDate|թ.-ին «ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ» ԲԲԸ-ում տեղի ունեցած |ISIN| տարբերակիչ ծածկագրով թողարկման պետական պարտատոմսերի |AuctionType|ի վերաբերյալ հաշվետվությունը:
Հաշվետվությունը կցվում է։

ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ
ՇՈՒԿԱՅԱԿԱՆ ԳՈՐԾԱՌՆՈՒԹՅՈՒՆՆԵՐԻ ԵՎ ՍՊԱՍԱՐԿՄԱՆ ԴԵՊԱՐՏԱՄԵՆՏ
Հեռ.` 060 69-55-55',
'17:00')

,(N'Հայկական Ծրագրեր', 'Armenian Software', null, null, '17:00')

,(N'ԿԲ 12', 'CB 12', null, null, '17:00')
,(N'ԿԲ 6', 'CB 6', null, null, '17:00')

,(N'Երկկողմանի անընդհատ աճուրդի կնքված գործարքներ', 'Registrations', null, null, '17:00')

,(N'GBondPrices', 'GBondPrices', null, null, '17:00')

,(N'ԿԲ 14', 'CB 14', null, null, '17:00')
GO
