﻿if exists (select * from sys.objects where name='ISIN' and type='U')
	drop table ISIN
GO

CREATE TABLE ISIN (
	ID					int				identity(1, 1),
	RTS_CODE			varchar(20) 	COLLATE SQL_Latin1_General_CP1_CS_AS	NOT NULL,
	ISIN_CODE			varchar(20) 	COLLATE SQL_Latin1_General_CP1_CS_AS	NOT NULL,
	CURRENCY			varchar(4)		NULL,
	NOMINAL				decimal(16,5)	NULL,
	REGISTRATION_NUMBER	varchar(50)		NULL,
	ISSUE_DATE			datetime		NULL,
	MATURITY_DATE		datetime		NULL,
	FREQUENCY			int				NULL,
	RATE				money			NULL,
	FORMULA				varchar(20)		NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iISIN ON dbo.ISIN(ID)
GO

CREATE UNIQUE INDEX iISIN2 ON dbo.ISIN(RTS_CODE,ISIN_CODE)
GO
