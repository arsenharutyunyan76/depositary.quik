﻿if exists (select * from sys.objects where name='PAYMENT_MESSAGE' and type='U')
	drop table PAYMENT_MESSAGE
GO

CREATE TABLE PAYMENT_MESSAGE(
	CREATION_DATE		datetime		NOT NULL default getdate(),
	ID					char(16)		NOT NULL,
	MESSAGE_TYPE		char(3)			NOT NULL,
	DOCUMENT_NUMBER		varchar(20)		NOT NULL,
	SECURITY_ID			varchar(20)		NULL,
	CURRENCY			varchar(3)		NULL,
	AMOUNT				decimal(28,8)	NOT NULL,
	PAYER				varchar(16)		NOT NULL,
	RECEIVER			varchar(16)		NOT NULL,
	PROCESSED			bit				NOT NULL default 0
)
GO

CREATE UNIQUE CLUSTERED INDEX iPAYMENT_MESSAGE1 ON PAYMENT_MESSAGE (ID)
GO
