﻿if exists (select * from sys.objects where name='CURRENCY' and type='U')
	drop table dbo.CURRENCY
GO

CREATE TABLE dbo.CURRENCY (
	ID				int			identity(1, 1),
	CODE			varchar(4)	NOT NULL,
	CURRENCY_TYPE	tinyint		NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iCURRENCY1 ON dbo.CURRENCY(ID)
GO

CREATE UNIQUE INDEX iCURRENCY2 ON dbo.CURRENCY(CODE)
GO



insert into dbo.CURRENCY (CODE, CURRENCY_TYPE)
values
('AMD', 1)
,('DEPO', 3)
,('USD', 2)
,('EUR', 2)
GO
