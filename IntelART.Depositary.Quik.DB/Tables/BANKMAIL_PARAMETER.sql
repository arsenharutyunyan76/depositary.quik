﻿if exists (select * from sys.objects where name='BANKMAIL_PARAMETER' and type='U')
	drop table BANKMAIL_PARAMETER
GO

CREATE TABLE BANKMAIL_PARAMETER(
	LAST_SEND_DATE		date	NULL,
	LAST_SEND_NUMBER	int		NULL
)
GO
