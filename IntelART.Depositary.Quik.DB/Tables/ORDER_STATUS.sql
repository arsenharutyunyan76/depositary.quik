﻿if exists (select * from sys.objects where name='ORDER_STATUS' and type='U')
	drop table ORDER_STATUS
GO

CREATE TABLE ORDER_STATUS
(
	ID		tinyint			identity(1,1),
	NAME_AM	nvarchar(50)	NOT NULL,
	NAME_EN	varchar(50)		NOT NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iORDER_STATUS1 ON dbo.ORDER_STATUS(ID)
GO



INSERT INTO ORDER_STATUS
(
	NAME_AM,
	NAME_EN
)
VALUES
(N'Սպասվող', 'Draft'),
(N'Հաստատված', 'Approved'),
(N'Մերժված', 'Rejected');
GO
