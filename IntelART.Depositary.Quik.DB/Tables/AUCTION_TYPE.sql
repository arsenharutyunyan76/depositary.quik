﻿if exists (select * from sys.objects where name='AUCTION_TYPE' and type='U')
	drop table dbo.AUCTION_TYPE
GO

CREATE TABLE dbo.AUCTION_TYPE (
	ID		tinyint			identity(1,1),
	NAME_AM	nvarchar(50)	NOT NULL,
	NAME_EN	varchar(50)		NOT NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iAUCTION_TYPE1 ON dbo.AUCTION_TYPE(ID)
GO



insert into dbo.AUCTION_TYPE (NAME_AM, NAME_EN)
values
(N'Տեղաբաշխման աճուրդ','Allocation auction')
,(N'Հետգնման աճուրդ','Buyback auction')
,(N'Լրացուցիչ տեղաբաշխման աճուրդ','Additional allocation auction')
GO
