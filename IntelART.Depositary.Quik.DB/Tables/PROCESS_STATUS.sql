﻿if exists (select * from sys.objects where name='PROCESS_STATUS' and type='U')
	drop table PROCESS_STATUS
GO

CREATE TABLE PROCESS_STATUS
(
	ID		tinyint			identity(1,1),
	NAME_AM	nvarchar(50)	NOT NULL,
	NAME_EN	varchar(50)		NOT NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iPROCESS_STATUS1 ON dbo.PROCESS_STATUS(ID)
GO



INSERT INTO PROCESS_STATUS
(
	NAME_AM,
	NAME_EN
)
VALUES
(N'Նոր', 'New'),
(N'Ուղարկված ՀՏ575', 'Sent MT575'),
(N'Ուղարկված ՀՏ515', 'Sent MT515'),
(N'Կատարված', 'Processed');
GO
