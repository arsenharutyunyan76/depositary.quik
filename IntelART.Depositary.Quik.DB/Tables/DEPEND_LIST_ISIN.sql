﻿if exists (select * from sys.objects where name='DEPEND_LIST_ISIN' and type='U')
	drop table dbo.DEPEND_LIST_ISIN
GO

CREATE TABLE dbo.DEPEND_LIST_ISIN (
	ID			int			identity(1, 1),
	ISIN_CCY	varchar(20)		NULL,
	ISIN_CODE	nvarchar(20)	NULL,
	ISIN_TYPE	varchar(20)		NULL,
)
GO

CREATE UNIQUE CLUSTERED INDEX iDEPEND_LIST_ISIN ON dbo.DEPEND_LIST_ISIN(ID)
GO
