﻿if exists (select * from sys.objects where name='ACTION_TYPE' and type='U')
	drop table dbo.ACTION_TYPE
GO

CREATE TABLE dbo.ACTION_TYPE (
	ID						smallint		identity(1,1),
	NAME_AM					nvarchar(100)	NOT NULL,
	NAME_EN					varchar(100)	NOT NULL,
	LAST_OPERATION_DATE		datetime		NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iACTION_TYPE1 ON dbo.ACTION_TYPE(ID)
GO



insert into dbo.ACTION_TYPE (NAME_AM, NAME_EN)
values
(N'Գործարքի ներմուծում', 'Import of trade')
,(N'Հայտի ներմուծում', 'Import of order')
,(N'ՀՏ575-ի ստեղծում', 'Creation of MT575')
,(N'ՀՏ515-ի ստեղծում', 'Creation of MT515')
,(N'Աճուրդի ներմուծում', 'Import of auction')
,(N'Ռեգիստրի ստեղծում', 'Creation of register')
,(N'ԿԲ 11 ստեղծում', 'Creation of CB 11')
,(N'ԿԲ 15 ստեղծում', 'Creation of CB 15')
,(N'Աճուրդի արդյունքների վերաբերյալ հայտարարության ստեղծում', 'Creation of bond auction results announcement')
,(N'Ֆինանսների նախարարություն ստեղծում', 'Creation of ministry of finance')
,(N'Հայկական Ծրագրեր ստեղծում', 'Creation of Armenian Software')
,(N'Աճուրդի հայտարարության ստեղծում', 'Creation of auction announcement')
,(N'ՀՏ535-ի մշակում', 'Processing of MT535')
,(N'Բանկմեյլի վճարումների մշակում', 'Processing of Bankmail payments')
,(N'ԿԲ 12 ստեղծում', 'Creation of CB 12')
,(N'ՀՏ522/ՀՏ100/ՀՏ202 -ի մշակում', 'Processing of MT522/MT100/MT202')
,(N'RTS-ից վճարումների ներմուծում (Ապադեպոնացում)', 'Payments import from RTS (Undeposition)')
,(N'RTS-ից վճարումների ներմուծում (Վերջնահաշվարկ)', 'Payments import from RTS (Final Calculation)')
,(N'ԿԲ 6 ստեղծում', 'Creation of CB 6')
,(N'Երկկողմանի անընդհատ աճուրդի կնքված գործարքների ստեղծում', 'Creation of Registrations')
,(N'GBondPrices-ի ստեղծում', 'Creation of GBondPrices')
,(N'ԿԲ 14 ստեղծում', 'Creation of CB 14')
GO
