﻿if exists (select * from sys.objects where name='ACCOUNT' and type='U')
	drop table dbo.ACCOUNT
GO

CREATE TABLE dbo.ACCOUNT (
	ID				 int			identity(1, 1),
	BANK_CODE		 char(5)		NOT NULL,
	CODE			 varchar(16)	NOT NULL,
	CURRENCY		 varchar(4)		NOT NULL,
	IS_OWN			 bit			NOT NULL,
	CUSTOMER_CODE	 varchar(20)	NULL,
	SECURITY_ACCOUNT varchar(16)	NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iACCOUNT1 ON dbo.ACCOUNT(ID)
GO

CREATE UNIQUE INDEX iACCOUNT2 ON dbo.ACCOUNT(CODE)
GO
