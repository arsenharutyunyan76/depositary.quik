﻿if exists (select * from sys.objects where name='PAYMENT_OUT_MESSAGE' and type='U')
	drop table PAYMENT_OUT_MESSAGE
GO

CREATE TABLE PAYMENT_OUT_MESSAGE(
	CREATION_DATE		datetime		NOT NULL default GETDATE(),
	ID					varchar(50)		NOT NULL,
	MESSAGE_TYPE		char(3)			NOT NULL,
	DOCUMENT_NUMBER		varchar(20)		NOT NULL,
	SECURITY_ID			varchar(20)		NULL,
	CURRENCY			varchar(3)		NULL,
	INITIAL_AMOUNT		decimal(28,8)	NOT NULL,
	AMOUNT				decimal(28,8)	NOT NULL,
	PAYER				varchar(16)		NOT NULL,
	RECEIVER			varchar(16)		NOT NULL,
	RECEIVER_NAME		varchar(16)		NOT NULL,
	IS_FINAL_CALCULATION	bit			NOT NULL,
	OPCODE				varchar(1)		NULL,
	CODE				varchar(7)		NULL,
	MOMENT				datetime		NULL,
	PROCESSED			bit				NOT NULL default 0
)
GO

CREATE UNIQUE CLUSTERED INDEX iPAYMENT_OUT_MESSAGE1 ON PAYMENT_OUT_MESSAGE (ID)
GO

CREATE UNIQUE INDEX iPAYMENT_OUT_MESSAGE2 ON PAYMENT_OUT_MESSAGE (MESSAGE_TYPE, DOCUMENT_NUMBER)
GO
