﻿if exists (select * from sys.objects where name='BANK' and type='U')
	drop table dbo.BANK
GO

CREATE TABLE dbo.BANK (
	CODE			char(5)			NOT NULL,
	SHORT_NAME		varchar(20)		NOT NULL,
	IS_ACTIVE		bit				NOT NULL,
	NAME_AM			nvarchar(50)	NOT NULL,
	NAME_EN			varchar(50)		NOT NULL,
	AS_CODE			varchar(8)		NOT NULL,
	REGISTER_NUMBER	int				NOT NULL default 0,
	REGISTRATIONS_NUMBER	int		NOT NULL default 0,
	EMAIL			nvarchar(max)	NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iBANK1 ON dbo.BANK(CODE)
GO

CREATE UNIQUE INDEX iBANK2 ON dbo.BANK(SHORT_NAME)
GO



insert into dbo.BANK (CODE, SHORT_NAME, IS_ACTIVE, NAME_AM, NAME_EN, AS_CODE)
values
('90000', 'MINFIN', 0, N'«ՖԻՆ․ՆԱԽ․', 'MOF', '')
,('11800', 'ANELK', 1, N'«ԱՅԴԻ ԲԱՆԿ» ՓԲԸ', 'ID BANK', '000314')
,('15100', 'ARART', 1, N'«ԱՐԱՐԱՏԲԱՆԿ» ԲԲԸ', 'ARARATBANK', '000309')
,('15700', 'AMERB', 1, N'«ԱՄԵՐԻԱԲԱՆԿ» ՓԲԸ', 'AMERIABANK', '000336')
,('16300', 'ECONM', 1, N'«ՀԱՅԷԿՈՆՈՄԲԱՆԿ» ԲԲԸ', 'ARMECONOMBANK', '000311')
,('19300', 'CONVS', 1, N'«ԿՈՆՎԵՐՍ ԲԱՆԿ» ՓԲԸ', 'CONVERSE BANK', '000310')
,('24700', 'ASHIB', 1, N'«ԱՐԴՇԻՆԲԱՆԿ» ՓԲԸ', 'ARDSHINBANK', '000304')
,('25000', 'SWISS', 1, N'«ԱՐՄՍՎԻՍԲԱՆԿ» ՓԲԸ', 'ARMSWISSBANK', '')
,('42000', 'ARMBR', 1, N'«ԱՐՄԵՆԲՐՈԿ» ԲԲԸ', 'ARMENBROK', '')
,('22300', 'ARTKH', 1, N'«ԱՐՑԱԽԲԱՆԿ» ՓԲԸ', 'ARTSAKHBANK', '')
,('11500', 'ABBNK', 1, N'«ՀԱՅԲԻԶՆԵՍԲԱՆԿ» ՓԲԸ', 'ARMBUSINESSBANK', '000305')
GO
