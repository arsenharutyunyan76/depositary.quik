﻿if exists (select * from sys.objects where name='OPERATION_TYPE' and type='U')
	drop table dbo.OPERATION_TYPE
GO

CREATE TABLE dbo.OPERATION_TYPE (
	ID		tinyint			identity(1,1),
	NAME_AM	nvarchar(50)	NOT NULL,
	NAME_EN	varchar(50)		NOT NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX iOPERATION_TYPE1 ON dbo.OPERATION_TYPE(ID)
GO



insert into dbo.OPERATION_TYPE (NAME_AM, NAME_EN)
values
(N'Առք','Buy')
,(N'Վաճառք','Sell')
GO
