﻿if exists (select * from sys.objects where name='AUCTION_ANNOUNCEMENT' and type='U')
	drop table AUCTION_ANNOUNCEMENT
GO

CREATE TABLE AUCTION_ANNOUNCEMENT
(
	EMAIL				nvarchar(max)	NULL,
	EMAIL_SUBJECT		nvarchar(500)	NULL,
	EMAIL_BODY			nvarchar(max)	NULL
)
GO



INSERT INTO AUCTION_ANNOUNCEMENT
(
	EMAIL_SUBJECT,
	EMAIL_BODY
)
VALUES
(N'Պետական պարտատոմսերի աճուրդներ_|CurrentDate|_Auction Announcement',
N'|AuctionDate|թ.-ին տեղի է ունենալու |ISIN| տարբերակիչ ծածկագրով թողարկման պետական պարտատոմսերի |AuctionType|։
Հայտարարությունը կցվում է:

ՀԱՅԱՍՏԱՆԻ ՖՈՆԴԱՅԻՆ ԲՈՐՍԱ
ՇՈՒԿԱՅԱԿԱՆ ԳՈՐԾԱՌՆՈՒԹՅՈՒՆՆԵՐԻ ԵՎ ՍՊԱՍԱՐԿՄԱՆ ԴԵՊԱՐՏԱՄԵՆՏ
Հեռ.` 060 69-55-55')
GO
