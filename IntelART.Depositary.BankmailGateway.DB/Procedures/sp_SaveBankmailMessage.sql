﻿if exists (select * from sys.objects where name='sp_SaveBankmailMessage' and type='P')
	drop procedure sp_SaveBankmailMessage
GO

create procedure sp_SaveBankmailMessage(
	@MESSAGE_TYPE		char(3),
	@NUMBER				int,
	@DOCUMENT_NUMBER	varchar(16),
	@SENDER_BANK		char(5),
	@RECEIVER_BANK		char(5),
	@BODY				varchar(max)
)
AS
	if right(@BODY,2)=(CHAR(13)+char(10))
		set @BODY=left(@BODY,len(@BODY)-2)
	INSERT INTO Bankmail.dbo.bmInterface
		(sr
		,number
		,dateprocessed
		,mt
		,sender
		,receiver
		,status
		,body
		,prioritet)
	VALUES
		('S'
		,@NUMBER
		,getdate()
		,@MESSAGE_TYPE
		,@SENDER_BANK
		,@RECEIVER_BANK
		,''
		,@BODY
		,0)
GO
