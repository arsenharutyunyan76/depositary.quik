﻿if exists (select * from sys.objects where name='sp_GetNotProcessedMessages' and type='P')
	drop procedure sp_GetNotProcessedMessages
GO

create procedure sp_GetNotProcessedMessages(@MESSAGE_TYPE	varchar(max))
AS

	DECLARE @tmp_MESSAGE_TYPE_list TABLE (MESSAGE_TYPE VARCHAR(20))
	INSERT INTO @tmp_MESSAGE_TYPE_list( MESSAGE_TYPE)
	SELECT DISTINCT val
	FROM dbo.fn_SplitString(@MESSAGE_TYPE, ',')

	declare @CURRENT_DATE date = getdate()
	SELECT b.ident as ID
		,b.body as BODY
		,b.mt as MESSAGE_TYPE
	FROM Bankmail.dbo.bmInterface b with (nolock)
	JOIN @tmp_MESSAGE_TYPE_list M
		ON M.MESSAGE_TYPE collate Latin1_General_BIN=b.mt collate Latin1_General_BIN
	LEFT JOIN PROCESSED_MESSAGES g with (nolock)
		ON b.ident collate Latin1_General_BIN=g.ID collate Latin1_General_BIN
	WHERE b.sr='R'
		AND convert(Date,dateprocessed)=@CURRENT_DATE
		AND g.ID is null
GO
