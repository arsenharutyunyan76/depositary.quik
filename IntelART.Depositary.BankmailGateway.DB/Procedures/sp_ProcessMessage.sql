﻿if exists (select * from sys.objects where name='sp_ProcessMessage' and type='P')
	drop procedure sp_ProcessMessage
GO

create procedure sp_ProcessMessage(
	@ID				char(16),
	@MESSAGE_TYPE	char(3)
)
AS
	INSERT INTO PROCESSED_MESSAGES
		(ID
		,MESSAGE_TYPE)
	VALUES
		(@ID
		,@MESSAGE_TYPE)
GO
