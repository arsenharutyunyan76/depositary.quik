﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;
using System.Timers;
using IntelART.Utilities;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class HTPaymentOutMessageService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<HTPaymentOutMessageService> logger;
        private string integrationConnectionString;
        private BankmailMessageRepository bankmailMessageRepository;
        private BankmailRepository bankmailRepository;
        private PaymentOutRepository paymentOutRepository;
        private ActionLogRepository actionLogRepository;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private BankRepository bankRepository;
        private bool canProcessTimerEvent = true;

        public HTPaymentOutMessageService(IListenerConfiguration configuration, ILogger<HTPaymentOutMessageService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 16 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.HTPaymentOutMessageServiceInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                this.integrationConnectionString = configuration.IntegrationConnectionString;
                this.bankmailMessageRepository = new BankmailMessageRepository(configuration.IntegrationConnectionString);
                this.bankmailRepository = new BankmailRepository(configuration.BankmailConnectionString);
                this.paymentOutRepository = new PaymentOutRepository(configuration.IntegrationConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);

                serviceTimer = new System.Timers.Timer(settings.HTPaymentOutMessageServiceInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessMessagePeriodical);
                serviceTimer.Enabled = true;

                this.bankRepository = new BankRepository(configuration.IntegrationConnectionString);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        private void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            if (this.canProcessTimerEvent)
            {
                IEnumerable<PaymentOutMessage> messages;
                try
                {
                    this.canProcessTimerEvent = false;
                    DateTime currentDate = DateTime.Now;
                    messages = paymentOutRepository.GetPaymentOutMessage(currentDate, currentDate);
                    foreach (PaymentOutMessage message in messages)
                    {
                        if (this.ProcessMessage(message) != null)
                            paymentOutRepository.SetProcessedPaymentOutMessage(message.ID);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing HT202 outgoing message.");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private Guid? ProcessMessage(PaymentOutMessage message)
        {
            Guid? result = null;
            try
            {
                Bank bank = bankRepository.GetBank(bankRepository.GetBankByShortName(message.RECEIVER_NAME));
                DateTime currDate = DateTime.Now.Date;
                StringBuilder builder = new StringBuilder();

                BankmailUtility.AddTagRow("20", message.DOCUMENT_NUMBER, ref builder);

                switch (message.MESSAGE_TYPE)
                {
                    case "202":
                        BankmailUtility.AddTagRow("32A", string.Format("{0}{1}{2}", currDate.ToString("ddMMyy"), message.CURRENCY, BankmailUtility.SerializeDecimal(message.AMOUNT)), ref builder);
                        BankmailUtility.AddTagRow("50A", message.PAYER, ref builder);
                        builder.AppendLine(this.settings.HTStockName);
                        BankmailUtility.AddTagRow("59", message.RECEIVER, ref builder);
                        break;

                    case "522":
                        BankmailUtility.AddTagRow("35B", message.SECURITY_ID, ref builder);
                        BankmailUtility.AddTagRow("35C", BankmailUtility.SerializeDecimal(message.AMOUNT), ref builder);
                        BankmailUtility.AddTagRow("83", message.PAYER, ref builder);
                        builder.AppendLine(this.settings.HTStockName);
                        BankmailUtility.AddTagRow("87", message.RECEIVER, ref builder);
                        break;
                }

                builder.AppendLine(ARMSCII8Encoding.TextConvert(Encoding.ASCII, Encoding.Unicode, bank.NAME_AM));
                BankmailUtility.AddTagRow("72", this.settings.HTFinalizingComment, ref builder);

                string body = null;
                result = BankmailUtility.SavePaymentOutMessage(builder, bankmailMessageRepository, bankmailRepository, message.PAYER,
                                                                message.RECEIVER, integrationConnectionString, ref body);
                DBLogger.WriteSuccess(actionLogRepository, 16, body);
                this.logger.LogInformation("Successfully created HTPayment Out Message, the ID is {0}", result.Value);
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("Some trades are not included in this package"))
                    DBLogger.WriteFail(actionLogRepository, 16, e.Message);
                this.logger.LogError(e, "Error while creating HTPayment Out Message");
            }
            return result;
        }

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Message listener successfully started");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
