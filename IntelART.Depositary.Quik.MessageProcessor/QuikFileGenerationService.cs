﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.FileUtility;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class QuikFileGenerationService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private System.Timers.Timer serviceTimer;
        private ILogger<QuikFileGenerationService> logger;
        private Settings settings;
        
        public QuikFileGenerationService(IListenerConfiguration configuration, ILogger<QuikFileGenerationService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.QuikFileGenerationInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                QuikFileUtility quikFileUtility = new QuikFileUtility(configuration, logger);

                serviceTimer = new System.Timers.Timer(settings.QuikFileGenerationInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(quikFileUtility.ProcessMessagePeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the service");
            }
        }

        #region Interface implementation

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        #endregion
    }
}
