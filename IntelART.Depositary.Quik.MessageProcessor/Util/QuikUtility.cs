﻿using System;
using System.Data.SqlClient;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class QuikUtility
    {
        public static Guid SaveTrade(Trade trade, Guid quikId, TradeRepository tradeRepository, QuikRepository quikRepository, string integrationConnectionString)
        {
            Guid id;
            using (SqlConnection connection = new SqlConnection(integrationConnectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                    try
                    {
                        id = tradeRepository.SaveTrade(trade, connection, trans);
                        quikRepository.ReplicateTrade(quikId, id);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw new ApplicationException(e.Message);
                    }
            }
            return id;
        }

        public static Guid SaveOrder(Order order, Guid quikId, OrderRepository orderRepository, QuikRepository quikRepository, string integrationConnectionString)
        {
            Guid id;
            using (SqlConnection connection = new SqlConnection(integrationConnectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                    try
                    {
                        id = orderRepository.SaveOrder(order, connection, trans);
                        quikRepository.ReplicateOrder(quikId, id);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw new ApplicationException(e.Message);
                    }
            }
            return id;
        }

        public static Guid SaveAuctionParam(AuctionParameter auctionParameter, Guid quikId, AuctionRepository auctionRepository, QuikRepository quikRepository, string integrationConnectionString)
        {
            Guid id;
            using (SqlConnection connection = new SqlConnection(integrationConnectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                    try
                    {
                        id = auctionRepository.SaveAuctionParam(auctionParameter, connection, trans);
                        quikRepository.ReplicateAuctionParam(quikId, id);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw new ApplicationException(e.Message);
                    }
            }
            return id;
        }

        public static string GetBankCode(Settings settings, BankRepository bankRepository, string source)
        {
            string result;
            if (source == settings.CompanyShort)
                result = settings.CompanyCode;
            else
                result = bankRepository.GetBankByShortName(source);
            return result;
        }

        public static string GetCleanBrokerReference(Settings settings, string source)
        {
            return source.Replace(string.Format("{0}/", settings.TradePrincipalName), string.Empty).Replace(string.Format("{0}/", settings.TradeAgentName), string.Empty);
        }

        public static byte GetOperationTypeID(Settings settings, string source)
        {
            return (byte)(source == settings.TradeBuyName ? 1 : 2);
        }
    }
}
