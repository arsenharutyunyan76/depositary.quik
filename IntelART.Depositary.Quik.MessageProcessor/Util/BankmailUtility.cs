﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class BankmailUtility
    {
        public static void AddTagRow(string tag, string value, ref StringBuilder builder)
        {
            builder.AppendFormat(":{0}:{1}", tag, value);
            builder.AppendLine();
        }

        public static string SerializeDecimal(decimal source, short precision = 0)
        {
            string result = source.ToString(string.Format("{0}{1}", new string('#', 12), precision > 0 ? string.Format(".{0}", new string('#', precision)) : string.Empty)).Replace(".", ",");
            if (precision > 0 && !result.Contains(','))
                result += ",";
            return result;
        }

        public static string SerializeDate(DateTime source)
        {
            return source.ToString("ddMMyy");
        }

        public static string GenerateBody(StringBuilder builder)
        {
            StringBuilder result = new StringBuilder();
            AddTagRow("20", "$NUMBER$", ref result);
            result.Append(builder);
            return result.ToString();
        }

        public static string GetFieldValue(string tag, string body)
        {
            string result = null;
            string row = body.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Where(item => item.StartsWith(string.Format(":{0}:", tag))).FirstOrDefault();
            if (!string.IsNullOrEmpty(row))
                result = row.Substring(tag.Length + 2);
            return result;
        }

        public static string GetFieldValueRows(string tag, string body)
        {
            body = body.Replace("\r\n:", "\r\n#:");
            string result = null;
            string row = body.Split(new string[] { "\r\n#" }, StringSplitOptions.RemoveEmptyEntries).Where(item => item.StartsWith(string.Format(":{0}:", tag))).FirstOrDefault();
            if (!string.IsNullOrEmpty(row))
                result = row.Substring(tag.Length + 2);
            return result;
        }

        public static Guid SavePaymentOutMessage(StringBuilder builder, BankmailMessageRepository bankmailMessageRepository, BankmailRepository bankmailRepository, 
                                                 string htSenderBankCode, string htReceiverBankCode, string integrationConnectionString, ref string body)
        {
            Guid id;
            using (SqlConnection connection = new SqlConnection(integrationConnectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                    try
                    {
                        int number = -1;
                        string documentNumber = null;
                        BankmailMessage message = new BankmailMessage()
                        {
                            BODY = GenerateBody(builder),
                            MESSAGE_TYPE = "202",
                            SENDER_BANK = htSenderBankCode,
                            RECEIVER_BANK = htReceiverBankCode
                        };
                        id = bankmailMessageRepository.SaveBankmailMessage(message, connection, trans, ref number, ref documentNumber);

                        bankmailRepository.SaveBankmailMessage(message, number, documentNumber, ref body);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw new ApplicationException(e.Message);
                    }
            }
            return id;
        }

        public static Guid SaveBankmail575Message(StringBuilder builder, BankmailMessageRepository bankmailMessageRepository, BankmailRepository bankmailRepository, TradeRepository tradeRepository
            , string htSenderBankCode, string htReceiverBankCode, string integrationConnectionString, IEnumerable<Guid> tradeIds, ref string body)
        {
            Guid id;
            using (SqlConnection connection = new SqlConnection(integrationConnectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                    try
                    {
                        int number = -1;
                        string documentNumber = null;
                        BankmailMessage message = new BankmailMessage()
                        {
                            BODY = GenerateBody(builder),
                            MESSAGE_TYPE = "575",
                            SENDER_BANK = htSenderBankCode,
                            RECEIVER_BANK = htReceiverBankCode
                        };
                        id = bankmailMessageRepository.SaveBankmailMessage(message, connection, trans, ref number, ref documentNumber);
                        foreach (Guid tradeId in tradeIds)
                        {
                            tradeRepository.SetTradeBankmailMessage575(tradeId, id, connection, trans);
                        }
                        bankmailRepository.SaveBankmailMessage(message, number, documentNumber, ref body);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw new ApplicationException(e.Message);
                    }
            }
            return id;
        }

        public static Guid SaveBankmail515Message(StringBuilder builder, BankmailMessageRepository bankmailMessageRepository, BankmailRepository bankmailRepository, TradeRepository tradeRepository
            , string htSenderBankCode, string htReceiverBankCode, string integrationConnectionString, IEnumerable<Guid> tradeIds, ref string body)
        {
            Guid id;
            using (SqlConnection connection = new SqlConnection(integrationConnectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                    try
                    {
                        int number = -1;
                        string documentNumber = null;
                        BankmailMessage message = new BankmailMessage()
                        {
                            BODY = GenerateBody(builder),
                            MESSAGE_TYPE = "515",
                            SENDER_BANK = htSenderBankCode,
                            RECEIVER_BANK = htReceiverBankCode
                        };
                        id = bankmailMessageRepository.SaveBankmailMessage(message, connection, trans, ref number, ref documentNumber);
                        foreach (Guid tradeId in tradeIds)
                        {
                            tradeRepository.SetTradeBankmailMessage515(tradeId, id, connection, trans);
                        }
                        bankmailRepository.SaveBankmailMessage(message, number, documentNumber, ref body);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        throw new ApplicationException(e.Message);
                    }
            }
            return id;
        }
    }
}
