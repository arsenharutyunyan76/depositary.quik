﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public static class DBLogger
    {
        public static void WriteSuccess(ActionLogRepository repository, byte typeId, string details = null)
        {
            try
            {
                repository.SaveActionLog(new ActionLog()
                {
                    ACTION_TYPE_ID = typeId,
                    IS_FAIL = false,
                    DETAILS = details
                });
            }
            catch
            {
            }
        }

        public static void WriteFail(ActionLogRepository repository, byte typeId, string error)
        {
            try
            {
                repository.SaveActionLog(new ActionLog()
                {
                    ACTION_TYPE_ID = typeId,
                    IS_FAIL = true,
                    DETAILS = error
                });
            }
            catch
            {
            }
        }
    }
}
