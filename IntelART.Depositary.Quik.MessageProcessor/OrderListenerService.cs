﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class OrderListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable
    {
        private OrderMessageListener messageListener;
        private ILogger<OrderListenerService> logger;
        private string integrationConnectionString;
        private OrderRepository orderRepository;
        private BankRepository bankRepository;
        private QuikRepository quikRepository;
        private ActionLogRepository actionLogRepository;
        private Settings settings;

        public OrderListenerService(IListenerConfiguration configuration, ILogger<OrderListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 2 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            try
            {
                UpdateServiceLastOperationDate();

                this.integrationConnectionString = configuration.IntegrationConnectionString;
                this.orderRepository = new OrderRepository(configuration.IntegrationConnectionString);
                this.bankRepository = new BankRepository(configuration.IntegrationConnectionString);
                this.quikRepository = new QuikRepository(configuration.ConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);
                this.messageListener = new OrderMessageListener(configuration.ConnectionString, this.ProcessMessage, this.logger, this.settings);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private Guid? ProcessMessage(OrderModel message)
        {
            Guid? id = null;
            if (message != null)
            {
                try
                {
                    byte auctionTypeId;
                    if (message.ClassCode == settings.AuctionTypePlacingCode)
                        auctionTypeId = 1;
                    else if (message.ClassCode == settings.AuctionTypeReturnCode)
                        auctionTypeId = 2;
                    else
                        auctionTypeId = 3;
                    id = QuikUtility.SaveOrder(new Order()
                    {
                        ORDER_NUMBER = message.OrderNum,
                        ISIN = message.SecCode,
                        ORDER_DATE = message.OrderDateTime,
                        AUCTION_TYPE_ID = auctionTypeId,
                        OPERATION_TYPE_ID = QuikUtility.GetOperationTypeID(settings, message.Operation),
                        PRICE = message.Price,
                        VALUE = message.Value,
                        YIELD = message.Yield,
                        QUANTITY = message.Qty,
                        BALANCE = message.Balance,
                        WITHDRAW_DATE = message.WithdrawDateTime,
                        BANK_CODE = QuikUtility.GetBankCode(settings, bankRepository, message.FirmId),
                        CLIENT_CODE = message.ClientCode,
                        BROKER_REFERENCE = QuikUtility.GetCleanBrokerReference(settings, message.BrokerRef),
                        USER_ID = message.UID,
                        ORDER_STATUS_ID = GetOrderStatusID(message.State)
                    }, message.ID, orderRepository, quikRepository, this.integrationConnectionString);
                    DBLogger.WriteSuccess(actionLogRepository, 2, message.OrderNum.ToString());
                    this.logger.LogInformation("Successfully processed order {0}, the ID is {1}", message.OrderNum, id.Value);
                }
                catch (Exception e)
                {
                    if (!e.Message.Contains("Order is already processed"))
                        DBLogger.WriteFail(actionLogRepository, 2, e.Message);
                    this.logger.LogError(e, "Error while processing order {0}", message.OrderNum);
                }
            }
            else
            {
                this.logger.LogWarning("Empty message received, skipping it. The message will not be considered processed.");
            }
            return id;
        }

        private byte GetOrderStatusID(string source)
        {
            if (source == settings.OrderApprovedName)
                return 2;
            else if (source == settings.OrderRejectedName)
                return 3;
            else
                return 1;
        }

        public void Dispose()
        {
            this.messageListener.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
                this.messageListener.Start();
                this.logger.LogInformation("Message listener successfully started");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.messageListener.Stop();

            return Task.CompletedTask;
        }
    }
}
