﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.DependUtility;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class DependListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<DependListenerService> logger;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private string sessionId;
        private IEnumerable<DependOrder> dependOrderList;
        private IEnumerable<DependIsin> dependIsinList;
        private IEnumerable<DependHolder> dependHolderList;
        private IEnumerable<DependMandate> dependMandateList;
        private IEnumerable<DependTransaction> dependTransactionList;

        private Dictionary<string, DependIsin> savedIsinList = new Dictionary<string, DependIsin>();
        private Dictionary<string, DependHolder> savedHolderList = new Dictionary<string, DependHolder>();
        private Dictionary<string, DependMandate> savedMandateList = new Dictionary<string, DependMandate>();

        private DependDataSetRepository dependDataSetRepository;
        SoapWebReuqestsService soapWebReuqestsService;

        public DependListenerService(IListenerConfiguration configuration, ILogger<DependListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.DependServiceInterval == 0)
            {
                return;
            }

            UpdateServiceLastOperationDate();

            LoginService loginService = new LoginService(settings, logger);

            try
            {
                this.dependDataSetRepository = new DependDataSetRepository(configuration.IntegrationConnectionString);

                soapWebReuqestsService = new SoapWebReuqestsService(settings);

                sessionId = loginService.InvokeLoginService(settings.DependUser, settings.DependPassword, settings.DependClientVersion, settings.DependParticipant);
                InvokeDataSetService(settings.DependOrderName, 1000000000, "ATSORDER_ATS = 1");

                SaveDependListOrder((IEnumerable<DependOrder> dependOrders) =>
                {
                    ProcessSucceededDependOrders(dependOrders);

                    LogoutService logoutService = new LogoutService(sessionId, settings, logger);
                    logoutService.InvokeLogoutService();
                });

                serviceTimer = new System.Timers.Timer(settings.DependServiceInterval);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
            }
        }

        private void ProcessSucceededDependOrders(IEnumerable<DependOrder> dependOrderListSucceeded)
        {
            HelperService helperService = new HelperService(settings);
            try
            {
                if (dependOrderListSucceeded.Count() > 0)
                {
                    foreach (DependOrder succeededDependOrder in dependOrderListSucceeded)
                    {
                        DependIsin dependIsin = savedIsinList.FirstOrDefault(x => x.Key == succeededDependOrder.ISIN_CODE).Value;
                        DependMandate dependMandate = savedMandateList.FirstOrDefault(x => x.Key == (dependIsin.ISIN_TYPE != "C" ? succeededDependOrder.ACCOUNT_REFERENCE : succeededDependOrder.ATSORDER_LINKED_ACCOUNT_REFERENCE)).Value;
                        DependHolder dependHolder = savedHolderList.FirstOrDefault(x => x.Key == (dependIsin.ISIN_TYPE != "C" ? succeededDependOrder.ACCOUNT_REFERENCE : succeededDependOrder.ATSORDER_LINKED_ACCOUNT_REFERENCE)).Value;

                        string accountCode = (dependIsin != null ? dependIsin.ISIN_CCY.Substring(0, 1) : "") +
                            (dependMandate != null && dependMandate.MANDATE_TYPE == "C" ? ("0" + succeededDependOrder.DEPOPART_CODE) :
                            (dependHolder != null ? dependHolder.ACCOUNT_ID.ToString() : ""));

                        string isinCode = succeededDependOrder.ISIN_CODE;
                        string accountReference = succeededDependOrder.ACCOUNT_REFERENCE;
                        string isinShortName = succeededDependOrder.ISIN_SHORT_NAME.StartsWith("CB:") ?
                            succeededDependOrder.ISIN_SHORT_NAME.Substring(3, succeededDependOrder.ISIN_SHORT_NAME.Length - 3) : succeededDependOrder.ISIN_SHORT_NAME;
                        string ballChangeQuantity = string.Format("{0:0.00}", Math.Abs(succeededDependOrder.BALCHANGE_QTY));
                        string tfrRef = string.Format("_IN_CORP_{0}_ORD{1}_{2}", DateTime.Now.ToString("yyyymmddhhmm"), succeededDependOrder.ATSORDER_ID, succeededDependOrder.DEPOPART_CODE);
                        string tfrRem = string.Format("_IN_CORP_{0},{1},{2},{3},{4},{5}",
                            DateTime.Now.ToString("yyyymmddhhmm"), succeededDependOrder.DEPOPART_CODE,
                            accountCode, isinShortName,
                            ballChangeQuantity, succeededDependOrder.DEPOPART_CODE);
                        string account = settings.PaperAccount;

                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine($"<![CDATA[<P_TWIN_SIDED_TRANSACTION.1><P_TWIN_SIDED_TRANSACTION><PARAM_TFR_ISIN_CODE>{isinCode}</PARAM_TFR_ISIN_CODE>");
                        sb.AppendLine($"<PARAM_TFR_DB_AC_REF>{accountReference }</PARAM_TFR_DB_AC_REF>");
                        sb.AppendLine($"<PARAM_TFR_CR_AC_REF>{account}</PARAM_TFR_CR_AC_REF>");
                        sb.AppendLine($"<PARAM_TFR_QTY>{ballChangeQuantity}</PARAM_TFR_QTY>");
                        sb.AppendLine($"<PARAM_TFR_REF>{tfrRef}</PARAM_TFR_REF>");
                        sb.AppendLine($"<PARAM_TFR_REM>{tfrRem}</PARAM_TFR_REM>");
                        sb.AppendLine("<PARAM_TFR_TYPE>OG</PARAM_TFR_TYPE></P_TWIN_SIDED_TRANSACTION></P_TWIN_SIDED_TRANSACTION.1>]]>");

                        InvokeSaveDataSetService(sb.ToString(), "P_TWIN_SIDED_TRANSACTION");
                        InvokeDataSetService(settings.DependTransactionName, 1, string.Format("TRANS_REFERENCE LIKE '%{0}%'", succeededDependOrder.ATSORDER_ID));
                    }
                    foreach (DependTransaction dependTransaction in dependTransactionList)
                    {
                        SaveDependTransaction(dependTransaction);
                    }
                }
            }
            catch (Exception e)
            {
                string exMessage = string.Format("Exception happened when trying to process succeeded depend orders");
                this.logger.LogCritical(e, exMessage);
            }
        }

        private void ProcessCancelledDependOrders(DependOrder cancelledDependOrder)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"<![CDATA[<P_ORDER_CANCEL.1><P_ORDER_CANCEL><PARAM_OCA_ORDER>{cancelledDependOrder.ATSORDER_ID}</PARAM_OCA_ORDER>");
            sb.AppendLine($"<PARAM_OCA_ATS>{cancelledDependOrder.ATSORDER_ATS}</PARAM_OCA_ATS></P_ORDER_CANCEL></P_ORDER_CANCEL.1>]]>");

            InvokeSaveDataSetService(sb.ToString(), "P_ORDER_CANCEL");
        }

        private async void SaveDependListOrder(Action<IEnumerable<DependOrder>> callback)
        {
            savedIsinList = new Dictionary<string, DependIsin>();
            savedHolderList = new Dictionary<string, DependHolder>();
            savedMandateList = new Dictionary<string, DependMandate>();

            List<DependOrder> dependOrders = new List<DependOrder>();

            foreach (DependOrder dependOrder in dependOrderList)
            {
                try
                {
                    await dependDataSetRepository.SaveDependListOrder(dependOrder);

                    DependIsin di = null;
                    DependMandate dm = null;
                    DependHolder dh = null;

                    if (!savedIsinList.Keys.Contains(dependOrder.ISIN_CODE))
                    {
                        InvokeDataSetService(settings.DependIsinName, 1, String.Format("ISIN_CODE='{0}'", dependOrder.ISIN_CODE));

                        if (dependIsinList != null && dependIsinList.Count() > 0)
                        {
                            di = dependIsinList.FirstOrDefault();
                            if (di != null)
                            {
                                SaveDependIsin(di);
                                savedIsinList.Add(dependOrder.ISIN_CODE, di);
                            }
                        }
                    }
                    else
                    {
                        di = savedIsinList.FirstOrDefault(x => x.Key == dependOrder.ISIN_CODE).Value;
                    }

                    if (di != null && di.ISIN_TRADEABLE == "Y")
                    {
                        string conditionPropertyValue = dependOrder.ATSORDER_LINKED_ACCOUNT_REFERENCE;
                        string conditionProperty = Constants.ATSORDER_LINKED_ACCOUNT_REFERENCE;

                        bool canContinue = true;
                        /// setting condition
                        if (di.ISIN_TYPE != "C")
                        {
                            string ownerList = dependOrder.ATSORDER_LINKED_ACCOUNT_OWNER_LIST;
                            if (di.ISIN_CCY == ownerList.Substring(ownerList.Length - 4).Substring(0, 3))
                            {
                                conditionPropertyValue = dependOrder.ACCOUNT_REFERENCE;
                                conditionProperty = Constants.ACCOUNT_REFERENCE;
                            }
                            else
                            {
                                canContinue = false;
                                //ProcessCancelledDependOrders(dependOrder);
                                await dependDataSetRepository.SetDependOrderStatus(dependOrder.ATSORDER_ID, true);
                            }
                        }

                        if (!savedHolderList.Keys.Contains(conditionPropertyValue) && canContinue)
                        {
                            InvokeDataSetService(settings.DependHolderName, 1, string.Format("ACCOUNT_REFERENCE='{0}'", conditionPropertyValue));

                            if (dependHolderList != null && dependHolderList.Count() > 0)
                            {
                                dh = dependHolderList.FirstOrDefault();
                                if (dh != null)
                                {
                                    SaveDependHolder(dh);
                                    savedHolderList.Add(conditionPropertyValue, dh);
                                }
                            }
                        }
                        else if (canContinue)
                        {
                            dh = savedHolderList.FirstOrDefault(x => x.Key == conditionPropertyValue).Value;
                        }

                        if (!savedMandateList.Keys.Contains(conditionPropertyValue) && canContinue)
                        {
                            InvokeDataSetService(settings.DependMandateName, 1, string.Format("ACCOUNT_REFERENCE='{0}'", conditionPropertyValue));

                            if (dependMandateList != null && dependMandateList.Count() > 0)
                            {
                                dm = dependMandateList.FirstOrDefault();
                                if (dm != null)
                                {
                                    SaveDependMandate(dm);
                                    savedMandateList.Add(conditionPropertyValue, dm);
                                }
                            }
                        }
                        else if (canContinue)
                        {
                            dm = savedMandateList.FirstOrDefault(x => x.Key == conditionPropertyValue).Value;
                        }

                        string depoMandateType = GetMandateType(dm, dependOrder, conditionProperty);

                        if (IsCheckingPassed(depoMandateType, dependOrder, conditionProperty))
                        {
                            dependOrders.Add(dependOrder);
                        }
                        else
                        {
                            //ProcessCancelledDependOrders(dependOrder);
                            await dependDataSetRepository.SetDependOrderStatus(dependOrder.ATSORDER_ID, true);
                        }
                    }
                    else
                    {
                        //ProcessCancelledDependOrders(dependOrder);
                        await dependDataSetRepository.SetDependOrderStatus(dependOrder.ATSORDER_ID, true);
                    }
                    ProcessCancelledDependOrders(dependOrder);
                }
                catch (Exception e)
                {
                    string exMessage = string.Format("Exception happened when trying to save order with id {0}", dependOrder.ATSORDER_ID);
                    this.logger.LogCritical(e, exMessage);
                }
            }

            callback(dependOrders);
        }

        private async void SaveDependIsin(DependIsin dependIsin)
        {
            try
            {
                await dependDataSetRepository.SaveDependIsin(dependIsin);
            }
            catch (Exception e)
            {
                string exMessage = string.Format("Exception happened when trying to save isin with code {0}", dependIsin.ISIN_CODE);
                this.logger.LogCritical(e, exMessage);
            }
        }

        private async void SaveDependHolder(DependHolder dependHolder)
        {
            try
            {
                await dependDataSetRepository.SaveDependHolder(dependHolder);
            }
            catch (Exception e)
            {
                string exMessage = string.Format("Exception happened when trying to save holder with reference {0}", dependHolder.ACCOUNT_REFERENCE);
                this.logger.LogCritical(e, exMessage);
            }
        }

        private async void SaveDependMandate(DependMandate dependMandate)
        {
            try
            {
                await dependDataSetRepository.SaveDependListMandate(dependMandate);
            }
            catch (Exception e)
            {
                string exMessage = string.Format("Exception happened when trying to save mandate with reference {0}", dependMandate.ACCOUNT_REFERENCE);
                this.logger.LogCritical(e, exMessage);
            }
        }

        private async void SaveDependTransaction(DependTransaction dependTransaction)
        {
            try
            {
                await dependDataSetRepository.SaveDependListTransaction(dependTransaction);
            }
            catch (Exception e)
            {
                string exMessage = string.Format("Exception happened when trying to save depend transaction with id {0}", dependTransaction.TRANS_ID);
                this.logger.LogCritical(e, exMessage);
            }
        }

        #region Dataset invoke

        public void InvokeDataSetService(string name, long topCount = 0, string whereCondition = null)
        {
            HttpWebRequest request = soapWebReuqestsService.CreateSOAPWebRequest();
            XmlDocument SOAPReqBody = new XmlDocument();
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("sessionId", sessionId);
                values.Add("name", name);
                if (topCount > 0)
                    values.Add("top", topCount.ToString());
                if (!string.IsNullOrWhiteSpace(whereCondition))
                    values.Add("where", whereCondition);
                SOAPReqBody.LoadXml(soapWebReuqestsService.GetSOAPBodyText("DataSetList", values));
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke data set service.";
                this.logger.LogCritical(e, exMessage);
            }

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    string serviceResult = rd.ReadToEnd();
                    string result = soapWebReuqestsService.GetXMLResponseValue(serviceResult, "DataSetListResponse", "data");

                    XDocument doc = XDocument.Parse(result);
                    SetDataSet(name, doc);
                }
            }
        }

        private void SetDataSet(string name, XDocument doc)
        {
            try
            {
                if (name == settings.DependOrderName)
                {
                    dependOrderList = doc.Descendants(name).Select(d =>
                               new DependOrder
                               {
                                   ACCOUNT_REFERENCE = d.Element("ACCOUNT_REFERENCE").Value,
                                   ATSORDER_ATS = Convert.ToInt32(d.Element("ATSORDER_ATS").Value),
                                   ATSORDER_LINKED_ACCOUNT_OWNER_LIST = d.Element("ATSORDER_LINKED_ACCOUNT_OWNER_LIST").Value,
                                   ATSORDER_ID = Convert.ToInt32(d.Element("ATSORDER_ID").Value),
                                   ATSORDER_LINKED_ACCOUNT_REFERENCE = d.Element("ATSORDER_LINKED_ACCOUNT_REFERENCE").Value,
                                   ATS_NAME = d.Element("ATS_NAME").Value,
                                   BALCHANGE_QTY = Convert.ToDecimal(d.Element("BALCHANGE_QTY").Value),
                                   DEPOPART_CODE = d.Element("DEPOPART_CODE").Value,
                                   ISIN_CODE = d.Element("ISIN_CODE").Value,
                                   ISIN_SHORT_NAME = d.Element("ISIN_SHORT_NAME").Value
                               }).Where(x => x.ATSORDER_ATS == 1).ToList();
                }
                else if (name == settings.DependIsinName)
                {
                    dependIsinList = doc.Descendants(settings.DependIsinName).Select(d =>
                             new DependIsin
                             {
                                 ISIN_CCY = d.Element("ISIN_CCY").Value,
                                 ISIN_TYPE = d.Element("ISIN_TYPE").Value,
                                 ISIN_CODE = d.Element("ISIN_CODE").Value,
                                 ISIN_TRADEABLE = d.Element("ISIN_TRADEABLE").Value
                             }).ToList();
                }
                else if (name == settings.DependHolderName)
                {
                    dependHolderList = doc.Descendants(settings.DependHolderName).Select(d =>
                             new DependHolder
                             {
                                 ACCOUNT_REFERENCE = d.Element("ACCOUNT_REFERENCE").Value,
                                 ACCOUNT_ID = Convert.ToInt32(d.Element("ACCOUNT_ID").Value)
                             }).ToList();
                }
                else if (name == settings.DependMandateName)
                {
                    dependMandateList = doc.Descendants(settings.DependMandateName).Select(d =>
                             new DependMandate
                             {
                                 ACCOUNT_REFERENCE = d.Element("ACCOUNT_REFERENCE").Value,
                                 MANDATE_DESCRIPTION = d.Element("MANDATE_DESCRIPTION") != null ? d.Element("MANDATE_DESCRIPTION").Value : "",
                                 MANDATE_TYPE = d.Element("MANDATE_TYPE").Value
                             }).ToList();
                }
                else if (name == settings.DependTransactionName)
                {
                    List<DependTransaction> dependTransactions = dependTransactionList == null ? new List<DependTransaction>() : dependTransactionList.ToList();
                    dependTransactions.Add(doc.Descendants(settings.DependTransactionName).Select(d =>
                             new DependTransaction
                             {
                                 ISIN_CODE = d.Element("ISIN_CODE").Value,
                                 ISIN_SHORT_NAME = d.Element("ISIN_SHORT_NAME").Value,
                                 TRANS_REFERENCE = d.Element("TRANS_REFERENCE").Value,
                                 TRANS_REMARKS = d.Element("TRANS_REMARKS").Value,
                                 TRANS_ID = Convert.ToInt32(d.Element("TRANS_ID").Value),
                                 BALCHANGE_QTY = Convert.ToDecimal(d.Element("BALCHANGE_QTY").Value)
                             }).FirstOrDefault());
                    dependTransactionList = dependTransactions;
                }
            }
            catch (Exception e)
            {
                string exMessage = string.Format("Exception happened when trying to set data set");
                this.logger.LogCritical(e, exMessage);
            }
        }

        #endregion

        #region Save Data set invoke

        public void InvokeSaveDataSetService(string data, string name)
        {
            HttpWebRequest request = soapWebReuqestsService.CreateSOAPWebRequest("POST");
            XmlDocument SOAPReqBody = new XmlDocument();
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("sessionId", sessionId);
                values.Add("name", name);
                values.Add("data", data);

                SOAPReqBody.LoadXml(soapWebReuqestsService.GetSaveSOAPBodyText("DataSetUpdate", values));
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke data set service.";
                this.logger.LogCritical(e, exMessage);
            }

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string serviceResult = rd.ReadToEnd();
                        //string result = GetXMLResponseValue(serviceResult, "DataSetListResponse", "data");

                        //XDocument doc = XDocument.Parse(result);
                        //SetDataSet(name, doc);
                    }
                }
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke data set service.";
                this.logger.LogCritical(e, exMessage);
            }
        }

        #endregion

        #region Local Helper Functions

        private string GetMandateType(DependMandate dependMandate, DependOrder dependOrder, string conditionProperty)
        {
            string depoMandateType = "1";
            try
            {
                if (dependMandate != null && dependOrder != null && dependMandate.MANDATE_DESCRIPTION == dependOrder.DEPOPART_CODE &&
                   ((conditionProperty == Constants.ATSORDER_LINKED_ACCOUNT_REFERENCE && dependMandate.MANDATE_TYPE == "C") || (conditionProperty == Constants.ACCOUNT_REFERENCE && dependMandate.MANDATE_TYPE == "C")))
                {
                    depoMandateType = "0";
                }
            }
            catch(Exception e)
            {
                string exMessage = "Exception happened when trying to get mandate type.";
                this.logger.LogCritical(e, exMessage);
            }
            return depoMandateType;
        }

        private bool IsCheckingPassed(string depoMandateType, DependOrder dependOrder, string conditionProperty)
        {
            bool isPassed = false;
            try
            {
                if ((dependOrder.ACCOUNT_REFERENCE.Substring(10, 1) == depoMandateType && conditionProperty == Constants.ATSORDER_LINKED_ACCOUNT_REFERENCE)
                    || (dependOrder.ATSORDER_LINKED_ACCOUNT_REFERENCE.Substring(10, 1) == depoMandateType && conditionProperty == Constants.ACCOUNT_REFERENCE))
                {
                    isPassed = true;
                }
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke is checking passed.";
                this.logger.LogCritical(e, exMessage);
            }
            return isPassed;
        }

        #endregion

        #region Interface Implementation

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        #endregion
    }
}
