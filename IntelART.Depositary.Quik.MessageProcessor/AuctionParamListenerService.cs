﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.Model;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class AuctionParamListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable
    {
        private AuctionParamMessageListener messageListener;
        private ILogger<AuctionParamListenerService> logger;
        private string integrationConnectionString;
        private AuctionRepository auctionRepository;
        private QuikRepository quikRepository;
        private ActionLogRepository actionLogRepository;

        public AuctionParamListenerService(IListenerConfiguration configuration, ILogger<AuctionParamListenerService> logger) 
            : base(configuration.IntegrationConnectionString, new byte[] { 1 } )
        {
            this.logger = logger;
            try
            {
                UpdateServiceLastOperationDate();

                this.integrationConnectionString = configuration.IntegrationConnectionString;
                this.auctionRepository = new AuctionRepository(configuration.IntegrationConnectionString);
                this.quikRepository = new QuikRepository(configuration.ConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);
                this.messageListener = new AuctionParamMessageListener(configuration.ConnectionString, this.ProcessMessage, this.logger);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private Guid? ProcessMessage(AuctionParameterModel message)
        {
            UpdateServiceLastOperationDate();

            Guid? id = null;
            if (message != null)
            {
                try
                {
                    id = QuikUtility.SaveAuctionParam(new AuctionParameter()
                    {
                        ID = message.ID,
                        TRADE_QUANTITY = message.TradeQuantity,
                        AVERAGE_YIELD = message.AverageYield,
                        MINIMUM_YIELD = message.MinimumYield,
                        MAXIMUM_YIELD = message.MaximumYield,
                        AUCTION_YIELD = message.AuctionYield,
                        PARTICIPANT_COUNT = Convert.ToInt32(message.ParticipantCount),
                        CLASS_CODE = message.ClassCode,
                        SEC_CODE = message.SecCode,
                        TRADE_DATE = message.TradeDate
                    }, message.ID, auctionRepository, quikRepository, this.integrationConnectionString);
                    DBLogger.WriteSuccess(actionLogRepository, 1);
                    this.logger.LogInformation("Successfully processed auction parameter with ID {0}", id.Value);
                }
                catch (Exception e)
                {
                    if (!e.Message.Contains("Auction parameter is already processed"))
                        DBLogger.WriteFail(actionLogRepository, 1, e.Message);
                    this.logger.LogError(e, "Error while processing auction parameter");
                }
            }
            else
            {
                this.logger.LogWarning("Empty message received, skipping it. The message will not be considered processed.");
            }
            return id;
        }

        public void Dispose()
        {
            this.messageListener.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
                this.messageListener.Start();
                this.logger.LogInformation("Message listener successfully started");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.messageListener.Stop();

            return Task.CompletedTask;
        }
    }
}
