﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.DependUtility;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class DependUndepositionListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<DependUndepositionListenerService> logger;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private string sessionId;
        SoapWebReuqestsService soapWebReuqestsService;
        private DependDataSetRepository dependDataSetRepository;

        public DependUndepositionListenerService(IListenerConfiguration configuration, ILogger<DependUndepositionListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] {  })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.DependUndepositionInterval == 0)
            {
                return;
            }

            UpdateServiceLastOperationDate();

            LoginService loginService = new LoginService(settings, logger);

            try
            {
                HelperService helperService = new HelperService(settings);
                soapWebReuqestsService = new SoapWebReuqestsService(settings);
                this.dependDataSetRepository = new DependDataSetRepository(configuration.IntegrationConnectionString);
                IEnumerable<DependUndeposition> dependUndepositionList = this.dependDataSetRepository.GetDependUndeposition(false);

                sessionId = loginService.InvokeLoginService(settings.DependUser, settings.DependPassword, settings.DependClientVersion, settings.DependParticipant);

                foreach (DependUndeposition dependUndeposition in dependUndepositionList)
                {
                    string isinCode = dependUndeposition.ISIN_CODE;
                    string dbAccountReference = dependUndeposition.DEPO_ACCOUNTS == "BNK" ? helperService.GetCashAccount(dependUndeposition.ISIN_SHORT_NAME) : settings.PaperAccount;
                    string crAccountReference = dependUndeposition.ACCOUNT_REFERENCE;
                    string ballChangeQuantity = string.Format("{0:0.00}", Math.Abs(dependUndeposition.BALCHANGE_QTY));
                    string tfrRef = string.Format("_OUT_CORP_{0}_{1}", DateTime.Now.ToString("yyyymmddhhmm"), dependUndeposition.DEPOPART_CODE);
                    string tfrRem = string.Format("_OUT_CORP_{0},{1},{2},{3},{4},{5}",
                        DateTime.Now.ToString("yyyymmddhhmm"), dependUndeposition.DEPOPART_CODE,
                        dependUndeposition.ACCOUNT_CODE, dependUndeposition.ISIN_SHORT_NAME,
                        ballChangeQuantity, dependUndeposition.LINKED_ACCOUNT_REFERENCE);

                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine($"<![CDATA[<P_TWIN_SIDED_TRANSACTION.1><P_TWIN_SIDED_TRANSACTION><PARAM_TFR_ISIN_CODE>{isinCode}</PARAM_TFR_ISIN_CODE>");
                    sb.AppendLine($"<PARAM_TFR_DB_AC_REF>{dbAccountReference }</PARAM_TFR_DB_AC_REF>");
                    sb.AppendLine($"<PARAM_TFR_CR_AC_REF>{crAccountReference}</PARAM_TFR_CR_AC_REF>");
                    sb.AppendLine($"<PARAM_TFR_QTY>{ballChangeQuantity}</PARAM_TFR_QTY>");
                    sb.AppendLine($"<PARAM_TFR_REF>{tfrRef}</PARAM_TFR_REF>");
                    sb.AppendLine($"<PARAM_TFR_REM>{tfrRem}</PARAM_TFR_REM>");
                    sb.AppendLine("<PARAM_TFR_TYPE>OG</PARAM_TFR_TYPE></P_TWIN_SIDED_TRANSACTION></P_TWIN_SIDED_TRANSACTION.1>]]>");

                    InvokeSaveDataSetService(sb.ToString(), "P_TWIN_SIDED_TRANSACTION");
                }

                LogoutService logoutService = new LogoutService(sessionId, settings, logger);
                logoutService.InvokeLogoutService();

                foreach (DependUndeposition dependUndeposition in dependUndepositionList)
                {
                    dependDataSetRepository.SetDependUndepositionStatus(dependUndeposition.ID, true);
                }

                serviceTimer = new System.Timers.Timer(settings.DependUndepositionInterval);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
            }
        }

     

        #region Save Data set invoke

        public void InvokeSaveDataSetService(string data, string name)
        {
            HttpWebRequest request = soapWebReuqestsService.CreateSOAPWebRequest("POST");
            XmlDocument SOAPReqBody = new XmlDocument();
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("sessionId", sessionId);
                values.Add("name", name);
                values.Add("data", data);

                SOAPReqBody.LoadXml(soapWebReuqestsService.GetSaveSOAPBodyText("DataSetUpdate", values));
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke save data set service.";
                this.logger.LogCritical(e, exMessage);
            }

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string serviceResult = rd.ReadToEnd();
                        //string result = GetXMLResponseValue(serviceResult, "DataSetListResponse", "data");

                        //XDocument doc = XDocument.Parse(result);
                        //SetDataSet(name, doc);
                    }
                }
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke save data set service.";
                this.logger.LogCritical(e, exMessage);
            }
        }

        #endregion


        #region Interface Implementation

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        #endregion

    }
}
