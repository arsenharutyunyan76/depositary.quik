﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class ISINUpdater : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<ISINUpdater> logger;
        private RTSOutDataRepository rtsSourceRepository;
        private IsinRepository rtsDestinationRepository;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private bool canProcessTimerEvent = true;

        public ISINUpdater(IListenerConfiguration configuration, ILogger<ISINUpdater> logger)
            : base(configuration.IntegrationConnectionString, new byte[] {  })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.ISINUpdaterServiceInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate(); 

                this.rtsSourceRepository = new RTSOutDataRepository(configuration.RTSGatewayConnectionString);
                this.rtsDestinationRepository = new IsinRepository(configuration.IntegrationConnectionString);

                serviceTimer = new System.Timers.Timer(settings.ISINUpdaterServiceInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessUpdaterPeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the service");
                throw;
            }
        }

        private void ProcessUpdaterPeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            if (this.canProcessTimerEvent)
            {
                try
                {
                    IEnumerable<Isin> ISINsource = rtsSourceRepository.GetNotProcessedISIN().Result;
                    foreach (Isin isin in ISINsource)
                    {
                        try
                        {
                            this.canProcessTimerEvent = false;
                            rtsDestinationRepository.SaveIsin(isin).Wait();
                            rtsSourceRepository.ProcessISIN(isin);
                        }
                        catch (Exception e)
                        {
                            this.logger.LogError(e, "Error while processing ISIN {0}", isin.ISIN_CODE);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing ISIN update");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }
    }
}
