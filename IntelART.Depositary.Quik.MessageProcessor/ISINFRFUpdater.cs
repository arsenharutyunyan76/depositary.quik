﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class ISINFRFUpdater : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private readonly ILogger<ISINFRFUpdater> logger;
        private readonly IsinRepository isinRepository;
        private readonly Settings settings;
        private readonly System.Timers.Timer serviceTimer;
        private bool canProcessTimerEvent = true;

        public ISINFRFUpdater(IListenerConfiguration configuration, ILogger<ISINFRFUpdater> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.ISINFRFUpdaterServiceInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                this.isinRepository = new IsinRepository(configuration.IntegrationConnectionString);

                serviceTimer = new System.Timers.Timer(settings.ISINFRFUpdaterServiceInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessUpdaterPeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the service");
                throw;
            }
        }

        private void ProcessUpdaterPeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            if (this.canProcessTimerEvent)
            {
                try
                {
                    try
                    {
                        this.canProcessTimerEvent = false;
                        isinRepository.ProcessFRFNullRows().Wait();
                    }
                    catch (Exception e)
                    {
                        this.logger.LogError(e, "Error while processing ISIN FRF null rows");
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing ISIN FRF null rows update");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }
    }
}
