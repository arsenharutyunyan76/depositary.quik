﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class HTPaymentListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<HTPaymentListenerService> logger;
        private BankmailRepository bankmailRepository;
        private PaymentRepository paymentRepository;
        private ActionLogRepository actionLogRepository;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private bool canProcessTimerEvent = true;

        public HTPaymentListenerService(IListenerConfiguration configuration, ILogger<HTPaymentListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 16 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.HTPaymentListenerServiceInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                this.bankmailRepository = new BankmailRepository(configuration.BankmailConnectionString);
                this.paymentRepository = new PaymentRepository(configuration.IntegrationConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);

                serviceTimer = new System.Timers.Timer(settings.HTPaymentListenerServiceInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessMessagePeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        private void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            IEnumerable<BankmailMessage> messages;

            if (this.canProcessTimerEvent)
            {
                try
                {
                    this.canProcessTimerEvent = false;
                    messages = bankmailRepository.GetNotProcessedMessages("522,100,202");
                    foreach (BankmailMessage message in messages)
                    {
                        if (this.ProcessMessage(message))
                        {
                            bankmailRepository.ProcessMessage(message);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing HT522 / HT100 / HT202");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private bool ProcessMessage(BankmailMessage message) 
        {
            bool result = false;
            try
            {
                string messageType = message.MESSAGE_TYPE;

                if (messageType == "522" || messageType == "100" || messageType == "202")
                {
                    string documentNumber = BankmailUtility.GetFieldValue("20", message.BODY);
                    PaymentMessage paymentMessage = new PaymentMessage()
                    {
                        ID = message.ID,
                        MESSAGE_TYPE = messageType,
                        DOCUMENT_NUMBER = documentNumber
                    };

                    switch (messageType)
                    {
                        case "522":
                            paymentMessage.SECURITY_ID = BankmailUtility.GetFieldValue("35B", message.BODY);
                            paymentMessage.AMOUNT = Convert.ToDecimal(BankmailUtility.GetFieldValue("35C", message.BODY));
                            paymentMessage.PAYER = BankmailUtility.GetFieldValue("83", message.BODY);
                            paymentMessage.RECEIVER = BankmailUtility.GetFieldValue("87", message.BODY);
                            break;

                        default:
                            string operDateCurrSumm = BankmailUtility.GetFieldValue("32A", message.BODY);
                            paymentMessage.CURRENCY = operDateCurrSumm.Substring(6, 3);
                            paymentMessage.AMOUNT = Convert.ToDecimal(operDateCurrSumm.Substring(9));
                            paymentMessage.PAYER = BankmailUtility.GetFieldValue("50A", message.BODY);
                            paymentMessage.RECEIVER = BankmailUtility.GetFieldValue("59", message.BODY);
                            break;
                    }
                    paymentRepository.SavePaymentMessage(paymentMessage);

                    DBLogger.WriteSuccess(actionLogRepository, 16, message.ID.ToString());
                    this.logger.LogInformation("Successfully processed HT{0}, the ID is {1}", message.MESSAGE_TYPE, message.ID);
                }
                result = true;
            }
            catch (Exception e)
            {
                DBLogger.WriteFail(actionLogRepository, 16, e.Message);
                this.logger.LogError(e, "Error while processing HT{0} with ID {1}", message.MESSAGE_TYPE, message.ID);
            }
            return result;
        }

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }
    }
}
