﻿using IntelART.Depositary.Quik.MessageProcessor.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Data.SqlClient;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class AuctionParamMessageListener : IDisposable
    {
        private string connectionString;
        private SqlDependency sqlDependency;
        private Func<AuctionParameterModel, Guid?> processMessage;
        private ILogger<AuctionParamListenerService> logger;

        public AuctionParamMessageListener(string connectionString, Func<AuctionParameterModel, Guid?> processMessage, ILogger<AuctionParamListenerService> logger)
        {
            this.connectionString = connectionString;
            this.processMessage = processMessage;
            this.logger = logger;
        }

        public void Dispose()
        {
            this.Stop();
        }

        public void Stop()
        {
            SqlDependency.Stop(connectionString);
        }

        private SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();
            return conn;
        }

        public void Start()
        {
            SqlDependency.Start(connectionString);
            this.QueryData();
        }

        private void QueryData()
        {
            using (SqlConnection con = this.GetConnection())
            {
                SqlCommand cmd = new SqlCommand($"SELECT ID,ClassCode,SecCode,TradeDate,isnull(totaltradeqty,0),isnull(wayield,0),isnull(lowyield,0),isnull(highyield,0),isnull(auctyield,0),isnull(numparticipants,0)" +
                    $" FROM dbo.Params WHERE PROCESSED_ID is null", con);
                this.sqlDependency = new SqlDependency(cmd);
                this.sqlDependency.OnChange += OnMessagesChanged;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            
                            this.ProcessMessage(new AuctionParameterModel(reader.GetGuid(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), 
                                reader.GetDecimal(4), reader.GetDecimal(5), reader.GetDecimal(6), reader.GetDecimal(7), reader.GetDecimal(8), reader.GetDecimal(9)));
                        }
                        catch (Exception e)
                        {
                            this.logger.LogError(e, "Error while processing auction params: {0}", e.ToString());
                        }
                    }
                }
            }
        }

        private void OnMessagesChanged(object sender, SqlNotificationEventArgs e)
        {
            this.sqlDependency.OnChange -= OnMessagesChanged;
            this.QueryData();
        }

        protected void ProcessMessage(AuctionParameterModel message)
        {
            Guid? processedId = null;
            try
            {
                processedId = this.processMessage(message);
            }
            catch (Exception e)
            {
                this.logger.LogError(e, "Error while processing auction parameter");
            }
        }
    }
}
