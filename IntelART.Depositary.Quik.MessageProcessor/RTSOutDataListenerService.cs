﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class RTSOutDataListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<RTSOutDataListenerService> logger;
        private RTSOutDataRepository rtsOutDataRepository;
        private RTSReportDataRepository rtsReportDataRepository;
        private ActionLogRepository actionLogRepository;
        private bool canProcessTimerEventQuote = true;
        private bool canProcessTimerEventTrade = true;

        private Settings settings;
        private System.Timers.Timer serviceTimer;

        public RTSOutDataListenerService(IListenerConfiguration configuration, ILogger<RTSOutDataListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 17 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.RTSDataProcessingInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                this.rtsOutDataRepository = new RTSOutDataRepository(configuration.RTSGatewayConnectionString);
                this.rtsReportDataRepository = new RTSReportDataRepository(configuration.IntegrationConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);

                serviceTimer = new System.Timers.Timer(settings.RTSDataProcessingInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessDataPeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
            }
        }

        private void ProcessDataPeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            if (this.canProcessTimerEventQuote)
            {
                try
                {
                    this.canProcessTimerEventQuote = false;
                    IEnumerable<QuoteHist> quoteData = rtsOutDataRepository.GetNotProcessedQUOTE_HIST();
                    foreach (QuoteHist quote in quoteData)
                    {
                        this.ProcessQuoteHistData(quote);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing RTS Out Data Quote_Hist");
                }
                finally
                {
                    this.canProcessTimerEventQuote = true;
                }
            }

            if (this.canProcessTimerEventTrade)
            {
                try
                {
                    this.canProcessTimerEventTrade = false;
                    IEnumerable<TradeHist> tradeData = rtsOutDataRepository.GetNotProcessedTRADE_HIST();
                    foreach (TradeHist trade in tradeData)
                    {
                        this.ProcessTradeHistData(trade);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing RTS Out Data Trade_Hist");
                }
                finally
                {
                    this.canProcessTimerEventTrade = true;
                }
            }
        }

        private void ProcessQuoteHistData(QuoteHist quote) 
        {
            try
            {
                rtsReportDataRepository.SaveRTSReportDataQuote(quote);
                rtsOutDataRepository.ProcessQUOTE_HIST(quote.ID);
                DBLogger.WriteSuccess(actionLogRepository, 17, Convert.ToString(quote.ID));
                this.logger.LogInformation("Successfully processed RTS Out Data Quote_Hist - {0}, the ID is {1}.", quote.ISSUE_NAME, quote.ID);
            }
            catch (Exception e)
            {
                DBLogger.WriteFail(actionLogRepository, 17, e.Message);
                this.logger.LogError(e, "Error while processing RTS Out Data Quote_Hist - {0} with ID {1}.", quote.ISSUE_NAME, quote.ID);
            }
        }

        private void ProcessTradeHistData(TradeHist trade) 
        {
            try
            {
                rtsReportDataRepository.SaveRTSReportDataTrade(trade);
                rtsOutDataRepository.ProcessTRADE_HIST(trade.ID);
                DBLogger.WriteSuccess(actionLogRepository, 17, Convert.ToString(trade.ID));
                this.logger.LogInformation("Successfully processed RTS Out Data Trade_Hist - {0}, the ID is {1}.", trade.ISSUE_NAME, trade.ID);
            }
            catch (Exception e)
            {
                DBLogger.WriteFail(actionLogRepository, 17, e.Message);
                this.logger.LogError(e, "Error while processing RTS Out Data Trade_Hist - {0} with ID {1}.", trade.ISSUE_NAME, trade.ID);
            }
        }

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
            }
            return Task.CompletedTask;
        }
    }
}
