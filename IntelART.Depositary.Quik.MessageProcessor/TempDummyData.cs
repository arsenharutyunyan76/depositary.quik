﻿using IntelART.Depositary.Quik.Entities;
using System.Collections.Generic;
using System.Linq;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class TempDummyData
    {

        /// DUMMY DATA FOR SAVING DEPEND ORDERS
        public IEnumerable<DependOrder> GetDummyData()
        {
            List<DependOrder> dependOrder = new List<DependOrder>();
            dependOrder.Add(new DependOrder()
            {
                ATSORDER_ID = 1,
                ISIN_SHORT_NAME = "sname1",
                ISIN_CODE = "code1",
                ACCOUNT_REFERENCE = "preferencefdgdfgdfgdfg1",
                ATSORDER_LINKED_ACCOUNT_OWNER_LIST = "alaol1",
                ATSORDER_LINKED_ACCOUNT_REFERENCE = "adfgdfgdfgdfglar1",
                ATS_NAME = "name1",
                BALCHANGE_QTY = 456,
                DEPOPART_CODE = "depoCode1"
            });
            dependOrder.Add(new DependOrder()
            {
                ATSORDER_ID = 2,
                ISIN_SHORT_NAME = "sname2",
                ISIN_CODE = "code2",
                ACCOUNT_REFERENCE = "preferencedfgdfgfdgdfgfdg2",
                ATSORDER_LINKED_ACCOUNT_OWNER_LIST = "alaol2",
                ATSORDER_LINKED_ACCOUNT_REFERENCE = "aldfgdfgdfgdfgdfgar2",
                ATS_NAME = "name2",
                BALCHANGE_QTY = 789,
                DEPOPART_CODE = "depoCode2"
            });
            return dependOrder;
        }

        /// DUMMY DATA FOR SAVING DEPEND ISIN
        public IEnumerable<DependIsin> GetIsinDummyData(string code)
        {
            List<DependIsin> dependIsin = new List<DependIsin>();
            dependIsin.Add(new DependIsin()
            {
                ISIN_CCY = "AMD",
                ISIN_CODE = "code1",
                ISIN_TYPE = "E",
                ISIN_TRADEABLE="Y"
            });
            dependIsin.Add(new DependIsin()
            {
                ISIN_CCY = "AMD",
                ISIN_CODE = "code2",
                ISIN_TYPE = "E",
                ISIN_TRADEABLE = "Y"
            });
            dependIsin.Add(new DependIsin()
            {
                ISIN_CCY = "AMD",
                ISIN_CODE = "code1",
                ISIN_TYPE = "EEE",
                ISIN_TRADEABLE = "Y"
            });

            DependIsin di = dependIsin.Where(x => x.ISIN_CODE == code).FirstOrDefault();
            return new List<DependIsin>() { di };
        }


        /// DUMMY DATA FOR SAVING DEPEND HOLDER
        public IEnumerable<DependHolder> GetHolderDummyData(string pref)
        {
            List<DependHolder> dependHolder = new List<DependHolder>();
            dependHolder.Add(new DependHolder()
            {
                ACCOUNT_ID = 1,
                ACCOUNT_REFERENCE = "preferdfgdfgdfgdfgdfgence1"
            }); ;
            dependHolder.Add(new DependHolder()
            {
                ACCOUNT_ID = 2,
                ACCOUNT_REFERENCE = "prefedfgdfgdfgdrence2"
            });
            dependHolder.Add(new DependHolder()
            {
                ACCOUNT_ID = 1,
                ACCOUNT_REFERENCE = "preferffgdfgdfgdfgence1"
            });

            DependHolder di = dependHolder.Where(x => x.ACCOUNT_REFERENCE == pref).FirstOrDefault();
            return new List<DependHolder>() { di };
        }


        public IEnumerable<DependMandate> GetMandateDummyData(string pref)
        {
            List<DependMandate> dependMandate = new List<DependMandate>();
            dependMandate.Add(new DependMandate()
            {
                MANDATE_TYPE = "type1",
                MANDATE_DESCRIPTION = "description1",
                ACCOUNT_REFERENCE = "preference1"
            });
            dependMandate.Add(new DependMandate()
            {
                MANDATE_TYPE = "type2",
                MANDATE_DESCRIPTION = "description2",
                ACCOUNT_REFERENCE = "preference2"
            });
            dependMandate.Add(new DependMandate()
            {
                MANDATE_TYPE = "type1",
                MANDATE_DESCRIPTION = "description1",
                ACCOUNT_REFERENCE = "preference1"
            });

            DependMandate di = dependMandate.Where(x => x.ACCOUNT_REFERENCE == pref).FirstOrDefault();
            return new List<DependMandate>() { di };
        }
    }
}
