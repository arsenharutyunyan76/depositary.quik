﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntelART.Depositary.Quik.MessageProcessor.Model
{
    public class AuctionParameterModel
    {
        public Guid ID { get; set; }
        public string ClassCode { get; set; }
        public string SecCode { get; set; }
        public DateTime TradeDate { get; set; }
        public decimal TradeQuantity { get; set; }
        public decimal AverageYield { get; set; }
        public decimal MinimumYield { get; set; }
        public decimal MaximumYield { get; set; }
        public decimal AuctionYield { get; set; }
        public decimal ParticipantCount { get; set; }

        public AuctionParameterModel(Guid id, string classCode, string secCode, DateTime tradeDate,
            decimal tradeQuantity,  decimal averageYield, 
            decimal minimumYield, decimal maximumYield, 
            decimal auctionYield, decimal participantCount)
        {
            this.ID = id;
            this.ClassCode = classCode;
            this.SecCode = secCode;
            this.TradeDate = tradeDate;
            this.TradeQuantity = tradeQuantity;
            this.AverageYield = averageYield;
            this.MinimumYield = minimumYield;
            this.MaximumYield = maximumYield;
            this.AuctionYield = auctionYield;
            this.ParticipantCount = participantCount;
        }
    }
}
