﻿using System;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class TradeModel
    {
        public Guid ID { get; set; }
        public decimal TradeNum { get; set; }
        public DateTime TradeDateTime { get; set; }
        public string SecCode { get; set; }
        public string Operation { get; set; }
        public decimal Price { get; set; }
        public decimal Value { get; set; }
        public decimal Yield { get; set; }
        public decimal Qty { get; set; }
        public DateTime SettleDate { get; set; }
        public string FirmId { get; set; }
        public string CPFirmId { get; set; }
        public string ClientCode { get; set; }
        public string BrokerRef { get; set; }
        public int UID { get; set; }
        public string ClassCode { get; set; }
        public decimal OrderNum { get; set; }

        public TradeModel(Guid id, decimal tradeNum, DateTime tradeDateTime, string secCode, string operation
            , decimal price, decimal value, decimal yield, decimal qty, DateTime settleDate, string firmId, string cPFirmId
            , string clientCode, string brokerRef, int uid, string classCode, decimal orderNum)
        {
            this.ID = id;
            this.TradeNum = tradeNum;
            this.TradeDateTime = tradeDateTime;
            this.SecCode = secCode;
            this.Operation = operation;
            this.Price = price;
            this.Value = value;
            this.Yield = yield;
            this.Qty = qty;
            this.SettleDate = settleDate;
            this.FirmId = firmId;
            this.CPFirmId = cPFirmId;
            this.ClientCode = clientCode;
            this.BrokerRef = brokerRef;
            this.UID = uid;
            this.ClassCode = classCode;
            this.OrderNum = orderNum;
        }
    }
}
