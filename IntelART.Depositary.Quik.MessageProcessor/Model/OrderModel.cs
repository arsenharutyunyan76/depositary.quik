﻿using System;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class OrderModel
    {
        public Guid ID { get; set; }
        public decimal OrderNum { get; set; }
        public DateTime OrderDateTime { get; set; }
        public string SecCode { get; set; }
        public string Operation { get; set; }
        public decimal Price { get; set; }
        public decimal Value { get; set; }
        public decimal Yield { get; set; }
        public decimal Qty { get; set; }
        public decimal Balance { get; set; }
        public DateTime? WithdrawDateTime { get; set; }
        public string FirmId { get; set; }
        public string ClientCode { get; set; }
        public string BrokerRef { get; set; }
        public int UID { get; set; }
        public string ClassCode { get; set; }
        public string State { get; set; }

        public OrderModel(Guid id, decimal orderNum, DateTime orderDateTime, string secCode, string operation
            , decimal price, decimal value, decimal yield, decimal qty, decimal balance, DateTime? withdrawDateTime, string firmId
            , string clientCode, string brokerRef, int uid, string classCode, string state)
        {
            this.ID = id;
            this.OrderNum = orderNum;
            this.OrderDateTime = orderDateTime;
            this.SecCode = secCode;
            this.Operation = operation;
            this.Price = price;
            this.Value = value;
            this.Yield = yield;
            this.Qty = qty;
            this.Balance = balance;
            this.WithdrawDateTime = withdrawDateTime;
            this.FirmId = firmId;
            this.ClientCode = clientCode;
            this.BrokerRef = brokerRef;
            this.UID = uid;
            this.ClassCode = classCode;
            this.State = state;
        }
    }
}
