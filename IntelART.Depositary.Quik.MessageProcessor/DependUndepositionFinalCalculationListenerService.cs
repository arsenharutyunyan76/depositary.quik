﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.DependUtility;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class DependUndepositionFinalCalculationListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<DependUndepositionFinalCalculationListenerService> logger;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private FileGenerationTimeRepository fileGenerationTimeRepository;
        private string sessionId;
        private DependDataSetRepository dependDataSetRepository;
        SoapWebReuqestsService soapWebReuqestsService;

        public DependUndepositionFinalCalculationListenerService(IListenerConfiguration configuration, ILogger<DependUndepositionFinalCalculationListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] {  })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.DependUndepositionFinalCalcInterval == 0)
            {
                return;
            }

            UpdateServiceLastOperationDate();

            this.fileGenerationTimeRepository = new FileGenerationTimeRepository(configuration.IntegrationConnectionString);
            LoginService loginService = new LoginService(settings, logger);

            DateTime? fileGenerationTime = fileGenerationTimeRepository.GetFileGenerationTime(3).GENERATE_DATE;
            DateTime currDateTime = DateTime.Now;

            if (TimeSpan.Parse(settings.DependUndepositionFinalCalcStartTime) <= currDateTime.TimeOfDay &&
                TimeSpan.Parse(settings.DependUndepositionFinalCalcEndTime) >= currDateTime.TimeOfDay &&
                (!fileGenerationTime.HasValue || fileGenerationTime.Value.Date != currDateTime.Date))
            {
                try
                {
                    this.dependDataSetRepository = new DependDataSetRepository(configuration.IntegrationConnectionString);
                    IEnumerable<DependUndeposition> dependUndepositionList = this.dependDataSetRepository.GetDependUndeposition(true);

                    sessionId = loginService.InvokeLoginService(settings.DependUser, settings.DependPassword, settings.DependClientVersion, settings.DependParticipant);
                    soapWebReuqestsService = new SoapWebReuqestsService(settings);

                    foreach (DependUndeposition dependUndeposition in dependUndepositionList)
                    {
                        string isinCode = dependUndeposition.ISIN_CODE;
                        string dbAccountReference = "NOA";
                        string crAccountReference = dependUndeposition.ACCOUNT_REFERENCE;
                        string linkedAccountRef = dependUndeposition.LINKED_ACCOUNT_REFERENCE;
                        string ballChangeQuantity = string.Format("{0:0.00}", Math.Abs(dependUndeposition.BALCHANGE_QTY));
                        string tfrRef = string.Format("_OUT_CORP_{0}_{1}", DateTime.Now.ToString("yyyymmddhhmm"), dependUndeposition.DEPOPART_CODE);
                        string tfrRem = string.Format("_OUT_CORP_{0},{1},{2},{3},{4},{5}",
                            DateTime.Now.ToString("yyyymmddhhmm"), dependUndeposition.DEPOPART_CODE,
                            dependUndeposition.ACCOUNT_CODE, dependUndeposition.ISIN_SHORT_NAME,
                            ballChangeQuantity, dependUndeposition.LINKED_ACCOUNT_REFERENCE);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine($"<![CDATA[<P_TWIN_SIDED_TRANSACTION.1><P_TWIN_SIDED_TRANSACTION><PARAM_TFR_ISIN_CODE>{isinCode}</PARAM_TFR_ISIN_CODE>");
                        sb.AppendLine($"<PARAM_TFR_DB_AC_REF>{dbAccountReference }</PARAM_TFR_DB_AC_REF>");
                        sb.AppendLine($"<PARAM_TFR_CR_AC_REF>{crAccountReference}</PARAM_TFR_CR_AC_REF>");
                        sb.AppendLine($"<PARAM_TFR_QTY>{ballChangeQuantity}</PARAM_TFR_QTY>");
                        sb.AppendLine($"<PARAM_TFR_REF>{tfrRef}</PARAM_TFR_REF>");
                        sb.AppendLine($"<PARAM_TFR_REM>{tfrRem}</PARAM_TFR_REM>");
                        sb.AppendLine("<PARAM_TFR_TYPE>OG</PARAM_TFR_TYPE></P_TWIN_SIDED_TRANSACTION></P_TWIN_SIDED_TRANSACTION.1>]]>");

                        InvokeSaveDataSetService(sb.ToString(), "P_TWIN_SIDED_TRANSACTION");


                        sb = new StringBuilder();
                        sb.AppendLine($"<![CDATA[<P_ORDER_INSERT_AMEND.1><P_ORDER_INSERT_AMEND>");
                        sb.AppendLine($"<PARAM_OIA_AC_REF>{crAccountReference }</PARAM_OIA_AC_REF>");
                        sb.AppendLine($"<PARAM_OIA_ISIN_CODE>{isinCode }</PARAM_OIA_ISIN_CODE>");
                        sb.AppendLine($"<PARAM_OIA_QTY>{ballChangeQuantity}</PARAM_OIA_QTY>");
                        sb.AppendLine($"<PARAM_OIA_TYPE>SELL</PARAM_OIA_TYPE>");
                        sb.AppendLine($"<PARAM_OIA_ATS>1</PARAM_OIA_ATS>");
                        sb.AppendLine($"<PARAM_OIA_LINK_AC_REF>{linkedAccountRef}</PARAM_OIA_LINK_AC_REF>");
                        sb.AppendLine($"<IUD>I</IUD>");
                        sb.AppendLine("</P_ORDER_INSERT_AMEND></P_ORDER_INSERT_AMEND.1>]]>");

                        InvokeSaveDataSetService(sb.ToString(), "P_ORDER_INSERT_AMEND");
                    }

                    fileGenerationTimeRepository.SetFileGenerationTime(3, currDateTime);

                    LogoutService logoutService = new LogoutService(sessionId, settings, logger);
                    logoutService.InvokeLogoutService();

                    foreach (DependUndeposition dependUndeposition in dependUndepositionList)
                    {
                        dependDataSetRepository.SetDependUndepositionStatus(dependUndeposition.ID, true);
                    }
                    serviceTimer = new System.Timers.Timer(settings.DependUndepositionFinalCalcInterval);
                }
                catch (Exception e)
                {
                    this.logger.LogCritical(e, "Exception happened when creating the listener service");
                }
            }
        }

        #region Save Data set invoke

        public void InvokeSaveDataSetService(string data, string name)
        {
            HttpWebRequest request = soapWebReuqestsService.CreateSOAPWebRequest("POST");
            XmlDocument SOAPReqBody = new XmlDocument();
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("sessionId", sessionId);
                values.Add("name", name);
                values.Add("data", data);

                SOAPReqBody.LoadXml(soapWebReuqestsService.GetSaveSOAPBodyText("DataSetUpdate", values));
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke data set service.";
                this.logger.LogCritical(e, exMessage);
            }

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string serviceResult = rd.ReadToEnd();
                        //string result = GetXMLResponseValue(serviceResult, "DataSetListResponse", "data");

                        //XDocument doc = XDocument.Parse(result);
                        //SetDataSet(name, doc);
                    }
                }
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke data set service.";
                this.logger.LogCritical(e, exMessage);
            }
        }
        #endregion

        #region Interface Implementation

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        #endregion

    }
}
