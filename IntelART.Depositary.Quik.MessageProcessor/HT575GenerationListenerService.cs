﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class HT575GenerationListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable
    {
        private HT575GenerationMessageListener messageListener;
        private ILogger<HT575GenerationListenerService> logger;
        private string integrationConnectionString;
        private BankmailMessageRepository bankmailMessageRepository;
        private BankmailRepository bankmailRepository;
        private TradeRepository tradeRepository;
        private AuctionRepository auctionRepository;
        private QuikRepository quikRepository;
        private ActionLogRepository actionLogRepository;
        private Settings settings;

        public HT575GenerationListenerService(IListenerConfiguration configuration, ILogger<HT575GenerationListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 3 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            try
            {
                UpdateServiceLastOperationDate();

                this.integrationConnectionString = configuration.IntegrationConnectionString;
                this.bankmailMessageRepository = new BankmailMessageRepository(configuration.IntegrationConnectionString);
                this.bankmailRepository = new BankmailRepository(configuration.BankmailConnectionString);
                this.tradeRepository = new TradeRepository(configuration.IntegrationConnectionString);
                this.auctionRepository = new AuctionRepository(configuration.IntegrationConnectionString);
                this.quikRepository = new QuikRepository(configuration.ConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);
                this.messageListener = new HT575GenerationMessageListener(configuration.IntegrationConnectionString, settings.TradeProcessInterval, this.ProcessMessage, this.logger);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private Guid? ProcessMessage(List<Trade> messages)
        {
            Guid? result = null;
            try
            {
                Trade message = messages.FirstOrDefault();

                if (tradeRepository.GetSameAuctionTradesCount(message.TRADE_DATE, message.ISIN, message.AUCTION_TYPE_ID) != messages.Count)
                    throw new ApplicationException(string.Format("Some trades are not included in this package", message.ISIN));

                Auction auction = auctionRepository.GetAuctionData(message.TRADE_DATE, message.ISIN, message.AUCTION_TYPE_ID);
                if (auction == null)
                    throw new ApplicationException(string.Format("Auction with ISIN {0} doesn't exist", message.ISIN));

                bool isAuction = auction.AUCTION_TYPE_ID == 1 || auction.AUCTION_TYPE_ID == 3;

                IEnumerable<Trade> uniqueMessages = messages.Where(item => item.BANK_CODE == settings.CompanyCode);
                IEnumerable<string> banks = uniqueMessages.Select(item => item.CP_BANK_CODE).Distinct();

                decimal totalQuantity = uniqueMessages.Sum(item => item.QUANTITY);

                decimal avgYield;
                if (totalQuantity == 0)
                    avgYield = 0;
                else
                    avgYield = uniqueMessages.Sum(item => item.YIELD * item.QUANTITY) / totalQuantity;

                decimal minYield = uniqueMessages.Min(item => item.YIELD);
                decimal maxYield = uniqueMessages.Max(item => item.YIELD);

                if (settings.CheckAuctionParameters)
                {
                    string auctionTypeName;
                    switch (message.AUCTION_TYPE_ID)
                    {
                        case 1:
                            auctionTypeName = settings.AuctionTypePlacingCode;
                            break;
                        case 2:
                            auctionTypeName = settings.AuctionTypeReturnCode;
                            break;
                        default:
                            auctionTypeName = settings.AuctionTypeAdditionalCode;
                            break;
                    }

                    AuctionParameter parameters = auctionRepository.GetAuctionParameters(message.TRADE_DATE, message.ISIN, auctionTypeName);
                    if (parameters == null)
                        throw new ApplicationException(string.Format("Auction parameters with ISIN {0} don't exist", message.ISIN));

                    if (Math.Round(totalQuantity, 1) != Math.Round(parameters.TRADE_QUANTITY, 1))
                        throw new ApplicationException("Total trade quantity is wrong");

                    if (Math.Round(avgYield, 4) != Math.Round(parameters.AVERAGE_YIELD, 4))
                        throw new ApplicationException("Average yield is wrong");

                    if (Math.Round(minYield, 4) != Math.Round(parameters.MINIMUM_YIELD, 4))
                        throw new ApplicationException("Minimum yield is wrong");

                    if (Math.Round(maxYield, 4) != Math.Round(parameters.MAXIMUM_YIELD, 4))
                        throw new ApplicationException("Maximum yield is wrong");

                    if ((isAuction && Math.Round(maxYield, 4) != Math.Round(parameters.AUCTION_YIELD, 4))
                        || (!isAuction && Math.Round(minYield, 4) != Math.Round(parameters.AUCTION_YIELD, 4)))
                        throw new ApplicationException("Auction yield is wrong");

                    if (banks.Count() != parameters.PARTICIPANT_COUNT)
                        throw new ApplicationException("Participants count is wrong");
                }

                StringBuilder builder = new StringBuilder();
                BankmailUtility.AddTagRow("24B", isAuction ? "AUCS" : "RETS", ref builder);
                BankmailUtility.AddTagRow("35B", message.ISIN, ref builder);
                BankmailUtility.AddTagRow("12B", auction.AUCTION_ID, ref builder);

                BankmailUtility.AddTagRow("12C", string.Format("VLM{0}", BankmailUtility.SerializeDecimal(totalQuantity, 1)), ref builder);
                builder.AppendLine(string.Format("AMD{0}", BankmailUtility.SerializeDecimal(uniqueMessages.Sum(item => item.VALUE), 1)));
                builder.AppendLine(string.Format("ATP{0}", auction.AUCTION_TYPE_ID == 3 ? 0 : 1));
                builder.AppendLine(string.Format("MIN{0}", BankmailUtility.SerializeDecimal(minYield, 4)));
                builder.AppendLine(string.Format("AVG{0}", BankmailUtility.SerializeDecimal(avgYield, 4)));
                builder.AppendLine(string.Format("MAX{0}", BankmailUtility.SerializeDecimal(maxYield, 4)));

                BankmailUtility.AddTagRow("31", BankmailUtility.SerializeDate(message.TRADE_DATE), ref builder);
                BankmailUtility.AddTagRow("30V", BankmailUtility.SerializeDate(message.SETTLE_DATE), ref builder);

                if (isAuction)
                    BankmailUtility.AddTagRow("83A", settings.CompanyCode, ref builder);
                else
                    BankmailUtility.AddTagRow("87A", settings.CompanyCode, ref builder);

                foreach (string bank in banks)
                {
                    if (isAuction)
                        BankmailUtility.AddTagRow("87A", bank, ref builder);
                    else
                        BankmailUtility.AddTagRow("83A", bank, ref builder);

                    IEnumerable<Trade> bankAgentMessages = messages.Where(item => item.BANK_CODE == bank && item.CLIENT_CODE != settings.TradePrincipalName);
                    if (bankAgentMessages != null && bankAgentMessages.Count() > 0)
                    {
                        BankmailUtility.AddTagRow("24C", "INV", ref builder);
                        BankmailUtility.AddTagRow("35C", BankmailUtility.SerializeDecimal(bankAgentMessages.Sum(item => item.QUANTITY), 1), ref builder);
                        BankmailUtility.AddTagRow("35A", "AMD" + BankmailUtility.SerializeDecimal(bankAgentMessages.Sum(item => item.VALUE), 1), ref builder);
                    }

                    IEnumerable<Trade> bankPrincipalMessages = messages.Where(item => item.BANK_CODE == bank && item.CLIENT_CODE == settings.TradePrincipalName);
                    if (bankPrincipalMessages != null && bankPrincipalMessages.Count() > 0)
                    {
                        BankmailUtility.AddTagRow("24C", "OWN", ref builder);
                        BankmailUtility.AddTagRow("35C", BankmailUtility.SerializeDecimal(bankPrincipalMessages.Sum(item => item.QUANTITY), 1), ref builder);
                        BankmailUtility.AddTagRow("35A", "AMD" + BankmailUtility.SerializeDecimal(bankPrincipalMessages.Sum(item => item.VALUE), 1), ref builder);
                    }
                }
                string body = null;
                result = BankmailUtility.SaveBankmail575Message(builder, bankmailMessageRepository, bankmailRepository, tradeRepository, settings.HTSenderBankCode, settings.HTReceiverBankCode, integrationConnectionString, messages.Select(item => item.ID).Distinct(), ref body);
                DBLogger.WriteSuccess(actionLogRepository, 3, body);
                this.logger.LogInformation("Successfully created HT575, the ID is {0}", result.Value);
            }
            catch (Exception e)
            {
                if (!e.Message.Contains("Some trades are not included in this package"))
                    DBLogger.WriteFail(actionLogRepository, 3, e.Message);
                this.logger.LogError(e, "Error while creating HT575");
            }
            return result;
        }

        public void Dispose()
        {
            this.messageListener.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
                this.messageListener.Start();
                this.logger.LogInformation("Message listener successfully started");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.messageListener.Stop();

            return Task.CompletedTask;
        }
    }
}
