﻿using IntelART.Depositary.Quik.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace IntelART.Depositary.Quik.MessageProcessor.DependUtility
{
    public class SoapWebReuqestsService
    {
        Settings settings;
        public SoapWebReuqestsService(Settings settings)
        {
            this.settings = settings;
        }
        public HttpWebRequest CreateSOAPWebRequest(string method = "POST")
        {
            HttpWebRequest Req = (HttpWebRequest)WebRequest.Create(settings.DependServiceURL);
            Req.ContentType = "application/soap+xml;charset=\"utf-8\"";
            Req.Accept = "text/xml";
            Req.Method = method;
            return Req;
        }

        public string GetSOAPBodyText(string method, Dictionary<string, string> values)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(@"<?xml version=""1.0"" encoding=""utf-8""?>");
            builder.AppendLine(@"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ml7=""HTTP://WWW.PERCIVAL.CO.UK/ML7"">");
            builder.AppendLine(@"<soap:Header xmlns:wsa=""http://www.w3.org/2005/08/addressing"">");
            builder.AppendFormat(@"<wsa:Action>HTTP://WWW.PERCIVAL.CO.UK/ML7/IRDService/{0}</wsa:Action>", method);
            builder.AppendLine();
            builder.AppendLine(@"</soap:Header>");
            builder.AppendLine(@"<soap:Body>");
            builder.AppendFormat(@"<ml7:{0}>", method);
            builder.AppendLine();
            foreach (string key in values.Keys)
            {
                builder.AppendFormat(@"<ml7:{0}>{1}</ml7:{0}>", key, values[key]);
                builder.AppendLine();
            }
            builder.AppendFormat(@"</ml7:{0}>", method);
            builder.AppendLine();
            builder.AppendLine(@"</soap:Body>");
            builder.AppendLine(@"</soap:Envelope>");
            return builder.ToString();
        }

        public string GetXMLResponseValue(string xmlStr, string responseName, string resultName)
        {
            string serviceResult = string.Empty;
            XDocument doc = XDocument.Parse(xmlStr);
            XNamespace ns = "HTTP://WWW.PERCIVAL.CO.UK/ML7";
            XElement result = doc.Root.Descendants(ns + responseName).Elements(ns + resultName).FirstOrDefault();
            if (!String.IsNullOrEmpty(result.Value.ToString()))
            {
                serviceResult = result.Value.ToString();
            }
            return serviceResult;
        }

        public string GetSaveSOAPBodyText(string method, Dictionary<string, string> values)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(@"<?xml version=""1.0"" encoding=""utf-8""?>");
            builder.AppendLine(@"<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ml7=""HTTP://WWW.PERCIVAL.CO.UK/ML7"">");
            builder.AppendLine(@"<soap:Header xmlns:wsa=""http://www.w3.org/2005/08/addressing"">");
            builder.AppendFormat(@"<wsa:Action>HTTP://WWW.PERCIVAL.CO.UK/ML7/IRDService/{0}</wsa:Action>", method);
            builder.AppendLine();
            builder.AppendLine(@"</soap:Header>");
            builder.AppendLine(@"<soap:Body>");
            builder.AppendFormat(@"<ml7:{0}>", method);
            builder.AppendLine();
            foreach (string key in values.Keys)
            {
                builder.AppendFormat(@"<ml7:{0}>{1}</ml7:{0}>", key, values[key]);
                builder.AppendLine();
            }
            builder.AppendFormat(@"</ml7:{0}>", method);
            builder.AppendLine();
            builder.AppendLine(@"</soap:Body>");
            builder.AppendLine(@"</soap:Envelope>");
            return builder.ToString();
        }
    }
}
