﻿using IntelART.Depositary.Quik.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace IntelART.Depositary.Quik.MessageProcessor.DependUtility
{
    class LoginService
    {
        SoapWebReuqestsService soapWebReuqestsService;

        ILogger<IBaseLogger> logger;
        public LoginService(Settings settings, ILogger<IBaseLogger> logger)
        {
            soapWebReuqestsService = new SoapWebReuqestsService(settings);
            this.logger = logger;
        }

        public string InvokeLoginService(string user, string password, string clientVersion, string participant)
        {
            string sessionId = null;
            password = ConvertMD5(password);
            HttpWebRequest request = soapWebReuqestsService.CreateSOAPWebRequest();
            XmlDocument SOAPReqBody = new XmlDocument();
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("user", user);
                values.Add("password", password);
                values.Add("clientVersion", clientVersion);
                values.Add("participant", participant);
                SOAPReqBody.LoadXml(soapWebReuqestsService.GetSOAPBodyText("DependLogin", values));
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke login service.";
                logger.LogCritical(e, exMessage);
                throw new Exception(exMessage, e);
            }

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    string serviceResult = rd.ReadToEnd();
                    sessionId = soapWebReuqestsService.GetXMLResponseValue(serviceResult, "DependLoginResponse", "sessionId");
                }
            }
            return sessionId;
        }

        #region Converting functions

        public string ConvertMD5(string rawText)
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] hashBytes = md5.ComputeHash(Encoding.ASCII.GetBytes(rawText));

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        #endregion
    }
}
