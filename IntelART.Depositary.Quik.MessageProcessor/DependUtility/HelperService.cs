﻿using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.MessageProcessor.DependUtility
{
    public class HelperService
    {
        Settings settings;

        public HelperService(Settings settings)
        {
            this.settings = settings;
        }
        public string GetCashAccount(string isinCcy)
        {
            switch (isinCcy)
            {
                case "AMD": return settings.AmdAccount;
                case "USD": return settings.UsdAccount;
                case "EUR": return settings.EurAccount;
                default: return "";
            }
        }
    }
}
