﻿using IntelART.Depositary.Quik.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;

namespace IntelART.Depositary.Quik.MessageProcessor.DependUtility
{
    public class LogoutService
    {
        string sessionId;
        SoapWebReuqestsService soapWebReuqestsService;
        ILogger<IBaseLogger> logger;

        public LogoutService(string sessionId, Settings settings, ILogger<IBaseLogger> logger)
        {
            this.sessionId = sessionId;
            soapWebReuqestsService = new SoapWebReuqestsService(settings);
            this.logger = logger;
        }
        public void InvokeLogoutService()
        {
            HttpWebRequest request = soapWebReuqestsService.CreateSOAPWebRequest();
            XmlDocument SOAPReqBody = new XmlDocument();
            try
            {
                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("sessionId", sessionId);
                SOAPReqBody.LoadXml(soapWebReuqestsService.GetSOAPBodyText("Logout", values));
            }
            catch (Exception e)
            {
                string exMessage = "Exception happened when trying to invoke logout service.";
                logger.LogCritical(e, exMessage);
                throw new Exception(exMessage, e);
            }

            using (Stream stream = request.GetRequestStream())
            {
                SOAPReqBody.Save(stream);
            }
            using (WebResponse Serviceres = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(Serviceres.GetResponseStream()))
                {
                    string serviceResult = rd.ReadToEnd();
                    soapWebReuqestsService.GetXMLResponseValue(serviceResult, "LogoutResponse", "LogoutResult");
                }
            }
        }
    }
}
