﻿namespace IntelART.Depositary.Quik.MessageProcessor.DependUtility
{
    public static class Constants
    {
        public static string ATSORDER_LINKED_ACCOUNT_REFERENCE = "ATSORDER_LINKED_ACCOUNT_REFERENCE";
        public static string ACCOUNT_REFERENCE = "ACCOUNT_REFERENCE";
    }
}
