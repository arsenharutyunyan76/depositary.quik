﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class HT535ListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable
    {
        private ILogger<HT535ListenerService> logger;
        private string integrationConnectionString;
        private BankmailMessageRepository bankmailMessageRepository;
        private BankmailRepository bankmailRepository;
        private TradeRepository tradeRepository;
        private AuctionRepository auctionRepository;
        private ActionLogRepository actionLogRepository;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private bool canProcessTimerEvent = true;

        public HT535ListenerService(IListenerConfiguration configuration, ILogger<HT535ListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 13, 4 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.BankmailServiceInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                this.integrationConnectionString = configuration.IntegrationConnectionString;
                this.auctionRepository = new AuctionRepository(configuration.IntegrationConnectionString);
                this.bankmailMessageRepository = new BankmailMessageRepository(configuration.IntegrationConnectionString);
                this.bankmailRepository = new BankmailRepository(configuration.BankmailConnectionString);
                this.tradeRepository = new TradeRepository(configuration.IntegrationConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);

                serviceTimer = new System.Timers.Timer(settings.BankmailServiceInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessMessagePeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        private void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            if (this.canProcessTimerEvent)
            {
                try
                {
                    this.canProcessTimerEvent = false;
                    IEnumerable<BankmailMessage> messages = bankmailRepository.GetNotProcessedMessages("535");
                    foreach (BankmailMessage message in messages)
                    {
                        if (this.ProcessMessage(message))
                        {
                            bankmailRepository.ProcessMessage(message);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing HT535");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private bool ProcessMessage(BankmailMessage message)
        {
            bool result = false;
            try
            {
                string reference = BankmailUtility.GetFieldValue("21", message.BODY);
                string messageType = BankmailUtility.GetFieldValue("11R", message.BODY).Split('/')[1];
                bool messageResult = (BankmailUtility.GetFieldValue("23", message.BODY) == settings.BankmailApprovalCode);
                if (messageType == "575" || messageType == "515")
                {
                    Guid? id = bankmailMessageRepository.GetAcknowledgedBankmailMessage(reference, messageType);
                    if (id.HasValue)
                    {
                        if (messageResult)
                        {
                            switch (messageType)
                            {
                                case "575":
                                    IEnumerable<Trade> trades575 = tradeRepository.GetBankmailMessage575Trades(id.Value);
                                    IEnumerable<Trade> uniqueMessages = trades575.Where(item => item.BANK_CODE == settings.CompanyCode);
                                    Trade trade = uniqueMessages.FirstOrDefault();

                                    Auction auction = auctionRepository.GetAuctionData(trade.TRADE_DATE, trade.ISIN, trade.AUCTION_TYPE_ID);
                                    if (auction == null)
                                        throw new ApplicationException(string.Format("Auction with ISIN {0} doesn't exist", trade.ISIN));
                                    bool isAuction = auction.AUCTION_TYPE_ID == 1 || auction.AUCTION_TYPE_ID == 3;

                                    IEnumerable<string> banks = trades575.Where(item => item.BANK_CODE == settings.CompanyCode).Select(item => item.CP_BANK_CODE).Distinct();
                                    foreach (string bank in banks)
                                    {
                                        StringBuilder builder = new StringBuilder();
                                        BankmailUtility.AddTagRow("24B", isAuction ? "AUC" : "RET", ref builder);
                                        BankmailUtility.AddTagRow("35B", trade.ISIN, ref builder);
                                        BankmailUtility.AddTagRow("12B", auction.AUCTION_ID, ref builder);
                                        BankmailUtility.AddTagRow("31", BankmailUtility.SerializeDate(trade.TRADE_DATE), ref builder);
                                        BankmailUtility.AddTagRow("30V", BankmailUtility.SerializeDate(trade.SETTLE_DATE), ref builder);

                                        if (isAuction)
                                        {
                                            BankmailUtility.AddTagRow("83A", settings.CompanyCode, ref builder);
                                            BankmailUtility.AddTagRow("87A", bank, ref builder);
                                        }
                                        else
                                        {
                                            BankmailUtility.AddTagRow("87A", settings.CompanyCode, ref builder);
                                            BankmailUtility.AddTagRow("83A", bank, ref builder);
                                        }

                                        IEnumerable<Trade> bankAgentMessages = trades575.Where(item => item.BANK_CODE == bank && item.CLIENT_CODE != settings.TradePrincipalName);
                                        if (bankAgentMessages != null && bankAgentMessages.Count() > 0)
                                        {
                                            foreach (Trade agentTrade in bankAgentMessages)
                                            {
                                                BankmailUtility.AddTagRow("24C", "INV", ref builder);
                                                BankmailUtility.AddTagRow("35C", BankmailUtility.SerializeDecimal(agentTrade.QUANTITY, 1), ref builder);
                                                BankmailUtility.AddTagRow("35A", "AMD" + BankmailUtility.SerializeDecimal(agentTrade.VALUE, 1), ref builder);
                                                BankmailUtility.AddTagRow("23", agentTrade.TRADE_NUMBER.ToString(), ref builder);
                                            }
                                        }

                                        IEnumerable<Trade> bankPrincipalMessages = trades575.Where(item => item.BANK_CODE == bank && item.CLIENT_CODE == settings.TradePrincipalName);
                                        if (bankPrincipalMessages != null && bankPrincipalMessages.Count() > 0)
                                        {
                                            foreach (Trade principalTrade in bankPrincipalMessages)
                                            {
                                                BankmailUtility.AddTagRow("24C", "OWN", ref builder);
                                                BankmailUtility.AddTagRow("35C", BankmailUtility.SerializeDecimal(principalTrade.QUANTITY, 1), ref builder);
                                                BankmailUtility.AddTagRow("35A", "AMD" + BankmailUtility.SerializeDecimal(principalTrade.VALUE, 1), ref builder);
                                                BankmailUtility.AddTagRow("23", principalTrade.TRADE_NUMBER.ToString(), ref builder);
                                            }
                                        }
                                        string body = null;
                                        BankmailUtility.SaveBankmail515Message(builder, bankmailMessageRepository, bankmailRepository, tradeRepository, settings.HTSenderBankCode, settings.HTReceiverBankCode, integrationConnectionString, trades575.Where(item => item.BANK_CODE == bank || item.CP_BANK_CODE == bank).Select(item => item.ID).Distinct(), ref body);
                                        DBLogger.WriteSuccess(actionLogRepository, 4, body);
                                    }
                                    break;
                                case "515":
                                    tradeRepository.SetTradesBankmailMessage515Acknowledged(id.Value);
                                    break;
                            }
                            bankmailMessageRepository.AcknowledgeBankmailMessage(id.Value, messageResult);
                        }
                    }
                    DBLogger.WriteSuccess(actionLogRepository, 13, id.Value.ToString());
                    this.logger.LogInformation("Successfully processed HT535, the ID is {0}", message.ID);
                }
                result = true;
            }
            catch (Exception e)
            {
                DBLogger.WriteFail(actionLogRepository, 13, e.Message);
                this.logger.LogError(e, "Error while processing HT535 with ID {0}", message.ID);
            }
            return result;
        }

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }
    }
}
