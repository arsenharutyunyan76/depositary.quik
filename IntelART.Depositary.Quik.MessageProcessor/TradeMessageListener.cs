﻿using System;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class TradeMessageListener : IDisposable
    {
        private string connectionString;
        private SqlDependency sqlDependency;
        private Func<TradeModel, Guid?> processMessage;
        private ILogger<TradeListenerService> logger;

        public TradeMessageListener(string connectionString, Func<TradeModel, Guid?> processMessage, ILogger<TradeListenerService> logger)
        {
            this.connectionString = connectionString;
            this.processMessage = processMessage;
            this.logger = logger;
        }

        public void Dispose()
        {
            this.Stop();
        }

        public void Stop()
        {
            SqlDependency.Stop(connectionString);
        }

        private SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();
            return conn;
        }

        public void Start()
        {
            SqlDependency.Start(connectionString);
            this.QueryData();
        }

        private void QueryData()
        {
            using (SqlConnection con = this.GetConnection())
            {
                SqlCommand cmd = new SqlCommand($"SELECT ID,TradeNum,TradeDateTime,SecCode,Operation,Price,Value,Yield,Qty,SettleDate,FirmId,CPFirmId,ClientCode,BrokerRef,UID,ClassCode,OrderNum FROM dbo.Trades WHERE PROCESSED_ID is null", con);
                this.sqlDependency = new SqlDependency(cmd);
                this.sqlDependency.OnChange += OnMessagesChanged;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            int uid;
                            if (reader.IsDBNull(14))
                                uid = 0;
                            else
                                uid = reader.GetInt32(14);
                            this.ProcessMessage(new TradeModel(reader.GetGuid(0), reader.GetDecimal(1), reader.GetDateTime(2), reader.GetString(3), reader.GetString(4)
                                , reader.GetDecimal(5), reader.GetDecimal(6), reader.GetDecimal(7), reader.GetDecimal(8), reader.GetDateTime(9), reader.GetString(10), reader.GetString(11)
                                , reader.GetString(12), reader.GetString(13), uid, reader.GetString(15), reader.GetDecimal(16)));
                        }
                        catch (Exception e)
                        {
                            this.logger.LogError(e, "Error while processing trades: {0}", e.ToString());
                        }
                    }
                }
            }
        }

        private void OnMessagesChanged(object sender, SqlNotificationEventArgs e)
        {
            this.sqlDependency.OnChange -= OnMessagesChanged;
            this.QueryData();
        }

        protected void ProcessMessage(TradeModel message)
        {
            Guid? processedId = null;
            try
            {
                processedId = this.processMessage(message);
            }
            catch (Exception e)
            {
                this.logger.LogError(e, "Error while processing trade {0}", message.TradeNum);
            }
        }
    }
}
