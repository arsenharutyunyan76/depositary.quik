﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class TradeListenerService : ServiceLastOpDateUpdater,  IHostedService, IDisposable
    {
        private TradeMessageListener messageListener;
        private ILogger<TradeListenerService> logger;
        private string integrationConnectionString;
        private TradeRepository tradeRepository;
        private BankRepository bankRepository;
        private QuikRepository quikRepository;
        private ActionLogRepository actionLogRepository;
        private Settings settings;

        public TradeListenerService(IListenerConfiguration configuration, ILogger<TradeListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 1 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            try
            {
                UpdateServiceLastOperationDate();

                this.integrationConnectionString = configuration.IntegrationConnectionString;
                this.tradeRepository = new TradeRepository(configuration.IntegrationConnectionString);
                this.bankRepository = new BankRepository(configuration.IntegrationConnectionString);
                this.quikRepository = new QuikRepository(configuration.ConnectionString);
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);
                this.messageListener = new TradeMessageListener(configuration.ConnectionString, this.ProcessMessage, this.logger);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
                throw;
            }
        }

        /// <summary>
        /// Processes the message
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private Guid? ProcessMessage(TradeModel message)
        {
            Guid? id = null;
            if (message != null)
            {
                try
                {
                    byte auctionTypeId;
                    if (message.ClassCode == settings.AuctionTypePlacingCode)
                        auctionTypeId = 1;
                    else if (message.ClassCode == settings.AuctionTypeReturnCode)
                        auctionTypeId = 2;
                    else
                        auctionTypeId = 3;
                    id = QuikUtility.SaveTrade(new Trade()
                    {
                        TRADE_NUMBER = message.TradeNum,
                        ISIN = message.SecCode,
                        TRADE_DATE = message.TradeDateTime,
                        AUCTION_TYPE_ID = auctionTypeId,
                        OPERATION_TYPE_ID = QuikUtility.GetOperationTypeID(settings, message.Operation),
                        PRICE = message.Price,
                        VALUE = message.Value,
                        YIELD = message.Yield,
                        QUANTITY = message.Qty,
                        SETTLE_DATE = message.SettleDate,
                        BANK_CODE = QuikUtility.GetBankCode(settings, bankRepository, message.FirmId),
                        CP_BANK_CODE = QuikUtility.GetBankCode(settings, bankRepository, message.CPFirmId),
                        CLIENT_CODE = message.ClientCode,
                        BROKER_REFERENCE = QuikUtility.GetCleanBrokerReference(settings, message.BrokerRef),
                        USER_ID = message.UID,
                        ORDER_NUMBER = message.OrderNum
                    }, message.ID, tradeRepository, quikRepository, this.integrationConnectionString);
                    DBLogger.WriteSuccess(actionLogRepository, 1, message.TradeNum.ToString());
                    this.logger.LogInformation("Successfully processed trade {0}, the ID is {1}", message.TradeNum, id.Value);
                }
                catch (Exception e)
                {
                    if (!e.Message.Contains("Trade is already processed"))
                        DBLogger.WriteFail(actionLogRepository, 1, e.Message);
                    this.logger.LogError(e, "Error while processing trade {0}", message.TradeNum);
                }
            }
            else
            {
                this.logger.LogWarning("Empty message received, skipping it. The message will not be considered processed.");
            }
            return id;
        }

        public void Dispose()
        {
            this.messageListener.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
                this.messageListener.Start();
                this.logger.LogInformation("Message listener successfully started");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.messageListener.Stop();

            return Task.CompletedTask;
        }
    }
}
