﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.CommonUtility;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace IntelART.Depositary.Quik.MessageProcessor.FileUtility
{
    public class DependFileUtility : ServiceLastOpDateUpdater 
    {
        private ILogger<IBaseLogger> logger;
        private string connectionString;
        private DependDataSetRepository dependDataSetRepository;
        private Settings settings;
        private FileGenerationTimeRepository fileGenerationTimeRepository;
        private IEnumerable<DependFileInfo> dependFileInfoList;
        private bool canProcessTimerEvent = true;

        public DependFileUtility(IListenerConfiguration configuration, ILogger<IBaseLogger> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            try
            {
                this.connectionString = configuration.IntegrationConnectionString;
                this.dependDataSetRepository = new DependDataSetRepository(connectionString);
                this.fileGenerationTimeRepository = new FileGenerationTimeRepository(configuration.IntegrationConnectionString);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the service");
                throw;
            }
        }

        public void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            DateTime currDateTime = DateTime.Now;
            if (TimeSpan.Parse(settings.DependFileGenerationStartTime) <= currDateTime.TimeOfDay &&
                TimeSpan.Parse(settings.DependFileGenerationEndTime) >= currDateTime.TimeOfDay && this.canProcessTimerEvent)
            {
                try
                {
                    this.canProcessTimerEvent = false;
                    dependFileInfoList = dependDataSetRepository.GetDependFileInfo();

                    if (dependFileInfoList.Count() > 0)
                    {
                        dependFileInfoList = FillMissingReferences(dependFileInfoList);

                        FileWriter fw = new FileWriter(logger);

                        string path = fw.GetFullPath(settings.FilePath);

                        string[] lines;
                        string[] commandLines;
                        string assetsFileName = string.Format("{0}Depend_assets_{1}.txt", path, currDateTime.ToString("yyyy-MM-dd_HH.mm.ss"));
                        string commandFileName = string.Format("{0}Depend_command_{1}.txt", path, currDateTime.ToString("yyyy-MM-dd_HH.mm.ss"));

                        DateTime? fileGenerationTime = fileGenerationTimeRepository.GetFileGenerationTime(1).GENERATE_DATE;
                        if (!fileGenerationTime.HasValue || fileGenerationTime.Value.Date != currDateTime.Date)
                        {
                            lines = GetInsertFileContent();
                            commandLines = fw.GetCommandFileContent(true, assetsFileName, true);
                        }
                        else
                        {
                            lines = GetUpdateFileContent();
                            commandLines = fw.GetCommandFileContent(true, assetsFileName, false);
                        }
                        fw.ProcessFile(assetsFileName, lines);
                        fw.ProcessFile(commandFileName, commandLines);

                        foreach (DependFileInfo dependFileInfo in dependFileInfoList)
                        {
                            dependDataSetRepository.SetDependFileGenerationStatus(dependFileInfo.ATSORDER_ID, true);
                        }
                        fileGenerationTimeRepository.SetFileGenerationTime(1, currDateTime);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing Depend File Generation");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }

        private List<DependFileInfo> FillMissingReferences(IEnumerable<DependFileInfo> dependFileInfoList)
        {
            List<DependFileInfo> dependFileInfoListUpdated = new List<DependFileInfo>();

            foreach (DependFileInfo dependFileInfo in dependFileInfoList)
            {
                dependFileInfoListUpdated.Add(dependFileInfo);

                DependFileInfo existingDependFileInfo = dependFileInfoList.FirstOrDefault(x => x.ACCOUNT_REFERENCE == dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE);
                if (existingDependFileInfo == null)
                {
                    existingDependFileInfo = dependFileInfoListUpdated.FirstOrDefault(x => x.ACCOUNT_REFERENCE == dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE);
                    if (existingDependFileInfo == null)
                    {
                        DependFileInfo madeUpDependFileInfo = new DependFileInfo();
                        madeUpDependFileInfo.ISIN_CODE = GetIsinShortName(dependFileInfo.ISIN_CODE, dependFileInfo.ISIN_CCY);
                        madeUpDependFileInfo.ACCOUNT_REFERENCE = dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE;
                        madeUpDependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE = dependFileInfo.ACCOUNT_REFERENCE;
                        madeUpDependFileInfo.DEPOPART_CODE = dependFileInfo.DEPOPART_CODE;
                        madeUpDependFileInfo.ISIN_SHORT_NAME = GetIsinShortName(dependFileInfo.ISIN_SHORT_NAME, dependFileInfo.ISIN_CCY);
                        madeUpDependFileInfo.BALCHANGE_QTY = 0;
                        madeUpDependFileInfo.ISIN_CCY = dependFileInfo.ISIN_CCY;
                        madeUpDependFileInfo.MANDATE_TYPE = dependFileInfo.MANDATE_TYPE;
                        madeUpDependFileInfo.ACCOUNT_ID = dependFileInfo.ACCOUNT_ID;

                        dependFileInfoListUpdated.Add(madeUpDependFileInfo);
                    }
                }
            }

            return dependFileInfoListUpdated;
        }

        /// <summary>
        /// Returns file content for daily first time generation
        /// </summary>
        /// <returns></returns>
        private string[] GetInsertFileContent()
        {
            List<string> contentFirst = new List<string>();
            List<string> contentSecond = new List<string>();
            List<string> contentThird = new List<string>();
            List<string> content = new List<string>();

            content.Add("[assets]");
            content.Add("");

            List<string> accountCodes = new List<string>();
            foreach (DependFileInfo dependFileInfo in dependFileInfoList)
            {
                string accountCode = (dependFileInfo.ISIN_CCY != null ? dependFileInfo.ISIN_CCY.Substring(0, 1) : "") +
                  (dependFileInfo.MANDATE_TYPE == "C" ? ("0" + dependFileInfo.DEPOPART_CODE) :
                  GetAccountId(dependFileInfo.ACCOUNT_ID, dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE, dependFileInfoList));
                string bankNumber = ((dependFileInfo.ISIN_CODE == "CB:AMD" || dependFileInfo.ISIN_CODE == "CB:EUR" || dependFileInfo.ISIN_CODE == "CB:USD") ?
                    dependFileInfo.ACCOUNT_REFERENCE : dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE) + accountCode;
                string clientInn = (dependFileInfo.ISIN_CODE == "CB:AMD" || dependFileInfo.ISIN_CODE == "CB:EUR" || dependFileInfo.ISIN_CODE == "CB:USD") ?
                    dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE : dependFileInfo.ACCOUNT_REFERENCE;
                string isinShortName = dependFileInfo.ISIN_SHORT_NAME.StartsWith("CB:") ?
                       dependFileInfo.ISIN_SHORT_NAME.Substring(3, dependFileInfo.ISIN_SHORT_NAME.Length - 3) : dependFileInfo.ISIN_SHORT_NAME;
                string ballChangeQuantity = string.Format("{0:0.00}", Math.Abs(dependFileInfo.BALCHANGE_QTY));

                string contentLine;
                string accountNumber;
                if (dependFileInfo.ISIN_CODE != "CB:AMD" && dependFileInfo.ISIN_CODE != "CB:EUR" && dependFileInfo.ISIN_CODE != "CB:USD")
                {
                    accountNumber = string.Format("{0}{1}", dependFileInfo.ACCOUNT_REFERENCE, accountCode);
                    contentLine = string.Format("CDA,{0},{1},I,{2},RF1,{3},DCC",
                        accountNumber, dependFileInfo.DEPOPART_CODE, isinShortName, ballChangeQuantity);
                    contentFirst.Add(contentLine);
                }
                if (dependFileInfo.ISIN_CODE == "CB:AMD" || dependFileInfo.ISIN_CODE == "CB:EUR" || dependFileInfo.ISIN_CODE == "CB:USD")
                {
                    accountNumber = string.Format("{0}{1}", dependFileInfo.ACCOUNT_REFERENCE, accountCode);
                    contentLine = string.Format("BANK,{0},{1},M,{2},____________,{3},BNK",
                        accountNumber, dependFileInfo.DEPOPART_CODE, dependFileInfo.ISIN_CCY, ballChangeQuantity);
                    contentSecond.Add(contentLine);
                }
                if (!accountCodes.Contains(accountCode))
                {
                    accountNumber = string.Format("{0}{1}", (dependFileInfo.ISIN_CODE == "CB:AMD" || dependFileInfo.ISIN_CODE == "CB:EUR" || dependFileInfo.ISIN_CODE == "CB:USD") ?
                        dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE : dependFileInfo.ACCOUNT_REFERENCE, accountCode);
                    contentLine = string.Format("{0},{1},CDA,{2},BANK,{3},{4},DCC,BNK,Y,Y,INN{5},INN{6},_,_,A,_,G",
                        dependFileInfo.DEPOPART_CODE, accountCode, accountNumber, bankNumber, dependFileInfo.ISIN_CCY, dependFileInfo.DEPOPART_CODE, clientInn);
                    contentThird.Add(contentLine);
                    accountCodes.Add(accountCode);
                }
            }

            content.AddRange(contentFirst);
            content.Add("");
            content.Add("");
            content.AddRange(contentSecond);
            content.Add("");
            content.Add("");
            content.Add("[pairs]");
            content.Add("");
            content.AddRange(contentThird);

            return content.ToArray();
        }

        /// <summary>
        ///  Returns file content for daily generation after first generation
        /// </summary>
        /// <returns></returns>
        private string[] GetUpdateFileContent()
        {
            List<string> contentFirst = new List<string>();
            List<string> content = new List<string>();

            content.Add("[assets]");
            content.Add("");

            foreach (DependFileInfo dependFileInfo in dependFileInfoList)
            {
                string accountCode = (dependFileInfo.ISIN_CCY != null ? dependFileInfo.ISIN_CCY.Substring(0, 1) : "") +
             (dependFileInfo.MANDATE_TYPE == "C" ? ("0" + dependFileInfo.DEPOPART_CODE) :
             GetAccountId(dependFileInfo.ACCOUNT_ID, dependFileInfo.ATSORDER_LINKED_ACCOUNT_REFERENCE, dependFileInfoList));
                string isinShortName = dependFileInfo.ISIN_SHORT_NAME.StartsWith("CB:") ?
                       dependFileInfo.ISIN_SHORT_NAME.Substring(3, dependFileInfo.ISIN_SHORT_NAME.Length - 3) : dependFileInfo.ISIN_SHORT_NAME;
                string ballChangeQuantity = string.Format("{0:0.00}", Math.Abs(dependFileInfo.BALCHANGE_QTY));

                string contentLine;
                string accountNumber;
                if (dependFileInfo.ISIN_CODE != "CB:AMD" && dependFileInfo.ISIN_CODE != "CB:EUR" && dependFileInfo.ISIN_CODE != "CB:USD")
                {
                    accountNumber = string.Format("{0}{1}", dependFileInfo.ACCOUNT_REFERENCE, accountCode);
                    contentLine = string.Format("CDA,{0},{1},I,{2},RF1,{3},DCC",
                        accountNumber, dependFileInfo.DEPOPART_CODE, isinShortName, ballChangeQuantity);
                    contentFirst.Add(contentLine);
                }
            }

            content.AddRange(contentFirst);
            return content.ToArray();
        }

        private string GetIsinShortName(string value, string ccy)
        {
            if (value != "CB:AMD" || value != "CB:EUR" || value != "CB:USD")
            { return "CB:" + ccy; }
            else
            {
                switch (value)
                {
                    case "CB:AMD": return IsinShortNames.AMD;
                    case "CB:EUR": return IsinShortNames.EUR;
                    case "CB:USD": return IsinShortNames.USD;
                    default: return "";
                }
            }
        }

        private string GetAccountId(int accountId, string linekdReference, IEnumerable<DependFileInfo> dependFileInfoList)
        {
            if (accountId != 0)
            { return accountId.ToString(); }
            else
            {
                DependFileInfo dependFileInfo = dependFileInfoList.FirstOrDefault(x => x.ACCOUNT_REFERENCE == linekdReference);
                if (dependFileInfo != null)
                { return dependFileInfo.ACCOUNT_ID.ToString(); }
                return "";
            }
        }
    }

    public static class IsinShortNames
    {
        public const string AMD = "ACBAB2";
        public const string USD = "ACBAB1";
        public const string EUR = "SWISB5";
    }
}
