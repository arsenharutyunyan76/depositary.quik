﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;

namespace IntelART.Depositary.Quik.MessageProcessor.FileUtility
{
    public class QuikFileUtility : ServiceLastOpDateUpdater
    {
        private ILogger<IBaseLogger> logger;
        private AuctionRepository auctionRepository;
        private Settings settings;
        private FileGenerationTimeRepository fileGenerationTimeRepository;
        private QuikOperationRepository quikOperationRepository;
        private bool canProcessTimerEvent = true;

        public QuikFileUtility(IListenerConfiguration configuration, ILogger<IBaseLogger> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            try
            {
                this.auctionRepository = new AuctionRepository(configuration.IntegrationConnectionString);
                this.fileGenerationTimeRepository = new FileGenerationTimeRepository(configuration.IntegrationConnectionString);
                this.quikOperationRepository = new QuikOperationRepository(configuration.OperationConnectionString);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the service");
                throw;
            }
        }

        public void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            DateTime currDateTime = DateTime.Now;
            if (TimeSpan.Parse(settings.QuikFileGenerationStartTime) <= currDateTime.TimeOfDay &&
                TimeSpan.Parse(settings.QuikFileGenerationEndTime) >= currDateTime.TimeOfDay && this.canProcessTimerEvent)
            {
                try
                {
                    this.canProcessTimerEvent = false;
                    DateTime? fileGenerationTime = fileGenerationTimeRepository.GetFileGenerationTime(4).GENERATE_DATE;
                    if (!fileGenerationTime.HasValue || fileGenerationTime.Value.Date != currDateTime.Date)
                    {
                        IEnumerable<Auction> auctions = auctionRepository.GetAuctions(currDateTime, currDateTime).Result.Where(item => !item.IS_CANCELLED);
                        if (auctions != null && auctions.Count() > 0)
                        {
                            IEnumerable<ClientFirm> clientFirms = quikOperationRepository.GetClientFirms();
                            if (clientFirms != null && clientFirms.Count() > 0)
                            {
                                StringBuilder builder = new StringBuilder();

                                foreach (ClientFirm clientFirm in clientFirms)
                                {
                                    builder.AppendFormat("MONEY:  FIRM_ID = {0}; TAG = EQTV; CURR_CODE = AMD; CLIENT_CODE = {1}; LIMIT_KIND = 365; OPEN_BALANCE = 0; OPEN_LIMIT = 0; LEVERAGE = -1;", clientFirm.FIRM_ID.Trim(), clientFirm.CLIENT_CODE.Trim());
                                    builder.AppendLine();
                                }
                                builder.AppendLine();

                                foreach (Auction auction in auctions)
                                {
                                    foreach (ClientFirm clientFirm in clientFirms)
                                    {
                                        builder.AppendFormat("DEPO:  FIRM_ID = {0}; SECCODE = {1}; CLIENT_CODE = {2}; LIMIT_KIND = 365; OPEN_BALANCE = 0; OPEN_LIMIT = 0; TRDACCID = TRDACC;", clientFirm.FIRM_ID.Trim(), auction.ISIN.Trim(), clientFirm.CLIENT_CODE.Trim());
                                        builder.AppendLine();
                                    }
                                }
                                builder.AppendLine();

                                string fileName = string.Format("{0}\\ImportLimits.lim", settings.QuikFilePath);
                                File.WriteAllText(fileName, builder.ToString());
                            }
                        }
                        fileGenerationTimeRepository.SetFileGenerationTime(4, currDateTime);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing Depend File Generation");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }
    }
}
