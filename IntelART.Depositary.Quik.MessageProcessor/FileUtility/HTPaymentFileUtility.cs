﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.MessageProcessor.CommonUtility;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace IntelART.Depositary.Quik.MessageProcessor.FileUtility
{
    public class HTPaymentFileUtility : ServiceLastOpDateUpdater, IBaseLogger
    {
        private ILogger<IBaseLogger> logger;
        private PaymentRepository paymentRepository;
        private Settings settings;
        private BankRepository bankRepository;
        private AccountRepository accountRepository;
        private IsinRepository isinRepository;
        private FileGenerationTimeRepository fileGenerationTimeRepository;
        private bool canProcessTimerEvent = true;

        public HTPaymentFileUtility(IListenerConfiguration configuration, ILogger<IBaseLogger> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            try
            {
                this.paymentRepository = new PaymentRepository(configuration.IntegrationConnectionString);
                this.bankRepository = new BankRepository(configuration.IntegrationConnectionString);
                this.accountRepository = new AccountRepository(configuration.IntegrationConnectionString);
                this.isinRepository = new IsinRepository(configuration.IntegrationConnectionString);
                this.fileGenerationTimeRepository = new FileGenerationTimeRepository(configuration.IntegrationConnectionString);
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the service");
                throw;
            }
        }

        public void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            DateTime currDateTime = DateTime.Now;
            if (TimeSpan.Parse(settings.RTSFileGenerationFromBankMailStartTime) <= currDateTime.TimeOfDay &&
                TimeSpan.Parse(settings.RTSFileGenerationFromBankMailEndTime) >= currDateTime.TimeOfDay && this.canProcessTimerEvent)
            {
                try
                {
                    this.canProcessTimerEvent = false;
                    IEnumerable<PaymentMessage> messages;
                    List<string> lineList1 = new List<string>();
                    List<string> lineList2 = new List<string>();
                    List<string> lineList3 = new List<string>();

                    List<string> list_SECURITY_ACCOUNT = new List<string>();

                    lineList1.Add("[assets]");

                    messages = paymentRepository.GetPaymentMessage(currDateTime, currDateTime);

                    foreach (PaymentMessage message in messages)
                    {
                        Bank bank;
                        Account account;
                        switch (message.MESSAGE_TYPE)
                        {
                            case "522":
                                account = accountRepository.GetAccountBank(message.PAYER);
                                bank = bankRepository.GetBank(account.BANK_CODE);
                                lineList2.Add(string.Format("CDV,{0},{1},I,{2},RF1,{3},DCV",
                                                            account.SECURITY_ACCOUNT, 
                                                            bank.SHORT_NAME, 
                                                            isinRepository.GetRTScodeByIsin(message.SECURITY_ID), 
                                                            message.AMOUNT / 100));
                                list_SECURITY_ACCOUNT.Add(account.SECURITY_ACCOUNT);
                                break;

                            case "100":
                            case "202":
                                account = accountRepository.GetAccountBank(message.PAYER);
                                bank = bankRepository.GetBank(account.BANK_CODE);
                                if (message.CURRENCY == "AMD")
                                {
                                    lineList1.Add(string.Format("BANV,{0},{1},M,{2},____________,{3},BNV",
                                                                account.SECURITY_ACCOUNT,
                                                                bank.SHORT_NAME,
                                                                message.CURRENCY,
                                                                message.AMOUNT));
                                    list_SECURITY_ACCOUNT.Add(account.SECURITY_ACCOUNT);
                                }
                                else
                                {
                                    lineList2.Add(string.Format("CDV,{0},{1},I,{2},RF1,{3},DCV",
                                                                account.SECURITY_ACCOUNT,
                                                                bank.SHORT_NAME,
                                                                message.CURRENCY,
                                                                message.AMOUNT));
                                    list_SECURITY_ACCOUNT.Add(account.SECURITY_ACCOUNT);
                                };
                                break;
                        }
                    }

                    FileWriter fw = new FileWriter(this.logger);
                    string path = fw.GetFullPath(settings.FilePath);
                    string assetsFileName = string.Format("{0}BankMail_asset_{1}.txt", path, currDateTime.ToString("yyyy-MM-dd_HH.mm.ss"));
                    string commandFileName = string.Format("{0}BankMail_command_{1}", path, currDateTime.ToString("yyyy-MM-dd_HH.mm.ss"));
                    string[] commandLines;

                    DateTime? dt = fileGenerationTimeRepository.GetFileGenerationTime(2).GENERATE_DATE;
                    if (!dt.HasValue || dt.Value.Date != currDateTime.Date)
                    {
                        lineList3.Add("[pairs]");

                        IEnumerable<AccountPairs> accountPairs = accountRepository.GetAccountPairs();
                        foreach (AccountPairs accountPair in accountPairs)
                        {
                            lineList3.Add(string.Format("{0},{1},CDV,{2},BANV,{3},AMD,DCV,BNV,Y,Y,INNfirm,INNclient,_,_,A,_,G",
                                                       accountPair.BANK_SHORT_NAME,
                                                       accountPair.ACCOUNT_CUSTOMER_CODE,
                                                       accountPair.ACCOUNT_CODE_2,
                                                       accountPair.ACCOUNT_CODE_1));

                            if(list_SECURITY_ACCOUNT.Contains(accountPair.ACCOUNT_CODE_2))
                            {
                                lineList2.Add(string.Format("CDV,{0},{1},I,{2},RF1,{3},DCV",
                                                            accountPair.ACCOUNT_CODE_2,
                                                            accountPair.BANK_SHORT_NAME,
                                                            settings.HTPaymentFileDefaultCDVcurrency,
                                                            0));
                            }

                            if (list_SECURITY_ACCOUNT.Contains(accountPair.ACCOUNT_CODE_1))
                            {
                                lineList1.Add(string.Format("BANV,{0},{1},M,{2},____________,{3},BNV",
                                                            accountPair.ACCOUNT_CODE_1,
                                                            accountPair.BANK_SHORT_NAME,
                                                            settings.HTPaymentFileDefaultBANVcurrency,
                                                            0));
                            }
                        }

                        commandLines = fw.GetCommandFileContent(false, assetsFileName, true);
                    }
                    else
                    {
                        if (messages.Count() == 0)
                            return;
                        commandLines = fw.GetCommandFileContent(false, assetsFileName, false);
                    }

                    lineList1.AddRange(lineList2);
                    lineList1.AddRange(lineList3);
                    fw.ProcessFile(assetsFileName, lineList1.ToArray());
                    fw.ProcessFile(commandFileName, commandLines);
                    paymentRepository.SetProcessedPaymentMessage(messages);

                    if (!dt.HasValue || dt.Value.Date != currDateTime.Date)
                    {
                        fileGenerationTimeRepository.SetFileGenerationTime(2, currDateTime);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing file generation for HT522 / HT100 / HT202");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }
        }
    }
}