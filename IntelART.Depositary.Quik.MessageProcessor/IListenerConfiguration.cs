﻿using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public interface IListenerConfiguration
    {
        string ConnectionString { get; }
        string IntegrationConnectionString { get; }
        string BankmailConnectionString { get; }
        string BankmailPaymentConnectionString { get; }
        string RTSConnectionString { get; }
        string RTSGatewayConnectionString { get; }
        string OperationConnectionString { get; }
        Settings AppSettings { get; set; }
    }
}
