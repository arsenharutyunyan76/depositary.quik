﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class HT575GenerationMessageListener
    {
        private string connectionString;
        private int waitSeconds;
        private SqlDependency sqlDependency;
        private Func<List<Trade>, Guid?> processMessage;
        private ILogger<HT575GenerationListenerService> logger;

        public HT575GenerationMessageListener(string connectionString, int waitSeconds, Func<List<Trade>, Guid?> processMessage, ILogger<HT575GenerationListenerService> logger)
        {
            this.connectionString = connectionString;
            this.waitSeconds = waitSeconds;
            this.processMessage = processMessage;
            this.logger = logger;
        }

        public void Dispose()
        {
            this.Stop();
        }

        public void Stop()
        {
            SqlDependency.Stop(connectionString);
        }

        private SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();
            return conn;
        }

        public void Start()
        {
            SqlDependency.Start(connectionString);
            this.QueryData();
        }

        private void QueryData()
        {
            Dictionary<string, List<Trade>> messages = new Dictionary<string, List<Trade>>();
            using (SqlConnection con = this.GetConnection())
            {
                SqlCommand cmd = new SqlCommand($"SELECT ID,TRADE_NUMBER,TRADE_DATE,ISIN,OPERATION_TYPE_ID,VALUE,YIELD,QUANTITY,SETTLE_DATE,BANK_CODE,CP_BANK_CODE,CLIENT_CODE,AUCTION_TYPE_ID FROM dbo.TRADE WHERE PROCESS_STATUS_ID=1 and DATEDIFF(second,CREATION_DATE,getdate())>{waitSeconds}", con);
                this.sqlDependency = new SqlDependency(cmd);
                this.sqlDependency.OnChange += OnMessagesChanged;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string isin = reader.GetString(3);
                        DateTime tradeDate = reader.GetDateTime(2);
                        byte auctionTypeId = reader.GetByte(12);
                        string key = string.Format("{0}{1}{2}", tradeDate.ToString("yyyyMMdd"), isin, auctionTypeId);
                        if (!messages.ContainsKey(key))
                            messages.Add(key, new List<Trade>());
                        messages[key].Add(new Trade()
                        {
                            ID = reader.GetGuid(0),
                            TRADE_NUMBER = reader.GetDecimal(1),
                            TRADE_DATE = tradeDate,
                            ISIN = isin,
                            OPERATION_TYPE_ID = reader.GetByte(4),
                            VALUE = reader.GetDecimal(5),
                            YIELD = reader.GetDecimal(6),
                            QUANTITY = reader.GetDecimal(7),
                            SETTLE_DATE = reader.GetDateTime(8),
                            BANK_CODE = reader.GetString(9),
                            CP_BANK_CODE = reader.GetString(10),
                            CLIENT_CODE = reader.GetString(11),
                            AUCTION_TYPE_ID = auctionTypeId
                        });
                    }
                }
            }

            if (messages.Count > 0)
            {
                foreach (string key in messages.Keys)
                {
                    this.ProcessMessage(messages[key]);
                }
            }
        }

        private void OnMessagesChanged(object sender, SqlNotificationEventArgs e)
        {
            this.sqlDependency.OnChange -= OnMessagesChanged;
            this.QueryData();
        }

        protected void ProcessMessage(List<Trade> messages)
        {
            Guid? processedId = null;
            try
            {
                processedId = this.processMessage(messages);
            }
            catch (Exception e)
            {
                this.logger.LogError(e, "Error while creating HT575");
            }
        }
    }
}
