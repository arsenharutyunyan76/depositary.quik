﻿using System;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using IntelART.Depositary.Quik.Entities;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class OrderMessageListener : IDisposable
    {
        private string connectionString;
        private SqlDependency sqlDependency;
        private Func<OrderModel, Guid?> processMessage;
        private ILogger<OrderListenerService> logger;
        private Settings settings;

        public OrderMessageListener(string connectionString, Func<OrderModel, Guid?> processMessage, ILogger<OrderListenerService> logger, Settings settings)
        {
            this.connectionString = connectionString;
            this.processMessage = processMessage;
            this.logger = logger;
            this.settings = settings;
        }

        public void Dispose()
        {
            this.Stop();
        }

        public void Stop()
        {
            SqlDependency.Stop(connectionString);
        }

        private SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();
            return conn;
        }

        public void Start()
        {
            SqlDependency.Start(connectionString);
            this.QueryData();
        }

        private void QueryData()
        {
            using (SqlConnection con = this.GetConnection())
            {
                SqlCommand cmd = new SqlCommand($"SELECT ID,OrderNum,OrderDateTime,SecCode,Operation,Price,Value,Yield,Qty,Balance,WithdrawDateTime,FirmId,ClientCode,BrokerRef,UID,ClassCode,State FROM dbo.Orders WHERE PROCESSED_ID is null and State in (N'" + settings.OrderApprovedName + "',N'" + settings.OrderRejectedName + "')", con);
                this.sqlDependency = new SqlDependency(cmd);
                this.sqlDependency.OnChange += OnMessagesChanged;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            int uid;
                            if (reader.IsDBNull(14))
                                uid = 0;
                            else
                                uid = reader.GetInt32(14);

                            DateTime? withdrawDateTime;
                            if (reader.IsDBNull(10))
                                withdrawDateTime = null;
                            else
                                withdrawDateTime = reader.GetDateTime(10);

                            this.ProcessMessage(new OrderModel(reader.GetGuid(0), reader.GetDecimal(1), reader.GetDateTime(2), reader.GetString(3), reader.GetString(4)
                                , reader.GetDecimal(5), reader.GetDecimal(6), reader.GetDecimal(7), reader.GetDecimal(8), reader.GetDecimal(9), withdrawDateTime, reader.GetString(11)
                                , reader.GetString(12), reader.GetString(13), uid, reader.GetString(15), reader.GetString(16)));
                        }
                        catch (Exception e)
                        {
                            this.logger.LogError(e, "Error while processing orders: {0}", e.ToString());
                        }
                    }
                }
            }
        }

        private void OnMessagesChanged(object sender, SqlNotificationEventArgs e)
        {
            this.sqlDependency.OnChange -= OnMessagesChanged;
            this.QueryData();
        }

        protected void ProcessMessage(OrderModel message)
        {
            Guid? processedId = null;
            try
            {
                processedId = this.processMessage(message);
            }
            catch (Exception e)
            {
                this.logger.LogError(e, "Error while processing order {0}", message.OrderNum);
            }
        }
    }
}
