﻿using IntelART.Depositary.Quik.Repository;
using System;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public abstract class ServiceLastOpDateUpdater
    {
        private readonly ActionTypeRepository actionTypeRepository;
        private readonly byte[] ids;
        public ServiceLastOpDateUpdater(string connectionString, byte[] ids)
        {
            this.actionTypeRepository = new ActionTypeRepository(connectionString);
            this.ids = ids ?? new byte[] { };
        }

        protected void UpdateServiceLastOperationDate()
        {
            try
            {
                foreach (var id in this.ids)
                {
                    this.actionTypeRepository.UpdateServiceLastOperationDate(id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
