﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.Hosting;
using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    public class RTSPaymentOutMessageListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<RTSPaymentOutMessageListenerService> logger;
        private RTSOutMessageRepository rtsOutMessageRepository;
        private PaymentOutRepository paymentOutRepository;
        private StockAccountRepository stockAccountRepository;
        private ActionLogRepository actionLogRepository;
        private CurrencyRepository currencyRepository;
        private List<Currency> listCurrency;
        private Settings settings;
        private System.Timers.Timer serviceTimer;
        private bool canProcessTimerEvent = true;
        private bool canProcessTimerEventCalculation = true;
        private RTSOutDataRepository rtsOutDataRepository;

        public RTSPaymentOutMessageListenerService(IListenerConfiguration configuration, ILogger<RTSPaymentOutMessageListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] { 17,18 })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.RTSDataProcessingInterval == 0)
            {
                return;
            }

            try
            {
                UpdateServiceLastOperationDate();

                this.rtsOutDataRepository = new RTSOutDataRepository(configuration.RTSGatewayConnectionString);
                this.rtsOutMessageRepository = new RTSOutMessageRepository(configuration.RTSConnectionString);
                this.paymentOutRepository = new PaymentOutRepository(configuration.IntegrationConnectionString);            
                this.stockAccountRepository = new StockAccountRepository(configuration.IntegrationConnectionString);            
                this.actionLogRepository = new ActionLogRepository(configuration.IntegrationConnectionString);
                this.currencyRepository = new CurrencyRepository(configuration.IntegrationConnectionString);
                this.listCurrency = currencyRepository.GetCurrencies().Result.ToList();

                serviceTimer = new System.Timers.Timer(settings.RTSDataProcessingInterval);
                serviceTimer.Elapsed += new ElapsedEventHandler(ProcessMessagePeriodical);
                serviceTimer.Enabled = true;
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
            }
        }

        private void ProcessMessagePeriodical(object state, ElapsedEventArgs args)
        {
            UpdateServiceLastOperationDate();

            IEnumerable<RTSOutMessage> messages;

            DateTime currDateTime = DateTime.Now;
            if (TimeSpan.Parse(settings.RTSDataProcessingStartTime) <= currDateTime.TimeOfDay &&
               TimeSpan.Parse(settings.RTSDataProcessingEndTime) >= currDateTime.TimeOfDay && this.canProcessTimerEvent)
            {
                try
                {
                    this.canProcessTimerEvent = false;
                    messages = rtsOutMessageRepository.GetRTSOutMessage("BANV,CDV");
                    foreach (RTSOutMessage message in messages)
                    {
                        if (message.OPCODE == "o")
                        {
                            this.ProcessMessageUndeposition(message);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing HT522 / HT202 Undeposition");
                }
                finally
                {
                    this.canProcessTimerEvent = true;
                }
            }

            if (TimeSpan.Parse(settings.RTSDataProcessingStartTime) <= currDateTime.TimeOfDay &&
               TimeSpan.Parse(settings.RTSDataProcessingEndTime) >= currDateTime.TimeOfDay && this.canProcessTimerEventCalculation)
            {
                try
                {
                    this.canProcessTimerEventCalculation = false;
                    messages = rtsOutMessageRepository.GetRTSOutMessageFinalCalculation("BANV,CDV");
                    foreach (RTSOutMessage message in messages)
                    {
                        if (message.FREE != 0)
                        {
                            this.ProcessMessageFinalCalculation(message);
                        }
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogError(e, "Error while processing HT522 / HT202 Final Calculation");
                }
                finally
                {
                    this.canProcessTimerEventCalculation = true;
                }
            }
        }

        /// <summary>
        /// Processes the message  (Undeposition)
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private bool ProcessMessageUndeposition(RTSOutMessage message) 
        {
            bool result = false;
            int number;
            string documentNumber;
            string MessageID;
            try
            {
                PaymentOutMessage paymentOutMessage;

                if (message.ACTIVE_TYPE == "I" && this.listCurrency.Where(x => (x.CODE == message.NAME) && (x.CURRENCY_TYPE !=2) ) != null )
                {
                    number = -1;
                    documentNumber = null;
                    MessageID = null;
                    paymentOutMessage = new PaymentOutMessage
                    {
                        ID = message.ID,
                        MESSAGE_TYPE = "522",
                        SECURITY_ID = message.NAME,
                        INITIAL_AMOUNT = 0,
                        AMOUNT = message.FREE*100,
                        PAYER = stockAccountRepository.GetStockAccountByCurrency(message.NAME),
                        RECEIVER = message.ACCT_NUM,
                        RECEIVER_NAME=message.FIRM_NAME,
                        IS_FINAL_CALCULATION = false,
                        OPCODE = message.OPCODE,
                        CODE = message.CODE,
                        MOMENT = message.MOMENT
                    };
                    paymentOutRepository.SavePaymentOutMessage(paymentOutMessage, ref number, ref documentNumber, ref MessageID);
                    rtsOutDataRepository.ProcessASSET_IO(MessageID);
                    result = true;
                    DBLogger.WriteSuccess(actionLogRepository, 17, paymentOutMessage.ID);
                    this.logger.LogInformation("Successfully processed HT{0}, the ID is {1}. (Undeposition)", paymentOutMessage.MESSAGE_TYPE, paymentOutMessage.ID);
                }

                if ((message.ACTIVE_TYPE == "I" && this.listCurrency.Where(x => (x.CODE == message.NAME) && (x.CURRENCY_TYPE == 2)) != null || message.ACTIVE_TYPE == "M"))
                {
                    number = -1;
                    documentNumber = null;
                    MessageID = null;
                    paymentOutMessage = new PaymentOutMessage
                    {
                        ID = message.ID,
                        MESSAGE_TYPE = "202",
                        CURRENCY = message.NAME,
                        INITIAL_AMOUNT = 0,
                        AMOUNT = message.FREE,
                        PAYER = stockAccountRepository.GetStockAccountByCurrency(message.NAME),
                        RECEIVER = message.ACCT_NUM,
                        RECEIVER_NAME=message.FIRM_NAME,
                        IS_FINAL_CALCULATION = false,
                        OPCODE = message.OPCODE,
                        CODE = message.CODE,
                        MOMENT = message.MOMENT
                    };
                    paymentOutRepository.SavePaymentOutMessage(paymentOutMessage, ref number, ref documentNumber, ref MessageID);
                    rtsOutDataRepository.ProcessASSET_IO(MessageID);
                    result = true;
                    DBLogger.WriteSuccess(actionLogRepository, 17, paymentOutMessage.ID);
                    this.logger.LogInformation("Successfully processed HT{0}, the ID is {1}. (Undeposition)", paymentOutMessage.MESSAGE_TYPE, paymentOutMessage.ID);
                }
            }
            catch (Exception e)
            {
                DBLogger.WriteFail(actionLogRepository, 17, e.Message);
                this.logger.LogError(e, "Error while processing HT{0} with ID {1}. (Undeposition)", message.CODE, message.ID);
            }
            return result;
        }


        /// <summary>
        /// Processes the message (Final Calculation)
        /// </summary>
        /// <param name="message">Message to process</param>
        /// <returns>true if the message was successfully processed, false otherwise</returns>
        private bool ProcessMessageFinalCalculation(RTSOutMessage message) 
        {
            bool result = false;
            int number;
            string documentNumber;
            string MessageID;
            try
            {
                PaymentOutMessage paymentOutMessage;

                if (message.ACTIVE_TYPE == "I" && this.listCurrency.Where(x => (x.CODE == message.NAME) && (x.CURRENCY_TYPE != 2)) != null)
                {
                    number = -1;
                    documentNumber = null;
                    MessageID = null;
                    paymentOutMessage = new PaymentOutMessage
                    {
                        ID = message.ID,
                        MESSAGE_TYPE = "522",
                        SECURITY_ID = message.NAME,
                        INITIAL_AMOUNT = message.INITIAL_AMOUNT * 100,
                        AMOUNT = message.FREE*100,
                        PAYER = stockAccountRepository.GetStockAccountByCurrency(message.NAME),
                        RECEIVER = message.ACCT_NUM,
                        RECEIVER_NAME=message.FIRM_NAME,
                        IS_FINAL_CALCULATION = true,
                        CODE = message.CODE
                    };
                    paymentOutRepository.SavePaymentOutMessage(paymentOutMessage, ref number, ref documentNumber, ref MessageID);
                    rtsOutDataRepository.ProcessASSET_IO(MessageID);
                    result = true;
                    DBLogger.WriteSuccess(actionLogRepository, 18, paymentOutMessage.ID);
                    this.logger.LogInformation("Successfully processed HT{0}, the ID is {1}. (Final Calculation)", paymentOutMessage.MESSAGE_TYPE, paymentOutMessage.ID);
                }

                if ((message.ACTIVE_TYPE == "I" && this.listCurrency.Where(x => (x.CODE == message.NAME) && (x.CURRENCY_TYPE == 2)) != null || message.ACTIVE_TYPE == "M"))
                {
                    number = -1;
                    documentNumber = null;
                    MessageID = null;
                    paymentOutMessage = new PaymentOutMessage
                    {
                        ID = message.ID,
                        MESSAGE_TYPE = "202",
                        CURRENCY = message.NAME,
                        INITIAL_AMOUNT = message.INITIAL_AMOUNT * 100,
                        AMOUNT = message.FREE,
                        PAYER = stockAccountRepository.GetStockAccountByCurrency(message.NAME),
                        RECEIVER = message.ACCT_NUM,
                        RECEIVER_NAME=message.FIRM_NAME,
                        IS_FINAL_CALCULATION = true,
                        CODE = message.CODE
                    };
                    paymentOutRepository.SavePaymentOutMessage(paymentOutMessage, ref number, ref documentNumber, ref MessageID);
                    rtsOutDataRepository.ProcessASSET_IO(MessageID);
                    result = true;
                    DBLogger.WriteSuccess(actionLogRepository, 18, paymentOutMessage.ID);
                    this.logger.LogInformation("Successfully processed HT{0}, the ID is {1}. (Final Calculation)", paymentOutMessage.MESSAGE_TYPE, paymentOutMessage.ID);
                }
            }
            catch (Exception e)
            {
                DBLogger.WriteFail(actionLogRepository, 18, e.Message);
                this.logger.LogError(e, "Error while processing HT{0} with ID {1}. (Final Calculation)", message.CODE, message.ID);
            }
            return result;
        }

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
            }
            return Task.CompletedTask;
        }
    }
}
