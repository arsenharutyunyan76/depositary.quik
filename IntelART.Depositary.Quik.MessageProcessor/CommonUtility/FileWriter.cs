﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace IntelART.Depositary.Quik.MessageProcessor.CommonUtility
{
    public class FileWriter
    {
        ILogger<IBaseLogger> logger;
        public FileWriter(ILogger<IBaseLogger> logger)
        {
            this.logger = logger;
        }
        public void ProcessFile(string fileName, string[] values)
        {
            try
            {
                if (!File.Exists(fileName))
                {
                    using (FileStream fs = File.Create(fileName))
                    {
                        logger.LogInformation(string.Format("The file {0} has been created successfully", fileName));
                    }
                }
                WriteToFile(fileName, values);
            }
            catch (Exception e)
            {
                logger.LogError(string.Format("The file processing has failed: {0}", e.Message));
            }
        }

        private void WriteToFile(string fullPath, string[] values)
        {
            try
            {
                using (StreamWriter file = new StreamWriter(fullPath))
                {
                    foreach (string val in values)
                    {
                        file.WriteLine(val);
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError(string.Format("Writing to file has failed: {0}", e.Message));
                throw;
            }
        }

        /// <summary>
        /// Returns the full path where the file is going to be generated
        /// </summary>
        /// <returns></returns>
        public string GetFullPath(string basePath)
        {
            StringBuilder sb = new StringBuilder(basePath);
            sb.Append(DateTime.Now.Year);
            sb.Append("\\");
            sb.Append(string.Format("{0}.{1}", DateTime.Now.Year, DateTime.Now.Month.ToString().PadLeft(2, '0')));
            sb.Append("\\");
            sb.Append(string.Format("{0}.{1}.{2}", DateTime.Now.Year, DateTime.Now.Month.ToString().PadLeft(2, '0'), DateTime.Now.Day.ToString().PadLeft(2, '0')));
            sb.Append("\\");
            string path = sb.ToString();

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        /// <summary>
        /// Returns command file content based on generation sequence
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <param name="isFirstDailyGeneration"></param>
        /// <returns></returns>
        public string[] GetCommandFileContent(bool isDepend, string fileName, bool isFirstDailyGeneration)
        {
            List<string> content = new List<string>();
            string bn = isDepend ? " BNK" : " BNV";
            string dc = isDepend ? " DCC" : " DCV";

            if (isFirstDailyGeneration)
            {
                content.Add(string.Format("set_flag M{0} 2", bn));
                content.Add(string.Format("set_flag I{0} 2", dc));
                content.Add(string.Format("send_asset {0}{1}", fileName, bn));
                content.Add(string.Format("send_asset {0}{1}", fileName, dc));
                content.Add(string.Format("send_settl_pair {0}{1}{2}", fileName, dc, bn));
                content.Add(string.Format("set_flag M{0} 1", bn));
                content.Add(string.Format("set_flag I{0} 1", dc));
            }
            else
            {
                content.Add(string.Format("input_asset {0}", fileName));
                content.Add(string.Format("add_settl_pair {0}", fileName));
            }
            content.Add("exit");

            return content.ToArray();
        }
    }
}
