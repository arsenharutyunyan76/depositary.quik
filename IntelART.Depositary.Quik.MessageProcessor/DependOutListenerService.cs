﻿using IntelART.Depositary.Quik.Entities;
using IntelART.Depositary.Quik.Repository;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IntelART.Depositary.Quik.MessageProcessor
{
    /// <summary>
    /// The service is responsible for getting data from RTS and storing in local database
    /// </summary>
    public class DependOutListenerService : ServiceLastOpDateUpdater, IHostedService, IDisposable, IBaseLogger
    {
        private ILogger<DependOutListenerService> logger;
        private Settings settings;
        private System.Timers.Timer serviceTimer;

        private DependDataSetRepository dependDataSetRepository;
        private RTSOutMessageRepository rtsOutMessageRepository;

        public DependOutListenerService(IListenerConfiguration configuration, ILogger<DependOutListenerService> logger)
            : base(configuration.IntegrationConnectionString, new byte[] {  })
        {
            this.settings = configuration.AppSettings;
            this.logger = logger;
            if (settings.RTSDataProcessingInterval == 0)
            {
                return;
            }

            UpdateServiceLastOperationDate();

            this.dependDataSetRepository = new DependDataSetRepository(configuration.IntegrationConnectionString);
            this.rtsOutMessageRepository = new RTSOutMessageRepository(configuration.RTSConnectionString);

            UndepositionDataFetch();
            UndepositionFinalCalculationDataFetch();

            serviceTimer = new System.Timers.Timer(settings.RTSDataProcessingInterval);
        }

        private void UndepositionDataFetch()
        {
            DateTime currDateTime = DateTime.Now;
            if (TimeSpan.Parse(settings.RTSDataProcessingStartTime) <= currDateTime.TimeOfDay &&
               TimeSpan.Parse(settings.RTSDataProcessingEndTime) >= currDateTime.TimeOfDay)
            {
                try
                {
                    IEnumerable<DependUndeposition> dependUndepositionList = this.rtsOutMessageRepository.GetDependUndepositionInfo("BANK,CDA");

                    foreach (DependUndeposition dependUndeposition in dependUndepositionList)
                    {
                        string initialAccountRef = dependUndeposition.ACCOUNT_REFERENCE;
                        string sttlPair = dependUndeposition.SETTL_PAIR;
                        string accountRef = initialAccountRef.Substring(0, initialAccountRef.IndexOf(sttlPair));
                        dependUndeposition.ACCOUNT_REFERENCE = accountRef;

                        string initialLinkedAccountRef = dependUndeposition.LINKED_ACCOUNT_REFERENCE;
                        string linkedAccountRef = initialLinkedAccountRef.Substring(0, initialLinkedAccountRef.IndexOf(sttlPair));
                        dependUndeposition.LINKED_ACCOUNT_REFERENCE = linkedAccountRef;
                        dependUndeposition.FINAL_CALCULATION = false;
                        dependUndeposition.ACCOUNT_CODE = dependUndeposition.SETTL_PAIR;

                        dependDataSetRepository.SaveDependUndeposition(dependUndeposition, false);
                    }
                }
                catch (Exception e)
                {
                    this.logger.LogCritical(e, "Exception happened when creating the listener service");
                }
            }
        }

        private void UndepositionFinalCalculationDataFetch()
        {
            try
            {
                IEnumerable<DependUndeposition> dependUndepositionList = this.rtsOutMessageRepository.GetDependUndepositionFinalCalculationInfo();

                foreach (DependUndeposition dependUndeposition in dependUndepositionList)
                {
                    dependUndeposition.FINAL_CALCULATION = true;
                    dependUndeposition.ACCOUNT_CODE = dependUndeposition.SETTL_PAIR;

                    dependDataSetRepository.SaveDependUndeposition(dependUndeposition, true);
                }
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Exception happened when creating the listener service");
            }
        }

        #region Interface Implementation

        public void Dispose()
        {
            if (serviceTimer != null)
                this.serviceTimer.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Starting message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to start the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                this.logger.LogInformation("Stopping message listener");
            }
            catch (Exception e)
            {
                this.logger.LogCritical(e, "Failed to stop the message listener");
                throw;
            }
            return Task.CompletedTask;
        }

        #endregion

    }
}
