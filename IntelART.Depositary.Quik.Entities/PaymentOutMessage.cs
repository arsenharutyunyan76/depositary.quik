﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class PaymentOutMessage
    {
        public string CREATION_DATE { get; set; }
        public string ID { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string DOCUMENT_NUMBER { get; set; }
        public string SECURITY_ID { get; set; }
        public string CURRENCY { get; set; }
        public decimal INITIAL_AMOUNT { get; set; }
        public decimal AMOUNT { get; set; }
        public string PAYER { get; set; }
        public string RECEIVER { get; set; }
        public string RECEIVER_NAME { get; set; }
        public bool IS_FINAL_CALCULATION { get; set; }
        public string OPCODE { get; set; }
        public string CODE { get; set; }
        public DateTime? MOMENT { get; set; }
        public bool PROCESSED { get; set; }
    }
}
