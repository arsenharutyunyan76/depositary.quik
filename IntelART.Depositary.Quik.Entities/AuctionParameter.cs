﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class AuctionParameter
    {
        public Guid ID { get; set; }
        public decimal TRADE_QUANTITY { get; set; }
        public decimal AVERAGE_YIELD { get; set; }
        public decimal MINIMUM_YIELD { get; set; }
        public decimal MAXIMUM_YIELD { get; set; }
        public decimal AUCTION_YIELD { get; set; }
        public int PARTICIPANT_COUNT { get; set; }
        public string CLASS_CODE { get; set; }
        public string SEC_CODE { get; set; }
        public DateTime TRADE_DATE { get; set; }
    }
}
