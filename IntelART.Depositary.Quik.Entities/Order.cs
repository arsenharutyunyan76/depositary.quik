﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class Order
    {
        public Guid ID { get; set; }
        public decimal ORDER_NUMBER { get; set; }
        public DateTime ORDER_DATE { get; set; }
        public string ISIN { get; set; }
        public byte AUCTION_TYPE_ID { get; set; }
        public byte OPERATION_TYPE_ID { get; set; }
        public decimal PRICE { get; set; }
        public decimal VALUE { get; set; }
        public decimal YIELD { get; set; }
        public decimal QUANTITY { get; set; }
        public decimal BALANCE { get; set; }
        public DateTime? WITHDRAW_DATE { get; set; }
        public string BANK_CODE { get; set; }
        public string CLIENT_CODE { get; set; }
        public string BROKER_REFERENCE { get; set; }
        public int USER_ID { get; set; }
        public byte ORDER_STATUS_ID { get; set; }
    }
}
