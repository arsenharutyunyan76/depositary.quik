﻿namespace IntelART.Depositary.Quik.Entities
{
    public class BankIsinDiscount
    {
        public string BANK_CODE { get; set; }
        public string ISIN_CODE { get; set; }
        public decimal DISCOUNT { get; set; }
        public bool IS_ACTIVE { get; set; }
    }
}
