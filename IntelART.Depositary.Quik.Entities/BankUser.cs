﻿namespace IntelART.Depositary.Quik.Entities
{
    public class BankUser
    {
        public string BANK_CODE { get; set; }
        public int USER_ID { get; set; }
        public string USER_CODE { get; set; }
    }
}
