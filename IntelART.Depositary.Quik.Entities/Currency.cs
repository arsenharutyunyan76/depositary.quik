﻿namespace IntelART.Depositary.Quik.Entities
{
    public class Currency
    {
        public string CODE { get; set; }
        public byte CURRENCY_TYPE { get; set; }
    }
}
