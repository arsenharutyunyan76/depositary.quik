﻿namespace IntelART.Depositary.Quik.Entities
{
    public class AuctionAnnouncement
    {
        public string EMAIL { get; set; }
        public string EMAIL_SUBJECT { get; set; }
        public string EMAIL_BODY { get; set; }
    }
}
