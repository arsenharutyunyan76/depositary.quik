﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class FileGenerationTime
    {
        public int FILE_TYPE { get; set; }
        public DateTime? GENERATE_DATE { get; set; }
    }
}
