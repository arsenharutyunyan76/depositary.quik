﻿using System.Collections.Generic;

namespace IntelART.Depositary.Quik.Entities
{
    public class MessageRecipients
    {
        public MessageRecipients()
        {
            FILES = new List<string>();
        }

        public string RECIPIENT_LIST { get; set; }
        public string EMAIL_SUBJECT { get; set; }
        public string EMAIL_BODY { get; set; }
        public List<string> FILES { get; set; }
    }
}
