﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class Auction
    {
        public string AUCTION_ID { get; set; }
        public byte AUCTION_TYPE_ID { get; set; }
        public int ID { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public string ISIN { get; set; }
        public DateTime AUCTION_DATE { get; set; }
        public string AUCTION_EXTRA_ID { get; set; }
        public DateTime SETTLEMENT_DATE { get; set; }
        public decimal YIELD { get; set; }
        public DateTime ISSUE_DATE { get; set; }
        public decimal ISSUE_AMOUNT { get; set; }
        public DateTime MATURITY_DATE { get; set; }
        public DateTime MATURITY_PAYMENT_DATE { get; set; }
        public DateTime FIRST_PAYMENT_DATE { get; set; }
        public decimal? AUCTION_YIELD { get; set; }
        public decimal? MINIMUM_AMOUNT { get; set; }
        public decimal? MAXIMUM_AMOUNT { get; set; }
        public decimal? AMOUNT { get; set; }
        public string TIME_FROM { get; set; }
        public string TIME_TO { get; set; }
        public string ORDER_TIME_TO { get; set; }
        public bool IS_CANCELLED { get; set; }
        public string AUCTION_TYPE_NAME_AM { get; set; }
        public string AUCTION_TYPE_NAME_EN { get; set; }
    }
}
