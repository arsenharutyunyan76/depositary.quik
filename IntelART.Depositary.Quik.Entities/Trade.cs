﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class Trade
    {
        public Guid ID { get; set; }
        public decimal TRADE_NUMBER { get; set; }
        public DateTime TRADE_DATE { get; set; }
        public string ISIN { get; set; }
        public byte AUCTION_TYPE_ID { get; set; }
        public byte OPERATION_TYPE_ID { get; set; }
        public decimal PRICE { get; set; }
        public decimal VALUE { get; set; }
        public decimal YIELD { get; set; }
        public decimal QUANTITY { get; set; }
        public DateTime SETTLE_DATE { get; set; }
        public string BANK_CODE { get; set; }
        public string CP_BANK_CODE { get; set; }
        public string CLIENT_CODE { get; set; }
        public string BROKER_REFERENCE { get; set; }
        public int USER_ID { get; set; }
        public decimal ORDER_NUMBER { get; set; }
    }
}
