﻿namespace IntelART.Depositary.Quik.Entities
{
    public class Settings
    {
        public string TradeBuyName { get; set; }
        public string TradeSellName { get; set; }
        public string TradePrincipalName { get; set; }
        public string TradeAgentName { get; set; }
        public string AuctionTypePlacingCode { get; set; }
        public string AuctionTypeReturnCode { get; set; }
        public string AuctionTypeAdditionalCode { get; set; }
        public string CompanyShort { get; set; }
        public string CompanyCode { get; set; }
        public string HTSenderBankCode { get; set; }
        public string HTReceiverBankCode { get; set; }
        public int TradeProcessInterval { get; set; }
        public string BankmailApprovalCode { get; set; }
        public string OrderApprovedName { get; set; }
        public string OrderRejectedName { get; set; }
        public bool CheckAuctionParameters { get; set; }
        public string DependUser { get; set; }
        public string DependPassword { get; set; }
        public string DependClientVersion { get; set; }
        public string DependParticipant { get; set; }
        public string DependServiceURL { get; set; }
        public string DependOrderName { get; set; }
        public string DependIsinName { get; set; }
        public string DependHolderName { get; set; }
        public string DependMandateName { get; set; }
        public string DependTransactionName { get; set; }
        public string FilePath { get; set; }
        public string RTSFileGenerationFromBankMailStartTime { get; set; }
        public string RTSFileGenerationFromBankMailEndTime { get; set; }
        public string DependFileGenerationStartTime { get; set; }
        public string DependFileGenerationEndTime { get; set; }
        public string PaperAccount { get; set; }
        public string AmdAccount { get; set; }
        public string UsdAccount { get; set; }
        public string EurAccount { get; set; }
        public string HTFinalizingComment { get; set; }
        public string HTStockName { get; set; }
        public string HTPaymentFileDefaultCDVcurrency { get; set; }
        public string HTPaymentFileDefaultBANVcurrency { get; set; }

        private int isinUpdaterServiceInterval;
        public int ISINUpdaterServiceInterval
        {
            get { return isinUpdaterServiceInterval; }
            set { isinUpdaterServiceInterval = value * 1000; }
        }

        public string RTSDataProcessingStartTime { get; set; }
        public string RTSDataProcessingEndTime { get; set; }
        public string DependUndepositionFinalCalcStartTime { get; set; }
        public string DependUndepositionFinalCalcEndTime { get; set; }

        private int rtsFileGenerationInterval;
        public int RTSFileGenerationInterval
        {
            get { return rtsFileGenerationInterval; }
            set { rtsFileGenerationInterval = value * 1000; }
        }

        private int dependServiceInterval;
        public int DependServiceInterval
        {
            get { return dependServiceInterval; }
            set { dependServiceInterval = value * 1000; }
        }

        private int bankmailServiceInterval;
        public int BankmailServiceInterval
        {
            get { return bankmailServiceInterval; }
            set { bankmailServiceInterval = value * 1000; }
        }

        private int rtsDataProcessingInterval;
        public int RTSDataProcessingInterval
        {
            get { return rtsDataProcessingInterval; }
            set { rtsDataProcessingInterval = value * 1000; }
        }

        private int htPaymentOutMessageServiceInterval;
        public int HTPaymentOutMessageServiceInterval
        {
            get { return htPaymentOutMessageServiceInterval; }
            set { htPaymentOutMessageServiceInterval = value * 1000; }
        }

        private int htPaymentListenerServiceInterval;
        public int HTPaymentListenerServiceInterval
        {
            get { return htPaymentListenerServiceInterval; }
            set { htPaymentListenerServiceInterval = value * 1000; }
        }

        private int dependUndepositionInterval;
        public int DependUndepositionInterval
        {
            get { return dependUndepositionInterval; }
            set { dependUndepositionInterval = value * 1000; }
        }

        private int dependUndepositionFinalCalcInterval;
        public int DependUndepositionFinalCalcInterval
        {
            get { return dependUndepositionFinalCalcInterval; }
            set { dependUndepositionFinalCalcInterval = value * 1000; }
        }

        private int quikFileGenerationInterval;
        public int QuikFileGenerationInterval
        {
            get { return quikFileGenerationInterval; }
            set { quikFileGenerationInterval = value * 1000; }
        }

        public string QuikFileGenerationStartTime { get; set; }
        public string QuikFileGenerationEndTime { get; set; }
        public string QuikFilePath { get; set; }

        private int isinFRFUpdaterServiceInterval;
        public int ISINFRFUpdaterServiceInterval
        {
            get { return isinFRFUpdaterServiceInterval; }
            set { isinFRFUpdaterServiceInterval = value * 1000; }
        }
    }
}
