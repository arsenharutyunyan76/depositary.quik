﻿namespace IntelART.Depositary.Quik.Entities
{
    public class DependUndeposition
    {
        public int ID { get; set; }
        public string DEPOPART_CODE { get; set; }
        public string SETTL_PAIR { get; set; }
        public string DEPO_ACCOUNTS { get; set; }
        public string ISIN_SHORT_NAME { get; set; }
        public string ISIN_CODE { get; set; }
        public string ACCOUNT_CODE { get; set; }
        public decimal BALCHANGE_QTY { get; set; }
        public string ACCOUNT_REFERENCE { get; set; }
        public string LINKED_ACCOUNT_REFERENCE { get; set; }
        public bool FINAL_CALCULATION { get; set; }
    }
}
