﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class ActionLog
    {
        public long ID { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public short ACTION_TYPE_ID { get; set; }
        public bool IS_FAIL { get; set; }
        public string DETAILS { get; set; }
        public string ACTION_TYPE_NAME_AM { get; set; }
        public string ACTION_TYPE_NAME_EN { get; set; }
    }
}
