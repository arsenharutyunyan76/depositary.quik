﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class ReportSchedule
    {
        public byte ID { get; set; }
        public string NAME_AM { get; set; }
        public string NAME_EN { get; set; }
        public string GENERATION_TIME { get; set; }
        public DateTime? LAST_GENERATE_DATE { get; set; }
        public string EMAIL { get; set; }
        public string EMAIL_SUBJECT { get; set; }
        public string EMAIL_BODY { get; set; }
    }
}
