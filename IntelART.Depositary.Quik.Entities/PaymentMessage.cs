﻿namespace IntelART.Depositary.Quik.Entities
{
    public class PaymentMessage
    {
        public string CREATION_DATE { get; set; }
        public string ID { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string DOCUMENT_NUMBER { get; set; }
        public string SECURITY_ID { get; set; }
        public string CURRENCY { get; set; }
        public decimal AMOUNT { get; set; }
        public string PAYER { get; set; }
        public string RECEIVER { get; set; }
    }
}
