﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class QuoteHist
    {
        public int ID { get; set; }
        public byte STATUS { get; set; }
        public string ISSUE_NAME { get; set; }
        public decimal PRICE { get; set; }
        public decimal YIELD { get; set; }
        public int HISTORY_ID { get; set; }
        public string HISTORY_ACTION { get; set; }
        public DateTime? HISTORY_MOMENT { get; set; }

        public string LSTG { get; set; }
        public int INIT_QTY { get; set; }
        public DateTime? MOMENT { get; set; }
        public string FIRM_NAME { get; set; }
        public string WKS { get; set; }
        public string SETTL_PAIR { get; set; }
        public byte ORDER_TYPE { get; set; }
        public byte ALL_NON { get; set; }
        public int LEAVE { get; set; }
        public byte CATALYST { get; set; }
        public string TYPE { get; set; }

        public int HISTORY_EVENT { get; set; }
        public string DEPO_ACCOUNTS { get; set; }

        public int QUOTE_QTY { get; set; }
    }
}
