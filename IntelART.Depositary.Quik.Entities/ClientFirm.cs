﻿namespace IntelART.Depositary.Quik.Entities
{
    public class ClientFirm
    {
        public string CLIENT_CODE { get; set; }
        public string FIRM_ID { get; set; }
    }
}
