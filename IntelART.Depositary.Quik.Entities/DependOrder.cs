﻿namespace IntelART.Depositary.Quik.Entities
{
    public class DependOrder
    {
        public int ATSORDER_ID { get; set; }
        public string DEPOPART_CODE { get; set; }
        public string ISIN_CODE { get; set; }
        public string ISIN_SHORT_NAME { get; set; }
        public decimal BALCHANGE_QTY { get; set; }
        public string ACCOUNT_REFERENCE { get; set; }
        public string ATSORDER_LINKED_ACCOUNT_REFERENCE { get; set; }
        public string ATSORDER_LINKED_ACCOUNT_OWNER_LIST { get; set; }
        public string ATS_NAME { get; set; }
        public int ATSORDER_ATS { get; set; }
    }
}
