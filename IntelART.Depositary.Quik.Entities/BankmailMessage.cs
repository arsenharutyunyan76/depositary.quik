﻿namespace IntelART.Depositary.Quik.Entities
{
    public class BankmailMessage
    {
        public string ID { get; set; }
        public string MESSAGE_TYPE { get; set; }
        public string SENDER_BANK { get; set; }
        public string RECEIVER_BANK { get; set; }
        public string BODY { get; set; }
    }
}
