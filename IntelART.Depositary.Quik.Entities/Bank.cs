﻿using System.Collections.Generic;

namespace IntelART.Depositary.Quik.Entities
{
    public class Bank
    {
        public string CODE { get; set; }
        public string SHORT_NAME { get; set; }
        public bool IS_ACTIVE { get; set; }
        public string NAME_AM { get; set; }
        public string NAME_EN { get; set; }
        public string AS_CODE { get; set; }
        public int REGISTER_NUMBER { get; set; }
        public int REGISTRATIONS_NUMBER { get; set; }
        public string EMAIL { get; set; }
        public List<Account> Accounts { get; set; }

        public Bank()
        {
            Accounts = new List<Account>();
        }
    }
}
