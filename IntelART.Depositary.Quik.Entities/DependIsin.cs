﻿namespace IntelART.Depositary.Quik.Entities
{
    public class DependIsin
    {
        public string ISIN_CCY { get; set; }
        public string ISIN_CODE { get; set; }
        public string ISIN_TYPE { get; set; }
        public string ISIN_TRADEABLE { get; set; }
    }
}
