﻿namespace IntelART.Depositary.Quik.Entities
{
    public class DependMandate
    {
        public string ACCOUNT_REFERENCE { get; set; }
        public string MANDATE_TYPE { get; set; }
        public string MANDATE_DESCRIPTION { get; set; }
    }
}
