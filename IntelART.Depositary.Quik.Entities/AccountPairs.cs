﻿namespace IntelART.Depositary.Quik.Entities
{
    public class AccountPairs
    {
        public int BANK_ID { get; set; }
        public string BANK_CODE { get; set; }
        public string BANK_SHORT_NAME { get; set; }
        public string ACCOUNT_CUSTOMER_CODE { get; set; }
        public string ACCOUNT_CODE_1 { get; set; }
        public string CURRENCY_1 { get; set; }
        public string ACCOUNT_CODE_2 { get; set; }
        public string CURRENCY_2 { get; set; }
    }
}
