﻿namespace IntelART.Depositary.Quik.Entities
{
    public class IsinCompact
    {
        public int ID { get; set; }
        public string ISIN_CODE { get; set; }
        public string REGISTRATION_NUMBER { get; set; }
        public int FREQUENCY { get; set; }
        public decimal RATE { get; set; }
        public string FORMULA { get; set; }
    }
}
