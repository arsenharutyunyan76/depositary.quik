﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class RTSOutMessage
    {
        public string ID { get; set; }
        public string REV { get; set; }
        public DateTime? MOMENT { get; set; }
        public string FIRM_NAME { get; set; }
        public string SETTL_PAIR { get; set; }
        public string DEPO_ACCOUNTS { get; set; }
        public string NAME { get; set; }
        public string I_CODE { get; set; }
        public string ACTIVE_TYPE { get; set; }
        public string CODE { get; set; }
        public string ACCT_NUM { get; set; }
        public decimal FREE { get; set; }
        public string OPCODE { get; set; }
        public string EXT_ID { get; set; }
        public int STATUS { get; set; }
        public decimal INITIAL_AMOUNT { get; set; }
        public decimal BLOCKED { get; set; }
        public string ACCOUNTS { get; set; }
    }
}
