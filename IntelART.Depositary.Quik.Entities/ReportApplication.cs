﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class ReportApplication
    {
        public Guid ID { get; set; }
        public DateTime CREATION_DATE { get; set; }
        public byte REPORT_ID { get; set; }
        public DateTime REPORT_FROM_DATE { get; set; }
        public DateTime REPORT_TO_DATE { get; set; }
        public DateTime? GENERATE_DATE { get; set; }
    }
}
