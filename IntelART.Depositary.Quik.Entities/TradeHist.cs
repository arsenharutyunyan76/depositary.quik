﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class TradeHist
    {
        public int ID { get; set; }
        public byte STATUS { get; set; }
        public string ISSUE_NAME { get; set; }
        public decimal PRICE { get; set; }
        public int HISTORY_ID { get; set; }
        public string HISTORY_ACTION { get; set; }
        public DateTime? HISTORY_MOMENT { get; set; }
        public string LSTG { get; set; }

        public int QTY { get; set; }
        public decimal VOLUME { get; set; }
        public string TYPE_EXT { get; set; }
        public decimal RATE { get; set; }
        public DateTime? DELIVERY_DATE { get; set; }
        public DateTime? NEW_DELIVERY_DATE { get; set; }
        public string INIT_NAME { get; set; }
        public string INIT_WKS { get; set; }
        public string INIT_ACTION { get; set; }
        public string INIT_PAIR { get; set; }
        public string CONF_NAME { get; set; }
        public int CONF_QUOTEID { get; set; }
        public int TRADE_REF { get; set; }
        public string REPO_ACTION { get; set; }

        public string CONF_ACTION { get; set; }
        public string CONF_PAIR { get; set; }
        public DateTime? TRADE_MOMENT { get; set; }
        public string PRICE_CURRENCY { get; set; }
        public string TYPE { get; set; }

        public string CONF_WKS { get; set; }
        public decimal PRICE_RATE { get; set; }

        public int HISTORY_EVENT { get; set; }
        public string DEPO_ACCOUNTS { get; set; }
    }
}
