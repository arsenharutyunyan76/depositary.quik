﻿namespace IntelART.Depositary.Quik.Entities
{
    public class DependFileInfo
    {
        public int ATSORDER_ID { get; set; }
        public string DEPOPART_CODE { get; set; }
        public string ISIN_CODE { get; set; }
        public string ISIN_SHORT_NAME { get; set; }
        public decimal BALCHANGE_QTY { get; set; }
        public string ACCOUNT_REFERENCE { get; set; }
        public string ATSORDER_LINKED_ACCOUNT_REFERENCE { get; set; }
        public string ISIN_CCY { get; set; }
        public string MANDATE_TYPE { get; set; }
        public int ACCOUNT_ID { get; set; }
    }
}
