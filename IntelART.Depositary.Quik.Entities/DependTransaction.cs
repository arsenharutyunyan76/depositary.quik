﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntelART.Depositary.Quik.Entities
{
    public class DependTransaction
    {
        public int TRANS_ID { get; set; }
        public string ISIN_CODE { get; set; }
        public string ISIN_SHORT_NAME { get; set; }
        public decimal BALCHANGE_QTY { get; set; }
        public string TRANS_REFERENCE { get; set; }
        public string TRANS_REMARKS { get; set; }
    }
}
