﻿namespace IntelART.Depositary.Quik.Entities
{
    public class Account
    {
        public int ID { get; set; }
        public string BANK_CODE { get; set; }
        public string CODE { get; set; }
        public string CURRENCY { get; set; }
        public bool IS_OWN { get; set; }
        public string CUSTOMER_CODE { get; set; }
        public string SECURITY_ACCOUNT { get; set; }
    }
}
