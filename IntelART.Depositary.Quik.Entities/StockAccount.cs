﻿namespace IntelART.Depositary.Quik.Entities
{
    public class StockAccount
    {
        public string CURRENCY { get; set; }
        public string ACCOUNT { get; set; }
    }
}
