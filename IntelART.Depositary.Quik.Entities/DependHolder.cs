﻿namespace IntelART.Depositary.Quik.Entities
{
    public class DependHolder
    {
        public string ACCOUNT_REFERENCE { get; set; }
        public int ACCOUNT_ID { get; set; }
    }
}
