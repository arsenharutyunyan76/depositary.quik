﻿using System;

namespace IntelART.Depositary.Quik.Entities
{
    public class Isin
    {
        public string RTS_CODE { get; set; }
        public string ISIN_CODE { get; set; }
        public string CURRENCY { get; set; }
        public decimal NOMINAL { get; set; }
        public string REGISTRATION_NUMBER { get; set; }
        public DateTime? ISSUE_DATE { get; set; }
        public DateTime? MATURITY_DATE { get; set; }
        public int? FREQUENCY { get; set; }
        public decimal? RATE { get; set; }
        public string FORMULA { get; set; }
    }
}
